package me.relevante.core;

import me.relevante.guesser.GuesserProvider;
import org.springframework.stereotype.Component;

@Component
public class AnyMailFinder extends AbstractNetwork implements Network, GuesserProvider {

    private static final String NAME = "anyMailFinder";
    private static AnyMailFinder instance = new AnyMailFinder();

    public static AnyMailFinder getInstance() {
        return instance;
    }

    private AnyMailFinder() {
        super(NAME);
    }

}
