package me.relevante.api;

public class AnyMailFinderRequest {

    private final String domain;
    private final String name;

    public AnyMailFinderRequest(final String domain,
                                final String name) {
        this.domain = domain;
        this.name = name;
    }

    public String getDomain() {
        return domain;
    }

    public String getName() {
        return name;
    }
}
