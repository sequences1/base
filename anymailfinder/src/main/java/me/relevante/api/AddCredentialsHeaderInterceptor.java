package me.relevante.api;

import feign.RequestInterceptor;
import feign.RequestTemplate;

public class AddCredentialsHeaderInterceptor implements RequestInterceptor {

    private String apiKey;

    public AddCredentialsHeaderInterceptor(String apiKey) {
        this.apiKey = apiKey;
    }

    @Override
    public void apply(RequestTemplate template) {
        template.header("X-Api-Key", apiKey);
    }

}
