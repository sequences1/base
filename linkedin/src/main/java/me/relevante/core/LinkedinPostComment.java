package me.relevante.core;

import org.springframework.data.annotation.Transient;

import java.util.Date;

public class LinkedinPostComment extends AbstractNetworkEngagePostAction<Linkedin> implements NetworkEntity<Linkedin> {

	private String comment;

	@Transient
	private LinkedinProfile authorProfile;

    private LinkedinPostComment() {
        this((String)null, null, null, null);
    }

    public LinkedinPostComment(final LinkedinProfile authorProfile,
                               final String targetProfileId,
                               final String targetPostId,
                               final String comment) {
        this(authorProfile.getId(), targetProfileId, targetPostId, comment);
        setAuthorProfile(authorProfile);
    }

    public LinkedinPostComment(final String authorId,
                               final String targetProfileId,
                               final String targetPostId,
                               final String comment) {
        super(authorId, targetProfileId, targetPostId);
		this.comment = comment;
	}

    @Override
    public Linkedin getNetwork() {
        return Linkedin.getInstance();
    }

    public Date getCreationTimestamp() {
        return creationTimestamp;
    }

    public void setCreationTimestamp(Date timestamp) {
        this.creationTimestamp = timestamp;
    }

    public String getComment() {
        return comment;
    }

    public LinkedinProfile getAuthorProfile() {
        return authorProfile;
    }

    public void setAuthorProfile(LinkedinProfile authorProfile) {
        this.authorProfile = authorProfile;
        this.authorId = authorProfile.getId();
    }

}
