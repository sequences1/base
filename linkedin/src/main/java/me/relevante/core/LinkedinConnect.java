package me.relevante.core;

import org.springframework.data.annotation.Transient;
import org.springframework.data.mongodb.core.mapping.Document;

import java.util.Objects;

@Document(collection = "LinkedinConnect")
public class LinkedinConnect extends AbstractNetworkEngageAction<Linkedin> implements NetworkEngageAction<Linkedin> {

    @Transient
    private LinkedinProfile authorProfile;
    @Transient
    private LinkedinProfile targetProfile;

    private LinkedinConnect() {
        this(null, null);
    }

    public LinkedinConnect(final String authorId,
                           final String targetProfileId) {
        super(authorId, targetProfileId);
    }

    @Override
    public Linkedin getNetwork() {
        return Linkedin.getInstance();
    }

    public LinkedinProfile getAuthor() {
        return authorProfile;
    }

    public void setAuthor(LinkedinProfile authorProfile) {
        this.authorProfile = authorProfile;
        this.authorId = authorProfile.getId();
    }

    public LinkedinProfile getTargetProfile() {
        return targetProfile;
    }

    public void setTargetProfile(LinkedinProfile targetProfile) {
        this.targetProfile = targetProfile;
        this.targetProfileId = targetProfile.getId();
    }

    @Override
    public boolean equals(final Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        final LinkedinConnect that = (LinkedinConnect) o;
        return Objects.equals(this.authorId, that.authorId) &&
                Objects.equals(this.targetProfileId, that.targetProfileId);
    }

    @Override
    public int hashCode() {
        return Objects.hash(authorId, targetProfileId);
    }
}
