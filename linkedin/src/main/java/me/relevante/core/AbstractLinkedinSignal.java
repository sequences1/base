package me.relevante.core;

import java.util.Date;

public abstract class AbstractLinkedinSignal extends AbstractNetworkSignal<Linkedin> implements NetworkSignal<Linkedin> {

    public AbstractLinkedinSignal(final Date signalTimestamp,
                                  final String relatedProfileId,
                                  final String nlpAnalyzableContent) {
        super(signalTimestamp, relatedProfileId, nlpAnalyzableContent);
    }

    @Override
    public Linkedin getNetwork() {
        return Linkedin.getInstance();
    }
}

