package me.relevante.core;

public class LinkedinSignalBioDescription extends AbstractLinkedinSignal implements NetworkSignal<Linkedin> {

    public LinkedinSignalBioDescription() {
        super(null, null, null);
    }

    public LinkedinSignalBioDescription(final LinkedinProfile profile) {
        super(profile.getUpdateTimestamp(),
                profile.getId(),
                profile.getSummary());
    }

    @Override
    public SignalCategory getCategory() {
        return SignalCategory.SELF_DESCRIPTION;
    }
}
