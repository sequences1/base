package me.relevante.core;

import org.springframework.data.annotation.Transient;
import org.springframework.data.mongodb.core.mapping.Document;

import java.util.Objects;

@Document(collection = "LinkedinLike")
public class LinkedinLike extends AbstractNetworkEngagePostAction<Linkedin> implements NetworkEngagePostAction<Linkedin> {

    @Transient
    private LinkedinProfile authorProfile;

    private LinkedinLike() {
        this((String) null, null, null);
    }

    public LinkedinLike(final String authorId,
                        final String targetProfileId,
                        final String targetPostId) {
        super(authorId, targetProfileId, targetPostId);
    }

    public LinkedinLike(final LinkedinProfile authorProfile,
                        final String targetProfileId,
                        final String targetPostId) {
        this(authorProfile.getId(), targetProfileId, targetPostId);
        this.setAuthorProfile(authorProfile);
    }

    @Override
    public Linkedin getNetwork() {
        return Linkedin.getInstance();
    }

    public LinkedinProfile getAuthorProfile() {
        return authorProfile;
    }

    public void setAuthorProfile(LinkedinProfile authorProfile) {
        this.authorProfile = authorProfile;
        this.authorId = authorProfile.getId();
    }

    @Override
    public boolean equals(final Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        final LinkedinLike that = (LinkedinLike) o;
        return Objects.equals(this.authorId, that.authorId) &&
                Objects.equals(this.targetProfileId, that.targetProfileId) &&
                Objects.equals(this.targetPostId, that.targetPostId);
    }

    @Override
    public int hashCode() {
        return Objects.hash(authorId, targetProfileId, targetPostId);
    }

}
