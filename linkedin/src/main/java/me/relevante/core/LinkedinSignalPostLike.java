package me.relevante.core;

import java.util.Date;

public class LinkedinSignalPostLike extends AbstractLinkedinSignal implements NetworkSignal<Linkedin> {

    public LinkedinSignalPostLike() {
        super(null, null, null);
    }

    public LinkedinSignalPostLike(final LinkedinPost post,
                                  final String likeAuthorId,
                                  final Date likeTimestamp) {
        super(likeTimestamp,
                likeAuthorId,
                post.getNlpAnalyzableContent());
        this.shortDescription = LinkedinSignalPostAuthorship.composeShortDescription(post);
    }

    @Override
    public SignalCategory getCategory() {
        return SignalCategory.CONTENT_ACTIVITY;
    }
}

