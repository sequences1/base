package me.relevante.core;

import java.util.Date;

public class LinkedinSignalGroupMembership extends AbstractLinkedinSignal implements NetworkSignal<Linkedin> {

    public LinkedinSignalGroupMembership() {
        super(null, null, null);
    }

    public LinkedinSignalGroupMembership(final LinkedinGroup group,
                                         final String profileId) {
        super(new Date(),
                profileId,
                group.getNlpAnalyzableContent());
        this.shortDescription = group.getName();
    }

    @Override
    public SignalCategory getCategory() {
        return SignalCategory.OWNERSHIP;
    }
}
