package me.relevante.core;

import org.springframework.data.annotation.Transient;
import org.springframework.data.mongodb.core.mapping.Document;

import java.util.Objects;

@Document(collection = "LinkedinMessage")
public class LinkedinMessage extends AbstractNetworkEngageAction<Linkedin> implements NetworkEngageAction<Linkedin> {

	private String messageText;

	@Transient
	private LinkedinProfile authorProfile;
	@Transient
	private LinkedinProfile targetProfile;

    private LinkedinMessage() {
        this(null, null, null);
    }

    public LinkedinMessage(final String authorId,
                           final String targetProfileId,
                           final String messageText) {
        super(authorId, targetProfileId);
        this.messageText = messageText;
    }

    @Override
    public Linkedin getNetwork() {
        return Linkedin.getInstance();
    }

	public String getMessageText() {
		return messageText;
	}

    public LinkedinProfile getAuthorProfile() {
		return authorProfile;
	}

    public void setAuthorProfile(LinkedinProfile authorProfile) {
        this.authorProfile = authorProfile;
        this.authorId = authorProfile.getId();
    }

    public LinkedinProfile getTargetProfile() {
		return targetProfile;
	}

    public void setTargetProfile(LinkedinProfile targetProfile) {
        this.targetProfile = targetProfile;
        this.targetProfileId = targetProfile.getId();
    }

    @Override
    public boolean equals(final Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        final LinkedinMessage that = (LinkedinMessage) o;
        return Objects.equals(this.authorId, that.authorId) &&
                Objects.equals(this.targetProfileId, that.targetProfileId) &&
                Objects.equals(this.creationTimestamp, that.creationTimestamp);
    }

    @Override
    public int hashCode() {
        return Objects.hash(authorId, targetProfileId, creationTimestamp);
    }
}
