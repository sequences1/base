package me.relevante.core;

import java.util.Date;

public class LinkedinSignalPostComment extends AbstractLinkedinSignal implements NetworkSignal<Linkedin> {

    public LinkedinSignalPostComment() {
        super(null, null, null);
    }

    public LinkedinSignalPostComment(final LinkedinPost post,
                                     final String commentAuthorId,
                                     final Date commentTimestamp) {
        super(commentTimestamp,
                commentAuthorId,
                post.getNlpAnalyzableContent());
        this.shortDescription = LinkedinSignalPostAuthorship.composeShortDescription(post);
    }

    @Override
    public SignalCategory getCategory() {
        return SignalCategory.CONTENT_ACTIVITY;
    }
}
