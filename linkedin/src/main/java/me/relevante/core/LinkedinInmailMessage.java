package me.relevante.core;

import org.springframework.data.annotation.Transient;
import org.springframework.data.mongodb.core.mapping.Document;

@Document(collection = "LinkedinInmailMessage")
public class LinkedinInmailMessage extends AbstractNetworkEngageAction<Linkedin> implements NetworkEngageAction<Linkedin> {

    private String subject;
	private String messageText;

	@Transient
	private LinkedinProfile authorProfile;
	@Transient
	private LinkedinProfile targetProfile;

    private LinkedinInmailMessage() {
        this(null, null, null, null);
    }

    public LinkedinInmailMessage(final String authorId,
                                 final String targetProfileId,
                                 final String subject,
                                 final String messageText) {
        super(authorId, targetProfileId);
        this.subject = subject;
        this.messageText = messageText;
    }

    @Override
    public Linkedin getNetwork() {
        return Linkedin.getInstance();
    }

	public String getSubject() {
		return subject;
	}

	public String getMessageText() {
		return messageText;
	}

    public LinkedinProfile getAuthorProfile() {
		return authorProfile;
	}

    public void setAuthorProfile(LinkedinProfile authorProfile) {
        this.authorProfile = authorProfile;
        this.authorId = authorProfile.getId();
    }

    public LinkedinProfile getTargetProfile() {
		return targetProfile;
	}

    public void setTargetProfile(LinkedinProfile targetProfile) {
        this.targetProfile = targetProfile;
        this.targetProfileId = targetProfile.getId();
    }
}
