package me.relevante.core;

import org.apache.commons.lang3.StringUtils;
import org.springframework.data.annotation.Transient;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public final class LinkedinProfile implements NetworkProfile<Linkedin, LinkedinProfile> {

    private String id;
    private String name;
    private String firstName;
    private String lastName;
    private String email;
    private String headline;
    private String profileUrl;
    private String imageUrl;
    private String area;
    private String country;
    private String countryCode;
    private String summary;
    private String industry;
    private String title;
    private String positionSummary;
    private String company;
    private String numberConnections;
    private Date updateTimestamp;
    private String location;
    private List<String> twitterAccounts;
    private List<LinkedinPhoneNumber> phoneNumbers;

    @Transient
    private LinkedinPost currentShare;

    public LinkedinProfile() {
        this.twitterAccounts = new ArrayList<>();
        this.phoneNumbers = new ArrayList<>();
    }

    @Override
    public Linkedin getNetwork() {
        return Linkedin.getInstance();
    }

    @Override
    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    @Override
    public String getNlpAnalyzableContent() {
        String analyzableContent = StringUtils.defaultString(company) + " " +
                StringUtils.defaultString(headline) + " " +
                StringUtils.defaultString(summary) + " " +
                StringUtils.defaultString(industry) + " " +
                StringUtils.defaultString(positionSummary) + " " +
                StringUtils.defaultString(title) + " ";
        return analyzableContent;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getHeadline() {
        return headline;
    }

    public void setHeadline(String headline) {
        this.headline = (headline != null) ? headline : "";
    }

    public String getProfileUrl() {
        return profileUrl;
    }

    public void setProfileUrl(final String profileUrl) {
        this.profileUrl = profileUrl;
    }

    public String getImageUrl() {
        return imageUrl;
    }

    public void setImageUrl(String imageUrl) {
        this.imageUrl = imageUrl;
    }

    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    public String getArea() {
        return area;
    }

    public void setArea(String area) {
        this.area = area;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public String getCountryCode() {
        return countryCode;
    }

    public void setCountryCode(String countryCode) {
        this.countryCode = countryCode;
    }

    public String getSummary() {
        return summary;
    }

    public void setSummary(String summary) {
        this.summary = summary;
    }

    public String getIndustry() {
        return industry;
    }

    public void setIndustry(String industry) {
        this.industry = industry;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getPositionSummary() {
        return positionSummary;
    }

    public void setPositionSummary(String positionSummary) {
        this.positionSummary = positionSummary;
    }

    public String getCompany() {
        return company;
    }

    public void setCompany(String company) {
        this.company = company;
    }

    public String getNumberConnections() {
        return numberConnections;
    }

    public void setNumberConnections(String numberConnections) {
        this.numberConnections = numberConnections;
    }

    public Date getUpdateTimestamp() {
        return updateTimestamp;
    }

    public void setUpdateTimestamp(Date updateTimestamp) {
        this.updateTimestamp = updateTimestamp;
    }

    public LinkedinPost getCurrentShare() {
        return currentShare;
    }

    public void setCurrentShare(LinkedinPost currentShare) {
        this.currentShare = currentShare;
    }

    public List<String> getTwitterAccounts() {
        return new ArrayList(twitterAccounts);
    }

    public void setTwitterAccounts(final List<String> twitterAccounts) {
        this.twitterAccounts = new ArrayList<>(twitterAccounts);
    }

    public List<LinkedinPhoneNumber> getPhoneNumbers() {
        return new ArrayList(phoneNumbers);
    }

    public void setPhoneNumbers(final List<LinkedinPhoneNumber> phoneNumbers) {
        this.phoneNumbers = new ArrayList<>(phoneNumbers);
    }

    @Override
    public void completeMissingDataWith(final LinkedinProfile profile) {
        if (StringUtils.isBlank(this.id)) this.id = profile.id;
        if (StringUtils.isBlank(this.name)) this.name = profile.name;
        if (StringUtils.isBlank(this.firstName)) this.firstName = profile.firstName;
        if (StringUtils.isBlank(this.lastName)) this.lastName = profile.lastName;
        if (StringUtils.isBlank(this.email)) this.email = profile.email;
        if (StringUtils.isBlank(this.headline)) this.headline = profile.headline;
        if (StringUtils.isBlank(this.profileUrl)) this.profileUrl = profile.profileUrl;
        if (StringUtils.isBlank(this.imageUrl)) this.imageUrl = profile.imageUrl;
        if (StringUtils.isBlank(this.location)) this.location = profile.location;
        if (StringUtils.isBlank(this.area)) this.area = profile.area;
        if (StringUtils.isBlank(this.country)) this.country = profile.country;
        if (StringUtils.isBlank(this.countryCode)) this.countryCode = profile.countryCode;
        if (StringUtils.isBlank(this.summary)) this.summary = profile.summary;
        if (StringUtils.isBlank(this.industry)) this.industry = profile.industry;
        if (StringUtils.isBlank(this.title)) this.title = profile.title;
        if (StringUtils.isBlank(this.positionSummary)) this.positionSummary = profile.positionSummary;
        if (StringUtils.isBlank(this.company)) this.company = profile.company;
        if (this.updateTimestamp == null) this.updateTimestamp = profile.updateTimestamp;
        if (this.twitterAccounts.isEmpty()) this.twitterAccounts = new ArrayList<>(profile.twitterAccounts);
        if (this.phoneNumbers.isEmpty()) this.phoneNumbers = new ArrayList<>(profile.phoneNumbers);
    }

    @Override
    public void updateWithExistingDataIn(final LinkedinProfile profile) {
        if (StringUtils.isNotBlank(profile.id)) this.id = profile.id;
        if (StringUtils.isNotBlank(profile.name)) this.name = profile.name;
        if (StringUtils.isNotBlank(profile.firstName)) this.firstName = profile.firstName;
        if (StringUtils.isNotBlank(profile.lastName)) this.lastName = profile.lastName;
        if (StringUtils.isNotBlank(profile.email)) this.email = profile.email;
        if (StringUtils.isNotBlank(profile.headline)) this.headline = profile.headline;
        if (StringUtils.isNotBlank(profile.profileUrl)) this.profileUrl = profile.profileUrl;
        if (StringUtils.isNotBlank(profile.imageUrl)) this.imageUrl = profile.imageUrl;
        if (StringUtils.isNotBlank(profile.location)) this.location = profile.location;
        if (StringUtils.isNotBlank(profile.area)) this.area = profile.area;
        if (StringUtils.isNotBlank(profile.country)) this.country = profile.country;
        if (StringUtils.isNotBlank(profile.countryCode)) this.countryCode = profile.countryCode;
        if (StringUtils.isNotBlank(profile.summary)) this.summary = profile.summary;
        if (StringUtils.isNotBlank(profile.industry)) this.industry = profile.industry;
        if (StringUtils.isNotBlank(profile.title)) this.title = profile.title;
        if (StringUtils.isNotBlank(profile.positionSummary)) this.positionSummary = profile.positionSummary;
        if (StringUtils.isNotBlank(profile.company))
        if (profile.updateTimestamp != null) this.updateTimestamp = profile.updateTimestamp;
        if (!profile.twitterAccounts.isEmpty()) this.twitterAccounts = new ArrayList<>(profile.twitterAccounts);
        if (!profile.phoneNumbers.isEmpty()) this.phoneNumbers = new ArrayList<>(profile.phoneNumbers);
    }

    @Override
    public void replaceDataWith(final LinkedinProfile profile) {
        this.id = profile.id;
        this.name = profile.name;
        this.firstName = profile.firstName;
        this.lastName = profile.lastName;
        this.email = profile.email;
        this.headline = profile.headline;
        this.profileUrl = profile.profileUrl;
        this.imageUrl = profile.imageUrl;
        this.location = profile.location;
        this.area = profile.area;
        this.country = profile.country;
        this.countryCode = profile.countryCode;
        this.summary = profile.summary;
        this.industry = profile.industry;
        this.title = profile.title;
        this.positionSummary = profile.positionSummary;
        this.company = profile.company;
        this.updateTimestamp = profile.updateTimestamp;
        this.twitterAccounts = new ArrayList<>(profile.twitterAccounts);
        this.phoneNumbers = new ArrayList<>(profile.phoneNumbers);
    }

    @Override
    public String toString() {
        return "LinkedinProfile{" +
                "id='" + id + '\'' +
                ", name='" + name + '\'' +
                ", firstName='" + firstName + '\'' +
                ", lastName='" + lastName + '\'' +
                ", email='" + email + '\'' +
                ", headline='" + headline + '\'' +
                ", profileUrl='" + profileUrl + '\'' +
                ", imageUrl='" + imageUrl + '\'' +
                ", area='" + area + '\'' +
                ", country='" + country + '\'' +
                ", countryCode='" + countryCode + '\'' +
                ", summary='" + summary + '\'' +
                ", industry='" + industry + '\'' +
                ", title='" + title + '\'' +
                ", positionSummary='" + positionSummary + '\'' +
                ", company='" + company + '\'' +
                ", location='" + location + '\'' +
                ", updateTimestamp='" + updateTimestamp + '\'' +
                '}';
    }
}