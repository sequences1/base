package me.relevante.core;

import java.util.Date;

public class LinkedinSignalSkill extends AbstractLinkedinSignal implements NetworkSignal<Linkedin> {

    public LinkedinSignalSkill() {
        super(null, null, null);
    }

    public LinkedinSignalSkill(final String skill,
                               final String profileId) {
        super(new Date(),
                profileId,
                skill);
    }

    @Override
    public SignalCategory getCategory() {
        return SignalCategory.SELF_DESCRIPTION;
    }
}
