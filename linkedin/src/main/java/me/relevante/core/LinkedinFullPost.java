package me.relevante.core;

import org.springframework.data.annotation.Id;
import org.springframework.data.annotation.Transient;
import org.springframework.data.mongodb.core.mapping.Document;

import java.util.ArrayList;
import java.util.List;

@Document(collection = "LinkedinFullPost")
public class LinkedinFullPost implements NetworkFullPost<Linkedin, LinkedinPost, LinkedinFullPost> {

    @Id
    private String id;
    private LinkedinPost post;

    @Transient
    private List<LinkedinLike> likes;
    @Transient
    private List<LinkedinComment> comments;
    @Transient
    private LinkedinGroup group;

    public LinkedinFullPost() {
        super();
        this.likes = new ArrayList<>();
        this.comments = new ArrayList<>();
    }

    public LinkedinFullPost(LinkedinPost post) {
        this();
        this.id = post.getId();
        this.post = post;
    }

    @Override
    public Linkedin getNetwork() {
        return Linkedin.getInstance();
    }

    @Override
    public String getId() {
        return id;
    }

    @Override
    public LinkedinPost getPost() {
        return post;
    }

    @Override
    public int hashCode() {
        return this.id.hashCode();
    }

    @Override
    public boolean equals(Object obj) {
        LinkedinFullPost that = (LinkedinFullPost) obj;
        return this.id.equals(that.id);
    }

    @Override
    public void completeMissingDataWith(final LinkedinFullPost fullPost) {
        this.post.completeMissingDataWith(fullPost.post);
        if (this.group == null) this.group = fullPost.group;
        if (this.likes.isEmpty()) this.likes = new ArrayList<>(fullPost.likes);
        if (this.comments.isEmpty()) this.comments = new ArrayList<>(fullPost.comments);
    }

    @Override
    public void updateWithExistingDataIn(final LinkedinFullPost fullPost) {
        this.post.updateWithExistingDataIn(fullPost.post);
        if (fullPost.group != null) this.group = fullPost.group;
        if (!fullPost.likes.isEmpty()) this.likes = new ArrayList<>(fullPost.likes);
        if (!fullPost.comments.isEmpty()) this.comments = new ArrayList<>(fullPost.comments);
    }

    @Override
    public void replaceDataWith(final LinkedinFullPost fullPost) {
        this.post.replaceDataWith(fullPost.post);
        this.group = fullPost.group;
        this.likes = new ArrayList<>(fullPost.likes);
        this.comments = new ArrayList<>(fullPost.comments);
    }

    public LinkedinGroup getGroup() {
        return group;
    }

    public void setGroup(LinkedinGroup group) {
        this.group = group;
    }

    public List<LinkedinLike> getLikes() {
        return likes;
    }

    public void setLikes(List<LinkedinLike> likes) {
        this.likes = new ArrayList<>(likes);
    }

    public List<LinkedinComment> getComments() {
        return comments;
    }

    public void setComments(List<LinkedinComment> comments) {
        this.comments = new ArrayList<>(comments);
    }

    public List<LinkedinLike> findLikesByAuthorId(String authorId) {
        List<LinkedinLike> likes = new ArrayList<>();
        if (authorId == null) {
            return likes;
        }
        for (LinkedinLike like : this.likes) {
            if (authorId.equals(like.getAuthorId())) {
                likes.add(like);
            }
        }
        return likes;
    }

    public List<LinkedinComment> findCommentsByAuthorId(String authorId) {
        List<LinkedinComment> comments = new ArrayList<>();
        if (authorId == null) {
            return comments;
        }
        for (LinkedinComment comment : this.comments) {
            if (authorId.equals(comment.getAuthorId())) {
                comments.add(comment);
            }
        }
        return comments;
    }

    private void completeWithMissingElements(final List listToBeCompleted,
                                             final List listToCompleteWith) {
        for (Object object : listToCompleteWith) {
            if (!listToBeCompleted.contains(object)) {
                listToBeCompleted.add(object);
            }
        }
    }
}
