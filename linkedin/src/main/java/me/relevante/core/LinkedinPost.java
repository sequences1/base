package me.relevante.core;

import org.apache.commons.lang3.StringUtils;
import org.springframework.data.annotation.Transient;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public final class LinkedinPost implements NetworkPost<Linkedin, LinkedinPost, LinkedinProfile> {

    private String id;
    private String groupId;
    private String authorId;
    private String title;
    private String subTitle;
    private String content;
    private String subContent;
    private String url;
    private String imageUrl;
    private String language;
    private Date creationTimestamp;

    @Transient
    private List<LinkedinPostLike> likes;
    @Transient
    private List<LinkedinPostComment> comments;
    @Transient
    private LinkedinProfile author;

    public LinkedinPost() {
        super();
        this.likes = new ArrayList<>();
        this.comments = new ArrayList<>();
    }

    @Override
    public Linkedin getNetwork() {
        return Linkedin.getInstance();
    }

    @Override
    public String getId() {
        return id;
    }

	@Override
	public Date getCreationTimestamp() {
		return this.creationTimestamp;
	}

	@Override
	public void setCreationTimestamp(Date timestamp) {
		this.creationTimestamp = timestamp;
	}

    @Override
    public String getAuthorId() {
        return authorId;
    }

    @Override
    public LinkedinProfile getAuthor() {
        return author;
    }

    @Override
    public void setAuthor(LinkedinProfile author) {
        this.author = author;
        this.authorId = author.getId();
    }

    @Override
    public String getNlpAnalyzableContent() {
        String analyzableContent = StringUtils.defaultString(content) + " " +
                StringUtils.defaultString(subContent) + " " +
                StringUtils.defaultString(title) + " " +
                StringUtils.defaultString(subTitle) + " ";
        return analyzableContent;
    }

    @Override
    public void completeMissingDataWith(final LinkedinPost post) {
        if (StringUtils.isBlank(this.id)) this.id = post.id;
        if (StringUtils.isBlank(this.groupId)) this.groupId = post.groupId;
        if (StringUtils.isBlank(this.authorId)) this.authorId = post.authorId;
        if (StringUtils.isBlank(this.title)) this.title = post.title;
        if (StringUtils.isBlank(this.subTitle)) this.subTitle = post.subTitle;
        if (StringUtils.isBlank(this.content)) this.content = post.content;
        if (StringUtils.isBlank(this.subContent)) this.subContent = post.subContent;
        if (StringUtils.isBlank(this.url)) this.url = post.url;
        if (StringUtils.isBlank(this.imageUrl)) this.imageUrl = post.imageUrl;
        if (StringUtils.isBlank(this.language)) this.language = post.language;
        if (this.creationTimestamp == null) this.creationTimestamp = post.creationTimestamp;
    }

    @Override
    public void updateWithExistingDataIn(final LinkedinPost post) {
        if (StringUtils.isNotBlank(post.id)) this.id = post.id;
        if (StringUtils.isNotBlank(post.groupId)) this.groupId = post.groupId;
        if (StringUtils.isNotBlank(post.authorId)) this.authorId = post.authorId;
        if (StringUtils.isNotBlank(post.title)) this.title = post.title;
        if (StringUtils.isNotBlank(post.subTitle)) this.subTitle = post.subTitle;
        if (StringUtils.isNotBlank(post.content)) this.content = post.content;
        if (StringUtils.isNotBlank(post.subContent)) this.subContent = post.subContent;
        if (StringUtils.isNotBlank(post.url)) this.url = post.url;
        if (StringUtils.isNotBlank(post.imageUrl)) this.imageUrl = post.imageUrl;
        if (StringUtils.isNotBlank(post.language)) this.language = post.language;
        if (post.creationTimestamp != null) this.creationTimestamp = post.creationTimestamp;
    }

    @Override
    public void replaceDataWith(final LinkedinPost post) {
        this.id = post.id;
        this.groupId = post.groupId;
        this.authorId = post.authorId;
        this.title = post.title;
        this.subTitle = post.subTitle;
        this.content = post.content;
        this.subContent = post.subContent;
        this.url = post.url;
        this.imageUrl = post.imageUrl;
        this.language = post.language;
        this.creationTimestamp = post.creationTimestamp;
    }

    public String getLinkedinId() {
		return id;
	}

	public void setLinkedinId(String linkedinId) {
		this.id = linkedinId;
	}

    public void setAuthorId(String authorId) {
        this.authorId = authorId;
    }

	public String getGroupId() {
		return groupId;
	}

	public void setGroupId(String groupId) {
		this.groupId = groupId;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

    public String getSubTitle() {
        return subTitle;
    }

    public void setSubTitle(String subTitle) {
        this.subTitle = subTitle;
    }

    public String getContent() {
		return content;
	}

	public void setContent(String content) {
		this.content = content;
	}

    public String getSubContent() {
        return subContent;
    }

    public void setSubContent(String subContent) {
        this.subContent = subContent;
    }

	public String getUrl() {
        return url;
	}

	public void setUrl(String url) {
		this.url = url;
	}

	public String getImageUrl() {
		return imageUrl;
	}

	public void setImageUrl(String imageURL) {
		this.imageUrl = imageURL;
	}

	public String getLanguage() {
		return language;
	}

	public void setLanguage(String language) {
		this.language = language;
	}

    public List<LinkedinPostLike> getLikes() {
        return likes;
    }

    public void setLikes(List<LinkedinPostLike> likes) {
        this.likes = new ArrayList<>(likes);
    }

    public List<LinkedinPostComment> getComments() {
        return comments;
    }

    public void setComments(List<LinkedinPostComment> comments) {
        this.comments = new ArrayList<>(comments);
    }

    public LinkedinPostLike findLikeByAuthorId(String authorId) {
        if (authorId == null) {
            return null;
        }
        for (LinkedinPostLike like : this.likes) {
            if (authorId.equals(like.getAuthorId())) {
                return like;
            }
        }
        return null;
    }

    public LinkedinPostComment findCommentByAuthorId(String authorId) {
        if (authorId == null) {
            return null;
        }
        for (LinkedinPostComment comment : this.comments) {
            if (authorId.equals(comment.getAuthorId())) {
                return comment;
            }
        }
        return null;
    }

    @Override
    public String toString() {
        return "LinkedinPost{" +
                "id='" + id + '\'' +
                ", groupId='" + groupId + '\'' +
                ", authorId='" + authorId + '\'' +
                ", title='" + title + '\'' +
                ", subTitle='" + subTitle + '\'' +
                ", content='" + content + '\'' +
                ", subContent='" + subContent + '\'' +
                ", creationTimestamp=" + creationTimestamp +
                ", url='" + url + '\'' +
                ", imageUrl='" + imageUrl + '\'' +
                ", language='" + language + '\'' +
                ", likes=" + likes +
                ", comments=" + comments +
                ", author=" + author +
                '}';
    }
}
