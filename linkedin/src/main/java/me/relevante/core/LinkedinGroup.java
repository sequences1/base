package me.relevante.core;

import me.relevante.nlp.core.Keyword;
import me.relevante.nlp.core.NlpEntity;
import org.apache.commons.lang3.StringUtils;
import org.springframework.data.annotation.Id;
import org.springframework.data.annotation.Transient;
import org.springframework.data.mongodb.core.mapping.Document;

import java.util.ArrayList;
import java.util.List;

@Document(collection = "LinkedinGroup")
public class LinkedinGroup implements NetworkEntity, NlpEntity {

	@Id
	private String id;
    private String name;
    private String imageUrl;
	private String url;
    private String description;
    private List<String> terms;

    @Transient
    private List<LinkedinFullPost> posts;
    @Transient
    private List<Keyword> keywords;

    public LinkedinGroup() {
        this.terms = new ArrayList<>();
        this.posts = new ArrayList<>();
        this.keywords = new ArrayList<>();
    }

    @Override
    public Network getNetwork() {
        return Linkedin.getInstance();
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getImageUrl() {
        return imageUrl;
    }

    public void setImageUrl(String imageUrl) {
        this.imageUrl = imageUrl;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public List<String> getTerms() {
        return terms;
    }

    public List<LinkedinFullPost> getPosts() {
        return posts;
    }

    public List<Keyword> getKeywords() {
        return keywords;
    }

    @Override
    public String getNlpAnalyzableContent() {
        return StringUtils.defaultString(name) + " " + StringUtils.defaultString(description);
    }

    @Override
	public String toString() {
		return "LinkedinGroup [id=" + id + ", name=" + name
				+ ", description=" + description + ", keywords=" + keywords + "]";
	}
}
