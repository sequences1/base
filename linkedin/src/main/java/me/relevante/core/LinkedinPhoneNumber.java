package me.relevante.core;

public class LinkedinPhoneNumber {

    private String number;
    private String type;

    public LinkedinPhoneNumber() {
    }

    public LinkedinPhoneNumber(final String number, final String type) {
        this.number = number;
        this.type = type;
    }

    public String getNumber() {
        return number;
    }

    public void setNumber(String number) {
        this.number = number;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }
}
