package me.relevante.core;

public class LinkedinSignalPosition extends AbstractLinkedinSignal implements NetworkSignal<Linkedin> {

    public LinkedinSignalPosition() {
        super(null, null, null);
    }

    public LinkedinSignalPosition(final LinkedinPosition position,
                                  final String profileId) {
        super(position.getEndDate(),
                profileId,
                position.getNlpAnalyzableContent());
        this.shortDescription = composeShortDescription(position);
    }

    @Override
    public SignalCategory getCategory() {
        return SignalCategory.SELF_DESCRIPTION;
    }

    private static String composeShortDescription(LinkedinPosition position) {
        String description = (position.getTitle() != null) ? position.getTitle() : position.getDescription();
        if (position.getOrganizationName() != null) {
            description += " (" + position.getOrganizationName() + ")";
        }
        return description;
    }

}
