package me.relevante.core;

import org.springframework.stereotype.Component;

@Component
public class Linkedin extends AbstractNetwork implements Network {

    private static final String NAME = "linkedin";
    private static Linkedin instance = new Linkedin();

    public static Linkedin getInstance() {
        return instance;
    }

    private Linkedin() {
        super(NAME);
    }

}
