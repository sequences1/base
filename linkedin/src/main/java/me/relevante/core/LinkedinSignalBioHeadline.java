package me.relevante.core;

public class LinkedinSignalBioHeadline extends AbstractLinkedinSignal implements NetworkSignal<Linkedin> {

    public LinkedinSignalBioHeadline() {
        super(null, null, null);
    }

    public LinkedinSignalBioHeadline(final LinkedinProfile profile) {
        super(profile.getUpdateTimestamp(),
                profile.getId(),
                profile.getHeadline());
    }

    @Override
    public SignalCategory getCategory() {
        return SignalCategory.SELF_DESCRIPTION;
    }
}
