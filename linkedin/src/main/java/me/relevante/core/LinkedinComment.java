package me.relevante.core;

import org.springframework.data.annotation.Transient;
import org.springframework.data.mongodb.core.mapping.Document;

import java.util.Objects;

@Document(collection = "LinkedinComment")
public class LinkedinComment extends AbstractNetworkEngagePostAction<Linkedin> implements NetworkEngagePostAction<Linkedin> {

	private final String comment;

	@Transient
	private LinkedinProfile authorProfile;

    private LinkedinComment() {
        this((String) null, null, null, null);
    }

    public LinkedinComment(final LinkedinProfile authorProfile,
                           final String targetProfileId,
                           final String targetPostId,
                           final String comment) {
        this(authorProfile.getId(), targetProfileId, targetPostId, comment);
        setAuthorProfile(authorProfile);
    }

    public LinkedinComment(final String authorId,
                           final String targetProfileId,
                           final String targetPostId,
                           final String comment) {
        super(authorId, targetProfileId, targetPostId);
		this.comment = comment;
	}

    @Override
    public Linkedin getNetwork() {
        return Linkedin.getInstance();
    }

    public String getComment() {
        return comment;
    }

    public LinkedinProfile getAuthorProfile() {
        return authorProfile;
    }

    public void setAuthorProfile(LinkedinProfile authorProfile) {
        this.authorProfile = authorProfile;
        this.authorId = authorProfile.getId();
    }

    @Override
    public boolean equals(final Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        final LinkedinComment that = (LinkedinComment) o;
        return Objects.equals(this.authorId, that.authorId) &&
                Objects.equals(this.targetProfileId, that.targetProfileId) &&
                Objects.equals(this.targetPostId, that.targetPostId) &&
                Objects.equals(this.creationTimestamp, that.creationTimestamp);
    }

    @Override
    public int hashCode() {
        return Objects.hash(authorId, targetProfileId, targetPostId, creationTimestamp);
    }
}
