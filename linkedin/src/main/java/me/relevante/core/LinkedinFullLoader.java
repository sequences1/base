package me.relevante.core;

import me.relevante.persistence.NetworkFullPostRepo;
import me.relevante.persistence.NetworkPostActionRepo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Component;

/**
 * @author Daniel Ibanez
 */
@Component
public class LinkedinFullLoader
        extends AbstractNetworkFullLoader<Linkedin, LinkedinFullProfile, LinkedinFullPost, LinkedinLike, LinkedinComment>
        implements NetworkFullLoader<Linkedin, LinkedinFullProfile, LinkedinFullPost> {

    private final CrudRepository<LinkedinGroup, String> groupRepo;

    @Autowired
    public LinkedinFullLoader(final CrudRepository<LinkedinFullProfile, String> fullProfileRepo,
                              final NetworkFullPostRepo<LinkedinFullPost> fullPostRepo,
                              final NetworkPostActionRepo<LinkedinLike> level1ActionRepo,
                              final NetworkPostActionRepo<LinkedinComment> level2ActionRepo,
                              final CrudRepository<LinkedinGroup, String> groupRepo) {
        super(fullProfileRepo, fullPostRepo, level1ActionRepo, level2ActionRepo);
        this.groupRepo = groupRepo;
    }

    @Override
    public LinkedinFullProfile loadFullProfile(final String networkProfileId) {
        final LinkedinFullProfile fullProfile = super.loadFullProfile(networkProfileId);
        final Iterable<LinkedinGroup> groups = groupRepo.findAll(fullProfile.getGroupIds());
        for (final LinkedinGroup group : groups) {
            fullProfile.getGroups().add(group);
        }
        return fullProfile;
    }

    @Override
    protected void addLevel1ActionToFullPost(final LinkedinLike like, final LinkedinFullPost fullPost) {
        fullPost.getLikes().add(like);
    }

    @Override
    protected void addLevel2ActionToFullPost(final LinkedinComment comment, final LinkedinFullPost fullPost) {
        fullPost.getComments().add(comment);
    }

    @Override
    public Linkedin getNetwork() {
        return Linkedin.getInstance();
    }
}
