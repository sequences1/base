package me.relevante.core;

import org.springframework.data.annotation.Id;
import org.springframework.data.annotation.Transient;
import org.springframework.data.mongodb.core.index.CompoundIndex;
import org.springframework.data.mongodb.core.index.CompoundIndexes;
import org.springframework.data.mongodb.core.mapping.Document;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.UUID;

@Document(collection = "LinkedinFullProfile")
@CompoundIndexes({
        @CompoundIndex(name = "profile.profileUrl_1", def = "{ 'profile.profileUrl': 1 }", unique = true)
})
public class LinkedinFullProfile implements NetworkFullProfileWithPosts<Linkedin, LinkedinProfile, LinkedinFullPost, LinkedinFullProfile> {

    @Id
    private String id;
    private LinkedinProfile profile;
    private LinkedinFullPost lastPost;
    private List<String> groupIds;
    private List<String> skills;
    private List<LinkedinPosition> positions;
    private String uniqueProfileId;
    private Date lastUpdatedTimestamp;
    private Date lastActivityUpdatedTimestamp;

    @Transient
    private List<LinkedinGroup> groups;

    public LinkedinFullProfile() {
        super();
        this.groupIds = new ArrayList<>();
        this.skills = new ArrayList<>();
        this.positions = new ArrayList<>();
        this.groups = new ArrayList<>();
    }

    public LinkedinFullProfile(final LinkedinProfile profile) {
        this();
        this.id = UUID.randomUUID().toString();
        this.profile = profile;
    }

    @Override
    public Linkedin getNetwork() {
        return Linkedin.getInstance();
    }

    @Override
    public String getId() {
        return id;
    }

    @Override
    public LinkedinProfile getProfile() {
        return profile;
    }

    @Override
    public LinkedinFullPost getLastPost() {
        return lastPost;
    }

    @Override
    public void setLastPost(final LinkedinFullPost lastPost) {
        this.lastPost = lastPost;
    }

    @Override
    public String getUniqueProfileId() {
        return uniqueProfileId;
    }

    @Override
    public void setUniqueProfileId(final String uniqueProfileId) {
        this.uniqueProfileId = uniqueProfileId;
    }

    @Override
    public void completeMissingDataWith(final LinkedinFullProfile fullProfile) {
        this.profile.completeMissingDataWith(fullProfile.profile);
        if (this.positions.isEmpty()) this.positions = new ArrayList<>(fullProfile.positions);
        if (this.skills.isEmpty()) this.skills = new ArrayList<>(fullProfile.skills);
        if (this.groupIds.isEmpty()) this.groupIds = new ArrayList<>(fullProfile.groupIds);
        markAsUpdated();
    }

    @Override
    public void updateWithExistingDataIn(final LinkedinFullProfile fullProfile) {
        this.profile.updateWithExistingDataIn(fullProfile.profile);
        if (!fullProfile.positions.isEmpty()) this.positions = new ArrayList<>(fullProfile.positions);
        if (!fullProfile.skills.isEmpty()) this.skills = new ArrayList<>(fullProfile.skills);
        if (!fullProfile.groupIds.isEmpty()) this.groupIds = new ArrayList<>(fullProfile.groupIds);
        markAsUpdated();
    }

    @Override
    public void replaceDataWith(final LinkedinFullProfile fullProfile) {
        this.profile.replaceDataWith(fullProfile.profile);
        this.positions = new ArrayList<>(fullProfile.positions);
        this.skills = new ArrayList<>(fullProfile.skills);
        this.groupIds = new ArrayList<>(fullProfile.groupIds);
        markAsUpdated();
    }

    public List<String> getGroupIds() {
        return groupIds;
    }

    public List<String> getSkills() {
        return skills;
    }

    public List<LinkedinPosition> getPositions() {
        return positions;
    }

    public List<LinkedinGroup> getGroups() {
        return groups;
    }

    @Override
    public Date getLastUpdatedTimestamp() {
        return lastUpdatedTimestamp;
    }

    @Override
    public Date getLastActivityUpdatedTimestamp() {
        return lastActivityUpdatedTimestamp;
    }

    public void setActivityUpdated() {
        this.lastActivityUpdatedTimestamp = new Date();
    }

    private void markAsUpdated() {
        this.lastUpdatedTimestamp = new Date();
    }
}
