package me.relevante.core;

import org.apache.commons.lang3.StringUtils;

public class LinkedinSignalCurrentPosition extends AbstractLinkedinSignal implements NetworkSignal<Linkedin> {

    public LinkedinSignalCurrentPosition() {
        super(null, null, null);
    }

    public LinkedinSignalCurrentPosition(final LinkedinProfile profile) {
        super(profile.getUpdateTimestamp(),
                profile.getId(),
                composeNlpAnalyzableContent(profile));
        this.shortDescription = composeShortDescription(profile);
    }

    @Override
    public SignalCategory getCategory() {
        return SignalCategory.SELF_DESCRIPTION;
    }

    private static String composeShortDescription(final LinkedinProfile profile) {
        if (!StringUtils.isBlank(profile.getTitle())) {
            return profile.getTitle();
        }
        return StringUtils.defaultString(profile.getPositionSummary());
    }

    private static String composeNlpAnalyzableContent(final LinkedinProfile profile) {
        return StringUtils.defaultString(profile.getTitle(), "") + " " + StringUtils.defaultString(profile.getPositionSummary());
    }
}
