package me.relevante.core;

public class LinkedinSignalShareAuthorship extends AbstractLinkedinSignal implements NetworkSignal<Linkedin> {

    public LinkedinSignalShareAuthorship() {
        super(null, null, null);
    }

    public LinkedinSignalShareAuthorship(final LinkedinPost post) {
        super(post.getCreationTimestamp(),
                post.getAuthorId(),
                post.getNlpAnalyzableContent());
        this.shortDescription = LinkedinSignalPostAuthorship.composeShortDescription(post);
    }

    @Override
    public SignalCategory getCategory() {
        return SignalCategory.CONTENT_ACTIVITY;
    }
}
