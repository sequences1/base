package me.relevante.core;

import me.relevante.nlp.core.NlpEntity;
import org.apache.commons.lang3.StringUtils;

import java.time.LocalDate;
import java.time.ZoneOffset;
import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class LinkedinPosition implements NlpEntity {

    private static final Map<String, List<String>> MONTHS_BY_LANGUAGE = new HashMap<>();
    static {
        MONTHS_BY_LANGUAGE.put("en", Arrays.asList("january", "february", "march", "april", "may", "june", "july", "august", "september", "october", "november", "december"));
        MONTHS_BY_LANGUAGE.put("es", Arrays.asList("enero", "febrero", "marzo", "abril", "mayo", "junio", "julio", "agosto", "septiembre", "octubre", "noviembre", "diciembre"));
        MONTHS_BY_LANGUAGE.put("fr", Arrays.asList("janvier", "fevrier", "mars", "avril", "mai", "juin", "juillet", "aout", "septembre", "octobre", "novembre", "decembre"));
        MONTHS_BY_LANGUAGE.put("it", Arrays.asList("gennaio", "febbraio", "marzo", "aprile", "maggio", "giugno", "luglio", "agosto", "settembre", "ottobre", "novembre", "dicembre"));
    }

    private String title;
    private String description;
    private String organizationName;
    private String organizationUrl;
    private String end;

    @Override
    public String getNlpAnalyzableContent() {
        return StringUtils.defaultString(title) + " " +
                StringUtils.defaultString(description) + " " +
                StringUtils.defaultString(organizationName) + " ";
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getOrganizationName() {
        return organizationName;
    }

    public void setOrganizationName(String organizationName) {
        this.organizationName = organizationName;
    }

    public String getOrganizationUrl() {
        return organizationUrl;
    }

    public void setOrganizationUrl(String organizationUrl) {
        this.organizationUrl = organizationUrl;
    }

    public String getEnd() {
        return end;
    }

    public void setEnd(String end) {
        this.end = end;
    }

    public Date getEndDate() {
        Date date;
        try {
            date = convertStringToDate(end);
        } catch (Exception e) {
            date = new Date();
        }
        return date;
    }

    private Date convertStringToDate(String dateAsString) {
        String[] words = dateAsString.split(" ");
        if (words.length == 1) {
            int year = Integer.parseInt(words[0]);
            LocalDate localDate = LocalDate.of(year, 1, 1);
            Date date = Date.from(localDate.atStartOfDay(ZoneOffset.UTC).toInstant());
            return date;
        }
        if (words.length == 2) {
            int year = Integer.parseInt(words[0]);
            int month = getMonthNumber(words[0]);
            LocalDate localDate = LocalDate.of(year, month, 1);
            Date date = Date.from(localDate.atStartOfDay(ZoneOffset.UTC).toInstant());
            return date;
        }
        return new Date();
    }

    private int getMonthNumber(String monthName) {
        String cleanMonthName = monthName.replaceAll("[^a-zA-Z ]", "").toLowerCase();
        for (List<String> months : MONTHS_BY_LANGUAGE.values()) {
            int i = 1;
            for (String month : months) {
                if (cleanMonthName.equals(month)) {
                    return i;
                }
                i++;
            }
        }
        throw new IllegalArgumentException("Month Name is wrong");
    }
}
