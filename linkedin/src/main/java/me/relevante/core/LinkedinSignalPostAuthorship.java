package me.relevante.core;

public class LinkedinSignalPostAuthorship extends AbstractLinkedinSignal implements NetworkSignal<Linkedin> {

    public LinkedinSignalPostAuthorship() {
        super(null, null, null);
    }

    public LinkedinSignalPostAuthorship(final LinkedinPost post) {
        super(post.getCreationTimestamp(),
                post.getAuthorId(),
                post.getNlpAnalyzableContent());
        this.shortDescription = composeShortDescription(post);
    }

    @Override
    public SignalCategory getCategory() {
        return SignalCategory.CONTENT_ACTIVITY;
    }

    public static String composeShortDescription(LinkedinPost post) {
        String description = post.getTitle();
        if (post.getContent() == null) {
            description = post.getContent();
        }
        return description;
    }

}
