package me.relevante.core;

import java.util.Date;

public class LinkedinPostLike implements NetworkEntity {

    private String authorId;
    private String postId;
    private Date creationTimestamp;
    private LinkedinProfile author;

    public LinkedinPostLike() {
    }

    public LinkedinPostLike(String authorId,
                            String postId,
                            Date creationTimestamp) {
        this.authorId = authorId;
        this.postId = postId;
        this.creationTimestamp = creationTimestamp;
    }

    public LinkedinPostLike(LinkedinProfile author,
                            String postId,
                            Date creationTimestamp) {
        this.setAuthor(author);
        this.postId = postId;
        this.creationTimestamp = creationTimestamp;
    }

    @Override
    public Network getNetwork() {
        return Linkedin.getInstance();
    }

    public String getAuthorId() {
        return authorId;
    }

    public String getPostId() {
        return postId;
    }

    public Date getCreationTimestamp() {
        return creationTimestamp;
    }

    public void setCreationTimestamp(Date timestamp) {
        this.creationTimestamp = timestamp;
    }

    public LinkedinProfile getAuthor() {
        return author;
    }

    public void setAuthor(LinkedinProfile author) {
        this.author = author;
        this.authorId = author.getId();
    }
}
