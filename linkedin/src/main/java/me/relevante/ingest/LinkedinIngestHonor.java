package me.relevante.ingest;

/**
 * @author Daniel Ibanez
 */
public class LinkedinIngestHonor {

    private String title;
    private String description;
    private String organization;
    private LinkedinIngestDate issue_date;

    public String getTitle() {
        return title;
    }

    public void setTitle(final String title) {
        this.title = title;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(final String description) {
        this.description = description;
    }

    public String getOrganization() {
        return organization;
    }

    public void setOrganization(final String organization) {
        this.organization = organization;
    }

    public LinkedinIngestDate getIssue_date() {
        return issue_date;
    }

    public void setIssue_date(LinkedinIngestDate issue_date) {
        this.issue_date = issue_date;
    }
}
