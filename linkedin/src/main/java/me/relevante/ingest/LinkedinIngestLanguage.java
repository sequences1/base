package me.relevante.ingest;

/**
 * @author Daniel Ibanez
 */
public class LinkedinIngestLanguage {

    private String name;
    private String proficiency;

    public String getName() {
        return name;
    }

    public void setName(final String name) {
        this.name = name;
    }

    public String getProficiency() {
        return proficiency;
    }

    public void setProficiency(final String proficiency) {
        this.proficiency = proficiency;
    }
}
