package me.relevante.ingest;

/**
 * @author Daniel Ibanez
 */
public class LinkedinIngestProjectMember {

    private String full_name;
    private String url;

    public String getFull_name() {
        return full_name;
    }

    public void setFull_name(final String full_name) {
        this.full_name = full_name;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(final String url) {
        this.url = url;
    }
}
