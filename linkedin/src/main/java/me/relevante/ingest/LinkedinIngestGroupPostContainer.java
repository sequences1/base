package me.relevante.ingest;

import java.util.List;

public class LinkedinIngestGroupPostContainer {

    private List<LinkedinIngestGroupPost> posts;

    public List<LinkedinIngestGroupPost> getPosts() {
        return posts;
    }

    public void setPosts(List<LinkedinIngestGroupPost> posts) {
        this.posts = posts;
    }
}
