package me.relevante.ingest;

/**
 * @author Daniel Ibanez
 */
public class LinkedinIngestCertification {

    private String title;
    private String certificate_authority;
    private LinkedinIngestDate start;
    private LinkedinIngestDate end;
    private String license;

    public String getTitle() {
        return title;
    }

    public void setTitle(final String title) {
        this.title = title;
    }

    public String getCertificate_authority() {
        return certificate_authority;
    }

    public void setCertificate_authority(final String certificate_authority) {
        this.certificate_authority = certificate_authority;
    }

    public String getLicense() {
        return license;
    }

    public void setLicense(final String license) {
        this.license = license;
    }

    public LinkedinIngestDate getStart() {
        return start;
    }

    public void setStart(LinkedinIngestDate start) {
        this.start = start;
    }

    public LinkedinIngestDate getEnd() {
        return end;
    }

    public void setEnd(LinkedinIngestDate end) {
        this.end = end;
    }
}
