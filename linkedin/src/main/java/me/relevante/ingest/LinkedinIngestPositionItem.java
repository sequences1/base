package me.relevante.ingest;

import java.util.ArrayList;
import java.util.List;

/**
 * @author Daniel Ibanez
 */
public class LinkedinIngestPositionItem {

    private String title;
    private String description;
    private LinkedinIngestDate start;
    private LinkedinIngestDate end;
    private String location;
    private String duration;
    private LinkedinIngestExperienceOrganization organization;

    public String getTitle() {
        return title;
    }

    public void setTitle(final String title) {
        this.title = title;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(final String description) {
        this.description = description;
    }

    public LinkedinIngestDate getStart() {
        return start;
    }

    public void setStart(LinkedinIngestDate start) {
        this.start = start;
    }

    public LinkedinIngestDate getEnd() {
        return end;
    }

    public void setEnd(LinkedinIngestDate end) {
        this.end = end;
    }

    public String getLocation() {
        return location;
    }

    public void setLocation(final String location) {
        this.location = location;
    }

    public String getDuration() {
        return duration;
    }

    public void setDuration(final String duration) {
        this.duration = duration;
    }

    public LinkedinIngestExperienceOrganization getOrganization() {
        return organization;
    }
}

