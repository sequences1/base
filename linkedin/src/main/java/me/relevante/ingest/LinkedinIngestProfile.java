package me.relevante.ingest;

import me.relevante.core.Linkedin;
import org.springframework.data.mongodb.core.mapping.Document;

import java.util.ArrayList;
import java.util.List;

/**
 * @author Daniel Ibanez
 */
@Document(collection = "LinkedinIngestProfile")
public class LinkedinIngestProfile extends NetworkIngestProfile<Linkedin> {

    private String _key;
    private String linkedin_id;
    private String unique_id;
    private String full_name;
    private String updated;
    private String canonical_url;
    private String locality;
    private String headline;
    private String given_name;
    private String family_name;
    private String url;
    private String email;
    private String industry;
    private String last_visited;
    private String num_connections;
    private String summary;
    private String image_url;
    private String recommendations;
    private List<LinkedinIngestTestScore> test_scores;
    private List<String> also_viewed;
    private List<LinkedinIngestWebsite> websites;
    private List<LinkedinIngestEducationItem> education;
    private List<LinkedinIngestHonor> honors;
    private List<LinkedinIngestLanguage> languages;
    private List<String> skills;
    private List<String> interests;
    private List<String> recommendations_preview;
    private List<LinkedinIngestGroup> groups;
    private List<LinkedinIngestCourse> courses;
    private List<LinkedinIngestProject> projects;
    private List<LinkedinIngestOrganization> organizations;
    private List<LinkedinIngestCertification> certifications;
    private List<LinkedinIngestVolunteeringItem> volunteering;
    private List<LinkedinIngestPositionItem> positions;
    private List<LinkedinIngestPublication> publications;
    private List<LinkedinIngestPatent> patents;
    private List<String> twitter;
    private List<LinkedinIngestPhoneNumber> phoneNumbers;
    private List<LinkedinIngestIms> ims;

    public LinkedinIngestProfile() {
        this.test_scores = new ArrayList<>();
        this.also_viewed = new ArrayList<>();
        this.websites = new ArrayList<>();
        this.education = new ArrayList<>();
        this.honors = new ArrayList<>();
        this.languages = new ArrayList<>();
        this.skills = new ArrayList<>();
        this.interests = new ArrayList<>();
        this.recommendations_preview = new ArrayList<>();
        this.groups = new ArrayList<>();
        this.courses = new ArrayList<>();
        this.projects = new ArrayList<>();
        this.organizations = new ArrayList<>();
        this.certifications = new ArrayList<>();
        this.volunteering = new ArrayList<>();
        this.positions = new ArrayList<>();
        this.publications = new ArrayList<>();
        this.patents = new ArrayList<>();
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String get_key() {
        return _key;
    }

    public void set_key(final String _key) {
        this._key = _key;
    }

    public String getLinkedin_id() {
        return linkedin_id;
    }

    public void setLinkedin_id(final String linkedin_id) {
        this.linkedin_id = linkedin_id;
    }

    public String getUnique_id() {
        return unique_id;
    }

    public void setUnique_id(final String unique_id) {
        this.unique_id = unique_id;
    }

    public String getFull_name() {
        return full_name;
    }

    public void setFull_name(final String full_name) {
        this.full_name = full_name;
    }

    public String getUpdated() {
        return updated;
    }

    public void setUpdated(final String updated) {
        this.updated = updated;
    }

    public String getCanonical_url() {
        return canonical_url;
    }

    public void setCanonical_url(final String canonical_url) {
        this.canonical_url = canonical_url;
    }

    public String getLocality() {
        return locality;
    }

    public void setLocality(final String locality) {
        this.locality = locality;
    }

    public String getHeadline() {
        return headline;
    }

    public void setHeadline(final String headline) {
        this.headline = headline;
    }

    public String getGiven_name() {
        return given_name;
    }

    public void setGiven_name(final String given_name) {
        this.given_name = given_name;
    }

    public String getFamily_name() {
        return family_name;
    }

    public void setFamily_name(final String family_name) {
        this.family_name = family_name;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(final String url) {
        this.url = url;
    }

    public String getIndustry() {
        return industry;
    }

    public void setIndustry(final String industry) {
        this.industry = industry;
    }

    public String getLast_visited() {
        return last_visited;
    }

    public void setLast_visited(final String last_visited) {
        this.last_visited = last_visited;
    }

    public String getNum_connections() {
        return num_connections;
    }

    public void setNum_connections(final String num_connections) {
        this.num_connections = num_connections;
    }

    public String getSummary() {
        return summary;
    }

    public void setSummary(final String summary) {
        this.summary = summary;
    }

    public String getImage_url() {
        return image_url;
    }

    public void setImage_url(final String image_url) {
        this.image_url = image_url;
    }

    public String getRecommendations() {
        return recommendations;
    }

    public void setRecommendations(final String recommendations) {
        this.recommendations = recommendations;
    }

    public List<LinkedinIngestTestScore> getTest_scores() {
        return test_scores;
    }

    public List<String> getAlso_viewed() {
        return also_viewed;
    }

    public List<LinkedinIngestWebsite> getWebsites() {
        return websites;
    }

    public List<LinkedinIngestEducationItem> getEducation() {
        return education;
    }

    public List<LinkedinIngestHonor> getHonors() {
        return honors;
    }

    public List<LinkedinIngestLanguage> getLanguages() {
        return languages;
    }

    public List<String> getSkills() {
        return skills;
    }

    public List<String> getInterests() {
        return interests;
    }

    public List<String> getRecommendations_preview() {
        return recommendations_preview;
    }

    public List<LinkedinIngestGroup> getGroups() {
        return groups;
    }

    public List<LinkedinIngestCourse> getCourses() {
        return courses;
    }

    public List<LinkedinIngestProject> getProjects() {
        return projects;
    }

    public List<LinkedinIngestOrganization> getOrganizations() {
        return organizations;
    }

    public List<LinkedinIngestCertification> getCertifications() {
        return certifications;
    }

    public List<LinkedinIngestVolunteeringItem> getVolunteering() {
        return volunteering;
    }

    public List<LinkedinIngestPositionItem> getPositions() {
        return positions;
    }

    public List<LinkedinIngestPublication> getPublications() {
        return publications;
    }

    public List<LinkedinIngestPatent> getPatents() {
        return patents;
    }

    public List<String> getTwitter() {
        return twitter;
    }

    public void setTwitter(List<String> twitter) {
        this.twitter = twitter;
    }

    public List<LinkedinIngestPhoneNumber> getPhoneNumbers() {
        return phoneNumbers;
    }

    public void setPhoneNumbers(List<LinkedinIngestPhoneNumber> phoneNumbers) {
        this.phoneNumbers = phoneNumbers;
    }

    public List<LinkedinIngestIms> getIms() {
        return ims;
    }

    public void setIms(List<LinkedinIngestIms> ims) {
        this.ims = ims;
    }
}
