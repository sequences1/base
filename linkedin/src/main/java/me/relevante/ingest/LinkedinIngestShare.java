package me.relevante.ingest;

import java.util.Date;

/**
 * @author Daniel Ibanez
 */
public class LinkedinIngestShare {

    private String authorUrl;
    private String contentText;
    private String contentUrl;
    private String imageUrl;
    private Date creationTimestamp;
    private String title;
    private String subtitle;

    public String getAuthorUrl() {
        return authorUrl;
    }

    public void setAuthorUrl(final String authorUrl) {
        this.authorUrl = authorUrl;
    }

    public String getContentText() {
        return contentText;
    }

    public void setContentText(final String contentText) {
        this.contentText = contentText;
    }

    public String getContentUrl() {
        return contentUrl;
    }

    public void setContentUrl(final String contentUrl) {
        this.contentUrl = contentUrl;
    }

    public String getImageUrl() {
        return imageUrl;
    }

    public void setImageUrl(final String imageUrl) {
        this.imageUrl = imageUrl;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(final String title) {
        this.title = title;
    }

    public String getSubtitle() {
        return subtitle;
    }

    public void setSubtitle(final String subtitle) {
        this.subtitle = subtitle;
    }

    public Date getCreationTimestamp() {
        return creationTimestamp;
    }

    public void setCreationTimestamp(final Date creationTimestamp) {
        this.creationTimestamp = creationTimestamp;
    }
}
