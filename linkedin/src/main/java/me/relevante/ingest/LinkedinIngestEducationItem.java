package me.relevante.ingest;

import java.util.ArrayList;
import java.util.List;

/**
 * @author Daniel Ibanez
 */
public class LinkedinIngestEducationItem {

    private String name;
    private String summary;
    private String major;
    private String start;
    private String end;
    private String degree;
    private String grade;
    private String profile_url;
    private List<String> degrees;

    public LinkedinIngestEducationItem() {
        this.degrees = new ArrayList<>();
    }

    public String getName() {
        return name;
    }

    public void setName(final String name) {
        this.name = name;
    }

    public String getSummary() {
        return summary;
    }

    public void setSummary(final String summary) {
        this.summary = summary;
    }

    public String getMajor() {
        return major;
    }

    public void setMajor(final String major) {
        this.major = major;
    }

    public String getStart() {
        return start;
    }

    public void setStart(final String start) {
        this.start = start;
    }

    public String getEnd() {
        return end;
    }

    public void setEnd(final String end) {
        this.end = end;
    }

    public String getDegree() {
        return degree;
    }

    public void setDegree(final String degree) {
        this.degree = degree;
    }

    public String getGrade() {
        return grade;
    }

    public void setGrade(final String grade) {
        this.grade = grade;
    }

    public String getProfile_url() {
        return profile_url;
    }

    public void setProfile_url(final String profile_url) {
        this.profile_url = profile_url;
    }

    public List<String> getDegrees() {
        return degrees;
    }
}
