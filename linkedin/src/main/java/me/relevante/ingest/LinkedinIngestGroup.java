package me.relevante.ingest;

/**
 * @author Daniel Ibanez
 */
public class LinkedinIngestGroup {

    private String name;
    private String profile_url;
    private String logo_url;

    public String getName() {
        return name;
    }

    public void setName(final String name) {
        this.name = name;
    }

    public String getProfile_url() {
        return profile_url;
    }

    public void setProfile_url(final String profile_url) {
        this.profile_url = profile_url;
    }

    public String getLogo_url() {
        return logo_url;
    }

    public void setLogo_url(final String logo_url) {
        this.logo_url = logo_url;
    }
}
