package me.relevante.ingest;

import java.util.ArrayList;
import java.util.List;

/**
 * @author Daniel Ibanez
 */
public class LinkedinIngestVolunteeringItem {

    private String title;
    private String description;
    private String start;
    private String end;
    private List<String> opportunities;
    private List<LinkedinIngestVolunteeringOrganization> organization;
    private List<String> causes;

    public LinkedinIngestVolunteeringItem() {
        this.opportunities = new ArrayList<>();
        this.organization = new ArrayList<>();
        this.causes = new ArrayList<>();
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(final String title) {
        this.title = title;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(final String description) {
        this.description = description;
    }

    public String getStart() {
        return start;
    }

    public void setStart(final String start) {
        this.start = start;
    }

    public String getEnd() {
        return end;
    }

    public void setEnd(final String end) {
        this.end = end;
    }

    public List<String> getOpportunities() {
        return opportunities;
    }

    public List<LinkedinIngestVolunteeringOrganization> getOrganization() {
        return organization;
    }

    public List<String> getCauses() {
        return causes;
    }
}
