package me.relevante.ingest;

import java.util.List;

public class LinkedinIngestShareContainer {

    public List<LinkedinIngestShare> shares;

    public List<LinkedinIngestShare> getShares() {
        return shares;
    }

    public void setShares(List<LinkedinIngestShare> shares) {
        this.shares = shares;
    }
}
