package me.relevante.ingest;

/**
 * @author Daniel Ibanez
 */
public class LinkedinIngestVolunteeringOrganization {

    private String name;
    private String specifics;

    public String getName() {
        return name;
    }

    public void setName(final String name) {
        this.name = name;
    }

    public String getSpecifics() {
        return specifics;
    }

    public void setSpecifics(final String specifics) {
        this.specifics = specifics;
    }
}
