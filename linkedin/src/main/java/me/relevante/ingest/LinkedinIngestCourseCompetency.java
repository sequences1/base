package me.relevante.ingest;

/**
 * @author Daniel Ibanez
 */
public class LinkedinIngestCourseCompetency {

    private String name;
    private String number;

    public String getName() {
        return name;
    }

    public void setName(final String name) {
        this.name = name;
    }

    public String getNumber() {
        return number;
    }

    public void setNumber(final String number) {
        this.number = number;
    }
}
