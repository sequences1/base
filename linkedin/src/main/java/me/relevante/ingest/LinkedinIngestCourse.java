package me.relevante.ingest;

import java.util.List;

/**
 * @author Daniel Ibanez
 */
public class LinkedinIngestCourse {

    private String title;
    private String organization;
    private List<LinkedinIngestCourseCompetency> competencies;

    public String getTitle() {
        return title;
    }

    public void setTitle(final String title) {
        this.title = title;
    }

    public String getOrganization() {
        return organization;
    }

    public void setOrganization(final String organization) {
        this.organization = organization;
    }

    public List<LinkedinIngestCourseCompetency> getCompetencies() {
        return competencies;
    }
}
