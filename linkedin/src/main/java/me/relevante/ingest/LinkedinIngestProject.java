package me.relevante.ingest;

import java.util.ArrayList;
import java.util.List;

/**
 * @author Daniel Ibanez
 */
public class LinkedinIngestProject {

    private String title;
    private String description;
    private String url;
    private String date;
    private List<LinkedinIngestProjectMember> members;

    public LinkedinIngestProject() {
        this.members = new ArrayList<>();
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(final String title) {
        this.title = title;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(final String description) {
        this.description = description;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(final String url) {
        this.url = url;
    }

    public String getDate() {
        return date;
    }

    public void setDate(final String date) {
        this.date = date;
    }

    public List<LinkedinIngestProjectMember> getMembers() {
        return members;
    }
}


