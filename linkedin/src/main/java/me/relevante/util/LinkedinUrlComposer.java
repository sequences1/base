package me.relevante.util;

/**
 * @author Daniel Ibanez
 */
public class LinkedinUrlComposer {

    private static final String PROTOCOL = "http";
    private static final String DOMAIN = "www.linkedin.com";
    private static final String PROTOCOL_AND_DOMAIN = PROTOCOL + "://" + DOMAIN;
    private static final String PATH = "/in";
    private static final String FULL_PATH = PROTOCOL_AND_DOMAIN + PATH;

    public static String toPublicProfileUrlfromPublicIdentifier(final String publicIdentifier) {
        return FULL_PATH + "/" + publicIdentifier;
    }
}
