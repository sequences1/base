package me.relevante.persistence;

import me.relevante.core.LinkedinFullProfile;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.data.repository.NoRepositoryBean;

/**
 * @author daniel-ibanez
 */
@NoRepositoryBean
public interface BaseLinkedinFullProfileRepo extends MongoRepository<LinkedinFullProfile, String> {
    LinkedinFullProfile findOneByProfileProfileUrl(String profileUrl);
}
