package me.relevante.search;

import me.relevante.core.Linkedin;
import me.relevante.core.LinkedinFullPost;
import me.relevante.core.LinkedinFullProfile;
import me.relevante.core.LinkedinGroup;
import me.relevante.core.LinkedinPosition;
import me.relevante.core.LinkedinPost;
import me.relevante.core.LinkedinProfile;
import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
public class LinkedinSearchTextExtractor
        extends AbstractNetworkSearchTextExtractor<Linkedin, LinkedinFullProfile, LinkedinFullPost>
        implements NetworkSearchTextExtractor<Linkedin, LinkedinFullProfile, LinkedinFullPost> {

    @Override
    public String extractSearchText(final LinkedinFullProfile fullProfile) {
        return fullProfile.getNetwork().getName() + " " +
                extractSearchText(fullProfile.getProfile()) + " " +
                extractSkillsSearchText(fullProfile.getSkills()) + " " +
                extractPositionsSearchText(fullProfile.getPositions()) + " " +
                extractGroupsSearchText(fullProfile.getGroups()) + " ";
    }

    @Override
    public String extractSearchText(final LinkedinFullPost fullPost) {
        return extractSearchText(fullPost.getPost());
    }

    @Override
    public Linkedin getNetwork() {
        return Linkedin.getInstance();
    }

    private String extractGroupsSearchText(final List<LinkedinGroup> groups) {
        final StringBuilder sb = new StringBuilder();
        for (final LinkedinGroup group : groups) {
            sb.append(extractSearchText(group)).append(" ");
        }
        return sb.toString();
    }

    private String extractPositionsSearchText(final List<LinkedinPosition> positions) {
        final StringBuilder sb = new StringBuilder();
        for (final LinkedinPosition position : positions) {
            sb.append(extractSearchText(position)).append(" ");
        }
        return sb.toString();
    }

    private String extractSearchText(final LinkedinGroup group) {
        return StringUtils.defaultString(group.getDescription()) + " " +
                StringUtils.defaultString(group.getName()) + " ";
    }

    private String extractSearchText(final LinkedinPosition position) {
        return StringUtils.defaultString(position.getDescription()) + " " +
                StringUtils.defaultString(position.getTitle()) + " " +
                StringUtils.defaultString(position.getOrganizationName()) + " ";
    }

    private String extractSearchText(final LinkedinProfile profile) {
        return StringUtils.defaultString(profile.getArea()) + " " +
                StringUtils.defaultString(profile.getCompany()) + " " +
                StringUtils.defaultString(profile.getCountry()) + " " +
                StringUtils.defaultString(profile.getEmail()) + " " +
                StringUtils.defaultString(profile.getFirstName()) + " " +
                StringUtils.defaultString(profile.getLastName()) + " " +
                StringUtils.defaultString(profile.getHeadline()) + " " +
                StringUtils.defaultString(profile.getIndustry()) + " " +
                StringUtils.defaultString(profile.getLocation()) + " " +
                StringUtils.defaultString(profile.getName()) + " " +
                StringUtils.defaultString(profile.getPositionSummary()) + " " +
                StringUtils.defaultString(profile.getSummary()) + " " +
                StringUtils.defaultString(profile.getTitle()) + " ";
    }

    private String extractSearchText(final LinkedinPost post) {
        return StringUtils.defaultString(post.getContent()) + " " +
                StringUtils.defaultString(post.getSubContent()) + " " +
                StringUtils.defaultString(post.getSubTitle()) + " " +
                StringUtils.defaultString(post.getTitle()) + " ";
    }

    private String extractSkillsSearchText(final List<String> skills) {
        final StringBuilder sb = new StringBuilder();
        for (final String skill : skills) {
            sb.append(skill).append(" ");
        }
        return sb.toString();
    }

}
