package me.relevante.search;

import me.relevante.core.Linkedin;
import me.relevante.core.LinkedinFullLoader;
import me.relevante.core.LinkedinFullPost;
import me.relevante.core.LinkedinFullProfile;
import me.relevante.core.LinkedinPosition;
import me.relevante.core.LinkedinProfile;
import me.relevante.core.NetworkSignal;
import me.relevante.nlp.LinkedinSignalExtractor;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;

/**
 * @author daniel-ibanez
 */
@Component
public class LinkedinSearchAttributesLoader
        extends AbstractNetworkSearchAttributesLoader<Linkedin, LinkedinFullProfile, LinkedinFullPost>
        implements NetworkSearchAttributesLoader<Linkedin> {

    @Autowired
    public LinkedinSearchAttributesLoader(final LinkedinFullLoader fullLoader,
                                          final LinkedinSignalExtractor signalExtractor,
                                          final LinkedinSearchTextExtractor searchTextExtractor) {
        super(fullLoader, signalExtractor, searchTextExtractor);
    }

    @Override
    protected SearchAttributes<Linkedin> createSearchAttributes(final LinkedinFullProfile fullProfile,
                                                                final List<NetworkSignal<Linkedin>> networkSignals,
                                                                final String fullText) {
        final LinkedinProfile profile = fullProfile.getProfile();
        final List<String> companies = extractCompanies(fullProfile);
        final SearchAttributes<Linkedin> searchAttributes = new SearchAttributes<>(profile.getLocation(), profile.getIndustry(), companies,
                fullText, networkSignals);
        return searchAttributes;

    }

    @Override
    public Linkedin getNetwork() {
        return Linkedin.getInstance();
    }

    private List<String> extractCompanies(final LinkedinFullProfile fullProfile) {
        final List<String> companies = new ArrayList<>();
        for (final LinkedinPosition position : fullProfile.getPositions()) {
            if (StringUtils.isNotBlank(position.getOrganizationName())) {
                companies.add(position.getOrganizationName());
            }
        }
        return companies;
    }
}
