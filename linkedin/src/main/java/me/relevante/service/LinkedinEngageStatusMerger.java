package me.relevante.service;

import me.relevante.core.Linkedin;
import me.relevante.request.LinkedinEngageStatus;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * @author Daniel Ibanez
 */
@Component
public class LinkedinEngageStatusMerger
        extends AbstractNetworkEngageStatusMerger<Linkedin, LinkedinEngageStatus>
        implements NetworkEngageStatusMerger<Linkedin, LinkedinEngageStatus> {

    @Autowired
    public LinkedinEngageStatusMerger(final ActionEngageStatusMerger actionEngageStatusMerger) {
        super(actionEngageStatusMerger);
    }

    @Override
    protected LinkedinEngageStatus mergeNotNullObjects(final LinkedinEngageStatus status1,
                                                       final LinkedinEngageStatus status2) {
        final LinkedinEngageStatus mergedEngageStatus = new LinkedinEngageStatus();
        mergedEngageStatus.setLikeStatus(mergeActionStatus(status1.getLikeStatus(), status2.getLikeStatus()));
        mergedEngageStatus.setCommentStatus(mergeActionStatus(status1.getCommentStatus(), status2.getCommentStatus()));
        mergedEngageStatus.setConnectStatus(mergeActionStatus(status1.getConnectStatus(), status2.getConnectStatus()));
        mergedEngageStatus.setMessageStatus(mergeActionStatus(status1.getMessageStatus(), status2.getMessageStatus()));
        mergedEngageStatus.setInmailMessageStatus(mergeActionStatus(status1.getInmailMessageStatus(), status2.getInmailMessageStatus()));
        return mergedEngageStatus;
    }

    @Override
    public Linkedin getNetwork() {
        return Linkedin.getInstance();
    }
}
