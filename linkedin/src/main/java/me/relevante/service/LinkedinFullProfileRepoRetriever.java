package me.relevante.service;

import me.relevante.core.Linkedin;
import me.relevante.core.LinkedinFullProfile;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Component;

/**
 * @author Daniel Ibanez
 */
@Component
public class LinkedinFullProfileRepoRetriever
        extends AbstractNetworkFullProfileRepoRetriever<Linkedin, LinkedinFullProfile>
        implements NetworkFullProfileRepoRetriever<Linkedin, LinkedinFullProfile> {

    public LinkedinFullProfileRepoRetriever(final CrudRepository<LinkedinFullProfile, String> fullProfileRepo) {
        super(fullProfileRepo);
    }

    @Override
    public Linkedin getNetwork() {
        return Linkedin.getInstance();
    }
}
