package me.relevante.nlp;

import me.relevante.core.Linkedin;
import me.relevante.core.LinkedinComment;
import me.relevante.core.LinkedinFullPost;
import me.relevante.core.LinkedinFullProfile;
import me.relevante.core.LinkedinGroup;
import me.relevante.core.LinkedinLike;
import me.relevante.core.LinkedinPosition;
import me.relevante.core.LinkedinProfile;
import me.relevante.core.LinkedinSignalBioDescription;
import me.relevante.core.LinkedinSignalBioHeadline;
import me.relevante.core.LinkedinSignalCurrentPosition;
import me.relevante.core.LinkedinSignalGroupMembership;
import me.relevante.core.LinkedinSignalPosition;
import me.relevante.core.LinkedinSignalPostAuthorship;
import me.relevante.core.LinkedinSignalPostComment;
import me.relevante.core.LinkedinSignalPostLike;
import me.relevante.core.LinkedinSignalShareAuthorship;
import me.relevante.core.LinkedinSignalSkill;
import me.relevante.core.NetworkSignal;
import me.relevante.nlp.core.NlpCore;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;

@Component
public class LinkedinSignalExtractor
        extends AbstractNetworkSignalExtractor<Linkedin, LinkedinFullProfile, LinkedinFullPost>
        implements NetworkSignalExtractor<Linkedin, LinkedinFullProfile, LinkedinFullPost> {

    @Autowired
    public LinkedinSignalExtractor(final LinkedinSignalWeightFunction signalWeightFunction,
                                   final NlpCore nlpCore) {
        super(signalWeightFunction, nlpCore);
    }

    @Override
    public Linkedin getNetwork() {
        return Linkedin.getInstance();
    }

    @Override
    protected List<NetworkSignal<Linkedin>> extract(final LinkedinFullProfile fullProfile) {

        final List<NetworkSignal<Linkedin>> signals = new ArrayList<>();

        final List<NetworkSignal<Linkedin>> profileSignals = extract(fullProfile.getProfile());
        signals.addAll(profileSignals);

        for (final LinkedinPosition position : fullProfile.getPositions()) {
            final LinkedinSignalPosition signal = new LinkedinSignalPosition(position, fullProfile.getId());
            signals.add(signal);
        }

        for (final String skill : fullProfile.getSkills()) {
            final LinkedinSignalSkill signal = new LinkedinSignalSkill(skill, fullProfile.getId());
            signals.add(signal);
        }

        fullProfile.getGroups().forEach(group -> signals.add(extract(group, fullProfile.getId())));

        return signals;
    }

    @Override
    protected List<NetworkSignal<Linkedin>> extract(final LinkedinFullPost fullPost) {

        final List<NetworkSignal<Linkedin>> signals = new ArrayList<>();

        if (fullPost.getId().startsWith("UPDATE") || fullPost.getId().startsWith("s")) {
            final LinkedinSignalShareAuthorship signal = new LinkedinSignalShareAuthorship(fullPost.getPost());
            signals.add(signal);
        } else {
            final LinkedinSignalPostAuthorship signal = new LinkedinSignalPostAuthorship(fullPost.getPost());
            signals.add(signal);
        }
        for (final LinkedinLike like : fullPost.getLikes()) {
            final LinkedinSignalPostLike signal = new LinkedinSignalPostLike(fullPost.getPost(), like.getAuthorId(), like.getCreationTimestamp());
            signals.add(signal);
        }
        for (final LinkedinComment comment : fullPost.getComments()) {
            final LinkedinSignalPostComment signal = new LinkedinSignalPostComment(fullPost.getPost(), comment.getAuthorId(), comment.getCreationTimestamp());
            signals.add(signal);
        }

        return signals;
    }

    private NetworkSignal<Linkedin> extract(final LinkedinGroup group, final String profileId) {
        return new LinkedinSignalGroupMembership(group, profileId);
    }

    private List<NetworkSignal<Linkedin>> extract(final LinkedinProfile profile) {

        final List<NetworkSignal<Linkedin>> signals = new ArrayList<>();

        if (profile.getHeadline() != null) {
            final LinkedinSignalBioHeadline signal = new LinkedinSignalBioHeadline(profile);
            signals.add(signal);
        }
        if (profile.getSummary() != null) {
            final LinkedinSignalBioDescription signal = new LinkedinSignalBioDescription(profile);
            signals.add(signal);
        }
        if (profile.getTitle() != null || profile.getPositionSummary() != null) {
            final LinkedinSignalCurrentPosition signal = new LinkedinSignalCurrentPosition(profile);
            signals.add(signal);
        }

        return signals;
    }

}
