package me.relevante.nlp;

import me.relevante.core.Linkedin;
import me.relevante.core.LinkedinSignalBioDescription;
import me.relevante.core.LinkedinSignalBioHeadline;
import me.relevante.core.LinkedinSignalCurrentPosition;
import me.relevante.core.LinkedinSignalGroupMembership;
import me.relevante.core.LinkedinSignalPosition;
import me.relevante.core.LinkedinSignalPostAuthorship;
import me.relevante.core.LinkedinSignalPostComment;
import me.relevante.core.LinkedinSignalPostLike;
import me.relevante.core.LinkedinSignalShareAuthorship;
import me.relevante.core.LinkedinSignalSkill;
import me.relevante.core.NetworkSignal;
import me.relevante.search.AbstractNetworkSignalWeightFunction;
import me.relevante.search.NetworkSignalWeightFunction;
import org.springframework.stereotype.Component;

import java.util.HashMap;
import java.util.Map;
import java.util.function.Function;

@Component
public class LinkedinSignalWeightFunction extends AbstractNetworkSignalWeightFunction<Linkedin> implements NetworkSignalWeightFunction<Linkedin> {

    public LinkedinSignalWeightFunction() {
        super();
    }

    @Override
    public Linkedin getNetwork() {
        return Linkedin.getInstance();
    }

    @Override
    protected Map<Class<? extends NetworkSignal<Linkedin>>, Function<Double, Double>> createMiscellaneaWeightFunctionsBySignal() {
        Map<Class<? extends NetworkSignal<Linkedin>>, Function<Double, Double>> weightFunctionsBySignal = new HashMap<>();
        weightFunctionsBySignal.put(LinkedinSignalBioHeadline.class, n -> 7.5);
        weightFunctionsBySignal.put(LinkedinSignalCurrentPosition.class, n -> 7.5);
        weightFunctionsBySignal.put(LinkedinSignalBioDescription.class, n -> 5.0);
        weightFunctionsBySignal.put(LinkedinSignalPosition.class, n -> 2.5);
        weightFunctionsBySignal.put(LinkedinSignalSkill.class, n -> 2.5);

        weightFunctionsBySignal.put(LinkedinSignalPostAuthorship.class, n -> 8.333 / (1 + Math.pow(5.0, -n)));
        weightFunctionsBySignal.put(LinkedinSignalShareAuthorship.class, n -> 4.167 / (1 + Math.pow(5.0, -n)));
        weightFunctionsBySignal.put(LinkedinSignalPostLike.class, n -> 8.333 / (1 + Math.pow(5.0, -n)));
        weightFunctionsBySignal.put(LinkedinSignalPostComment.class, n -> 4.167 / (1 + Math.pow(5.0, -n)));

        weightFunctionsBySignal.put(LinkedinSignalGroupMembership.class, n -> 12.5 + (12.5 * Math.min(n, 5)) / 5.0);
        return weightFunctionsBySignal;
    }

    @Override
    protected Map<Class<? extends NetworkSignal<Linkedin>>, Function<Double, Double>> createProfessionalsWeightFunctionsBySignal() {
        Map<Class<? extends NetworkSignal<Linkedin>>, Function<Double, Double>> weightFunctionsBySignal = new HashMap<>();
        weightFunctionsBySignal.put(LinkedinSignalBioHeadline.class, n -> 7.5);
        weightFunctionsBySignal.put(LinkedinSignalCurrentPosition.class, n -> 7.5);
        weightFunctionsBySignal.put(LinkedinSignalBioDescription.class, n -> 5.0);
        weightFunctionsBySignal.put(LinkedinSignalPosition.class, n -> 2.5);
        weightFunctionsBySignal.put(LinkedinSignalSkill.class, n -> 2.5);

        weightFunctionsBySignal.put(LinkedinSignalPostAuthorship.class, n -> 8.333 / (1 + Math.pow(5.0, -n)));
        weightFunctionsBySignal.put(LinkedinSignalShareAuthorship.class, n -> 8.333 / (1 + Math.pow(5.0, -n)));
        weightFunctionsBySignal.put(LinkedinSignalPostLike.class, n -> 0.0);
        weightFunctionsBySignal.put(LinkedinSignalPostComment.class, n -> 0.0);

        weightFunctionsBySignal.put(LinkedinSignalGroupMembership.class, n -> 0.0);
        return weightFunctionsBySignal;
    }

    @Override
    protected Map<Class<? extends NetworkSignal<Linkedin>>, Function<Double, Double>> createProspectsWeightFunctionsBySignal() {
        Map<Class<? extends NetworkSignal<Linkedin>>, Function<Double, Double>> weightFunctionsBySignal = new HashMap<>();
        weightFunctionsBySignal.put(LinkedinSignalBioHeadline.class, n -> -7.5);
        weightFunctionsBySignal.put(LinkedinSignalCurrentPosition.class, n -> -7.5);
        weightFunctionsBySignal.put(LinkedinSignalBioDescription.class, n -> -5.0);
        weightFunctionsBySignal.put(LinkedinSignalPosition.class, n -> -2.5);
        weightFunctionsBySignal.put(LinkedinSignalSkill.class, n -> -2.5);

        weightFunctionsBySignal.put(LinkedinSignalPostAuthorship.class, n -> 4.167 / (1 + Math.pow(5.0, -n)));
        weightFunctionsBySignal.put(LinkedinSignalShareAuthorship.class, n -> 4.167 / (1 + Math.pow(5.0, -n)));
        weightFunctionsBySignal.put(LinkedinSignalPostLike.class, n -> 8.333 / (1 + Math.pow(5.0, -n)));
        weightFunctionsBySignal.put(LinkedinSignalPostComment.class, n -> 8.333 / (1 + Math.pow(5.0, -n)));

        weightFunctionsBySignal.put(LinkedinSignalGroupMembership.class, n -> 12.5 + (12.5 * Math.min(n, 5)) / 5.0);
        return weightFunctionsBySignal;
    }
}
