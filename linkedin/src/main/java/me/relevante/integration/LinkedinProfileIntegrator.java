package me.relevante.integration;

import me.relevante.core.Linkedin;
import me.relevante.core.LinkedinFullProfile;
import me.relevante.core.UniqueProfile;
import me.relevante.persistence.BaseLinkedinFullProfileRepo;
import me.relevante.queue.QueueService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Component;

@Component
public class LinkedinProfileIntegrator
        extends AbstractNetworkProfileIntegrator<Linkedin, LinkedinFullProfile>
        implements NetworkProfileIntegrator<Linkedin, LinkedinFullProfile> {

    private BaseLinkedinFullProfileRepo fullProfileRepo;

    @Autowired
    public LinkedinProfileIntegrator(
            final CrudRepository<UniqueProfile, String> uniqueProfileRepo,
            final BaseLinkedinFullProfileRepo fullProfileRepo,
            final QueueService queueService) {
        super(uniqueProfileRepo, fullProfileRepo, queueService);
        this.fullProfileRepo = fullProfileRepo;
    }

    @Override
    protected LinkedinFullProfile findAlreadyExistingProfile(final LinkedinFullProfile fullProfile) {
        return fullProfileRepo.findOneByProfileProfileUrl(fullProfile.getProfile().getProfileUrl());
    }

    @Override
    public Linkedin getNetwork() {
        return Linkedin.getInstance();
    }
}
