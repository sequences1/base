package me.relevante.api;

import me.relevante.auth.OAuthKeyPair;
import me.relevante.auth.OAuthTokenPair;
import me.relevante.core.LinkedinPost;
import me.relevante.core.LinkedinProfile;
import me.relevante.core.Linkedin;
import org.apache.commons.lang3.StringUtils;
import org.jdom2.Document;
import org.jdom2.Element;
import org.jdom2.input.SAXBuilder;
import org.scribe.builder.ServiceBuilder;
import org.scribe.builder.api.LinkedInApi;
import org.scribe.model.OAuthRequest;
import org.scribe.model.Response;
import org.scribe.model.Token;
import org.scribe.model.Verb;
import org.scribe.oauth.OAuthService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.Reader;
import java.io.StringReader;
import java.net.URLEncoder;
import java.util.Date;

public class LinkedinApiClientImpl implements LinkedinApiClient {

    private static final Logger logger = LoggerFactory.getLogger(LinkedinApiClientImpl.class);

    private static final String ID_TOKEN = "$$$id$$$";
    private static final String URL_TOKEN = "$$$url$$$";
    private static final String PROFILE_FIELDS = "(id,first-name,last-name,picture-url,public-profile-url,email-address,headline,current-share,location,summary,industry,group-memberships,primary-twitter-account,twitter-accounts,positions:(id,title,summary,start-date,end-date,is-current,company:(id,name,type,size,industry,ticker)))";
    private static final String PROFILE_BY_ID_REQUEST = "https://api.linkedin.com/v1/people/id=" + ID_TOKEN + ":" + PROFILE_FIELDS;
    private static final String PROFILE_BY_URL_REQUEST = "https://api.linkedin.com/v1/people/url=" + URL_TOKEN + ":" + PROFILE_FIELDS;

    private OAuthService oAuthService;
    private OAuthTokenPair<Linkedin> oAuthTokenPair;

    public LinkedinApiClientImpl(OAuthKeyPair<Linkedin> oAuthConsumerKeyPair,
            OAuthTokenPair<Linkedin> oAuthAccessTokenPair) {
        this.oAuthTokenPair = oAuthAccessTokenPair;
        this.oAuthService = createOAuthService(oAuthConsumerKeyPair);
    }

    @Override
    public Linkedin getNetwork() {
        return Linkedin.getInstance();
    }

    public LinkedinProfile getProfile() {
        String profileUrl = PROFILE_BY_ID_REQUEST.replace("id=" + ID_TOKEN, "~");
        return getProfileFromUrl(profileUrl);
    }

    public LinkedinProfile getProfileByPublicUrl(String publicUrl) {
        String encodedUrl = URLEncoder.encode(publicUrl);
        String profileUrl = PROFILE_BY_URL_REQUEST.replace(URL_TOKEN, encodedUrl);
        return getProfileFromUrl(profileUrl);
    }

    private OAuthService createOAuthService(OAuthKeyPair oAuthConsumerKeyPair) {
        OAuthService oAuthService = new ServiceBuilder()
                .provider(LinkedInApi.class)
                .apiKey(oAuthConsumerKeyPair.getKey())
                .apiSecret(oAuthConsumerKeyPair.getSecret())
                .build();
        return oAuthService;
    }

    private LinkedinProfile getProfileFromUrl(String url) {
        try {
            Token oAuthToken = new Token(oAuthTokenPair.getToken(), oAuthTokenPair.getSecret());
            OAuthRequest oAuthRequest = new OAuthRequest(Verb.GET, url);
            oAuthService.signRequest(oAuthToken, oAuthRequest);
            Response response = oAuthRequest.send();
            SAXBuilder builder = new SAXBuilder();
            Reader in = new StringReader(response.getBody());
            Document document = builder.build(in);
            Element profileNode = document.getRootElement();
            LinkedinProfile profile = createProfileFromProfileNode(profileNode);
            return profile;
        } catch (Exception e) {
            logger.error("Error", e);
        }
        return null;

    }

    private LinkedinProfile createProfileFromProfileNode(Element profileNode) {

        if (profileNode == null) {
            return null;
        }
        if (profileNode.getChildText("id") == null || profileNode.getChildText("id").equals("private")) {
            return null;
        }
        LinkedinProfile profile = new LinkedinProfile();
        profile.setFirstName(profileNode.getChildText("first-name"));
        profile.setLastName(profileNode.getChildText("last-name"));
        profile.setName(profile.getFirstName() + " " + profile.getLastName());
        profile.setImageUrl(profileNode.getChildText("picture-url"));
        profile.setId(composeUrlBasedId(profileNode.getChildText("public-profile-url")));
        profile.setEmail(profileNode.getChildText("email-address"));
        profile.setHeadline(profileNode.getChildText("headline"));
        profile.setPositionSummary(profileNode.getChildText("summary"));
        profile.setIndustry(profileNode.getChildText("industry"));
        profile.setProfileUrl(profileNode.getChildText("public-profile-url").replace("https", "http"));
        profile.setUpdateTimestamp(new Date());

        Element shareNode = profileNode.getChild("current-share");
        if (shareNode != null) {
            Element contentNode = shareNode.getChild("content");
            LinkedinPost post = new LinkedinPost();
            profile.setCurrentShare(post);
            post.setLinkedinId(shareNode.getChildText("id"));
            post.setCreationTimestamp(new Date(Long.parseLong(shareNode.getChildText("timestamp"))));
            post.setUrl(contentNode.getChildText("resolved-url"));
            post.setAuthor(profile);
            post.setAuthorId(profile.getId());
            post.setTitle(contentNode.getChildText("title"));
            post.setContent(contentNode.getChildText("description"));
            post.setImageUrl(contentNode.getChildText("submitted-image-url"));
        }

        Element locationNode = profileNode.getChild("location");
        if (locationNode != null) {
            profile.setArea(locationNode.getChildText("name"));
            Element countryNode = locationNode.getChild("country");
            profile.setCountryCode(countryNode.getChildText("code"));
            String location = StringUtils.defaultIfBlank(locationNode.getChildText("name"), "") + " " +
                    StringUtils.defaultIfBlank(countryNode.getChildText("code"), "").trim();
            profile.setLocation(location);
        }

        return profile;
    }

    private String composeUrlBasedId(String publicUrl) {

        int publicUrlIdIndex = publicUrl.indexOf("/in/");
        if (publicUrlIdIndex < 0) {
            publicUrlIdIndex = publicUrl.indexOf("/pub/");
        }
        if (publicUrlIdIndex < 0) {
            throw new IllegalArgumentException("Public URL not valid");
        }
        String publicUrlId = publicUrl.substring(publicUrlIdIndex + 1);
        return publicUrlId;
    }

}
