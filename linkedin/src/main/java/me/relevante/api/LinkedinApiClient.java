package me.relevante.api;

import me.relevante.core.LinkedinProfile;
import me.relevante.core.Linkedin;

public interface LinkedinApiClient extends NetworkApiClient<Linkedin, LinkedinProfile> {

    LinkedinProfile getProfile();
    LinkedinProfile getProfileByPublicUrl(String publicUrl);

}
