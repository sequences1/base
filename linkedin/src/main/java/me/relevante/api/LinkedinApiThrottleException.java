package me.relevante.api;

public class LinkedinApiThrottleException extends LinkedinApiForbiddenException {

    public LinkedinApiThrottleException() {
        super();
    }

    public LinkedinApiThrottleException(String message) {
        super(message);
    }

    public LinkedinApiThrottleException(String message, Throwable cause) {
        super(message, cause);
    }

    public LinkedinApiThrottleException(Throwable cause) {
        super(cause);
    }

    protected LinkedinApiThrottleException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }
}
