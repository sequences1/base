package me.relevante.api;

public enum LinkedinApiResult {
    EXCEPTION,
    FORBIDDEN,
    SUCCESS,
    UNDEFINED_ERROR

}
