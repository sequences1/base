package me.relevante.api;

public class LinkedinApiResponse {

    private LinkedinApiResult result;
    private Object relatedObject;

    public LinkedinApiResponse(LinkedinApiResult result,
                               Object expectedObject) {
        this.result = result;
        this.relatedObject = expectedObject;
    }

    public LinkedinApiResponse(LinkedinApiResult result) {
        this(result, null);
    }

    public LinkedinApiResult getResult() {
        return result;
    }

    public Object getRelatedObject() {
        return relatedObject;
    }
}
