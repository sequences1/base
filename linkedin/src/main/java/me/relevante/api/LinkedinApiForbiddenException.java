package me.relevante.api;

public class LinkedinApiForbiddenException extends Exception {
    public LinkedinApiForbiddenException() {
        super();
    }

    public LinkedinApiForbiddenException(String message) {
        super(message);
    }

    public LinkedinApiForbiddenException(String message, Throwable cause) {
        super(message, cause);
    }

    public LinkedinApiForbiddenException(Throwable cause) {
        super(cause);
    }

    protected LinkedinApiForbiddenException(String message, Throwable cause, boolean enableSuppression,
            boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }
}
