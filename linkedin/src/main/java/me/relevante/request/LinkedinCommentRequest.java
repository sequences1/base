package me.relevante.request;

import me.relevante.core.Linkedin;
import org.springframework.data.mongodb.core.mapping.Document;

@Document(collection = "LinkedinCommentRequest")
public class LinkedinCommentRequest extends AbstractNetworkEngagePostActionRequest<Linkedin> implements NetworkEngagePostActionRequest<Linkedin> {

	private String comment;

    private LinkedinCommentRequest() {
        this(null, null, null, null);
    }

    public LinkedinCommentRequest(final String requesterRelevanteId,
                                  final String targetProfileId,
                                  final String targetPostId,
                                  final String comment) {
        super(requesterRelevanteId, targetProfileId, targetPostId);
		this.comment = comment;
	}

    public String getComment() {
        return comment;
    }

}
