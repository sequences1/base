package me.relevante.request;

import me.relevante.core.Linkedin;
import org.springframework.data.mongodb.core.mapping.Document;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by daniel-ibanez on 22/09/16.
 */
@Document(collection = "LinkedinExtractFeedProfilesRequest")
public class LinkedinExtractFeedProfilesRequest extends AbstractActionRequest implements NetworkActionRequest<Linkedin> {

    private List<LinkedinExtractedFeedProfile> extractedProfiles;

    private LinkedinExtractFeedProfilesRequest() {
        this(null);
    }

    public LinkedinExtractFeedProfilesRequest(final String requesterRelevanteId) {
        super(requesterRelevanteId);
        this.extractedProfiles = new ArrayList<>();
    }

    public List<LinkedinExtractedFeedProfile> getExtractedProfiles() {
        return extractedProfiles;
    }

    public void addExtractedProfile(final LinkedinExtractedFeedProfile extractedFeedProfile) {
        extractedProfiles.add(extractedFeedProfile);
    }
}
