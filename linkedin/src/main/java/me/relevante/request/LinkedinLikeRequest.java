package me.relevante.request;

import me.relevante.core.Linkedin;
import org.springframework.data.mongodb.core.mapping.Document;

@Document(collection = "LinkedinLikeRequest")
public class LinkedinLikeRequest extends AbstractNetworkEngagePostActionRequest<Linkedin> implements NetworkEngagePostActionRequest<Linkedin> {

    private LinkedinLikeRequest() {
        this(null, null, null);
    }

    public LinkedinLikeRequest(final String requesterRelevanteId,
                               final String targetProfileId,
                               final String targetPostId) {
        super(requesterRelevanteId, targetProfileId, targetPostId);
    }

}
