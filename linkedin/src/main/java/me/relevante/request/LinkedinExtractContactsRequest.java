package me.relevante.request;

import me.relevante.core.Linkedin;
import org.springframework.data.mongodb.core.mapping.Document;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by daniel-ibanez on 22/09/16.
 */
@Document(collection = "LinkedinExtractContactsRequest")
public class LinkedinExtractContactsRequest extends AbstractActionRequest implements NetworkActionRequest<Linkedin> {

    private List<LinkedinExtractedContact> extractedContacts;

    private LinkedinExtractContactsRequest() {
        this(null);
    }

    public LinkedinExtractContactsRequest(final String requesterRelevanteId) {
        super(requesterRelevanteId);
        this.extractedContacts = new ArrayList<>();
    }

    public List<LinkedinExtractedContact> getExtractedContacts() {
        return extractedContacts;
    }

    public void addExtractedContact(final LinkedinExtractedContact extractedContact) {
        extractedContacts.add(extractedContact);
    }
}
