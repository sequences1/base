package me.relevante.request;

/**
 * Created by daniel-ibanez on 22/09/16.
 */
public class LinkedinExtractedContact {

    private final String publicIdentifier;
    private final String firstName;
    private final String lastName;
    private final String occupation;

    private LinkedinExtractedContact() {
        this(null, null, null, null);
    }

    public LinkedinExtractedContact(final String publicIdentifier,
                                    final String firstName,
                                    final String lastName,
                                    final String occupation) {
        this.publicIdentifier = publicIdentifier;
        this.firstName = firstName;
        this.lastName = lastName;
        this.occupation = occupation;
    }

    public String getPublicIdentifier() {
        return publicIdentifier;
    }

    public String getFirstName() {
        return firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public String getOccupation() {
        return occupation;
    }
}
