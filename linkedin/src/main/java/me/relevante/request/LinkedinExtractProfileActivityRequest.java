package me.relevante.request;

import me.relevante.core.Linkedin;
import me.relevante.ingest.LinkedinIngestGroupPost;
import me.relevante.ingest.LinkedinIngestShare;
import org.springframework.data.mongodb.core.mapping.Document;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by daniel-ibanez on 22/09/16.
 */
@Document(collection = "LinkedinExtractProfileActivityRequest")
public class LinkedinExtractProfileActivityRequest extends AbstractNetworkEngageActionRequest<Linkedin> implements NetworkActionRequest<Linkedin> {

    private List<LinkedinIngestShare> extractedShares;
    private List<LinkedinIngestGroupPost> extractedGroupPosts;

    private LinkedinExtractProfileActivityRequest() {
        this(null, null);
    }

    public LinkedinExtractProfileActivityRequest(final String requesterRelevanteId,
                                                 final String targetProfileId) {
        super(requesterRelevanteId, targetProfileId);
        this.extractedShares = new ArrayList<>();
        this.extractedGroupPosts = new ArrayList<>();
    }

    public List<LinkedinIngestShare> getExtractedShares() {
        return extractedShares;
    }

    public void setExtractedShares(final List<LinkedinIngestShare> extractedShares) {
        this.extractedShares = extractedShares;
    }

    public List<LinkedinIngestGroupPost> getExtractedGroupPosts() {
        return extractedGroupPosts;
    }

    public void setExtractedGroupPosts(final List<LinkedinIngestGroupPost> extractedGroupPosts) {
        this.extractedGroupPosts = extractedGroupPosts;
    }
}
