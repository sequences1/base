package me.relevante.request;

import me.relevante.core.Linkedin;

/**
 * @author Daniel Ibanez
 */
public class LinkedinEngageStatus implements NetworkEngageStatus<Linkedin, LinkedinEngageStatus> {

    private ActionEngageStatus likeStatus;
    private ActionEngageStatus commentStatus;
    private ActionEngageStatus connectStatus;
    private ActionEngageStatus messageStatus;
    private ActionEngageStatus inmailMessageStatus;

    @Override
    public Linkedin getNetwork() {
        return Linkedin.getInstance();
    }

    public ActionEngageStatus getLikeStatus() {
        return likeStatus;
    }

    public void setLikeStatus(final ActionEngageStatus likeStatus) {
        this.likeStatus = likeStatus;
    }

    public ActionEngageStatus getCommentStatus() {
        return commentStatus;
    }

    public void setCommentStatus(final ActionEngageStatus commentStatus) {
        this.commentStatus = commentStatus;
    }

    public ActionEngageStatus getConnectStatus() {
        return connectStatus;
    }

    public void setConnectStatus(final ActionEngageStatus connectStatus) {
        this.connectStatus = connectStatus;
    }

    public ActionEngageStatus getMessageStatus() {
        return messageStatus;
    }

    public void setMessageStatus(final ActionEngageStatus messageStatus) {
        this.messageStatus = messageStatus;
    }

    public ActionEngageStatus getInmailMessageStatus() {
        return inmailMessageStatus;
    }

    public void setInmailMessageStatus(final ActionEngageStatus inmailMessageStatus) {
        this.inmailMessageStatus = inmailMessageStatus;
    }

    @Override
    public ActionEngageStatus getLevel1Status() {
        return getLikeStatus();
    }

    @Override
    public void setLevel1Status(final ActionEngageStatus level1Status) {
        setLikeStatus(level1Status);
    }

    @Override
    public ActionEngageStatus getLevel2Status() {
        return getCommentStatus();
    }

    @Override
    public void setLevel2Status(final ActionEngageStatus level2Status) {
        setCommentStatus(level2Status);
    }

    @Override
    public ActionEngageStatus getLevel3Status() {
        return getConnectStatus();
    }

    @Override
    public void setLevel3Status(final ActionEngageStatus level3Status) {
        setConnectStatus(level3Status);
    }

    @Override
    public ActionEngageStatus getLevel4Status() {
        return getMessageStatus();
    }

    @Override
    public void setLevel4Status(final ActionEngageStatus level4Status) {
        setMessageStatus(level4Status);
    }

    @Override
    public LinkedinEngageStatus clone() {
        final LinkedinEngageStatus cloned = new LinkedinEngageStatus();
        cloned.likeStatus = this.likeStatus == null ? null : likeStatus.clone();
        cloned.commentStatus = this.commentStatus == null ? null : commentStatus.clone();
        cloned.connectStatus = this.connectStatus == null ? null : connectStatus.clone();
        cloned.messageStatus = this.messageStatus == null ? null : messageStatus.clone();
        cloned.inmailMessageStatus = this.inmailMessageStatus == null ? null : inmailMessageStatus.clone();
        return cloned;
    }
}
