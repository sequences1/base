package me.relevante.request;

import me.relevante.core.Linkedin;
import me.relevante.ingest.LinkedinIngestProfile;
import org.springframework.data.mongodb.core.mapping.Document;

/**
 * Created by daniel-ibanez on 22/09/16.
 */
@Document(collection = "LinkedinExtractProfileDataRequest")
public class LinkedinExtractProfileDataRequest extends AbstractNetworkEngageActionRequest<Linkedin> implements NetworkActionRequest<Linkedin> {

    private LinkedinIngestProfile extractedProfile;

    private LinkedinExtractProfileDataRequest() {
        this(null, null);
    }

    public LinkedinExtractProfileDataRequest(final String requesterRelevanteId,
                                             final String targetProfileId) {
        super(requesterRelevanteId, targetProfileId);
    }

    public LinkedinIngestProfile getExtractedProfile() {
        return extractedProfile;
    }

    public void setExtractedProfile(final LinkedinIngestProfile extractedProfile) {
        this.extractedProfile = extractedProfile;
    }

}
