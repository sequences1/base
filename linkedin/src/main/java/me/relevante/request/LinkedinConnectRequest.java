package me.relevante.request;

import me.relevante.core.Linkedin;
import org.springframework.data.mongodb.core.mapping.Document;

@Document(collection = "LinkedinConnectRequest")
public class LinkedinConnectRequest extends AbstractNetworkEngageActionRequest<Linkedin> implements NetworkEngageActionRequest<Linkedin> {

    private LinkedinConnectRequest() {
        this(null, null);
    }

    public LinkedinConnectRequest(final String requesterRelevanteId,
                                  final String targetProfileId) {
        super(requesterRelevanteId, targetProfileId);
    }

}
