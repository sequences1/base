package me.relevante.request;

import me.relevante.core.Linkedin;
import org.springframework.data.mongodb.core.mapping.Document;

@Document(collection = "LinkedinMessageRequest")
public class LinkedinMessageRequest extends AbstractNetworkEngageActionRequest<Linkedin> implements NetworkEngageActionRequest<Linkedin> {

	private String messageText;

    private LinkedinMessageRequest() {
        this(null, null, null);
    }

    public LinkedinMessageRequest(final String requesterRelevanteId,
                                  final String targetProfileId,
                                  final String messageText) {
        super(requesterRelevanteId, targetProfileId);
        this.messageText = messageText;
    }

	public String getMessageText() {
		return messageText;
	}

}
