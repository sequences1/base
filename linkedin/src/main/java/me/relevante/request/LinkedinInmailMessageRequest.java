package me.relevante.request;

import me.relevante.core.Linkedin;
import org.springframework.data.mongodb.core.mapping.Document;

@Document(collection = "LinkedinInmailMessageRequest")
public class LinkedinInmailMessageRequest extends AbstractNetworkEngageActionRequest<Linkedin> implements NetworkEngageActionRequest<Linkedin> {

    private String subject;
	private String messageText;

    private LinkedinInmailMessageRequest() {
        this(null, null, null, null);
    }

    public LinkedinInmailMessageRequest(final String requesterRelevanteId,
                                        final String targetProfileId,
                                        final String subject,
                                        final String messageText) {
        super(requesterRelevanteId, targetProfileId);
        this.subject = subject;
        this.messageText = messageText;
    }

	public String getSubject() {
		return subject;
	}

	public String getMessageText() {
		return messageText;
	}

}
