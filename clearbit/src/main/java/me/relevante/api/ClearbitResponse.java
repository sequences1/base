package me.relevante.api;

public class ClearbitResponse {

    private String domain;
    private String logo;
    private String name;

    public ClearbitResponse(final String domain,
                            final String logo,
                            final String name) {
        this.domain = domain;
        this.logo = logo;
        this.name = name;
    }

    private ClearbitResponse() {
    }

    public String getDomain() {
        return domain;
    }

    public String getLogo() {
        return logo;
    }

    public String getName() {
        return name;
    }
}
