package me.relevante.api;

import org.springframework.cloud.netflix.feign.FeignClient;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.List;

@FeignClient(name = "clearbit", url = "${clearbit.url}", configuration = ClearbitApiClientConfig.class)
public interface ClearbitApiClient {

    @RequestMapping(method = RequestMethod.GET, value = "/companies/suggest", params = {"query"})
    List<ClearbitResponse> getCompanyAutoComplete(@RequestParam("query") String query);

}
