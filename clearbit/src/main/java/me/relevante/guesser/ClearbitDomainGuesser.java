package me.relevante.guesser;

import me.relevante.api.ClearbitApiClient;
import me.relevante.guesser.domain.DomainGuesser;
import me.relevante.guesser.domain.GuessedDomain;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.stream.Collectors;

@Component
public class ClearbitDomainGuesser implements DomainGuesser {

    private final ClearbitApiClient clearbitClient;

    @Autowired
    public ClearbitDomainGuesser(final ClearbitApiClient clearbitClient) {
        this.clearbitClient = clearbitClient;
    }

    @Override
    public List<GuessedDomain> guess(final Clues clues) {
        return clearbitClient.getCompanyAutoComplete(clues.getCompany()).stream()
                .map(domain -> new GuessedDomain(domain.getDomain(), 0L))
                .collect(Collectors.toList());
    }

}
