package me.relevante.core;

import me.relevante.guesser.GuesserProvider;
import org.springframework.stereotype.Component;

@Component
public class Clearbit extends AbstractNetwork implements Network, GuesserProvider {

    private static final String NAME = "clearbit";
    private static Clearbit instance = new Clearbit();

    public static Clearbit getInstance() {
        return instance;
    }

    private Clearbit() {
        super(NAME);
    }

}
