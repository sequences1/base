package me.relevante.util;

import me.relevante.core.UserProfile;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.stream.Collectors;

@Component
public class UserProfileToProfileIdBiAdapter {

    public List<String> toProfileIds(final Collection<? extends UserProfile> userProfiles) {
        return userProfiles.stream().map(UserProfile::getProfileId).collect(Collectors.toList());
    }

    public List<UserProfile> toUserProfiles(final Collection<String> profileIds) {
        final List userProfiles = new ArrayList<>();
        for (final String profileId : profileIds) {
            final UserProfile userProfile = new UserProfile() {
                @Override
                public String getProfileId() {
                    return profileId;
                }
            };
            userProfiles.add(userProfile);
        }
        return userProfiles;
    }
}
