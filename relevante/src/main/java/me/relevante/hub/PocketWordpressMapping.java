package me.relevante.hub;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

/**
 * Created by daniel-ibanez on 2/08/16.
 */
@Document(collection = "PocketWordpressMapping")
public class PocketWordpressMapping {

    @Id
    private String id;
    private String relevanteId;
    private String pocketTag;
    private String wpCategory;
    private String wpHubId;
    private String language;

    public PocketWordpressMapping(final String relevanteId,
                                  final String pocketTag,
                                  final String wpCategory,
                                  final String wpHubId,
                                  final String language) {
        this();
        this.relevanteId = relevanteId;
        this.pocketTag = pocketTag;
        this.wpCategory = wpCategory;
        this.wpHubId = wpHubId;
        this.language = language;
    }

    private PocketWordpressMapping() {
    }

    public String getId() {
        return id;
    }

    public String getRelevanteId() {
        return relevanteId;
    }

    public String getPocketTag() {
        return pocketTag;
    }

    public String getWpCategory() {
        return wpCategory;
    }

    public String getWpHubId() {
        return wpHubId;
    }

    public String getLanguage() {
        return language;
    }
}
