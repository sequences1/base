package me.relevante.search.impl.elasticsearch;

import com.fasterxml.jackson.databind.ObjectMapper;
import me.relevante.nlp.SignalVectorizer;
import me.relevante.nlp.core.NlpCore;
import me.relevante.search.ProfileSearch;
import me.relevante.search.ProfileSearcher;
import me.relevante.search.SearchOperator;
import me.relevante.search.SearchProfile;
import me.relevante.util.Chronometer;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Primary;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;

@Component(value = "elasticSearchSignalBasedProfileSearcher")
@Primary
public class ElasticSearchSignalBasedProfileSearcher extends AbstractElasticSearchProfileSearcher implements ProfileSearcher {

    private static final String CRITERIUM_COMPANY = "{\"match\":{\"company\": {\"query\":\"%s\",\"operator\":\"and\"}}}";
    private static final String CRITERIUM_INDUSTRY = "{\"match\":{\"industry\": {\"query\":\"%s\",\"operator\":\"and\"}}}";
    private static final String CRITERIUM_LOCATION = "{\"match\":{\"location\": {\"query\":\"%s\",\"operator\":\"and\"}}}";
    private static final String CRITERIUM_SIGNAL = "{\"match\":{\"signals.nlpAnalyzableContent\": {\"query\":\"%s\",\"operator\":\"and\"}}}";
    private static final String SIGNALS_QUERY = "{\"from\":%s, \"size\":%s, \"_source\":[\"industry\", \"location\", \"company\", \"signals\"], \"query\":{\"bool\":{\"must\":[%s],\"should\":[%s],\"must_not\":[%s]}}}";

    @Autowired
    public ElasticSearchSignalBasedProfileSearcher(final NlpCore nlpCore,
                                                   final SignalVectorizer signalVectorizer,
                                                   final ObjectMapper jsonMapper,
                                                   final Chronometer chrono,
                                                   final @Value("${search.elasticsearch.endPointUrl}") String elasticProfileEndpointUrl,
                                                   final @Value("${search.elasticsearch.authUsername}") String elasticAuthUsername,
                                                   final @Value("${search.elasticsearch.authPassword}") String elasticAuthPassword,
                                                   final @Value("${search.elasticsearch.resultProfilesMaxSize}") int resultProfilesMaxSize) {
        super(nlpCore, signalVectorizer, jsonMapper, chrono, elasticProfileEndpointUrl, elasticAuthUsername, elasticAuthPassword, resultProfilesMaxSize);
    }

    @Override
    protected String composeMustExcerpt(final ProfileSearch profileSearch) {
        final List<String> criteria = new ArrayList<>();
        if (StringUtils.isNotBlank(profileSearch.getLocation())) {
            criteria.add(String.format(CRITERIUM_LOCATION, profileSearch.getLocation()));
        }
        if (StringUtils.isNotBlank(profileSearch.getIndustry())) {
            criteria.add(String.format(CRITERIUM_INDUSTRY, profileSearch.getIndustry()));
        }
        if (StringUtils.isNotBlank(profileSearch.getCompany())) {
            criteria.add(String.format(CRITERIUM_COMPANY, profileSearch.getCompany()));
        }
        if (profileSearch.getSearchOperator().equals(SearchOperator.AND)) {
            profileSearch.getSearchTerms().forEach(searchTerm -> criteria.add(String.format(CRITERIUM_SIGNAL, searchTerm)));
        }
        final StringBuilder mustExcerpt = new StringBuilder();
        for (final String criterium : criteria) {
            if (mustExcerpt.length() > 0) {
                mustExcerpt.append(", ");
            }
            mustExcerpt.append(criterium);
        }
        return mustExcerpt.toString();
    }

    @Override
    protected String composeMustNotExcerpt(final ProfileSearch profileSearch) {
        final StringBuilder mustNotExcerpt = new StringBuilder();
        for (final String negativeTerm : profileSearch.getNegativeTerms()) {
            if (mustNotExcerpt.length() > 0) {
                mustNotExcerpt.append(", ");
            }
            mustNotExcerpt.append(String.format(CRITERIUM_SIGNAL, negativeTerm));
        }
        return mustNotExcerpt.toString();
    }

    @Override
    protected String composeShouldExcerpt(final ProfileSearch profileSearch) {
        final List<String> criteria = new ArrayList<>();
        if (profileSearch.getSearchOperator().equals(SearchOperator.OR)) {
            profileSearch.getSearchTerms().forEach(searchTerm -> criteria.add(String.format(CRITERIUM_SIGNAL, searchTerm)));
        }
        final StringBuilder shouldExcerpt = new StringBuilder();
        for (final String criterium : criteria) {
            if (shouldExcerpt.length() > 0) {
                shouldExcerpt.append(", ");
            }
            shouldExcerpt.append(criterium);
        }
        return shouldExcerpt.toString();
    }

    @Override
    protected String getQueryTemplate() {
        return SIGNALS_QUERY;
    }

    @Override
    protected List<SearchProfile> obtainCandidateProfiles(final ProfileSearch profileSearch,
                                                          final List<String> nlpSearchTerms,
                                                          final int maxProfiles) {
        final List<SearchProfile> searchProfiles = super.obtainCandidateProfiles(profileSearch, nlpSearchTerms, maxProfiles);
        removeProfilesWithoutSignals(searchProfiles);
        return searchProfiles;
    }

    private void removeProfilesWithoutSignals(final List<SearchProfile> profiles) {
        final List<SearchProfile> profilesWithoutSignals = new ArrayList<>();
        for (final SearchProfile searchProfile : profiles) {
            if (!searchProfile.getSignals().isEmpty()) {
                profilesWithoutSignals.add(searchProfile);
            }
        }
        profiles.clear();
        profiles.addAll(profilesWithoutSignals);
    }

}
