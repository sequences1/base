package me.relevante.search.impl.elasticsearch;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import me.relevante.auth.BasicAuthPair;
import me.relevante.core.NetworkSignal;
import me.relevante.core.PolymorphicalSignalMixIn;
import me.relevante.search.SearchProfile;
import me.relevante.search.SearchProfileRepository;
import org.apache.commons.io.IOUtils;
import org.apache.http.HttpResponse;
import org.apache.http.client.methods.HttpDelete;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.methods.HttpUriRequest;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import java.io.IOException;

@Component
public class ElasticSearchProfileRepository implements SearchProfileRepository {

    private final String elasticProfileEndpointUrl;
    private final BasicAuthPair elasticAuthentication;
    private final ObjectMapper jsonMapper;

    @Autowired
    public ElasticSearchProfileRepository(final @Value("${search.elasticsearch.endPointUrl}") String elasticProfileEndpointUrl,
                                          final @Value("${search.elasticsearch.authUsername}") String elasticAuthUsername,
                                          final @Value("${search.elasticsearch.authPassword}") String elasticAuthPassword) {
        this.elasticProfileEndpointUrl = elasticProfileEndpointUrl;
        this.elasticAuthentication = new BasicAuthPair(elasticAuthUsername, elasticAuthPassword);
        this.jsonMapper = new ObjectMapper().addMixIn(NetworkSignal.class, PolymorphicalSignalMixIn.class);
    }

    @Override
    public void save(final SearchProfile searchProfile) {
        final String serializedProfile = serialize(searchProfile);
        final HttpPost httpPost = new HttpPost(elasticProfileEndpointUrl + "/" + searchProfile.getId());
        httpPost.setEntity(new StringEntity(serializedProfile, "UTF-8"));
        executeAuthorizedRequest(httpPost);
    }

    @Override
    public SearchProfile get(final String uniqueProfileId) {
        final HttpGet httpGet = new HttpGet(elasticProfileEndpointUrl + "/" + uniqueProfileId);
        final ElasticSearchHit elasticSearchHit = executeAuthorizedRequestReturningObject(httpGet, ElasticSearchHit.class);
        if (elasticSearchHit.getProfile() == null) {
            return null;
        }
        final SearchProfile searchProfile = elasticSearchHit.getProfile();
        searchProfile.setId(elasticSearchHit.getId());
        return searchProfile;
    }

    @Override
    public void remove(final String uniqueProfileId) {
        final HttpDelete httpDelete = new HttpDelete(elasticProfileEndpointUrl + "/" + uniqueProfileId);
        executeAuthorizedRequest(httpDelete);
    }

    private void executeAuthorizedRequest(final HttpUriRequest httpRequest) {
        try {
            httpRequest.setHeader("Authorization", elasticAuthentication.getAuthHeaderValue());
            httpRequest.setHeader("Content-Type", "application/json; charset=UTF-8");
            final CloseableHttpClient httpClient = HttpClients.createDefault();
            httpClient.execute(httpRequest);
            httpClient.close();
        } catch (final Exception e) {
            throw new RuntimeException(e);
        }
    }

    private <T> T executeAuthorizedRequestReturningObject(final HttpUriRequest httpRequest,
                                                           final Class<T> clazz) {
        try {
            httpRequest.setHeader("Authorization", elasticAuthentication.getAuthHeaderValue());
            httpRequest.setHeader("Content-Type", "application/json; charset=UTF-8");
            final CloseableHttpClient httpClient = HttpClients.createDefault();
            final HttpResponse httpResponse = httpClient.execute(httpRequest);
            final String responseText = IOUtils.toString(httpResponse.getEntity().getContent(), "UTF-8");
            T object = deserialize(responseText, clazz);
            httpClient.close();
            return object;
        } catch (final Exception e) {
            throw new RuntimeException(e);
        }
    }


    private String serialize(final Object serializedObject) {
        try {
            return jsonMapper.writeValueAsString(serializedObject);
        } catch (final JsonProcessingException e) {
            throw new IllegalArgumentException(e);
        }
    }

    private <T> T deserialize(final String serializedObject, final Class<T> clazz) {
        try {
            return jsonMapper.readValue(serializedObject, clazz);
        } catch (final IOException e) {
            throw new RuntimeException(e);
        }

    }
}
