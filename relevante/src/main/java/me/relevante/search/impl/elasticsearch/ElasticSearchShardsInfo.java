package me.relevante.search.impl.elasticsearch;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * @author Daniel Ibanez
 */
public class ElasticSearchShardsInfo {

    private long total;
    private long successful;
    private long failed;

    @JsonCreator
    public ElasticSearchShardsInfo(@JsonProperty("total") final long total,
                                   @JsonProperty("successful") final long successful,
                                   @JsonProperty("failed") final long failed) {
        this.total = total;
        this.successful = successful;
        this.failed = failed;
    }

    public long getTotal() {
        return total;
    }

    public long getSuccessful() {
        return successful;
    }

    public long getFailed() {
        return failed;
    }
}

