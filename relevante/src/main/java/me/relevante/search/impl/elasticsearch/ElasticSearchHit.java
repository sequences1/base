package me.relevante.search.impl.elasticsearch;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import me.relevante.search.SearchProfile;

/**
 * @author Daniel Ibanez
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class ElasticSearchHit {

    private String index;
    private String type;
    private String id;
    private Double score;
    private SearchProfile profile;

    @JsonCreator
    public ElasticSearchHit(@JsonProperty("_index") final String index,
                            @JsonProperty("_type") final String type,
                            @JsonProperty("_id") final String id,
                            @JsonProperty("_score") final Double score,
                            @JsonProperty("_source") final SearchProfile profile) {
        this.index = index;
        this.type = type;
        this.id = id;
        this.score = score;
        this.profile = profile;
    }

    public String getIndex() {
        return index;
    }

    public String getType() {
        return type;
    }

    public String getId() {
        return id;
    }

    public Double getScore() {
        return score;
    }

    public SearchProfile getProfile() {
        return profile;
    }
}

