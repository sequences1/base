package me.relevante.search.impl.elasticsearch;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * @author Daniel Ibanez
 */
public class ElasticSearchResult {

    private long took;
    private boolean timedOut;
    private ElasticSearchShardsInfo shardsInfo;
    private ElasticSearchHitsInfo hitsInfo;

    @JsonCreator
    public ElasticSearchResult(@JsonProperty("took") final long took,
                               @JsonProperty("timed_out") final boolean timedOut,
                               @JsonProperty("_shards") final ElasticSearchShardsInfo shardsInfo,
                               @JsonProperty("hits") final ElasticSearchHitsInfo hitsInfo) {
        this.took = took;
        this.timedOut = timedOut;
        this.shardsInfo = shardsInfo;
        this.hitsInfo = hitsInfo;
    }

    public long getTook() {
        return took;
    }

    public boolean isTimedOut() {
        return timedOut;
    }

    public ElasticSearchShardsInfo getShardsInfo() {
        return shardsInfo;
    }

    public ElasticSearchHitsInfo getHitsInfo() {
        return hitsInfo;
    }
}

