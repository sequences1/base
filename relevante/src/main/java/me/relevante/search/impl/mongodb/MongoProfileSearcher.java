package me.relevante.search.impl.mongodb;

import me.relevante.nlp.SignalVectorizer;
import me.relevante.nlp.core.NlpCore;
import me.relevante.persistence.BaseSearchProfileRepo;
import me.relevante.search.ProfileSearch;
import me.relevante.search.ProfileSearcher;
import me.relevante.search.SearchProfile;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
public class MongoProfileSearcher implements ProfileSearcher {

    private static final int MAX_PAGE = 1000;

    private final NlpCore nlpCore;
    private final BaseSearchProfileRepo searchRepo;
    private final SignalVectorizer signalVectorizer;

    @Autowired
    public MongoProfileSearcher(final NlpCore nlpCore,
                                final BaseSearchProfileRepo searchRepo,
                                final SignalVectorizer signalVectorizer) {
        this.nlpCore = nlpCore;
        this.searchRepo = searchRepo;
        this.signalVectorizer = signalVectorizer;
    }

    @Override
    public List<SearchProfile> search(final ProfileSearch profileSearch) {
        throw new UnsupportedOperationException();
    }

    @Override
    public List<SearchProfile> search(final ProfileSearch search, final int maxProfiles) {
        throw new UnsupportedOperationException();
    }

    //        if (searchTerms == null || searchTerms.isEmpty()) {
//            throw new IllegalArgumentException();
//        }
//        List<SearchQuery> multipleQuery = composeMultipleQuery(searchTerms, 2);
//        List<SearchProfile> matchingProfiles = getMatchingProfiles(multipleQuery, location, industry, company);
//        removeNonRelatedSignals(matchingProfiles, multipleQuery);
//        setScore(matchingProfiles, multipleQuery);
//        matchingProfiles.sort((o1, o2) -> new Double(o2.getScore()).compareTo(new Double(o1.getScore())));
//
//        return matchingProfiles;
//    }
//
//    private List<SearchQuery> composeMultipleQuery(List<String> terms,
//                                                   int maxTerms) {
//
//        List<SearchQuery> multipleQuery = new ArrayList<>();
//        for (String term : terms) {
//            String lowercaseTerm = term.toLowerCase();
//            List<String> termWords = nlpCore.removeStopWords(lowercaseTerm);
//            List<String> cleanTerms = new ArrayList<>();
//            termWords.forEach(termWord -> cleanTerms.add(nlpCore.stem(termWord)));
//            SearchQuery query = new SearchQuery(cleanTerms, maxTerms);
//            multipleQuery.add(query);
//        }
//        return multipleQuery;
//    }
//
//    private List<SearchProfile> getMatchingProfiles(List<SearchQuery> multipleQuery,
//                                                    String location,
//                                                    String industry,
//                                                    String company) {
//        String mainQueryTermStem = multipleQuery.get(0).getMainQueryTerm();
//        List<SearchProfile> matchingProfiles;
//        if (StringUtils.isBlank(location)) {
//            if (StringUtils.isBlank(industry)) {
//                if (StringUtils.isBlank(company)) {
//                    matchingProfiles = searchRepo.findBySignalsContentStems(mainQueryTermStem, new PageRequest(0, MAX_PAGE));
//                } else {
//                    matchingProfiles = searchRepo.findBySignalsContentStemsAndCompany(mainQueryTermStem, company, new PageRequest(0, MAX_PAGE));
//                }
//            } else {
//                if (StringUtils.isBlank(company)) {
//                    matchingProfiles = searchRepo.findBySignalsContentStemsAndIndustry(mainQueryTermStem, industry, new PageRequest(0, MAX_PAGE));
//                } else {
//                    matchingProfiles = searchRepo.findBySignalsContentStemsAndIndustryAndCompany(mainQueryTermStem, industry, company, new PageRequest(0, MAX_PAGE));
//                }
//            }
//        } else {
//            if (StringUtils.isBlank(industry)) {
//                if (StringUtils.isBlank(company)) {
//                    matchingProfiles = searchRepo.findBySignalsContentStemsAndLocation(mainQueryTermStem, location, new PageRequest(0, MAX_PAGE));
//                } else {
//                    matchingProfiles = searchRepo.findBySignalsContentStemsAndLocationAndCompany(mainQueryTermStem, location, company, new PageRequest(0, MAX_PAGE));
//                }
//            } else {
//                if (StringUtils.isBlank(company)) {
//                    matchingProfiles = searchRepo.findBySignalsContentStemsAndLocationAndIndustry(mainQueryTermStem, location, industry, new PageRequest(0, MAX_PAGE));
//                } else {
//                    matchingProfiles = searchRepo.findBySearchCriteria(mainQueryTermStem, location, industry, company, new PageRequest(0, MAX_PAGE));
//                }
//            }
//
//        }
//        return matchingProfiles;
//    }
//
//    private void removeNonRelatedSignals(List<SearchProfile> searchProfiles,
//                                         List<SearchQuery> multipleQuery) {
//        for (SearchProfile searchProfile : searchProfiles) {
//            List<NetworkSignal> relatedSignals = new ArrayList<>();
//            for (NetworkSignal signal : searchProfile.getSignals()) {
//                for (SearchQuery query : multipleQuery) {
//                    if (isRelatedSignal(signal, query)) {
//                        relatedSignals.add(signal);
//                        break;
//                    }
//                }
//            }
//            searchProfile.getSignals().clear();
//            searchProfile.getSignals().addAll(relatedSignals);
//        }
//    }
//
//    private boolean isRelatedSignal(NetworkSignal signal,
//                                    SearchQuery query) {
//        List<String> stems = signal.getContentStems();
//        if (!stems.contains(query.getMainQueryTerm()))
//            return false;
//        for (String relatedStem : query.getLeftoverQueryTerms()) {
//            if (!stems.contains(relatedStem)) {
//                return false;
//            }
//        }
//        return true;
//    }
//
//    private void setScore(List<SearchProfile> searchProfiles,
//                          List<SearchQuery> multipleQuery) {
//
//        String mainQueryTermStem = multipleQuery.get(0).getMainQueryTerm();
//        for (SearchProfile searchProfile : searchProfiles) {
//            Map<String, Feature> vector = signalVectorizer.generateVector(searchProfile.getSignals());
//            StemProfileScore stemProfileScore = new StemProfileScore(mainQueryTermStem, searchProfile.getId(), vector.get(mainQueryTermStem).getScore());
//            searchProfile.setScore(stemProfileScore.getScore());
//        }
//    }
}
