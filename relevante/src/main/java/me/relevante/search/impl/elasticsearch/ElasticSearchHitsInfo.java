package me.relevante.search.impl.elasticsearch;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.List;

/**
 * @author Daniel Ibanez
 */
public class ElasticSearchHitsInfo {

    private long total;
    private double maxScore;
    private List<ElasticSearchHit> hits;

    @JsonCreator
    public ElasticSearchHitsInfo(@JsonProperty("total") final long total,
                                 @JsonProperty("max_score") final double maxScore,
                                 @JsonProperty("hits") final List<ElasticSearchHit> hits) {
        this.total = total;
        this.maxScore = maxScore;
        this.hits = hits;
    }

    public long getTotal() {
        return total;
    }

    public double getMaxScore() {
        return maxScore;
    }

    public List<ElasticSearchHit> getHits() {
        return hits;
    }

}

