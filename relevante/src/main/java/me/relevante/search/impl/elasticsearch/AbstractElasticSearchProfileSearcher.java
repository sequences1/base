package me.relevante.search.impl.elasticsearch;

import com.fasterxml.jackson.databind.ObjectMapper;
import me.relevante.auth.BasicAuthPair;
import me.relevante.core.NetworkSignal;
import me.relevante.nlp.SignalVectorizer;
import me.relevante.nlp.core.Feature;
import me.relevante.nlp.core.NlpCore;
import me.relevante.search.ProfileSearch;
import me.relevante.search.ProfileSearcher;
import me.relevante.search.SearchGoal;
import me.relevante.search.SearchOperator;
import me.relevante.search.SearchProfile;
import me.relevante.util.Chronometer;
import org.apache.commons.io.IOUtils;
import org.apache.http.HttpHeaders;
import org.apache.http.HttpResponse;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.HttpClients;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.nio.charset.UnsupportedCharsetException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

public abstract class AbstractElasticSearchProfileSearcher implements ProfileSearcher {

    private static final Logger LOGGER = LoggerFactory.getLogger(AbstractElasticSearchProfileSearcher.class);
    private final Chronometer chrono;
    private final int defaultMaxProfiles;
    private final BasicAuthPair elasticSearchAuthentication;
    private final String elasticSearchEndpointUrl;
    private final ObjectMapper jsonMapper;
    private final NlpCore nlpCore;
    private final SignalVectorizer signalVectorizer;

    public AbstractElasticSearchProfileSearcher(final NlpCore nlpCore,
                                                final SignalVectorizer signalVectorizer,
                                                final ObjectMapper jsonMapper,
                                                final Chronometer chrono,
                                                final String elasticProfileEndpointUrl,
                                                final String elasticAuthUsername,
                                                final String elasticAuthPassword,
                                                final int defaultMaxProfiles) {
        this.nlpCore = nlpCore;
        this.signalVectorizer = signalVectorizer;
        this.jsonMapper = jsonMapper;
        this.chrono = chrono;
        this.elasticSearchEndpointUrl = elasticProfileEndpointUrl + "/_search";
        this.elasticSearchAuthentication = new BasicAuthPair(elasticAuthUsername, elasticAuthPassword);
        this.defaultMaxProfiles = defaultMaxProfiles;
    }

    @Override
    public List<SearchProfile> search(final ProfileSearch profileSearch) {
        return search(profileSearch, defaultMaxProfiles);
    }

    @Override
    public List<SearchProfile> search(final ProfileSearch profileSearch,
                                      final int maxProfiles) {
        validateSearchParameters(profileSearch.getSearchTerms(), profileSearch.getSearchOperator(), profileSearch.getSearchGoal());
        LOGGER.info("Elapsed time to validate search parameters: " + chrono.getElapsedTimeText());
        final List<String> nlpSearchTerms = composeNlpSearchTerms(profileSearch);
        LOGGER.info("Elapsed time to compose NLP search terms: " + chrono.getElapsedTimeText());
        final List<SearchProfile> candidateProfiles = obtainCandidateProfiles(profileSearch, nlpSearchTerms, maxProfiles);
        LOGGER.info("Elapsed time to obtain candidate profiles: " + chrono.getElapsedTimeText());
        final List<SearchProfile> bestCandidateProfiles = chooseBestProfiles(candidateProfiles, profileSearch, nlpSearchTerms, maxProfiles);
        LOGGER.info("Elapsed time to choose best candidate profiles: " + chrono.getElapsedTimeText());
        return bestCandidateProfiles;
    }

    protected abstract String composeMustExcerpt(final ProfileSearch profileSearch);

    protected abstract String composeMustNotExcerpt(final ProfileSearch profileSearch);

    protected abstract String composeShouldExcerpt(final ProfileSearch profileSearch);

    protected abstract String getQueryTemplate();

    protected List<SearchProfile> obtainCandidateProfiles(final ProfileSearch profileSearch,
                                                          final List<String> nlpSearchTerms,
                                                          final int maxProfiles) {
        final String signalBasedSearchQuery = composeElasticSearchQuery(profileSearch, maxProfiles);
        LOGGER.info("Elapsed time to compose ElasticSearch query: " + chrono.getElapsedTimeText());
        final List<SearchProfile> candidateProfiles = getMatchingProfiles(signalBasedSearchQuery);
        LOGGER.info("Elapsed time to get matching candidate profiles from ElasticSearch: " + chrono.getElapsedTimeText());
        removeNonRelatedSignals(candidateProfiles, nlpSearchTerms);
        LOGGER.info("Elapsed time to remove signals not related to the search: " + chrono.getElapsedTimeText());
        return candidateProfiles;
    }

    private double calculateProfileSearchScore(final SearchProfile searchProfile,
                                               final SearchGoal searchGoal,
                                               final List<String> nlpSearchTerms) {
        final Map<String, Feature> vector = signalVectorizer.generateVector(searchProfile.getSignals(), searchGoal);
        for (final String stem : nlpSearchTerms) {
            if (vector.containsKey(stem)) {
                return vector.get(stem).getScore();
            }
        }
        return 0;
    }

    private List<SearchProfile> chooseBestProfiles(final Collection<SearchProfile> profiles,
                                                   final ProfileSearch profileSearch,
                                                   final List<String> nlpSearchTerms,
                                                   final int maxSize) {

        final List<SearchProfile> scoredProfiles = scoreProfiles(profiles, profileSearch, nlpSearchTerms);
        scoredProfiles.sort((o1, o2) -> new Double(o2.getScore()).compareTo(o1.getScore()));
        return scoredProfiles.subList(0, Math.min(maxSize, profiles.size()));
    }

    private String composeElasticSearchQuery(final ProfileSearch search, final int maxSize) {
        final String mustExcerpt = composeMustExcerpt(search);
        final String shouldExcerpt = composeShouldExcerpt(search);
        final String mustNotExcerpt = composeMustNotExcerpt(search);
        final String query = String.format(getQueryTemplate(), 0, maxSize, mustExcerpt, shouldExcerpt, mustNotExcerpt);
        LOGGER.info("QUERY:\n{}", query);
        return query;
    }

    private List<String> composeNlpSearchTerms(final ProfileSearch profileSearch) {
        final Set<String> nlpSearchTerms = new HashSet<>();
        for (final String searchTerm : profileSearch.getSearchTerms()) {
            final List<String> stems = nlpCore.extractTerms(searchTerm);
            nlpSearchTerms.addAll(stems);
        }
        return new ArrayList<>(nlpSearchTerms);
    }

    private HttpPost createHttpPost(final String query) {

        final HttpPost httpPost = new HttpPost(elasticSearchEndpointUrl);
        httpPost.addHeader(HttpHeaders.CONTENT_TYPE, "application/json; charset=UTF-8");
        httpPost.addHeader(HttpHeaders.AUTHORIZATION, elasticSearchAuthentication.getAuthHeaderValue());
        try {
            httpPost.setEntity(new StringEntity(query, "UTF-8"));
        } catch (UnsupportedCharsetException e) {
            throw new RuntimeException(e);
        }
        return httpPost;
    }

    private ElasticSearchResult executeQuery(final String elasticSearchQuery) {
        final HttpPost queryPost = createHttpPost(elasticSearchQuery);
        try {
            final HttpResponse httpResponse = HttpClients.createDefault().execute(queryPost);
            final String response = IOUtils.toString(httpResponse.getEntity().getContent());
            return parseJsonResponse(response);
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    private List<SearchProfile> getMatchingProfiles(final String elasticSearchQuery) {
        final ElasticSearchResult elasticSearchResult = executeQuery(elasticSearchQuery);
        final List<SearchProfile> searchProfiles = new ArrayList<>(elasticSearchResult.getHitsInfo().getHits().size());
        for (final ElasticSearchHit elasticSearchHit : elasticSearchResult.getHitsInfo().getHits()) {
            final SearchProfile searchProfile = elasticSearchHit.getProfile();
            searchProfile.setId(elasticSearchHit.getId());
            for (final NetworkSignal networkSignal : searchProfile.getSignals()) {
                final List<String> contentStems = nlpCore.extractTerms(networkSignal.getNlpAnalyzableContent());
                networkSignal.getContentStems().addAll(contentStems);
            }
            searchProfiles.add(searchProfile);
        }
        return searchProfiles;
    }

    private boolean isRelatedSignal(final NetworkSignal signal,
                                    final List<String> stems) {
        final Set<String> signalStems = new HashSet<>(signal.getContentStems());
        for (final String stem : stems) {
            if (signalStems.contains(stem)) {
                return true;
            }
        }
        return false;
    }

    private ElasticSearchResult parseJsonResponse(final String jsonResponse) throws IOException {
        return jsonMapper.readValue(jsonResponse, ElasticSearchResult.class);
    }

    private void removeNonRelatedSignals(final List<SearchProfile> searchProfiles,
                                         final List<String> nlpSearchTerms) {
        for (final SearchProfile searchProfile : searchProfiles) {
            final List<NetworkSignal> relatedSignals = new ArrayList<>();
            for (final NetworkSignal signal : searchProfile.getSignals()) {
                if (isRelatedSignal(signal, nlpSearchTerms)) {
                    relatedSignals.add(signal);
                }
            }
            searchProfile.getSignals().clear();
            searchProfile.getSignals().addAll(relatedSignals);
        }
    }

    private List<SearchProfile> scoreProfiles(final Collection<SearchProfile> profiles,
                                              final ProfileSearch profileSearch,
                                              final List<String> nlpSearchTerms) {
        final List<SearchProfile> scoredProfiles = new ArrayList<>(profiles);
        for (final SearchProfile searchProfile : scoredProfiles) {
            final double searchScore = calculateProfileSearchScore(searchProfile, profileSearch.getSearchGoal(), nlpSearchTerms);
            searchProfile.setScore(searchScore);
        }
        return scoredProfiles;
    }

    private void validateSearchParameters(final List<String> searchTerms,
                                          final SearchOperator searchOperator,
                                          final SearchGoal searchGoal) {
        if (searchTerms == null || searchTerms.isEmpty()) {
            throw new IllegalArgumentException("Search terms must be a non-empty list");
        }
        if (searchOperator == null) {
            throw new IllegalArgumentException("Search operator can not be null");
        }
        if (searchGoal == null) {
            throw new IllegalArgumentException("Search goal can not be null");
        }
    }

}
