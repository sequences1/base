package me.relevante.search.impl.elasticsearch;

import com.fasterxml.jackson.databind.ObjectMapper;
import me.relevante.nlp.SignalVectorizer;
import me.relevante.nlp.core.NlpCore;
import me.relevante.search.ProfileSearch;
import me.relevante.search.ProfileSearcher;
import me.relevante.search.SearchOperator;
import me.relevante.util.Chronometer;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;

@Component(value = "elasticSearchRawProfileSearcher")
public class ElasticSearchRawProfileSearcher extends AbstractElasticSearchProfileSearcher implements ProfileSearcher {

    private static final String CRITERIUM_FULL_TEXT = "{\"match\":{\"full_text\": {\"query\":\"%s\",\"operator\":\"and\"}}}";
    private static final String RAW_QUERY = "{\"from\":%s, \"size\":%s, \"_source\":[\"industry\", \"location\", \"company\", \"signals\"], \"query\":{\"bool\":{\"must\":[%s],\"should\":[%s],\"must_not\":[%s]}}}";

    @Autowired
    public ElasticSearchRawProfileSearcher(final NlpCore nlpCore,
                                           final SignalVectorizer signalVectorizer,
                                           final ObjectMapper jsonMapper,
                                           final Chronometer chrono,
                                           final @Value("${search.elasticsearch.endPointUrl}") String elasticProfileEndpointUrl,
                                           final @Value("${search.elasticsearch.authUsername}") String elasticAuthUsername,
                                           final @Value("${search.elasticsearch.authPassword}") String elasticAuthPassword,
                                           final @Value("${search.elasticsearch.resultProfilesMaxSize}") int resultProfilesMaxSize) {
        super(nlpCore, signalVectorizer, jsonMapper, chrono, elasticProfileEndpointUrl, elasticAuthUsername, elasticAuthPassword, resultProfilesMaxSize);
    }

    @Override
    protected String composeMustNotExcerpt(final ProfileSearch profileSearch) {
        final StringBuilder mustNotExcerpt = new StringBuilder();
        for (final String negativeTerm : profileSearch.getNegativeTerms()) {
            if (mustNotExcerpt.length() > 0) {
                mustNotExcerpt.append(", ");
            }
            mustNotExcerpt.append(String.format(CRITERIUM_FULL_TEXT, negativeTerm));
        }
        return mustNotExcerpt.toString();
    }

    @Override
    protected String composeMustExcerpt(final ProfileSearch profileSearch) {
        final List<String> criteria = new ArrayList<>();
        if (profileSearch.getSearchOperator().equals(SearchOperator.AND)) {
            profileSearch.getSearchTerms().forEach(searchTerm -> criteria.add(String.format(CRITERIUM_FULL_TEXT, searchTerm)));
        }
        final StringBuilder mustExcerpt = new StringBuilder();
        for (final String criterium : criteria) {
            if (mustExcerpt.length() > 0) {
                mustExcerpt.append(", ");
            }
            mustExcerpt.append(criterium);
        }
        return mustExcerpt.toString();
    }

    @Override
    protected String composeShouldExcerpt(final ProfileSearch profileSearch) {
        final List<String> criteria = new ArrayList<>();
        if (profileSearch.getSearchOperator().equals(SearchOperator.OR)) {
            profileSearch.getSearchTerms().forEach(searchTerm -> criteria.add(String.format(CRITERIUM_FULL_TEXT, searchTerm)));
        }
        final StringBuilder shouldExcerpt = new StringBuilder();
        for (final String criterium : criteria) {
            if (shouldExcerpt.length() > 0) {
                shouldExcerpt.append(", ");
            }
            shouldExcerpt.append(criterium);
        }
        return shouldExcerpt.toString();
    }

    @Override
    protected String getQueryTemplate() {
        return RAW_QUERY;
    }
}
