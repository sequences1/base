package me.relevante.search;

/**
 * @author daniel-ibanez
 */
public enum SearchOperator {

    AND("and"),
    OR("or");

    private String name;

    SearchOperator(final String name) {
        this.name = name;
    }

    public static SearchOperator fromName(String name) {
        if (name == null) {
            return null;
        }
        for (SearchOperator pricingPlan : SearchOperator.values()) {
            if (pricingPlan.name.equals(name)) {
                return pricingPlan;
            }
        }
        return null;
    }

    public String getName() {
        return name;
    }
}
