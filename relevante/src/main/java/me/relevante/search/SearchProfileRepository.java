package me.relevante.search;

public interface SearchProfileRepository {
    void save(SearchProfile searchProfile);
    SearchProfile get(String uniqueProfileId);
    void remove(String uniqueProfileId);
}
