package me.relevante.search;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import me.relevante.core.NetworkSignal;
import me.relevante.core.UserProfile;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

@JsonIgnoreProperties(value = {"profileId"})
public class SearchProfile implements UserProfile {

    @JsonIgnore
    private String id;
    private String industry;
    private String location;
    private String company;
    @JsonProperty("full_text")
    private String fullText;
    private List<NetworkSignal> signals;

    @JsonIgnore
    private double score;

    public SearchProfile(final String id,
                         final String industry,
                         final String location,
                         final String company,
                         final String fullText,
                         final Collection<NetworkSignal> signals) {
        this();
        this.id = id;
        this.industry = industry;
        this.location = location;
        this.company = company;
        this.fullText = fullText;
        this.signals = new ArrayList<>();
        if (signals != null) {
            this.signals.addAll(signals);
        }
    }

    @JsonCreator
    public SearchProfile(@JsonProperty("industry") final String industry,
                         @JsonProperty("location") final String location,
                         @JsonProperty("company") final String company,
                         @JsonProperty("full_text") final String fullText,
                         @JsonProperty("signals") final Collection<NetworkSignal> signals) {
        this(null, industry, location, company, fullText, signals);
    }

    private SearchProfile() {
        this.signals = new ArrayList<>();
    }

    @Override
    public String getProfileId() {
        return this.id;
    }

    public String getId() {
        return id;
    }

    public void setId(final String id) {
        this.id = id;
    }

    @JsonProperty("industry")
    public String getIndustry() {
        return industry;
    }

    @JsonProperty("location")
    public String getLocation() {
        return location;
    }

    @JsonProperty("company")
    public String getCompany() {
        return company;
    }

    @JsonProperty("full_text")
    public String getFullText() {
        return fullText;
    }

    @JsonProperty("signals")
    public List<NetworkSignal> getSignals() {
        return signals;
    }

    public double getScore() {
        return score;
    }

    public void setScore(double score) {
        this.score = score;
    }
}
