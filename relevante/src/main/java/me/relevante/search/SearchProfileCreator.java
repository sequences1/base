package me.relevante.search;

import me.relevante.core.NetworkProfileId;
import me.relevante.core.NetworkSignal;
import me.relevante.core.UniqueProfile;
import me.relevante.util.MapByNetwork;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Map;

@Component
public class SearchProfileCreator {

    private final CrudRepository<UniqueProfile, String> uniqueProfileRepo;
    private final Map<String, NetworkSearchAttributesLoader> searchAttributesLoaderByNetwork;

    @Autowired
    public SearchProfileCreator(final CrudRepository<UniqueProfile, String> uniqueProfileRepo,
                                final Collection<NetworkSearchAttributesLoader> searchAttributesLoaders) {
        this.uniqueProfileRepo = uniqueProfileRepo;
        this.searchAttributesLoaderByNetwork = MapByNetwork.from(searchAttributesLoaders);
    }

    public SearchProfile create(final String uniqueProfileId) {
        final UniqueProfile uniqueProfile = uniqueProfileRepo.findOne(uniqueProfileId);
        return create(uniqueProfile);
    }

    public SearchProfile create(final UniqueProfile uniqueProfile) {
        final StringBuilder allIndustries = new StringBuilder();
        final StringBuilder allLocations = new StringBuilder();
        final StringBuilder allCompanies = new StringBuilder();
        final StringBuilder fullText = new StringBuilder();
        final List<NetworkSignal> allSignals = new ArrayList<>();
        for (final NetworkProfileId networkProfileId : uniqueProfile.getNetworkProfileIds()) {
            addNetworkSearchAttributes(networkProfileId, allIndustries, allLocations, allCompanies, fullText, allSignals);
        }
        return new SearchProfile(uniqueProfile.getId(), allIndustries.toString(), allLocations.toString(),
                allCompanies.toString(), fullText.toString(), allSignals);
    }

    private void addNetworkSearchAttributes(final NetworkProfileId networkProfileId,
                                            final StringBuilder allIndustries,
                                            final StringBuilder allLocations,
                                            final StringBuilder allCompanies,
                                            final StringBuilder fullText,
                                            final List<NetworkSignal> allSignals) {
        final NetworkSearchAttributesLoader searchAttributesLoader = searchAttributesLoaderByNetwork.get(networkProfileId.getNetwork());
        final SearchAttributes searchAttributes = searchAttributesLoader.loadSearchAttributes(networkProfileId.getProfileId());
        if (StringUtils.isNotBlank(searchAttributes.getIndustry())) {
            allIndustries.append((StringUtils.isBlank(allIndustries) ? "" : ", ") + searchAttributes.getIndustry());
        }
        if (StringUtils.isNotBlank(searchAttributes.getLocation())) {
            allLocations.append((StringUtils.isBlank(allLocations) ? "" : ", ") + searchAttributes.getLocation());
        }
        for (int i = 0; i < searchAttributes.getCompanies().size(); i++) {
            allCompanies.append((StringUtils.isBlank(allCompanies) ? "" : ", ") + searchAttributes.getCompanies().get(i));
        }
        if (StringUtils.isNotBlank(searchAttributes.getFullText())) {
            fullText.append((StringUtils.isBlank(fullText) ? "" : ", ") + searchAttributes.getFullText());
        }
        allSignals.addAll(searchAttributes.getSignals());
    }
}
