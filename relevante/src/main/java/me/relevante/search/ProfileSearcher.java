package me.relevante.search;

import java.util.List;

public interface ProfileSearcher {
    List<SearchProfile> search(final ProfileSearch search);
    List<SearchProfile> search(final ProfileSearch search, final int maxProfiles);
}
