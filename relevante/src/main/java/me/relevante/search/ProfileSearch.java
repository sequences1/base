package me.relevante.search;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * @author Daniel Ibanez
 */
public class ProfileSearch {

    private final List<String> searchTerms;
    private final SearchOperator searchOperator;
    private final List<String> negativeTerms;
    private final String location;
    private final String industry;
    private final String company;
    private final SearchGoal searchGoal;

    public ProfileSearch(final List<String> searchTerms,
                         final String location,
                         final String industry,
                         final String company,
                         final SearchGoal searchGoal) {
        this(searchTerms, SearchOperator.OR, Collections.emptyList(), location, industry, company, searchGoal);
    }

    public ProfileSearch(final List<String> searchTerms,
                         final String location,
                         final String industry,
                         final String company) {
        this(searchTerms, SearchOperator.OR, Collections.emptyList(), location, industry, company, SearchGoal.MISCELLANEA);
    }

    public ProfileSearch(final List<String> searchTerms,
                         final SearchOperator searchOperator,
                         final List<String> negativeTerms,
                         final String location,
                         final String industry,
                         final String company,
                         final SearchGoal searchGoal) {
        this.searchTerms = new ArrayList<>(searchTerms);
        this.searchOperator = searchOperator;
        this.negativeTerms = new ArrayList<>(negativeTerms);
        this.location = location;
        this.industry = industry;
        this.company = company;
        this.searchGoal = searchGoal;
    }

    public List<String> getSearchTerms() {
        return new ArrayList<>(searchTerms);
    }

    public SearchOperator getSearchOperator() {
        return searchOperator;
    }

    public List<String> getNegativeTerms() {
        return new ArrayList<>(negativeTerms);
    }

    public String getLocation() {
        return location;
    }

    public String getIndustry() {
        return industry;
    }

    public String getCompany() {
        return company;
    }

    public SearchGoal getSearchGoal() {
        return searchGoal;
    }
}
