package me.relevante.search;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by daniel-ibanez on 9/05/16.
 */
public class SearchQuery {

    private final String text;
    private final String requiredStem;
    private final List<String> optionalStems;
    private final String location;
    private final String industry;
    private final String company;
    private final SearchGoal searchGoal;

    public SearchQuery(final String text,
                       final String requiredStem,
                       final List<String> optionalStems,
                       final String location,
                       final String industry,
                       final String company,
                       final SearchGoal searchGoal) {
        this.text = text;
        this.requiredStem = requiredStem;
        this.optionalStems = optionalStems;
        this.location = location;
        this.industry = industry;
        this.company = company;
        this.searchGoal = searchGoal;
    }

    public String getText() {
        return text;
    }

    public String getRequiredStem() {
        return requiredStem;
    }

    public List<String> getOptionalStems() {
        return new ArrayList<>(optionalStems);
    }

    public List<String> getAllStems() {
        final List<String> allStems = new ArrayList<>();
        allStems.add(requiredStem);
        allStems.addAll(optionalStems);
        return allStems;
    }

    public String getLocation() {
        return location;
    }

    public String getIndustry() {
        return industry;
    }

    public String getCompany() {
        return company;
    }

    public SearchGoal getSearchGoal() {
        return searchGoal;
    }
}
