package me.relevante.persistence;

import me.relevante.request.ProfileUnificationRequest;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ProfileUnificationRequestRepo extends MongoRepository<ProfileUnificationRequest, String> {
}