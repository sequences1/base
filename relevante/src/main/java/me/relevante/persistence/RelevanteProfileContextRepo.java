package me.relevante.persistence;

import me.relevante.core.RelevanteProfileContext;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

import java.util.Collection;
import java.util.List;

@Repository
public interface RelevanteProfileContextRepo extends MongoRepository<RelevanteProfileContext, String> {
    RelevanteProfileContext findOneByRelevanteIdAndTargetProfileId(String relevanteId, String targetProfileId);
    void deleteByIdIn(Collection<String> relevanteProfileIds);
    List<RelevanteProfileContext> findByTargetProfileId(String targetProfileId);
    List<RelevanteProfileContext> findByRelevanteIdAndTargetProfileIdIn(String relevanteId, Collection<String> targetProfileIds);
    List<RelevanteProfileContext> findByRelevanteIdAndContact(String relevanteId, boolean contact);
    List<RelevanteProfileContext> findByRelevanteIdAndBlacklisted(String relevanteId, boolean blacklisted);
}