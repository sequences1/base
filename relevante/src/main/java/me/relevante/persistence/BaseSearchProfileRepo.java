package me.relevante.persistence;

import me.relevante.search.SearchProfile;
import org.springframework.data.domain.Pageable;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.data.mongodb.repository.Query;
import org.springframework.data.repository.NoRepositoryBean;

import java.util.List;

/**
 * @author daniel-ibanez
 */
@NoRepositoryBean
public interface BaseSearchProfileRepo extends MongoRepository<SearchProfile, String> {
    List<SearchProfile> findBySignalsContentStems(String stem, Pageable pageable);
    @Query("{'signals.contentStems':?0, location:{ $regex: ?1, $options: 'i'}}")
    List<SearchProfile> findBySignalsContentStemsAndLocation(String stem, String location, Pageable pageable);
    @Query("{'signals.contentStems':?0, company:{ $regex: ?1, $options: 'i'}}")
    List<SearchProfile> findBySignalsContentStemsAndCompany(String stem, String company, Pageable pageable);
    @Query("{'signals.contentStems':?0, industry:{ $regex: ?1, $options: 'i'}}")
    List<SearchProfile> findBySignalsContentStemsAndIndustry(String stem, String company, Pageable pageable);
    @Query("{'signals.contentStems':?0, location:{ $regex: ?1, $options: 'i'}, company:{ $regex: ?2, $options: 'i'}}")
    List<SearchProfile> findBySignalsContentStemsAndLocationAndCompany(String stem, String location, String company, Pageable pageable);
    @Query("{'signals.contentStems':?0, location:{ $regex: ?1, $options: 'i'}, industry:{ $regex: ?2, $options: 'i'}}")
    List<SearchProfile> findBySignalsContentStemsAndLocationAndIndustry(String stem, String location, String industry, Pageable pageable);
    @Query("{'signals.contentStems':?0, location:{ $regex: ?1, $options: 'i'}, industry:{ $regex: ?2, $options: 'i'}}")
    List<SearchProfile> findBySignalsContentStemsAndIndustryAndCompany(String stem, String industry, String company, Pageable pageable);
    @Query("{'signals.contentStems':?0, location:{ $regex: ?1, $options: 'i'}, industry:{ $regex: ?2, $options: 'i' }, company:{ $regex: ?3, $options: 'i' }}")
    List<SearchProfile> findBySearchCriteria(String stem, String location, String industry, String company, Pageable pageable);
}
