package me.relevante.core;

import org.springframework.data.mongodb.core.index.CompoundIndex;
import org.springframework.data.mongodb.core.mapping.Document;

/**
 * @author Daniel Ibanez
 */
@Document(collection = "WatchlistFeedback")
@CompoundIndex(unique = true, name = "relevanteId_1", def = "{relevanteId:1}")
public class WatchlistFeedback {

    private final String id;
    private final String relevanteId;
    private final String watchlistId;
    private final String profileId;
    private final double score;

    public WatchlistFeedback(final String id, final String relevanteId, final String watchlistId, final String profileId, final double score) {
        this.id = id;
        this.relevanteId = relevanteId;
        this.watchlistId = watchlistId;
        this.profileId = profileId;
        this.score = score;
    }
}
