package me.relevante.core;

import me.relevante.auth.NetworkCredentials;
import org.apache.commons.lang3.StringUtils;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.index.CompoundIndex;
import org.springframework.data.mongodb.core.mapping.Document;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.Period;
import java.time.ZoneOffset;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Document(collection = "RelevanteAccount")
@CompoundIndex(unique = true, name = "email_1", def = "{email:1}")
public class RelevanteAccount {

    @Id
    private String id;
    private String accountStatus;
    private List<String> actionsTimeRanges;
    private boolean admin;
    private boolean bulkEngageEnabled;
    private List<NetworkCredentials> credentials;
    private String email;
    private double enrichedProfilesOveragePrice;
    private boolean hubEnabled;
    private String imageUrl;
    private String intendedPricingPlan;
    private Date creationTimestamp;
    private Date lastAccessTimestamp;
    private Date deletedTimestamp;
    private long maxMonitoredProfiles;
    private long maxMonitoredWatchlists;
    private long maxMonthlyEnrichedProfiles;
    private long maxTimeWithoutMonitoringInSeconds;
    private long minTimeBetweenActionsInSeconds;
    private String name;
    private String password;
    private String pricingPlan;
    private String stripeCustomerId;
    private String stripeSubscriptionId;
    private Date stripeSubscriptionStartDate;
    private Date trialPeriodEndTimestamp;
    private Date trialPeriodStartTimestamp;

    public RelevanteAccount() {
        this.credentials = new ArrayList<>();
        this.actionsTimeRanges = new ArrayList<>();
    }

    public AccountStatus getAccountStatus() {
        return AccountStatus.fromName(accountStatus);
    }

    public List<String> getActionsTimeRanges() {
        return new ArrayList<>(actionsTimeRanges);
    }

    public List<NetworkCredentials> getAllCredentials() {
        return new ArrayList<>(credentials);
    }

    public <T extends Network> NetworkCredentials<T> getCredentials(final T network) {
        return getCredentials(network.getName());
    }

    public <T extends Network> NetworkCredentials<T> getCredentials(final String networkName) {
        for (final NetworkCredentials credential : credentials) {
            if (credential.getNetwork().getName().equals(networkName)) {
                return credential;
            }
        }
        return null;
    }

    public String getEmail() {
        return email;
    }

    public double getEnrichedProfilesOveragePrice() {
        return enrichedProfilesOveragePrice;
    }

    public String getId() {
        return id;
    }

    public String getImageUrl() {
        return imageUrl;
    }

    public String getIntendedPricingPlan() {
        return intendedPricingPlan;
    }

    public Date getCreationTimestamp() {
        return creationTimestamp;
    }

    public Date getLastAccessTimestamp() {
        return lastAccessTimestamp;
    }

    public LocalDateTime getDeletedTimestamp() {
        return LocalDateTime.ofInstant(deletedTimestamp.toInstant(), ZoneOffset.UTC);
    }

    public long getMaxMonitoredProfiles() {
        return maxMonitoredProfiles;
    }

    public long getMaxMonitoredWatchlists() {
        return maxMonitoredWatchlists;
    }

    public long getMaxMonthlyEnrichedProfiles() {
        return maxMonthlyEnrichedProfiles;
    }

    public long getMaxTimeWithoutMonitoringInSeconds() {
        return maxTimeWithoutMonitoringInSeconds;
    }

    public long getMinTimeBetweenActionsInSeconds() {
        return minTimeBetweenActionsInSeconds;
    }

    public String getName() {
        return name;
    }

    public String getPassword() {
        return password;
    }

    public PricingPlan getPricingPlan() {
        return PricingPlan.fromName(pricingPlan);
    }

    public String getStripeCustomerId() {
        return stripeCustomerId;
    }

    public String getStripeSubscriptionId() {
        return stripeSubscriptionId;
    }

    public LocalDateTime getStripeSubscriptionStartDate() {
        if (stripeSubscriptionStartDate == null) {
            return null;
        }
        return LocalDateTime.ofInstant(stripeSubscriptionStartDate.toInstant(), ZoneOffset.UTC);
    }

    public LocalDateTime getTrialPeriodEndTimestamp() {
        if (trialPeriodEndTimestamp == null) {
            return null;
        }
        return LocalDateTime.ofInstant(trialPeriodEndTimestamp.toInstant(), ZoneOffset.UTC);
    }

    public long getTrialPeriodRemainingDays() {
        return Period.between(LocalDate.now(), getTrialPeriodEndTimestamp().toLocalDate()).getDays();
    }

    public LocalDateTime getTrialPeriodStartTimestamp() {
        if (trialPeriodStartTimestamp == null) {
            return null;
        }
        return LocalDateTime.ofInstant(trialPeriodStartTimestamp.toInstant(), ZoneOffset.UTC);
    }

    public boolean isAdmin() {
        return admin;
    }

    public boolean isActive() {
        return accountStatus.equals(AccountStatus.ACTIVE.getName());
    }

    public boolean isBulkEngageEnabled() {
        return bulkEngageEnabled;
    }

    public boolean isConnectedToAnyNetwork() {
        return (!credentials.isEmpty());
    }

    public boolean isHubEnabled() {
        return hubEnabled;
    }

    public boolean isConnectedToNetwork(final Network network) {
        return (getCredentials(network) != null);
    }

    public boolean isNotConnectedToNetwork(final Network network) {
        return !isConnectedToNetwork(network);
    }

    public boolean isConnectedToNetwork(final String networkName) {
        return (getCredentials(networkName) != null);
    }

    public boolean isNotConnectedToNetwork(final String networkName) {
        return !isConnectedToNetwork(networkName);
    }

    public boolean isOnTrialPeriod() {
        return LocalDateTime.now().isBefore(getTrialPeriodEndTimestamp());
    }


    public boolean isSubscribed() {
        return StringUtils.isNotBlank(stripeSubscriptionId) && StringUtils.isNotBlank(stripeCustomerId);
    }

    public <T extends Network> void putCredentials(final NetworkCredentials<T> networkCredentials) {
        final NetworkCredentials<T> existingCredentials = getCredentials((T) networkCredentials.getNetwork());
        if (existingCredentials == null) {
            this.credentials.add(networkCredentials);
        }
    }

    public void removeCredentials(final Network network) {
        final List<NetworkCredentials> newNetworkCredentials = new ArrayList<>();
        for (NetworkCredentials credential : credentials) {
            if (!credential.getNetwork().equals(network)) {
                newNetworkCredentials.add(credential);
            }
        }
        this.credentials.clear();
        this.credentials.addAll(newNetworkCredentials);
    }

    @Override
    public String toString() {
        return "RelevanteAccount{" +
                "id='" + id + '\'' +
                ", name='" + name + '\'' +
                ", email='" + email + '\'' +
                ", password='" + password + '\'' +
                ", imageUrl='" + imageUrl + '\'' +
                ", stripeCustomerId='" + stripeCustomerId + '\'' +
                ", stripeSubscriptionId='" + stripeSubscriptionId + '\'' +
                ", credentials=" + credentials +
                ", creationTimestamp=" + creationTimestamp +
                ", lastAccessTimestamp=" + lastAccessTimestamp +
                ", trialPeriodStartTimestamp=" + trialPeriodStartTimestamp +
                ", trialPeriodEndTimestamp=" + trialPeriodEndTimestamp +
                ", pricingPlan='" + pricingPlan + '\'' +
                ", accountStatus='" + accountStatus + '\'' +
                ", admin=" + admin +
                ", hubEnabled=" + hubEnabled +
                ", bulkEngageEnabled=" + bulkEngageEnabled +
                ", minTimeBetweenActionsInSeconds=" + minTimeBetweenActionsInSeconds +
                ", maxTimeWithoutMonitoringInSeconds=" + maxTimeWithoutMonitoringInSeconds +
                ", maxMonitoredProfiles=" + maxMonitoredProfiles +
                ", maxMonitoredWatchlists=" + maxMonitoredWatchlists +
                ", maxMonthlyEnrichedProfiles=" + maxMonthlyEnrichedProfiles +
                ", enrichedProfilesOveragePrice=" + enrichedProfilesOveragePrice +
                ", actionsTimeRanges=" + actionsTimeRanges +
                '}';
    }

    public RelevanteAccount withAccountStatus(final AccountStatus accountStatus) {
        this.accountStatus = accountStatus.getName();
        return this;
    }

    public RelevanteAccount withActionsTimeRanges(final List<String> actionsTimeRanges) {
        this.actionsTimeRanges = new ArrayList<>(actionsTimeRanges);
        return this;
    }

    public RelevanteAccount withEmail(String email) {
        this.email = email;
        return this;
    }

    public RelevanteAccount withHubEnabled(boolean hubEnabled) {
        this.hubEnabled = hubEnabled;
        return this;
    }

    public RelevanteAccount withId(String id) {
        this.id = id;
        return this;
    }

    public RelevanteAccount withImageUrl(String imageUrl) {
        this.imageUrl = imageUrl;
        return this;
    }

    public RelevanteAccount withIntendedPricingPlan(final PricingPlan intendedPricingPlan) {
        this.intendedPricingPlan = (intendedPricingPlan == null) ? null : intendedPricingPlan.getName();
        return this;
    }

    public RelevanteAccount withCreationTimestamp(final Date creationTimestamp) {
        this.creationTimestamp = creationTimestamp;
        return this;
    }

    public RelevanteAccount withDeletedTimestamp(final Date deletedTimestamp) {
        this.deletedTimestamp = deletedTimestamp;
        return this;
    }

    public RelevanteAccount withMaxTimeWithoutMonitoringInSeconds(long maxTimeWithoutMonitoringInSeconds) {
        this.maxTimeWithoutMonitoringInSeconds = maxTimeWithoutMonitoringInSeconds;
        return this;
    }

    public RelevanteAccount withMinTimeBetweenActionsInSeconds(long minTimeBetweenActionsInSeconds) {
        this.minTimeBetweenActionsInSeconds = minTimeBetweenActionsInSeconds;
        return this;
    }

    public RelevanteAccount withName(String name) {
        this.name = name;
        return this;
    }

    public RelevanteAccount withPassword(String password) {
        this.password = password;
        return this;
    }

    public RelevanteAccount withPricingPlan(final PricingPlan newPricingPlan) {
        if (newPricingPlan == null) {
            throw new IllegalArgumentException("There must exist a pricing plan");
        }
        final PricingPlan currentPricingPlan = this.getPricingPlan();
        if (currentPricingPlan != null) {
            if (newPricingPlan.getLevel() < currentPricingPlan.getLevel()) {
                throw new IllegalArgumentException("You cannot move to a lower pricing plan");
            }
            if (currentPricingPlan.getInterval().equals(PricingPlan.Interval.YEAR) &&
                    newPricingPlan.getInterval().equals(PricingPlan.Interval.MONTH)) {
                throw new IllegalArgumentException("You cannot move to a pricing plan with a shorter interval");
            }
        }
        this.pricingPlan = newPricingPlan.getName();
        this.bulkEngageEnabled = newPricingPlan.isBulkEngageEnabled();
        this.maxMonitoredProfiles = newPricingPlan.getMaxMonitoredProfiles();
        this.maxMonitoredWatchlists = newPricingPlan.getMaxMonitoredWatchlists();
        this.maxMonthlyEnrichedProfiles = newPricingPlan.getMaxMonthlyEnrichedProfiles();
        this.enrichedProfilesOveragePrice = newPricingPlan.getEnrichedProfilesOveragePrice();
        return this;
    }

    public RelevanteAccount withStripeCustomerId(final String stripeCustomerId) {
        this.stripeCustomerId = stripeCustomerId;
        return this;
    }

    public RelevanteAccount withStripeSubscriptionId(final String stripeSubscriptionId) {
        this.stripeSubscriptionId = stripeSubscriptionId;
        return this;
    }

    public RelevanteAccount withStripeSubscriptionStartDate(final Date stripeSubscriptionStartDate) {
        this.stripeSubscriptionStartDate = stripeSubscriptionStartDate;
        return this;
    }

    public RelevanteAccount withTrialPeriodEndTimestamp(Date trialPeriodEndTimestamp) {
        this.trialPeriodEndTimestamp = trialPeriodEndTimestamp;
        return this;
    }

    public RelevanteAccount withTrialPeriodStartTimestamp(Date trialPeriodStartTimestamp) {
        this.trialPeriodStartTimestamp = trialPeriodStartTimestamp;
        return this;
    }

    public void registerLastAccess() {
        this.lastAccessTimestamp = new Date();
    }


}