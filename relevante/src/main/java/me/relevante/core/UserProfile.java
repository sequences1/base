package me.relevante.core;

/**
 * @author daniel-ibanez
 */
public interface UserProfile {
    String getProfileId();
}
