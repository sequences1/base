package me.relevante.core;

import java.util.Date;

public class WatchlistProfile implements UserProfile, Monitorable, Cloneable<WatchlistProfile> {

    private String profileId;
    private Date creationTimestamp;
    private Date lastMonitoringTimestamp;

    private WatchlistProfile() {
        this(null);
    }

    public WatchlistProfile(final String profileId) {
        this.profileId = profileId;
        this.creationTimestamp = new Date();
    }

    @Override
    public String getProfileId() {
        return profileId;
    }

    public Date getCreationTimestamp() {
        return creationTimestamp;
    }

    @Override
    public Date getLastMonitoringTimestamp() {
        return lastMonitoringTimestamp;
    }

    public void setLastMonitoringTimestamp(Date lastMonitoringTimestamp) {
        this.lastMonitoringTimestamp = lastMonitoringTimestamp;
    }

    @Override
    public WatchlistProfile clone() {
        final WatchlistProfile cloned = new WatchlistProfile();
        cloned.profileId = this.profileId;
        cloned.creationTimestamp = this.creationTimestamp;
        cloned.lastMonitoringTimestamp = this.lastMonitoringTimestamp;
        return cloned;
    }
}
