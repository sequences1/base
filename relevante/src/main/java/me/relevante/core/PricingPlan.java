package me.relevante.core;

/**
 * @author daniel-ibanez
 */
public enum PricingPlan {

    FREE_TIER("free", 0, false, 3, 100, 0, 0.20D, null),
    TIER_1("level1", 1, false, 10, 500, 0, 0.20D, Interval.MONTH),
    TIER_2("level2", 2, true, 10000, 3000, 100, 0.10D, Interval.MONTH),
    TIER_3("level3", 3, true, 10000, 10000, 500, 0.10D, Interval.MONTH),
    TIER_1_YEARLY("level1yearly", 1, false, 10, 500, 0, 0.20D, Interval.YEAR),
    TIER_2_YEARLY("level2yearly", 2, true, 10000, 3000, 100, 0.10D, Interval.YEAR),
    TIER_3_YEARLY("level3yearly", 3, true, 10000, 10000, 500, 0.10D, Interval.YEAR);

    public enum Interval {
        MONTH,
        YEAR
    }

    private String name;
    private int level;
    private boolean bulkEngageEnabled;
    private long maxMonitoredWatchlists;
    private long maxMonitoredProfiles;
    private long maxMonthlyEnrichedProfiles;
    private double enrichedProfilesOveragePrice;
    private Interval interval;

    PricingPlan(final String name,
                final int level,
                final boolean bulkEngageEnabled,
                final long maxMonitoredWatchlists,
                final long maxMonitoredProfiles,
                final long maxMonthlyEnrichedProfiles,
                final double enrichedProfilesOveragePrice,
                final Interval interval) {
        this.name = name;
        this.level = level;
        this.bulkEngageEnabled = bulkEngageEnabled;
        this.maxMonitoredWatchlists = maxMonitoredWatchlists;
        this.maxMonitoredProfiles = maxMonitoredProfiles;
        this.maxMonthlyEnrichedProfiles = maxMonthlyEnrichedProfiles;
        this.enrichedProfilesOveragePrice = enrichedProfilesOveragePrice;
        this.interval = interval;
    }

    public static PricingPlan fromName(String name) {
        if (name == null) {
            return null;
        }
        for (PricingPlan pricingPlan : PricingPlan.values()) {
            if (pricingPlan.name.equals(name)) {
                return pricingPlan;
            }
        }
        return null;
    }

    public String getName() {
        return name;
    }

    public int getLevel() {
        return level;
    }

    public long getMaxMonitoredWatchlists() {
        return maxMonitoredWatchlists;
    }

    public long getMaxMonitoredProfiles() {
        return maxMonitoredProfiles;
    }

    public long getMaxMonthlyEnrichedProfiles() {
        return maxMonthlyEnrichedProfiles;
    }

    public double getEnrichedProfilesOveragePrice() {
        return enrichedProfilesOveragePrice;
    }

    public boolean isBulkEngageEnabled() {
        return bulkEngageEnabled;
    }

    public Interval getInterval() {
        return interval;
    }
}
