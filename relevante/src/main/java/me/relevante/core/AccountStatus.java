package me.relevante.core;

/**
 * @author daniel-ibanez
 */
public enum AccountStatus {

    ACTIVE("active"),
    DELETED("deleted");

    private String name;

    AccountStatus(final String name) {
        this.name = name;
    }

    public static AccountStatus fromName(String name) {
        if (name == null) {
            return null;
        }
        for (AccountStatus pricingPlan : AccountStatus.values()) {
            if (pricingPlan.name.equals(name)) {
                return pricingPlan;
            }
        }
        return null;
    }

    public String getName() {
        return name;
    }

}
