package me.relevante.core;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;

@Document(collection = "RelevanteContext")
public class RelevanteContext implements Monitorable {

    @Id
    private String relevanteId;
    private Map<String, Watchlist> watchlistById;
    private Map<String, Search> searchById;
    private List<ProfileTag> profileTags;
    private Date lastActionDate;
    private Date lastMonitoringTimestamp;

    private RelevanteContext() {
        this(null);
    }

    public RelevanteContext(final String relevanteId) {
        this.relevanteId = relevanteId;
        this.watchlistById = new HashMap<>();
        this.searchById = new HashMap<>();
        this.profileTags = new ArrayList<>();
    }

    public String getRelevanteId() {
        return relevanteId;
    }

    public String addProfileTag(final String profileId,
                                final String tag) {
        for (final ProfileTag profileTag : profileTags) {
            if (profileTag.getProfileId().equals(profileId) && profileTag.getTag().equals(tag)) {
                return null;
            }
        }
        final ProfileTag profileTagToAdd = new ProfileTag(profileId, tag);
        profileTags.add(profileTagToAdd);
        return tag;
    }

    public void addWatchlist(final Watchlist watchlist) {
        watchlistById.put(watchlist.getId(), watchlist);
    }

    public void addSearch(final Search search) {
        searchById.put(search.getId(), search);
    }

    public List<String> findProfileWatchlistNames(final String profileId) {
        return findWatchlistsByProfileId(profileId).stream().map(Watchlist::getName).collect(Collectors.toList());
    }

    public List<String> findTagsByProfileId(final String profileId) {
        final List<String> tagNames = new ArrayList<>();
        for (final ProfileTag profileTag : profileTags) {
            if (profileTag.getProfileId().equals(profileId)) {
                tagNames.add(profileTag.getTag());
            }
        }
        return tagNames;
    }

    public Watchlist getWatchlist(final String watchlistId) {
        return watchlistById.get(watchlistId);
    }

    public Watchlist findWatchlistByName(final String watchlistName) {
        for (final Watchlist watchlist : watchlistById.values()) {
            if (watchlist.getName().equals(watchlistName)) {
                return watchlist;
            }
        }
        return null;
    }

    public Search getSearch(final String searchId) {
        return searchById.get(searchId);
    }

    public Search findSearchByName(final String searchName) {
        for (final Search search : searchById.values()) {
            if (search.getName().equals(searchName)) {
                return search;
            }
        }
        return null;
    }

    public List<Watchlist> findWatchlistsByProfileId(final String profileId) {
        final List<Watchlist> profileWatchlists = new ArrayList<>();
        for (final Watchlist watchlist : watchlistById.values()) {
            if (watchlist.containsProfile(profileId)) {
                profileWatchlists.add(watchlist);
            }
        }
        return profileWatchlists;
    }

    public List<WatchlistProfile> findWatchlistProfilesByProfileId(final String profileId) {
        final List<WatchlistProfile> watchlistProfiles = new ArrayList<>();
        for (final Watchlist watchlist : watchlistById.values()) {
            final WatchlistProfile watchlistProfile = watchlist.findWatchlistProfile(profileId);
            if (watchlistProfile != null) {
                watchlistProfiles.add(watchlistProfile);
            }
        }
        return watchlistProfiles;
    }

    public List<String> getAllTags() {
        final Set<String> uniqueTags = new HashSet<>();
        for (final ProfileTag profileTag : profileTags) {
            uniqueTags.add(profileTag.getTag());
        }
        return new ArrayList<>(uniqueTags);
    }

    public Set<String> getAllWatchlistsProfileIds() {
        final Set<String> uniqueWatchlistProfiles = new HashSet<>();
        for (final Watchlist watchlist : watchlistById.values()) {
            for (final WatchlistProfile watchlistProfile : watchlist.getProfiles()) {
                uniqueWatchlistProfiles.add(watchlistProfile.getProfileId());
            }
        }
        return uniqueWatchlistProfiles;
    }

    public List<WatchlistProfile> getAllWatchlistsProfiles() {
        final List<WatchlistProfile> watchlistProfiles = new ArrayList<>();
        for (final Watchlist watchlist : watchlistById.values()) {
            for (final WatchlistProfile watchlistProfile : watchlist.getProfiles()) {
                watchlistProfiles.add(watchlistProfile);
            }
        }
        return watchlistProfiles;
    }

    public List<Watchlist> getWatchlists() {
        return new ArrayList<>(watchlistById.values());
    }

    public List<Search> getSearches() {
        return new ArrayList<>(searchById.values());
    }

    public boolean removeProfileFromAllWatchlists(final String profileId) {
        boolean removed = false;
        for (final Watchlist watchlist : watchlistById.values()) {
            if (watchlist.removeProfile(profileId)) {
                removed = true;
            }
        }
        return removed;
    }

    public String removeProfileTag(final String profileId,
                                   final String tag) {
        for (int i = 0; i < profileTags.size(); i++) {
            final ProfileTag profileTag = profileTags.get(i);
            if (profileTag.getProfileId().equals(profileId) && profileTag.getTag().equals(tag)) {
                profileTags.remove(i);
                return profileTag.getTag();
            }
        }
        return null;
    }

    public void removeWatchlist(final String watchlistId) {
        watchlistById.remove(watchlistId);
    }

    public void removeSearch(final String searchId) {
        searchById.remove(searchId);
    }

    public Date getLastActionDate() {
        return lastActionDate;
    }

    public void setLastActionDate(Date lastActionDate) {
        this.lastActionDate = lastActionDate;
    }

    @Override
    public Date getLastMonitoringTimestamp() {
        return lastMonitoringTimestamp;
    }

    public void setLastMonitoringTimestamp(Date lastMonitoringTimestamp) {
        this.lastMonitoringTimestamp = lastMonitoringTimestamp;
    }
}
