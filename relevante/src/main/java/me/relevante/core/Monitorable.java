package me.relevante.core;

import java.util.Date;

/**
 * @author daniel-ibanez
 */
public interface Monitorable {
    Date getLastMonitoringTimestamp();
}
