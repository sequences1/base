package me.relevante.core;

import me.relevante.search.SearchGoal;

import java.time.LocalDateTime;
import java.time.ZoneOffset;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Date;
import java.util.List;
import java.util.UUID;

public class Search implements Monitorable {

    private String id;
    private String name;
    private List<String> searchTerms;
    private List<String> negativeTerms;
    private String searchCondition;
    private SearchGoal searchGoal;
    private String company;
    private String industry;
    private String location;
    private Date lastMonitoringTimestamp;

    private Search() {
        this(null, Collections.emptyList(), Collections.emptyList(), null, null, null, null, null);
    }

    public Search(final String name,
                  final Collection<String> searchTerms,
                  final Collection<String> negativeTerms,
                  final String searchCondition,
                  final SearchGoal searchGoal,
                  final String location,
                  final String industry,
                  final String company) {
        this.id = UUID.randomUUID().toString();
        this.name = name;
        this.searchTerms = new ArrayList<>(searchTerms);
        this.negativeTerms = new ArrayList<>(negativeTerms);
        this.searchGoal = searchGoal == null ? SearchGoal.MISCELLANEA : searchGoal;
        this.searchCondition = searchCondition;
        this.location = location;
        this.industry = industry;
        this.company = company;
        final Date aYearAgo = Date.from(LocalDateTime.now().minusYears(1).toInstant(ZoneOffset.UTC));
        this.lastMonitoringTimestamp = aYearAgo;
    }

    public String getCompany() {
        return company;
    }

    public String getId() {
        return id;
    }

    public String getIndustry() {
        return industry;
    }

    public String getLocation() {
        return location;
    }

    public String getName() {
        return name;
    }

    public List<String> getNegativeTerms() {
        return new ArrayList<>(negativeTerms);
    }

    public String getSearchCondition() {
        return searchCondition;
    }

    public SearchGoal getSearchGoal() {
        return searchGoal;
    }

    public List<String> getSearchTerms() {
        return new ArrayList<>(searchTerms);
    }

    public void setCompany(String company) {
        this.company = company;
    }

    public void setIndustry(String industry) {
        this.industry = industry;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    public void setName(final String name) {
        this.name = name;
    }

    public void setSearchCondition(final String searchCondition) {
        this.searchCondition = searchCondition;
    }

    public void setSearchGoal(final SearchGoal searchGoal) {
        this.searchGoal = searchGoal;
    }

    public void setSearchTerms(final List<String> searchTerms) {
        this.searchTerms = new ArrayList<>(searchTerms);
    }

    public void setNegativeTerms(final List<String> negativeTerms) {
        this.negativeTerms = new ArrayList<>(negativeTerms);
    }

    @Override
    public Date getLastMonitoringTimestamp() {
        return lastMonitoringTimestamp;
    }

    public void setLastMonitoringTimestamp(final Date lastMonitoringTimestamp) {
        this.lastMonitoringTimestamp = lastMonitoringTimestamp;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        Search watchlist = (Search) o;
        return !(name != null ? !name.equals(watchlist.name) : watchlist.name != null);

    }

    @Override
    public int hashCode() {
        int result = name != null ? name.hashCode() : 0;
        return result;
    }

}
