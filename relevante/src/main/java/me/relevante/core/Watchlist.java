package me.relevante.core;

import java.time.LocalDateTime;
import java.time.ZoneOffset;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;
import java.util.stream.Collectors;

public class Watchlist implements Monitorable {

    private String id;
    private String name;
    private Map<String, WatchlistProfile> profileById;
    private Date lastMonitoringTimestamp;

    private Watchlist() {
        this(null);
    }

    public Watchlist(final String name) {
        this.id = UUID.randomUUID().toString();
        this.name = name;
        this.profileById = new HashMap<>();
        final Date aYearAgo = Date.from(LocalDateTime.now().minusYears(1).toInstant(ZoneOffset.UTC));
        this.lastMonitoringTimestamp = aYearAgo;
    }

    public String getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public void setName(final String name) {
        this.name = name;
    }

    public List<String> getProfileIds() {
        return profileById.values().stream().map(WatchlistProfile::getProfileId).collect(Collectors.toList());
    }

    public List<WatchlistProfile> getProfiles() {
        return new ArrayList<>(profileById.values());
    }

    public boolean addProfile(final String profileId) {
        if (containsProfile(profileId)) {
            return false;
        }
        profileById.put(profileId, new WatchlistProfile(profileId));
        return true;
    }

    public boolean containsProfile(final String profileId) {
        return profileById.containsKey(profileId);
    }

    public WatchlistProfile findWatchlistProfile(final String profileId) {
        return profileById.get(profileId);
    }

    public boolean removeProfile(final String profileId) {
        if (!containsProfile(profileId)) {
            return false;
        }
        profileById.remove(profileId);
        return true;
    }

    @Override
    public Date getLastMonitoringTimestamp() {
        return lastMonitoringTimestamp;
    }

    public void setLastMonitoringTimestamp(Date lastMonitoringTimestamp) {
        this.lastMonitoringTimestamp = lastMonitoringTimestamp;
    }
}
