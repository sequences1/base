/**
 * Copyright 2016 Pentasoft Sistemas SL.
 */
package me.relevante.core;

import com.fasterxml.jackson.annotation.JsonSubTypes;
import com.fasterxml.jackson.annotation.JsonTypeInfo;

/**
 * @author Juan Carlos González
 *
 */
@JsonTypeInfo(use = JsonTypeInfo.Id.NAME, include = JsonTypeInfo.As.PROPERTY, property = "signalClass")
@JsonSubTypes({
        @JsonSubTypes.Type(value = LinkedinSignalBioDescription.class, name = "LinkedinSignalBioDescription"),
        @JsonSubTypes.Type(value = LinkedinSignalBioHeadline.class, name = "LinkedinSignalBioHeadline"),
        @JsonSubTypes.Type(value = LinkedinSignalCurrentPosition.class, name = "LinkedinSignalCurrentPosition"),
        @JsonSubTypes.Type(value = LinkedinSignalGroupMembership.class, name = "LinkedinSignalGroupMembership"),
        @JsonSubTypes.Type(value = LinkedinSignalPosition.class, name = "LinkedinSignalPosition"),
        @JsonSubTypes.Type(value = LinkedinSignalPostAuthorship.class, name = "LinkedinSignalPostAuthorship"),
        @JsonSubTypes.Type(value = LinkedinSignalPostComment.class, name = "LinkedinSignalPostComment"),
        @JsonSubTypes.Type(value = LinkedinSignalPostLike.class, name = "LinkedinSignalPostLike"),
        @JsonSubTypes.Type(value = LinkedinSignalShareAuthorship.class, name = "LinkedinSignalShareAuthorship"),
        @JsonSubTypes.Type(value = LinkedinSignalSkill.class, name = "LinkedinSignalSkill"),
        @JsonSubTypes.Type(value = TwitterSignalBioDescription.class, name = "TwitterSignalBioDescription"),
        @JsonSubTypes.Type(value = TwitterSignalDescription.class, name = "TwitterSignalDescription"),
        @JsonSubTypes.Type(value = TwitterSignalPostAuthorship.class, name = "TwitterSignalPostAuthorship"),
        @JsonSubTypes.Type(value = TwitterSignalPostFav.class, name = "TwitterSignalPostFav"),
        @JsonSubTypes.Type(value = TwitterSignalPostReply.class, name = "TwitterSignalPostReply"),
        @JsonSubTypes.Type(value = TwitterSignalPostRetweet.class, name = "TwitterSignalPostRetweet")})
public abstract class PolymorphicalSignalMixIn { }
