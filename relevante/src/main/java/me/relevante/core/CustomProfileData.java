package me.relevante.core;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

public class CustomProfileData implements Cloneable<CustomProfileData> {

    private String name;
    private String headline;
    private String imageUrl;
    private String location;
    private List<String> emails;
    private List<String> phoneNumbers;
    private String notes;

    public CustomProfileData() {
        this.emails = new ArrayList<>();
        this.phoneNumbers = new ArrayList<>();
    }

    public String getName() {
        return name;
    }

    public void setName(final String name) {
        this.name = name;
    }

    public String getHeadline() {
        return headline;
    }

    public void setHeadline(final String headline) {
        this.headline = headline;
    }

    public String getImageUrl() {
        return imageUrl;
    }

    public void setImageUrl(final String imageUrl) {
        this.imageUrl = imageUrl;
    }

    public String getLocation() {
        return location;
    }

    public void setLocation(final String location) {
        this.location = location;
    }

    public List<String> getEmails() {
        return new ArrayList<>(emails);
    }

    public void setEmails(final Collection<String> emails) {
        this.emails.clear();
        this.emails.addAll(emails);
    }

    public List<String> getPhoneNumbers() {
        return new ArrayList<>(phoneNumbers);
    }

    public void setPhoneNumbers(final Collection<String> phoneNumbers) {
        this.phoneNumbers.clear();
        this.phoneNumbers.addAll(phoneNumbers);
    }

    public String getNotes() {
        return notes;
    }

    public void setNotes(final String notes) {
        this.notes = notes;
    }

    @Override
    public CustomProfileData clone() {
        final CustomProfileData cloned = new CustomProfileData();
        cloned.name = this.name;
        cloned.headline = this.headline;
        cloned.imageUrl = this.imageUrl;
        cloned.location = this.location;
        cloned.emails = new ArrayList<>(this.emails);
        cloned.phoneNumbers = new ArrayList<>(this.phoneNumbers);
        cloned.notes = this.notes;
        return cloned;
    }

    @Override
    public String toString() {
        return "CustomProfileData{" +
                "name='" + name + '\'' +
                ", headline='" + headline + '\'' +
                ", imageUrl='" + imageUrl + '\'' +
                ", location='" + location + '\'' +
                ", emails=" + emails +
                ", phoneNumbers=" + phoneNumbers +
                ", notes='" + notes + '\'' +
                '}';
    }
}
