package me.relevante.core;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.index.CompoundIndex;
import org.springframework.data.mongodb.core.index.CompoundIndexes;
import org.springframework.data.mongodb.core.mapping.Document;

import java.time.LocalDate;

@Document(collection = "MonthToPay")
@CompoundIndexes({
        @CompoundIndex(name = "only_one_payment_per_user_year_and_month", def = "{'relevanteId' : 1, 'year': 1, 'month': 1}", unique = true)
})
public class MonthToPay {

    @Id
    private String id;

    private Integer month;

    private Integer year;

    private String relevanteId;

    private String chargeId;

    private Integer amountToCharge;

    public MonthToPay() {}

    public MonthToPay(LocalDate date, String relevanteId, Integer amountToCharge) {
        this.month = date.getMonthValue();
        this.relevanteId = relevanteId;
        this.amountToCharge = amountToCharge;
        this.year = date.getYear();
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public Integer getMonth() {
        return month;
    }

    public void setMonth(Integer month) {
        this.month = month;
    }

    public String getRelevanteId() {
        return relevanteId;
    }

    public void setRelevanteId(String relevanteId) {
        this.relevanteId = relevanteId;
    }

    public String getChargeId() {
        return chargeId;
    }

    public void setChargeId(String chargeId) {
        this.chargeId = chargeId;
    }

    public Integer getAmountToCharge() {
        return amountToCharge;
    }

    public void setAmountToCharge(Integer amountToCharge) {
        this.amountToCharge = amountToCharge;
    }

    public Integer getYear() {
        return year;
    }

    public void setYear(Integer year) {
        this.year = year;
    }
}
