package me.relevante.core;

import java.util.Objects;

public class ProfileTag {

	private String profileId;
	private String tag;

	private ProfileTag() {
        this(null, null);
    }

    public ProfileTag(final String profileId,
                      final String tag) {
        this.profileId = profileId;
        this.tag = tag;
    }

    public String getProfileId() {
        return profileId;
    }

    public String getTag() {
        return tag;
    }

    @Override
    public boolean equals(final Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof ProfileTag)) {
            return false;
        }
        final ProfileTag that = (ProfileTag) o;
        return Objects.equals(profileId, that.profileId) &&
                Objects.equals(tag, that.tag);
    }

    @Override
    public int hashCode() {
        return Objects.hash(profileId, tag);
    }
}
