package me.relevante.core;

import me.relevante.request.ActionEngageStatus;
import me.relevante.request.NetworkEngageStatus;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.index.CompoundIndex;
import org.springframework.data.mongodb.core.index.CompoundIndexes;
import org.springframework.data.mongodb.core.mapping.Document;

import java.time.LocalDateTime;
import java.time.ZoneOffset;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;

@Document(collection = "RelevanteProfileContext")
@CompoundIndexes({@CompoundIndex(unique = true, name = "relevanteId_1_targetProfileId_1", def = "{'relevanteId' : 1, 'targetProfileId' : 1}"),
                         @CompoundIndex(unique = false, name = "targetProfileId_1", def = "{'targetProfileId' : 1}"),
                         @CompoundIndex(unique = false, name = "relevanteId_1_contact_1", def = "{'relevanteId' : 1, 'contact' : 1}"),
                         @CompoundIndex(unique = false, name = "relevanteId_1_blacklisted_1", def = "{'relevanteId' : 1, 'blacklisted' : 1}")})
public class RelevanteProfileContext implements UserProfile, Monitorable, Cloneable<RelevanteProfileContext> {

    @Id
    private String id;
    private String relevanteId;
    private String targetProfileId;
    private boolean level1Requested;
    private boolean level2Requested;
    private boolean level3Requested;
    private boolean level4Requested;
    private boolean contact;
    private boolean blacklisted;
    private ActionEngageStatus enrichmentStatus;
    private Map<String, NetworkEngageStatus> engageStatusByNetwork;
    private CustomProfileData data;
    private Date lastMonitoringTimestamp;

    private RelevanteProfileContext() {
        this(null, null);
    }

    public RelevanteProfileContext(final String relevanteId,
                                   final String uniqueProfileId) {
        this.id = UUID.randomUUID().toString();
        this.relevanteId = relevanteId;
        this.targetProfileId = uniqueProfileId;
        this.engageStatusByNetwork = new HashMap<>();
        final Date aYearAgo = Date.from(LocalDateTime.now().minusYears(1).toInstant(ZoneOffset.UTC));
        this.lastMonitoringTimestamp = aYearAgo;
    }

    @Override
    public String getProfileId() {
        return targetProfileId;
    }

    public String getId() {
        return id;
    }

    public String getTargetProfileId() {
        return targetProfileId;
    }

    public String getRelevanteId() {
        return relevanteId;
    }

    public CustomProfileData getData() {
        return data;
    }

    public void setData(final CustomProfileData data) {
        this.data = data;
    }

    public boolean isLevel1Requested() {
        return level1Requested;
    }

    public void setLevel1Requested(final boolean level1Requested) {
        this.level1Requested = level1Requested;
    }

    public boolean isLevel2Requested() {
        return level2Requested;
    }

    public void setLevel2Requested(final boolean level2Requested) {
        this.level2Requested = level2Requested;
    }

    public boolean isLevel3Requested() {
        return level3Requested;
    }

    public void setLevel3Requested(final boolean level3Requested) {
        this.level3Requested = level3Requested;
    }

    public boolean isLevel4Requested() {
        return level4Requested;
    }

    public void setLevel4Requested(final boolean level4Requested) {
        this.level4Requested = level4Requested;
    }

    public boolean isContact() {
        return contact;
    }

    public void setContact(final boolean contact) {
        this.contact = contact;
    }

    public boolean isBlacklisted() {
        return blacklisted;
    }

    public void setBlacklisted(final boolean blacklisted) {
        this.blacklisted = blacklisted;
    }

    public ActionEngageStatus getEnrichmentStatus() {
        return enrichmentStatus;
    }

    public void setEnrichmentStatus(final ActionEngageStatus enrichmentStatus) {
        this.enrichmentStatus = enrichmentStatus;
    }

    public List<NetworkEngageStatus> getEngageStatuses() {
        return new ArrayList<>(engageStatusByNetwork.values());
    }

    public <N extends Network, E extends NetworkEngageStatus<N, E>> E getEngageStatus(final N network) {
        return (E) engageStatusByNetwork.get(network.getName());
    }

    public <E extends NetworkEngageStatus> E getEngageStatus(final String networkName) {
        return (E) engageStatusByNetwork.get(networkName);
    }

    @Override
    public RelevanteProfileContext clone() {
        final RelevanteProfileContext cloned = new RelevanteProfileContext();
        cloned.id = this.id;
        cloned.relevanteId = this.relevanteId;
        cloned.targetProfileId = this.targetProfileId;
        cloned.level1Requested = this.level1Requested;
        cloned.level2Requested = this.level2Requested;
        cloned.level3Requested = this.level3Requested;
        cloned.level4Requested = this.level4Requested;
        cloned.contact = this.contact;
        cloned.blacklisted = this.blacklisted;
        cloned.enrichmentStatus = this.enrichmentStatus;
        cloned.engageStatusByNetwork = new HashMap<>(this.engageStatusByNetwork);
        cloned.data = this.data;
        cloned.lastMonitoringTimestamp = this.lastMonitoringTimestamp;
        return cloned;
    }

    public void setEngageStatus(final NetworkEngageStatus networkEngageStatus) {
        this.engageStatusByNetwork.put(networkEngageStatus.getNetwork().getName(), networkEngageStatus);
    }

    public void resetEngageData() {
        this.level1Requested = false;
        this.level2Requested = false;
        this.level3Requested = false;
        this.level4Requested = false;
        this.engageStatusByNetwork = new HashMap<>();
    }

    @Override
    public Date getLastMonitoringTimestamp() {
        return lastMonitoringTimestamp;
    }

    public void setLastMonitoringTimestamp(final Date lastMonitoringTimestamp) {
        this.lastMonitoringTimestamp = lastMonitoringTimestamp;
    }

    @Override
    public String toString() {
        return "RelevanteProfileContext{" +
                "id='" + id + '\'' +
                ", relevanteId='" + relevanteId + '\'' +
                ", targetProfileId='" + targetProfileId + '\'' +
                ", level1Requested=" + level1Requested +
                ", level2Requested=" + level2Requested +
                ", level3Requested=" + level3Requested +
                ", level4Requested=" + level4Requested +
                ", contact=" + contact +
                ", blacklisted=" + blacklisted +
                ", enrichmentStatus=" + enrichmentStatus +
                ", engageStatusByNetwork=" + engageStatusByNetwork +
                ", data=" + data +
                ", lastMonitoringTimestamp=" + lastMonitoringTimestamp +
                '}';
    }
}
