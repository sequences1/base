package me.relevante.service;

import me.relevante.core.CustomProfileData;
import me.relevante.core.RelevanteProfileContext;
import me.relevante.request.ActionEngageStatus;
import me.relevante.request.NetworkEngageStatus;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Collection;
import java.util.Date;

/**
 * @author Daniel Ibanez
 */
@Service
public class RelevanteProfileContextMerger
        extends AbstractMerger<RelevanteProfileContext>
        implements Merger<RelevanteProfileContext> {

    private final Collection<NetworkEngageStatusMerger> networkEngageStatusMergers;
    private final CustomProfileDataMerger customProfileDataMerger;
    private final ActionEngageStatusMerger actionEngageStatusMerger;
    private final LastMonitoringTimestampMerger lastMonitoringTimestampMerger;

    @Autowired
    public RelevanteProfileContextMerger(final Collection<NetworkEngageStatusMerger> networkEngageStatusMergers,
                                         final CustomProfileDataMerger customProfileDataMerger,
                                         final ActionEngageStatusMerger actionEngageStatusMerger,
                                         final LastMonitoringTimestampMerger lastMonitoringTimestampMerger) {
        this.networkEngageStatusMergers = networkEngageStatusMergers;
        this.customProfileDataMerger = customProfileDataMerger;
        this.actionEngageStatusMerger = actionEngageStatusMerger;
        this.lastMonitoringTimestampMerger = lastMonitoringTimestampMerger;
    }

    @Override
    public RelevanteProfileContext mergeNotNullObjects(final RelevanteProfileContext targetProfileContext,
                                                       final RelevanteProfileContext sourceProfileContext) {

        final RelevanteProfileContext mergedProfileContext = targetProfileContext.clone();

        mergedProfileContext.setLevel1Requested(mergedProfileContext.isLevel1Requested() || sourceProfileContext.isLevel1Requested());
        mergedProfileContext.setLevel2Requested(mergedProfileContext.isLevel2Requested() || sourceProfileContext.isLevel2Requested());
        mergedProfileContext.setLevel3Requested(mergedProfileContext.isLevel3Requested() || sourceProfileContext.isLevel3Requested());
        mergedProfileContext.setLevel4Requested(mergedProfileContext.isLevel4Requested() || sourceProfileContext.isLevel4Requested());
        mergedProfileContext.setBlacklisted(mergedProfileContext.isBlacklisted() || sourceProfileContext.isBlacklisted());
        mergedProfileContext.setContact(mergedProfileContext.isContact() || sourceProfileContext.isContact());

        assignEnrichmentStatus(mergedProfileContext, sourceProfileContext);
        assignNetworkEngageStatus(mergedProfileContext, sourceProfileContext);
        assignLastMonitoringTimestamp(mergedProfileContext, sourceProfileContext);
        assignCustomData(mergedProfileContext, sourceProfileContext);

        return mergedProfileContext;
    }

    private void assignNetworkEngageStatus(final RelevanteProfileContext mergedProfileContext,
                                           final RelevanteProfileContext sourceProfileContext) {
        for (final NetworkEngageStatusMerger networkEngageStatusMerger : networkEngageStatusMergers) {
            final NetworkEngageStatus engageStatusProfile1 = mergedProfileContext.getEngageStatus(networkEngageStatusMerger.getNetwork());
            final NetworkEngageStatus engageStatusProfile2 = sourceProfileContext.getEngageStatus(networkEngageStatusMerger.getNetwork());
            final NetworkEngageStatus mergedProfileEngageStatus = (NetworkEngageStatus) networkEngageStatusMerger.merge(engageStatusProfile1, engageStatusProfile2);
            mergedProfileContext.setEngageStatus(mergedProfileEngageStatus);
        }
    }

    private void assignEnrichmentStatus(final RelevanteProfileContext mergedProfileContext,
                                        final RelevanteProfileContext sourceProfileContext) {
        final ActionEngageStatus mergedEnrichmentActionEngageStatus = actionEngageStatusMerger.merge(mergedProfileContext.getEnrichmentStatus(),
                sourceProfileContext.getEnrichmentStatus());
        mergedProfileContext.setEnrichmentStatus(mergedEnrichmentActionEngageStatus);
    }

    private void assignLastMonitoringTimestamp(final RelevanteProfileContext mergedProfileContext,
                                               final RelevanteProfileContext sourceProfileContext) {
        final Date mergedProfileMonitoringTimestamp = mergedProfileContext.getLastMonitoringTimestamp();
        final Date sourceProfileMonitoringTimestamp = sourceProfileContext.getLastMonitoringTimestamp();
        final Date mergedLastMonitoringTimestamp = lastMonitoringTimestampMerger.merge(mergedProfileMonitoringTimestamp, sourceProfileMonitoringTimestamp);
        mergedProfileContext.setLastMonitoringTimestamp(mergedLastMonitoringTimestamp);
    }

    private void assignCustomData(final RelevanteProfileContext mergedProfileContext,
                                  final RelevanteProfileContext sourceProfileContext) {
        final CustomProfileData mergedCustomData = customProfileDataMerger.merge(mergedProfileContext.getData(), sourceProfileContext.getData());
        mergedProfileContext.setData(mergedCustomData);
    }
}
