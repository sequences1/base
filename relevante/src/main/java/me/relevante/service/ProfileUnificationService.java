package me.relevante.service;

import me.relevante.core.UniqueProfile;
import me.relevante.request.ProfileUnificationRequest;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Service;

/**
 * @author Daniel Ibanez
 */
@Service
public class ProfileUnificationService {

    private static Logger LOGGER = LoggerFactory.getLogger(ProfileUnificationService.class);

    private final CrudRepository<ProfileUnificationRequest, String> profileUnificationRequestRepo;
    private final ProfileUnificationPlanner planner;
    private final ProfileUnificationExecutor executor;

    @Autowired
    public ProfileUnificationService(final CrudRepository<ProfileUnificationRequest, String> profileUnificationRequestRepo,
                                     final ProfileUnificationPlanner planner,
                                     final ProfileUnificationExecutor executor) {
        this.profileUnificationRequestRepo = profileUnificationRequestRepo;
        this.planner = planner;
        this.executor = executor;
    }

    public UniqueProfile unify(final String relevanteId,
                               final String uniqueProfileToRemainId,
                               final String uniqueProfileToBeDeletedId) {

        final ProfileUnificationRequest request = new ProfileUnificationRequest(relevanteId, uniqueProfileToRemainId, uniqueProfileToBeDeletedId);
        persistProfileUnificationRequest(request);
        final ProfileUnificationPlan unificationPlan = planner.buildUnificationPlan(uniqueProfileToBeDeletedId, uniqueProfileToRemainId);
        try {
            executor.executePlan(unificationPlan);
            request.setSuccess();
            request.setIntegrated();
        } catch (final Exception e) {
            LOGGER.error("Error unifying profile " + uniqueProfileToBeDeletedId + " into " + uniqueProfileToRemainId + " for relevante User " + relevanteId, e);
            request.setError();
        } finally {
            persistProfileUnificationRequest(request);
        }
        return unificationPlan.getMergedUniqueProfile();
    }

    private void persistProfileUnificationRequest(final ProfileUnificationRequest request) {
        profileUnificationRequestRepo.save(request);
    }
}
