package me.relevante.service;

import me.relevante.core.RelevanteProfileContext;
import me.relevante.core.UniqueProfile;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

/**
 * @author Daniel Ibanez
 */
public class ProfileUnificationPlan {

    private UniqueProfile targetProfile;
    private UniqueProfile sourceProfile;
    private UniqueProfile mergedUniqueProfile;
    private List<RelevanteProfileContext> mergedRelevanteProfileContexts;
    private List<String> relevanteProfileContextIdsToDelete;

    public ProfileUnificationPlan(final UniqueProfile targetProfile, final UniqueProfile sourceProfile) {
        this.targetProfile = targetProfile;
        this.sourceProfile = sourceProfile;
    }

    public UniqueProfile getTargetProfile() {
        return targetProfile;
    }

    public UniqueProfile getSourceProfile() {
        return sourceProfile;
    }

    public UniqueProfile getMergedUniqueProfile() {
        return mergedUniqueProfile;
    }

    public void setMergedUniqueProfile(final UniqueProfile mergedUniqueProfile) {
        this.mergedUniqueProfile = mergedUniqueProfile;
    }

    public UniqueProfile getUniqueProfileToDelete() {
        return sourceProfile;
    }

    public ArrayList<RelevanteProfileContext> getMergedRelevanteProfileContexts() {
        return new ArrayList<>(mergedRelevanteProfileContexts);
    }

    public void setMergedRelevanteProfileContexts(final Collection<RelevanteProfileContext> mergedRelevanteProfileContexts) {
        this.mergedRelevanteProfileContexts = new ArrayList<>(mergedRelevanteProfileContexts);
    }

    public ArrayList<String> getRelevanteProfileContextIdsToDelete() {
        return new ArrayList<>(relevanteProfileContextIdsToDelete);
    }

    public void setRelevanteProfileContextIdsToDelete(final Collection<String> relevanteProfileContextIdsToDelete) {
        this.relevanteProfileContextIdsToDelete = new ArrayList<>(relevanteProfileContextIdsToDelete);
    }

    @Override
    public String toString() {
        return "ProfileUnificationPlan{" +
                "targetProfile=" + targetProfile +
                ", sourceProfile=" + sourceProfile +
                ", mergedUniqueProfile=" + mergedUniqueProfile +
                ", mergedRelevanteProfileContexts=" + mergedRelevanteProfileContexts +
                ", relevanteProfileContextIdsToDelete=" + relevanteProfileContextIdsToDelete +
                '}';
    }
}
