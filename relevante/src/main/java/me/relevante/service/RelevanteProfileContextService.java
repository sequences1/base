package me.relevante.service;

import me.relevante.core.CustomProfileData;
import me.relevante.core.RelevanteProfileContext;
import me.relevante.persistence.RelevanteProfileContextRepo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.List;
import java.util.Map;
import java.util.function.Function;
import java.util.stream.Collectors;

/**
 * @author Daniel Ibanez
 */
@Service
public class RelevanteProfileContextService {

    private final RelevanteProfileContextRepo relevanteProfileContextRepo;

    @Autowired
    public RelevanteProfileContextService(final RelevanteProfileContextRepo relevanteProfileContextRepo) {
        this.relevanteProfileContextRepo = relevanteProfileContextRepo;
    }

    public List<RelevanteProfileContext> getByTargetProfileId(final String uniqueProfileId) {
        return relevanteProfileContextRepo.findByTargetProfileId(uniqueProfileId);
    }

    public RelevanteProfileContext getOrCreate(final String relevanteId,
                                               final String uniqueProfileId) {
        RelevanteProfileContext relevanteProfileContext = relevanteProfileContextRepo.findOneByRelevanteIdAndTargetProfileId(relevanteId, uniqueProfileId);
        if (relevanteProfileContext == null) {
            relevanteProfileContext = new RelevanteProfileContext(relevanteId, uniqueProfileId);
            relevanteProfileContext.setData(new CustomProfileData());
        }
        return relevanteProfileContext;
    }

    public List<RelevanteProfileContext> getOrCreate(final String relevanteId,
                                                     final Collection<String> uniqueProfileIds) {
        final List<RelevanteProfileContext> existingRelevanteProfileContexts = relevanteProfileContextRepo.findByRelevanteIdAndTargetProfileIdIn(relevanteId, uniqueProfileIds);
        final Map<String, RelevanteProfileContext> relevanteProfileContextByProfileId = existingRelevanteProfileContexts.stream()
                .collect(Collectors.toMap(RelevanteProfileContext::getProfileId, Function.identity()));
        final List<RelevanteProfileContext> relevanteProfileContexts = new ArrayList<>();
        for (final String uniqueProfileId : uniqueProfileIds) {
            RelevanteProfileContext relevanteProfileContext = relevanteProfileContextByProfileId.get(uniqueProfileId);
            if (relevanteProfileContext == null) {
                relevanteProfileContext = new RelevanteProfileContext(relevanteId, uniqueProfileId);
                relevanteProfileContext.setData(new CustomProfileData());
                relevanteProfileContextRepo.save(relevanteProfileContext);
            }
            relevanteProfileContexts.add(relevanteProfileContext);
        }
        return relevanteProfileContexts;
    }

    public List<RelevanteProfileContext> getOrCreate(final String relevanteId,
                                                     final String... uniqueProfileIds) {
        return getOrCreate(relevanteId, Arrays.asList(uniqueProfileIds));
    }

    public List<RelevanteProfileContext> getContacts(final String relevanteId) {
        return relevanteProfileContextRepo.findByRelevanteIdAndContact(relevanteId, true);
    }

    public List<RelevanteProfileContext> getBlacklisted(final String relevanteId) {
        return relevanteProfileContextRepo.findByRelevanteIdAndBlacklisted(relevanteId, true);
    }

    public void save(final RelevanteProfileContext relevanteProfileContext) {
        relevanteProfileContextRepo.save(relevanteProfileContext);
    }

    public void save(final List<RelevanteProfileContext> relevanteProfileContexts) {
        relevanteProfileContextRepo.save(relevanteProfileContexts);
    }

    public void remove(final List<String> relevanteProfileContextIds) {
        relevanteProfileContextRepo.deleteByIdIn(relevanteProfileContextIds);
    }
}
