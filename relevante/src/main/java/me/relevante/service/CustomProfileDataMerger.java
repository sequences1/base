package me.relevante.service;

import me.relevante.core.CustomProfileData;
import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Service;

import java.util.HashSet;
import java.util.Set;

/**
 * @author Daniel Ibanez
 */
@Service
public class CustomProfileDataMerger
        extends AbstractMerger<CustomProfileData>
        implements Merger<CustomProfileData> {

    @Override
    public CustomProfileData mergeNotNullObjects(final CustomProfileData targetCustomData,
                                                 final CustomProfileData sourceCustomData) {
        final CustomProfileData mergedCustomData = new CustomProfileData();

        final Set<String> allEmails = new HashSet<>();
        allEmails.addAll(sourceCustomData.getEmails());
        allEmails.addAll(targetCustomData.getEmails());
        targetCustomData.setEmails(allEmails);

        final Set<String> allPhoneNumbers = new HashSet<>();
        allPhoneNumbers.addAll(sourceCustomData.getPhoneNumbers());
        allPhoneNumbers.addAll(targetCustomData.getPhoneNumbers());
        targetCustomData.setPhoneNumbers(allPhoneNumbers);
        if (StringUtils.isBlank(targetCustomData.getName())) {
            targetCustomData.setName(sourceCustomData.getName());
        }
        if (StringUtils.isBlank(targetCustomData.getHeadline())) {
            targetCustomData.setHeadline(sourceCustomData.getHeadline());
        }
        if (StringUtils.isBlank(targetCustomData.getImageUrl())) {
            targetCustomData.setImageUrl(sourceCustomData.getImageUrl());
        }
        if (StringUtils.isBlank(targetCustomData.getLocation())) {
            targetCustomData.setLocation(sourceCustomData.getLocation());
        }
        if (StringUtils.isNotBlank(sourceCustomData.getNotes())) {
            final String separator = StringUtils.isNotBlank(targetCustomData.getNotes()) ? " \n" : "";
            targetCustomData.setNotes(StringUtils.defaultString(targetCustomData.getNotes()) + separator +
                    StringUtils.defaultString(sourceCustomData.getNotes()));
        }
        return mergedCustomData;
    }

}
