package me.relevante.service;

import me.relevante.core.NetworkProfileId;
import me.relevante.core.ScoredEmail;
import me.relevante.core.UniqueProfile;
import org.springframework.stereotype.Service;

import java.util.HashSet;
import java.util.Set;

/**
 * @author Daniel Ibanez
 */
@Service
public class UniqueProfileMerger
        extends AbstractMerger<UniqueProfile>
        implements Merger<UniqueProfile> {

    @Override
    public UniqueProfile mergeNotNullObjects(final UniqueProfile targetProfile,
                                             final UniqueProfile sourceProfile) {

        final UniqueProfile mergedProfile = targetProfile.clone();

        final Set<NetworkProfileId> uniqueNetworkProfileIds = new HashSet<>(targetProfile.getNetworkProfileIds());
        sourceProfile.getNetworkProfileIds().forEach(networkProfileId -> uniqueNetworkProfileIds.add(networkProfileId));
        mergedProfile.setNetworkProfileIds(uniqueNetworkProfileIds);
        final Set<ScoredEmail> uniqueEmails = new HashSet<>(targetProfile.getScoredEmails());
        sourceProfile.getScoredEmails().forEach(scoredEmail -> uniqueEmails.add(scoredEmail));
        mergedProfile.setEmails(uniqueEmails);
        if (targetProfile.getKeywords().isEmpty()) {
            targetProfile.setKeywords(sourceProfile.getKeywords());
        }
        return mergedProfile;
    }

}
