package me.relevante.service;

import org.springframework.stereotype.Service;

import java.util.Date;

/**
 * @author Daniel Ibanez
 */
@Service
public class LastMonitoringTimestampMerger implements Merger<Date> {

    @Override
    public Date merge(final Date targetMonitorable, final Date sourceMonitorable) {
        if (targetMonitorable == null && sourceMonitorable == null) {
            return null;
        } else if (targetMonitorable == null) {
            return new Date(sourceMonitorable.getTime());
        } else if (sourceMonitorable == null) {
            return new Date(targetMonitorable.getTime());
        }
        return new Date(Math.max(targetMonitorable.getTime(), sourceMonitorable.getTime()));
    }
}
