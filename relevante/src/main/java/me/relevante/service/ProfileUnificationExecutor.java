package me.relevante.service;

import me.relevante.core.NetworkFullProfile;
import me.relevante.core.NetworkProfileId;
import me.relevante.core.RelevanteContext;
import me.relevante.core.RelevanteProfileContext;
import me.relevante.core.UniqueProfile;
import me.relevante.queue.ProfileIndexMessage;
import me.relevante.queue.QueueService;
import me.relevante.queue.RemoveProfileFromIndexMessage;
import me.relevante.util.IterableUtils;
import me.relevante.util.MapByNetwork;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Service;

import java.util.Collection;
import java.util.List;
import java.util.Map;

/**
 * @author Daniel Ibanez
 */
@Service
public class ProfileUnificationExecutor {

    private final CrudRepository<UniqueProfile, String> uniqueProfileRepo;
    private final Map<String, NetworkFullProfileRepoRetriever> networkFullProfileRepoRetrieverByNetwork;
    private final RelevanteProfileContextService relevanteProfileContextService;
    private final CrudRepository<RelevanteContext, String> relevanteContextRepo;
    private final QueueService queueService;

    @Autowired
    public ProfileUnificationExecutor(final CrudRepository<UniqueProfile, String> uniqueProfileRepo,
                                      final Collection<NetworkFullProfileRepoRetriever> networkFullProfileRepoRetrievers,
                                      final RelevanteProfileContextService relevanteProfileContextService,
                                      final CrudRepository<RelevanteContext, String> relevanteContextRepo,
                                      final QueueService queueService) {
        this.uniqueProfileRepo = uniqueProfileRepo;
        this.networkFullProfileRepoRetrieverByNetwork = MapByNetwork.from(networkFullProfileRepoRetrievers);
        this.relevanteProfileContextService = relevanteProfileContextService;
        this.relevanteContextRepo = relevanteContextRepo;
        this.queueService = queueService;
    }

    public void executePlan(final ProfileUnificationPlan unificationPlan) {
        persistUniqueProfile(unificationPlan.getMergedUniqueProfile());
        updateNetworkProfiles(unificationPlan.getUniqueProfileToDelete(), unificationPlan.getMergedUniqueProfile().getId());
        persistRelevanteProfileContexts(unificationPlan.getMergedRelevanteProfileContexts(), unificationPlan.getRelevanteProfileContextIdsToDelete());
        removeWatchlistProfilesRelatedToUniqueProfileToBeDeleted(unificationPlan.getUniqueProfileToDelete().getId());
        reindexMergedProfileInSearchService(unificationPlan.getMergedUniqueProfile().getId());
        removeProfileToDeleteInSearchService(unificationPlan.getUniqueProfileToDelete().getId());
        removeOldUniqueProfile(unificationPlan.getUniqueProfileToDelete().getId());
    }

    private void persistUniqueProfile(final UniqueProfile uniqueProfile) {
        uniqueProfileRepo.save(uniqueProfile);
    }

    private void updateNetworkProfiles(final UniqueProfile uniqueProfileToBeDeleted,
                                       final String uniqueProfileToRemainId) {
        for (final NetworkProfileId networkProfileId : uniqueProfileToBeDeleted.getNetworkProfileIds()) {
            final NetworkFullProfileRepoRetriever fullProfileRepoRetriever = networkFullProfileRepoRetrieverByNetwork.get(networkProfileId.getNetwork());
            final CrudRepository<NetworkFullProfile, String> fullProfileRepo = fullProfileRepoRetriever.getFullProfileRepo();
            final NetworkFullProfile fullProfile = fullProfileRepo.findOne(networkProfileId.getProfileId());
            fullProfile.setUniqueProfileId(uniqueProfileToRemainId);
            fullProfileRepo.save(fullProfile);
        }
    }

    private void persistRelevanteProfileContexts(final List<RelevanteProfileContext> relevanteProfileContextsToUpdate,
                                                 final List<String> relevanteProfileContextsToDeleteIds) {
        relevanteProfileContextService.save(relevanteProfileContextsToUpdate);
        relevanteProfileContextService.remove(relevanteProfileContextsToDeleteIds);
    }

    private void removeWatchlistProfilesRelatedToUniqueProfileToBeDeleted(final String uniqueProfileToBeDeletedId) {
        final List<RelevanteContext> relevanteContexts = IterableUtils.toList(relevanteContextRepo.findAll());
        for (final RelevanteContext relevanteContext : relevanteContexts) {
            if (relevanteContext.removeProfileFromAllWatchlists(uniqueProfileToBeDeletedId)) {
                relevanteContextRepo.save(relevanteContext);
            }
        }
    }

    private void reindexMergedProfileInSearchService(final String uniqueProfileId) {
        queueService.sendProfileIndexMessage(new ProfileIndexMessage(uniqueProfileId));
    }

    private void removeProfileToDeleteInSearchService(final String uniqueProfileId) {
        queueService.sendRemoveProfileFromIndexMessage(new RemoveProfileFromIndexMessage(uniqueProfileId));
    }

    private void removeOldUniqueProfile(final String uniqueProfileId) {
        uniqueProfileRepo.delete(uniqueProfileId);
    }
}
