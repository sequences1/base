package me.relevante.service;

import me.relevante.core.RelevanteProfileContext;
import me.relevante.core.UniqueProfile;
import me.relevante.util.IterableUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.function.Function;
import java.util.stream.Collectors;

/**
 * @author Daniel Ibanez
 */
@Service
public class ProfileUnificationPlanner {

    private final CrudRepository<UniqueProfile, String> uniqueProfileRepo;
    private final RelevanteProfileContextService relevanteProfileContextService;
    private final UniqueProfileMerger uniqueProfileMerger;
    private final RelevanteProfileContextMerger relevanteProfileContextMerger;

    @Autowired
    public ProfileUnificationPlanner(final CrudRepository<UniqueProfile, String> uniqueProfileRepo,
                                     final RelevanteProfileContextService relevanteProfileContextService,
                                     final UniqueProfileMerger uniqueProfileMerger,
                                     final RelevanteProfileContextMerger relevanteProfileContextMerger) {
        this.uniqueProfileRepo = uniqueProfileRepo;
        this.relevanteProfileContextService = relevanteProfileContextService;
        this.uniqueProfileMerger = uniqueProfileMerger;
        this.relevanteProfileContextMerger = relevanteProfileContextMerger;
    }

    public ProfileUnificationPlan buildUnificationPlan(final String uniqueProfileToRemainId,
                                                       final String uniqueProfileToBeDeletedId) {

        final List<UniqueProfile> uniqueProfiles = findUniqueProfiles(uniqueProfileToRemainId, uniqueProfileToBeDeletedId);
        final ProfileUnificationPlan unificationPlan = new ProfileUnificationPlan(uniqueProfiles.get(0), uniqueProfiles.get(1));
        addMergedUniqueProfileToPlan(unificationPlan);
        addMergedRelevanteProfileContextsToPlan(unificationPlan);
        return unificationPlan;
    }

    private void addMergedUniqueProfileToPlan(final ProfileUnificationPlan unificationPlan) {
        final UniqueProfile mergedProfile = uniqueProfileMerger.merge(unificationPlan.getTargetProfile(), unificationPlan.getSourceProfile());
        unificationPlan.setMergedUniqueProfile(mergedProfile);
    }

    private void addMergedRelevanteProfileContextsToPlan(final ProfileUnificationPlan unificationPlan) {
        final List<RelevanteProfileContext> profileContextsToRemain = relevanteProfileContextService.getByTargetProfileId(unificationPlan.getTargetProfile().getId());
        final List<RelevanteProfileContext> profileContextsToDelete = relevanteProfileContextService.getByTargetProfileId(unificationPlan.getSourceProfile().getId());
        final Map<String, RelevanteProfileContext> profileContextsToDeleteByRelevanteId = profileContextsToDelete.stream()
                .collect(Collectors.toMap(RelevanteProfileContext::getRelevanteId, Function.identity()));
        final List<RelevanteProfileContext> mergedProfileContexts = new ArrayList<>();
        for (final RelevanteProfileContext profileContextToRemain : profileContextsToRemain) {
            final RelevanteProfileContext profileContextToDelete = profileContextsToDeleteByRelevanteId.get(profileContextToRemain.getRelevanteId());
            if (profileContextToDelete != null) {
                final RelevanteProfileContext mergedProfileContext = relevanteProfileContextMerger.merge(profileContextToRemain, profileContextToDelete);
                mergedProfileContexts.add(mergedProfileContext);
            }
        }
        unificationPlan.setMergedRelevanteProfileContexts(mergedProfileContexts);
        unificationPlan.setRelevanteProfileContextIdsToDelete(profileContextsToDelete.stream().map(RelevanteProfileContext::getId)
                .collect(Collectors.toList()));
    }

    private List<UniqueProfile> findUniqueProfiles(final String... uniqueProfileIds) {
        return IterableUtils.toList(uniqueProfileRepo.findAll(Arrays.asList(uniqueProfileIds)));
    }
}
