package me.relevante.nlp;

import me.relevante.core.NetworkSignal;
import me.relevante.core.SignalCategory;
import me.relevante.nlp.core.Feature;
import me.relevante.search.NetworkSignalWeightFunction;
import me.relevante.search.SearchGoal;
import me.relevante.util.MapByNetwork;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

@Component
public class SignalVectorizer {

    private final Map<String, NetworkSignalWeightFunction> signalWeightFunctionByNetwork;

    @Autowired
    public SignalVectorizer(final Collection<NetworkSignalWeightFunction> signalWeightFunctionByNetwork) {
        this.signalWeightFunctionByNetwork = MapByNetwork.from(signalWeightFunctionByNetwork);
    }

    public Map<String, Feature> generateVector(final Collection<NetworkSignal> signals,
                                               final SearchGoal searchGoal) {

        final Map<String, List<NetworkSignal>> signalsByStem = generateSignalsByStem(signals);
        final Map<String, Feature> vector = new HashMap<>();
        for (Map.Entry<String, List<NetworkSignal>> entry : signalsByStem.entrySet()) {
            final String stem = entry.getKey();
            final List<NetworkSignal> stemSignals = entry.getValue();
            final double score = calculateStemSignalsScore(stemSignals, searchGoal);
            final Feature feature = new Feature(stem, score);
            vector.put(stem, feature);
        }
        return vector;
    }

    private Map<String, List<NetworkSignal>> generateSignalsByStem(final Collection<NetworkSignal> signals) {
        final Map<String, List<NetworkSignal>> signalsByStem = new HashMap<>();
        signals.forEach(signal -> addSignalKeywords(signal, signalsByStem));
        return signalsByStem;
    }


    private void addSignalKeywords(final NetworkSignal signal,
                                   final Map<String, List<NetworkSignal>> signalsByStem) {
        final List<String> contentStems = signal.getContentStems();
        for (String stem : contentStems) {
            List<NetworkSignal> stemsSignals = signalsByStem.get(stem);
            if (stemsSignals == null) {
                stemsSignals = new ArrayList<>();
                signalsByStem.put(stem, stemsSignals);
            }
            stemsSignals.add(signal);
        }
    }

    private double calculateStemSignalsScore(final Collection<NetworkSignal> stemSignals,
                                             final SearchGoal searchGoal) {

        final Map<Class, List<NetworkSignal>> signalsByClass = groupSignalsByClass(stemSignals);
        final double totalScore = calculateSignalsScore(signalsByClass, searchGoal);

        return totalScore;
    }

    private double calculateSignalsScore(final Map<Class, List<NetworkSignal>> signalsByClass,
                                         final SearchGoal searchGoal) {

        double totalScore = 0.0;
        final Set<SignalCategory> categories = new HashSet();
        for (Map.Entry<Class, List<NetworkSignal>> entry : signalsByClass.entrySet()) {
            final Class clazz = entry.getKey();
            final List<NetworkSignal> signals = entry.getValue();
            final NetworkSignalWeightFunction signalWeightFunction = signalWeightFunctionByNetwork.get(signals.get(0).getNetwork().getName());
            final double classWeight = signalWeightFunction.getSignalWeight(clazz, (double) signals.size(), searchGoal);
            totalScore += classWeight;
            categories.add(signals.get(0).getCategory());
        }

        return totalScore;
    }

    private Map<Class, List<NetworkSignal>> groupSignalsByClass(final Collection<NetworkSignal> signals) {

        final Map<Class, List<NetworkSignal>> signalsByClass = new HashMap<>();
        for (NetworkSignal signal : signals) {
            List<NetworkSignal> signalsList = signalsByClass.get(signal.getClass());
            if (signalsList == null) {
                signalsList = new ArrayList<>();
                signalsByClass.put(signal.getClass(), signalsList);
            }
            signalsList.add(signal);
        }
        return signalsByClass;
    }

}
