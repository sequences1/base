package me.relevante.nlp;

public class StemProfileScore {

    private String stem;
    private String profileId;
    private double score;

    public StemProfileScore(String stem, String profileId, double score) {
        this();
        this.stem = stem;
        this.profileId = profileId;
        this.score = score;
    }

    private StemProfileScore() {
    }

    public String getStem() {
        return stem;
    }

    public String getProfileId() {
        return profileId;
    }

    public double getScore() {
        return score;
    }
}
