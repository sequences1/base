package me.relevante.request;

import me.relevante.core.ScoredEmail;
import me.relevante.guesser.profile.GuessedProfile;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Document(collection = "EnrichmentRequest")
public class EnrichmentRequest {

    @Id
    private String id;
    private final String relevanteId;
    private final String targetProfileId;
    private final List<ScoredEmail> obtainedEmails;
    private final List<GuessedProfile> obtainedProfiles;
    private ActionRequestStatus status;
    private ActionRequestResult result;
    private Date creationTimestamp;
    private Date inProgressTimestamp;
    private Date processedTimestamp;

    private EnrichmentRequest() {
        this(null, null);
    }

    public EnrichmentRequest(final String relevanteId,
                             final String targetProfileId) {
        this.relevanteId = relevanteId;
        this.targetProfileId = targetProfileId;
        this.obtainedEmails = new ArrayList<>();
        this.obtainedProfiles = new ArrayList<>();
        this.creationTimestamp = new Date();
        this.inProgressTimestamp = this.creationTimestamp;
        this.status = ActionRequestStatus.IN_PROGRESS;
    }

    public String getId() {
        return id;
    }

    public String getRelevanteId() {
        return relevanteId;
    }

    public String getTargetProfileId() {
        return targetProfileId;
    }

    public ActionRequestStatus getStatus() {
        return status;
    }

    public ActionRequestResult getResult() {
        return result;
    }

    public Date getCreationTimestamp() {
        return creationTimestamp;
    }

    public Date getInProgressTimestamp() {
        return inProgressTimestamp;
    }

    public Date getProcessedTimestamp() {
        return processedTimestamp;
    }

    public List<ScoredEmail> getObtainedEmails() {
        return new ArrayList<>(obtainedEmails);
    }

    public void addObtainedEmail(final ScoredEmail newScoredEmail) {
        for (final ScoredEmail scoredEmail : obtainedEmails) {
            if (scoredEmail.getEmail().equals(newScoredEmail.getEmail())) {
                return;
            }
        }
        obtainedEmails.add(newScoredEmail);
    }

    public List<GuessedProfile> getObtainedProfiles() {
        return new ArrayList<>(obtainedProfiles);
    }

    public void addObtainedProfiles(final List<GuessedProfile> newObtainedProfiles) {
        for (final GuessedProfile newProfile : newObtainedProfiles) {
            if (!profileAlreadyExists(newProfile)) {
                obtainedProfiles.add(newProfile);
            }
        }
    }

    public void setSuccess() {
        this.result = ActionRequestResult.SUCCESS;
        this.status = ActionRequestStatus.PROCESSED;
        this.processedTimestamp = new Date();
    }

    public void setError() {
        this.result = ActionRequestResult.ERROR;
        this.status = ActionRequestStatus.PROCESSED;
        this.processedTimestamp = new Date();
    }

    public void setAlreadySucceeded() {
        this.result = ActionRequestResult.ALREADY_SUCCEEDED;
        this.status = ActionRequestStatus.PROCESSED;
        this.processedTimestamp = new Date();
    }

    public void setForbidden() {
        this.result = ActionRequestResult.FORBIDDEN;
        this.status = ActionRequestStatus.PROCESSED;
        this.processedTimestamp = new Date();
    }

    public void setUserManaged() {
        this.result = ActionRequestResult.USER_MANAGED;
        this.status = ActionRequestStatus.PROCESSED;
        this.processedTimestamp = new Date();
    }

    private boolean profileAlreadyExists(final GuessedProfile profile) {
        for (final GuessedProfile existingProfile : obtainedProfiles) {
            if (profile.getUrl().equals(existingProfile.getUrl())) {
                return true;
            }
        }
        return false;
    }
}
