package me.relevante.request;

import org.springframework.data.mongodb.core.index.CompoundIndex;
import org.springframework.data.mongodb.core.index.CompoundIndexes;
import org.springframework.data.mongodb.core.mapping.Document;

/**
 * @author Daniel Ibanez
 */
@Document(collection = "ProfileUnificationRequest")
@CompoundIndexes({@CompoundIndex(unique = true, name = "sourceUniqueProfileId_1", def = "{'sourceUniqueProfileId' : 1}"),
                         @CompoundIndex(unique = false, name = "targetUniqueProfileId_1", def = "{'targetUniqueProfileId' : 1}")})
public class ProfileUnificationRequest extends AbstractActionRequest implements ActionRequest {

    private String targetUniqueProfileId;
    private String sourceUniqueProfileId;

    private ProfileUnificationRequest() {
        this(null, null, null);
    }

    public ProfileUnificationRequest(final String requesterRelevanteId,
                                     final String targetUniqueProfileId,
                                     final String sourceUniqueProfileId) {
        super(requesterRelevanteId);
        this.targetUniqueProfileId = targetUniqueProfileId;
        this.sourceUniqueProfileId = sourceUniqueProfileId;
    }

    public String getTargetUniqueProfileId() {
        return targetUniqueProfileId;
    }

    public String getSourceUniqueProfileId() {
        return sourceUniqueProfileId;
    }

    @Override
    public String toString() {
        return "ProfileUnificationRequest{" +
                "targetUniqueProfileId='" + targetUniqueProfileId + '\'' +
                ", sourceUniqueProfileId='" + sourceUniqueProfileId + '\'' +
                "} " + super.toString();
    }
}
