package me.relevante.request;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import java.util.Date;

@Document(collection = "DeduplicationRequest")
public class DeduplicationRequest {

    @Id
    private String id;
    private final String relevanteId;
    private final String targetProfileId;
    private ActionRequestStatus status;
    private ActionRequestResult result;
    private Date creationTimestamp;
    private Date inProgressTimestamp;
    private Date processedTimestamp;

    private DeduplicationRequest() {
        this(null, null);
    }

    public DeduplicationRequest(final String relevanteId,
                                final String targetProfileId) {
        this.relevanteId = relevanteId;
        this.targetProfileId = targetProfileId;
        this.creationTimestamp = new Date();
        this.inProgressTimestamp = this.creationTimestamp;
        this.status = ActionRequestStatus.IN_PROGRESS;
    }

    public String getId() {
        return id;
    }

    public String getRelevanteId() {
        return relevanteId;
    }

    public String getTargetProfileId() {
        return targetProfileId;
    }

    public ActionRequestStatus getStatus() {
        return status;
    }

    public ActionRequestResult getResult() {
        return result;
    }

    public Date getCreationTimestamp() {
        return creationTimestamp;
    }

    public Date getInProgressTimestamp() {
        return inProgressTimestamp;
    }

    public Date getProcessedTimestamp() {
        return processedTimestamp;
    }

    public void setSuccess() {
        this.result = ActionRequestResult.SUCCESS;
        this.status = ActionRequestStatus.PROCESSED;
        this.processedTimestamp = new Date();
    }

    public void setError() {
        this.result = ActionRequestResult.ERROR;
        this.status = ActionRequestStatus.PROCESSED;
        this.processedTimestamp = new Date();
    }

    public void setAlreadySucceeded() {
        this.result = ActionRequestResult.ALREADY_SUCCEEDED;
        this.status = ActionRequestStatus.PROCESSED;
        this.processedTimestamp = new Date();
    }

    public void setForbidden() {
        this.result = ActionRequestResult.FORBIDDEN;
        this.status = ActionRequestStatus.PROCESSED;
        this.processedTimestamp = new Date();
    }

    public void setUserManaged() {
        this.result = ActionRequestResult.USER_MANAGED;
        this.status = ActionRequestStatus.PROCESSED;
        this.processedTimestamp = new Date();
    }

}
