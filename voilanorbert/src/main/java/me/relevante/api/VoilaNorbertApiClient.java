package me.relevante.api;

import me.relevante.core.VoilaNorbertSearchForEmail;
import org.springframework.cloud.netflix.feign.FeignClient;
import org.springframework.util.MultiValueMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

@FeignClient(name = "voila-norbert", url = "${voila-norbert.url}", configuration = VoilaNorbertApiClientConfig.class)
public interface VoilaNorbertApiClient {

    @RequestMapping(method = RequestMethod.POST, value = "/search/name")
    VoilaNorbertSearchForEmail searchForEmail(MultiValueMap map);

}
