
package me.relevante.core;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
        "avatar_url",
        "email",
        "is_done",
        "location",
        "position",
        "profiles",
        "score",
        "timezone"
})
public class VoilaNorbertEmail {

    @JsonProperty("avatar_url")
    private String avatarUrl;
    @JsonProperty("email")
    private String email;
    @JsonProperty("is_done")
    private Boolean isDone;
    @JsonProperty("location")
    private String location;
    @JsonProperty("position")
    private String position;
    @JsonProperty("profiles")
    private List<VoilaNorbertProfile> profiles;
    @JsonProperty("score")
    private Integer score;
    @JsonProperty("timezone")
    private Date timezone;

    @JsonProperty("avatar_url")
    public String getAvatarUrl() {
        return avatarUrl;
    }

    @JsonProperty("avatar_url")
    public void setAvatarUrl(String avatarUrl) {
        this.avatarUrl = avatarUrl;
    }

    @JsonProperty("email")
    public String getEmail() {
        return email;
    }

    @JsonProperty("email")
    public void setEmail(String email) {
        this.email = email;
    }

    @JsonProperty("is_done")
    public Boolean getIsDone() {
        return isDone;
    }

    @JsonProperty("is_done")
    public void setIsDone(Boolean isDone) {
        this.isDone = isDone;
    }

    @JsonProperty("location")
    public String getLocation() {
        return location;
    }

    @JsonProperty("location")
    public void setLocation(final String location) {
        this.location = location;
    }

    @JsonProperty("position")
    public String getPosition() {
        return position;
    }

    @JsonProperty("position")
    public void setPosition(final String position) {
        this.position = position;
    }

    @JsonProperty("profiles")
    public List<VoilaNorbertProfile> getProfiles() {
        return new ArrayList<>(profiles);
    }

    @JsonProperty("profiles")
    public void setProfiles(final List<VoilaNorbertProfile> profiles) {
        this.profiles = new ArrayList<>(profiles);
    }

    @JsonProperty("score")
    public Integer getScore() {
        return score;
    }

    @JsonProperty("score")
    public void setScore(Integer score) {
        this.score = score;
    }

    @JsonProperty("timezone")
    public Date getTimezone() {
        return timezone;
    }

    @JsonProperty("timezone")
    public void setTimezone(final Date timezone) {
        this.timezone = timezone;
    }
}
