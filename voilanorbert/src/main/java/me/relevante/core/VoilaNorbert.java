package me.relevante.core;

import me.relevante.guesser.GuesserProvider;
import org.springframework.stereotype.Component;

@Component
public class VoilaNorbert extends AbstractNetwork implements Network, GuesserProvider {

    private static final String NAME = "voilaNorbert";
    private static VoilaNorbert instance = new VoilaNorbert();

    public static VoilaNorbert getInstance() {
        return instance;
    }

    private VoilaNorbert() {
        super(NAME);
    }

}
