
package me.relevante.core;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.util.Date;
import java.util.List;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
        "company",
        "created",
        "email",
        "id",
        "is_new",
        "lists",
        "name",
        "owner",
        "searching",
        "status"
})
public class VoilaNorbertSearchForEmail {

    @JsonProperty("company")
    private VoilaNorbertCompany company;
    @JsonProperty("created")
    private Long created;
    @JsonProperty("email")
    private VoilaNorbertEmail email;
    @JsonProperty("id")
    private Long id;
    @JsonProperty("is_new")
    private Boolean isNew;
    @JsonProperty("lists")
    private List<Object> lists = null;
    @JsonProperty("name")
    private String name;
    @JsonProperty("owner")
    private VoilaNorbertOwner owner;
    @JsonProperty("searching")
    private Boolean isSearching;
    @JsonProperty("status")
    private String status;

    @JsonProperty("company")
    public VoilaNorbertCompany getCompany() {
        return company;
    }

    @JsonProperty("company")
    public void setCompany(VoilaNorbertCompany company) {
        this.company = company;
    }

    @JsonProperty("created")
    public Long getCreated() {
        return created;
    }

    @JsonProperty("created")
    public void setCreated(Long created) {
        this.created = created;
    }

    @JsonProperty("email")
    public VoilaNorbertEmail getEmail() {
        return email;
    }

    @JsonProperty("email")
    public void setEmail(VoilaNorbertEmail email) {
        this.email = email;
    }

    @JsonProperty("id")
    public Long getId() {
        return id;
    }

    @JsonProperty("id")
    public void setId(Long id) {
        this.id = id;
    }

    @JsonProperty("is_new")
    public Boolean getIsNew() {
        return isNew;
    }

    @JsonProperty("is_new")
    public void setIsNew(Boolean isNew) {
        this.isNew = isNew;
    }

    @JsonProperty("lists")
    public List<Object> getLists() {
        return lists;
    }

    @JsonProperty("lists")
    public void setLists(List<Object> lists) {
        this.lists = lists;
    }

    @JsonProperty("name")
    public String getName() {
        return name;
    }

    @JsonProperty("name")
    public void setName(String name) {
        this.name = name;
    }

    @JsonProperty("owner")
    public VoilaNorbertOwner getOwner() {
        return owner;
    }

    @JsonProperty("owner")
    public void setOwner(VoilaNorbertOwner owner) {
        this.owner = owner;
    }

    @JsonProperty("searching")
    public Boolean isSearching() {
        return isSearching;
    }

    @JsonProperty("searching")
    public void setIsSearching(Boolean isSearching) {
        this.isSearching = isSearching;
    }

    @JsonProperty("status")
    public String getStatus() {
        return status;
    }

    @JsonProperty("status")
    public void setStatus(String status) {
        this.status = status;
    }

}
