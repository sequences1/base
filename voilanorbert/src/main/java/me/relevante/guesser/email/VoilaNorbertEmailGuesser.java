package me.relevante.guesser.email;

import me.relevante.api.VoilaNorbertApiClient;
import me.relevante.core.VoilaNorbert;
import me.relevante.core.VoilaNorbertEmail;
import me.relevante.core.VoilaNorbertProfile;
import me.relevante.core.VoilaNorbertSearchForEmail;
import me.relevante.guesser.Clues;
import me.relevante.guesser.domain.DomainGuesser;
import me.relevante.guesser.domain.GuessedDomain;
import me.relevante.guesser.profile.GuessedProfile;
import me.relevante.guesser.tracking.RequestTracker;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.util.LinkedMultiValueMap;

import java.util.ArrayList;
import java.util.List;

@Component
public class VoilaNorbertEmailGuesser implements EmailGuesser {

    private static final int MAX_RETRIES = 5;

    private final VoilaNorbertApiClient voilaNorbertApiClient;
    private final RequestTracker requestTracker;
    private final DomainGuesser domainGuesser;

    @Autowired
    public VoilaNorbertEmailGuesser(final VoilaNorbertApiClient voilaNorbertApiClient,
                                    final RequestTracker requestTracker,
                                    final DomainGuesser domainGuesser) {
        this.voilaNorbertApiClient = voilaNorbertApiClient;
        this.requestTracker = requestTracker;
        this.domainGuesser = domainGuesser;
    }

    @Override
    public List<GuessedEmail> guess(final Clues clues) {
        final List<GuessedEmailWithProfiles> guessedEmailsWithProfiles = new ArrayList<>();
        final List<GuessedEmail> guessedEmails = new ArrayList<>();
        for (final GuessedEmailWithProfiles guessedEmailWithProfile : guessedEmailsWithProfiles) {
            guessedEmails.add(new GuessedEmail(guessedEmailWithProfile.getEmail(), guessedEmailWithProfile.getScore()));
        }
        return guessedEmails;
    }

    @Override
    public List<GuessedEmailWithProfiles> guessWithProfiles(final Clues clues) {
        final List<GuessedEmailWithProfiles> guessedEmails = new ArrayList<>();
        final List<GuessedDomain> guessedDomains = domainGuesser.guess(clues);
        for (final GuessedDomain guessedDomain : guessedDomains) {
            final VoilaNorbertSearchForEmail searchForEmail = getVoilaNorbertSearchForEmailResponse(clues, guessedDomain);
            if (!searchForEmail.isSearching()) {
                guessedEmails.add(convertVoilaNorbertEmailIntoGuessedWithProfilesEmail(searchForEmail.getEmail()));
            }
        }
        return guessedEmails;
    }

    private VoilaNorbertSearchForEmail getVoilaNorbertSearchForEmailResponse(final Clues clues,
                                                                             final GuessedDomain domain) {
        requestTracker.track(VoilaNorbert.getInstance(), "Started search for clues: " + clues);
        VoilaNorbertSearchForEmail response = voilaNorbertApiClient.searchForEmail(buildParams(clues, domain));
        for (int i = 0; response.isSearching() && i < MAX_RETRIES; i++) {
            waitSeconds((int) Math.pow(2, i));
            requestTracker.track(VoilaNorbert.getInstance(), "Started search for clues: " + clues);
            response = voilaNorbertApiClient.searchForEmail(buildParams(clues, domain));
        }
        return response;
    }

    private GuessedEmailWithProfiles convertVoilaNorbertEmailIntoGuessedWithProfilesEmail(final VoilaNorbertEmail voilaNorbertEmail) {
        final List<GuessedProfile> guessedProfiles = new ArrayList<>();
        for (final VoilaNorbertProfile voilaNorbertProfile : voilaNorbertEmail.getProfiles()) {
            guessedProfiles.add(new GuessedProfile(voilaNorbertProfile.getType(), voilaNorbertProfile.getUsername(),
                    voilaNorbertProfile.getUrl()));
        }
        return new GuessedEmailWithProfiles(voilaNorbertEmail.getEmail(), voilaNorbertEmail.getScore(), guessedProfiles);
    }

    private String buildFullName(Clues clues) {
        return clues.getName() + " " + clues.getSurname();
    }

    private LinkedMultiValueMap<String, String> buildParams(final Clues clues,
                                                            final GuessedDomain guessedDomain) {
        final LinkedMultiValueMap<String, String> params = new LinkedMultiValueMap<>();
        params.set("name", buildFullName(clues));
        params.set("domain", guessedDomain.getDomain());
        return params;
    }

    private void waitSeconds(int seconds) {
        try {
            Thread.sleep(1000*seconds);
        } catch (final InterruptedException e) {
            throw new IllegalArgumentException(e);
        }
    }
}
