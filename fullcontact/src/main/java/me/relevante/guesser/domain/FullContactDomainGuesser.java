package me.relevante.guesser.domain;

import me.relevante.api.FullContactApiClient;
import me.relevante.guesser.Clues;
import me.relevante.core.FullContact;
import me.relevante.guesser.tracking.RequestTracker;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.stream.Collectors;

@Component
public class FullContactDomainGuesser implements DomainGuesser {

    private final FullContactApiClient fullContactClient;
    private final RequestTracker requestTracker;

    @Autowired
    public FullContactDomainGuesser(final FullContactApiClient fullContactClient,
                                    final RequestTracker requestTracker) {
        this.fullContactClient = fullContactClient;
        this.requestTracker = requestTracker;
    }

    @Override
    public List<GuessedDomain> guess(final Clues clues) {
        requestTracker.track(FullContact.getInstance(), "Started search for clues: " + clues);
        return fullContactClient.getCompanyDataByName(clues.getCompany()).stream()
                .map(domain -> new GuessedDomain(domain.getLookupDomain(), 0L))
                .collect(Collectors.toList());
    }
}
