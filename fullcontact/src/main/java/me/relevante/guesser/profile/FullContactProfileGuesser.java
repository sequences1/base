package me.relevante.guesser.profile;

import me.relevante.api.FullContactApiClient;
import me.relevante.core.FullContact;
import me.relevante.guesser.Clues;
import me.relevante.guesser.tracking.RequestTracker;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.stream.Collectors;

@Component
public class FullContactProfileGuesser implements ProfileGuesser {

    private final FullContactApiClient fullContactClient;
    private final RequestTracker requestTracker;

    @Autowired
    public FullContactProfileGuesser(final FullContactApiClient fullContactClient,
                                     final RequestTracker requestTracker) {
        this.fullContactClient = fullContactClient;
        this.requestTracker = requestTracker;
    }

    @Override
    public List<GuessedProfile> guess(final Clues clues) {
        requestTracker.track(FullContact.getInstance(), "Started search for clues " + clues);
        return fullContactClient.getAllDataFromEmail(clues.getEmail()).getSocialProfiles().stream()
                .map(p -> new GuessedProfile(p.getType(), p.getUsername(), p.getUrl()))
                .collect(Collectors.toList());
    }
}
