package me.relevante.core;

import me.relevante.guesser.GuesserProvider;
import org.springframework.stereotype.Component;

@Component
public class FullContact extends AbstractNetwork implements Network, GuesserProvider {

    private static final String NAME = "fullContact";
    private static FullContact instance = new FullContact();

    public static FullContact getInstance() {
        return instance;
    }

    private FullContact() {
        super(NAME);
    }

}
