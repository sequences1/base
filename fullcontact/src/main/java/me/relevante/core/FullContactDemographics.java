
package me.relevante.core;

import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.util.HashMap;
import java.util.Map;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
    "locationDeduced",
    "gender",
    "locationGeneral"
})
public class FullContactDemographics {

    @JsonProperty("locationDeduced")
    private FullContactLocationDeduced locationDeduced;
    @JsonProperty("gender")
    private String gender;
    @JsonProperty("locationGeneral")
    private String locationGeneral;
    @JsonIgnore
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    @JsonProperty("locationDeduced")
    public FullContactLocationDeduced getLocationDeduced() {
        return locationDeduced;
    }

    @JsonProperty("locationDeduced")
    public void setLocationDeduced(FullContactLocationDeduced locationDeduced) {
        this.locationDeduced = locationDeduced;
    }

    @JsonProperty("gender")
    public String getGender() {
        return gender;
    }

    @JsonProperty("gender")
    public void setGender(String gender) {
        this.gender = gender;
    }

    @JsonProperty("locationGeneral")
    public String getLocationGeneral() {
        return locationGeneral;
    }

    @JsonProperty("locationGeneral")
    public void setLocationGeneral(String locationGeneral) {
        this.locationGeneral = locationGeneral;
    }

    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

}
