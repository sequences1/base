
package me.relevante.core;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
    "lookupDomain",
    "orgName",
    "logo",
    "location",
    "companyApiLink"
})
public class FullContactCompanyData {

    @JsonProperty("lookupDomain")
    private String lookupDomain;
    @JsonProperty("orgName")
    private String orgName;
    @JsonProperty("logo")
    private String logo;
    @JsonProperty("location")
    private FullContactLocation location;
    @JsonProperty("companyApiLink")
    private String companyApiLink;

    @JsonProperty("lookupDomain")
    public String getLookupDomain() {
        return lookupDomain;
    }

    @JsonProperty("lookupDomain")
    public void setLookupDomain(String lookupDomain) {
        this.lookupDomain = lookupDomain;
    }

    @JsonProperty("orgName")
    public String getOrgName() {
        return orgName;
    }

    @JsonProperty("orgName")
    public void setOrgName(String orgName) {
        this.orgName = orgName;
    }

    @JsonProperty("logo")
    public String getLogo() {
        return logo;
    }

    @JsonProperty("logo")
    public void setLogo(String logo) {
        this.logo = logo;
    }

    @JsonProperty("location")
    public FullContactLocation getLocation() {
        return location;
    }

    @JsonProperty("location")
    public void setLocation(FullContactLocation location) {
        this.location = location;
    }

    @JsonProperty("companyApiLink")
    public String getCompanyApiLink() {
        return companyApiLink;
    }

    @JsonProperty("companyApiLink")
    public void setCompanyApiLink(String companyApiLink) {
        this.companyApiLink = companyApiLink;
    }

}
