
package me.relevante.core;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
    "locality",
    "region",
    "country"
})
public class FullContactLocation {

    @JsonProperty("locality")
    private String locality;
    @JsonProperty("region")
    private FullContactRegion region;
    @JsonProperty("country")
    private FullContactCountry country;

    @JsonProperty("locality")
    public String getLocality() {
        return locality;
    }

    @JsonProperty("locality")
    public void setLocality(String locality) {
        this.locality = locality;
    }

    @JsonProperty("region")
    public FullContactRegion getRegion() {
        return region;
    }

    @JsonProperty("region")
    public void setRegion(FullContactRegion region) {
        this.region = region;
    }

    @JsonProperty("country")
    public FullContactCountry getCountry() {
        return country;
    }

    @JsonProperty("country")
    public void setCountry(FullContactCountry country) {
        this.country = country;
    }

}
