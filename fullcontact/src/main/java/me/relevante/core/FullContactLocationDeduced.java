
package me.relevante.core;

import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.util.HashMap;
import java.util.Map;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
    "normalizedLocation",
    "deducedLocation",
    "city",
    "state",
    "country",
    "continent",
    "county",
    "likelihood"
})
public class FullContactLocationDeduced {

    @JsonProperty("normalizedLocation")
    private String normalizedLocation;
    @JsonProperty("deducedLocation")
    private String deducedLocation;
    @JsonProperty("city")
    private FullContactCity city;
    @JsonProperty("state")
    private FullContactState state;
    @JsonProperty("country")
    private FullContactCountry country;
    @JsonProperty("continent")
    private FullContactContinent continent;
    @JsonProperty("county")
    private FullContactCounty county;
    @JsonProperty("likelihood")
    private Integer likelihood;
    @JsonIgnore
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    @JsonProperty("normalizedLocation")
    public String getNormalizedLocation() {
        return normalizedLocation;
    }

    @JsonProperty("normalizedLocation")
    public void setNormalizedLocation(String normalizedLocation) {
        this.normalizedLocation = normalizedLocation;
    }

    @JsonProperty("deducedLocation")
    public String getDeducedLocation() {
        return deducedLocation;
    }

    @JsonProperty("deducedLocation")
    public void setDeducedLocation(String deducedLocation) {
        this.deducedLocation = deducedLocation;
    }

    @JsonProperty("city")
    public FullContactCity getCity() {
        return city;
    }

    @JsonProperty("city")
    public void setCity(FullContactCity city) {
        this.city = city;
    }

    @JsonProperty("state")
    public FullContactState getState() {
        return state;
    }

    @JsonProperty("state")
    public void setState(FullContactState state) {
        this.state = state;
    }

    @JsonProperty("country")
    public FullContactCountry getCountry() {
        return country;
    }

    @JsonProperty("country")
    public void setCountry(FullContactCountry country) {
        this.country = country;
    }

    @JsonProperty("continent")
    public FullContactContinent getContinent() {
        return continent;
    }

    @JsonProperty("continent")
    public void setContinent(FullContactContinent continent) {
        this.continent = continent;
    }

    @JsonProperty("county")
    public FullContactCounty getCounty() {
        return county;
    }

    @JsonProperty("county")
    public void setCounty(FullContactCounty county) {
        this.county = county;
    }

    @JsonProperty("likelihood")
    public Integer getLikelihood() {
        return likelihood;
    }

    @JsonProperty("likelihood")
    public void setLikelihood(Integer likelihood) {
        this.likelihood = likelihood;
    }

    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

}
