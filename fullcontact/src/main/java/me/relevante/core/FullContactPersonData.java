
package me.relevante.core;

import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
    "status",
    "requestId",
    "likelihood",
    "photos",
    "contactInfo",
    "organizations",
    "demographics",
    "socialProfiles",
    "digitalFootprint"
})
public class FullContactPersonData {

    @JsonProperty("status")
    private Integer status;
    @JsonProperty("requestId")
    private String requestId;
    @JsonProperty("likelihood")
    private Double likelihood;
    @JsonProperty("photos")
    private List<FullContactPhoto> photos = null;
    @JsonProperty("contactInfo")
    private FullContactContactInfo contactInfo;
    @JsonProperty("organizations")
    private List<FullContactOrganization> organizations = null;
    @JsonProperty("demographics")
    private FullContactDemographics demographics;
    @JsonProperty("socialProfiles")
    private List<FullContactSocialProfile> socialProfiles = null;
    @JsonProperty("digitalFootprint")
    private FullContactDigitalFootprint digitalFootprint;
    @JsonIgnore
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    @JsonProperty("status")
    public Integer getStatus() {
        return status;
    }

    @JsonProperty("status")
    public void setStatus(Integer status) {
        this.status = status;
    }

    @JsonProperty("requestId")
    public String getRequestId() {
        return requestId;
    }

    @JsonProperty("requestId")
    public void setRequestId(String requestId) {
        this.requestId = requestId;
    }

    @JsonProperty("likelihood")
    public Double getLikelihood() {
        return likelihood;
    }

    @JsonProperty("likelihood")
    public void setLikelihood(Double likelihood) {
        this.likelihood = likelihood;
    }

    @JsonProperty("photos")
    public List<FullContactPhoto> getPhotos() {
        return photos;
    }

    @JsonProperty("photos")
    public void setPhotos(List<FullContactPhoto> photos) {
        this.photos = photos;
    }

    @JsonProperty("contactInfo")
    public FullContactContactInfo getContactInfo() {
        return contactInfo;
    }

    @JsonProperty("contactInfo")
    public void setContactInfo(FullContactContactInfo contactInfo) {
        this.contactInfo = contactInfo;
    }

    @JsonProperty("organizations")
    public List<FullContactOrganization> getOrganizations() {
        return organizations;
    }

    @JsonProperty("organizations")
    public void setOrganizations(List<FullContactOrganization> organizations) {
        this.organizations = organizations;
    }

    @JsonProperty("demographics")
    public FullContactDemographics getDemographics() {
        return demographics;
    }

    @JsonProperty("demographics")
    public void setDemographics(FullContactDemographics demographics) {
        this.demographics = demographics;
    }

    @JsonProperty("socialProfiles")
    public List<FullContactSocialProfile> getSocialProfiles() {
        return socialProfiles;
    }

    @JsonProperty("socialProfiles")
    public void setSocialProfiles(List<FullContactSocialProfile> socialProfiles) {
        this.socialProfiles = socialProfiles;
    }

    @JsonProperty("digitalFootprint")
    public FullContactDigitalFootprint getDigitalFootprint() {
        return digitalFootprint;
    }

    @JsonProperty("digitalFootprint")
    public void setDigitalFootprint(FullContactDigitalFootprint digitalFootprint) {
        this.digitalFootprint = digitalFootprint;
    }

    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

}
