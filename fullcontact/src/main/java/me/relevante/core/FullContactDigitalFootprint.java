
package me.relevante.core;

import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
    "scores",
    "topics"
})
public class FullContactDigitalFootprint {

    @JsonProperty("scores")
    private List<FullContactScore> scores = null;
    @JsonProperty("topics")
    private List<FullContactTopic> topics = null;
    @JsonIgnore
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    @JsonProperty("scores")
    public List<FullContactScore> getScores() {
        return scores;
    }

    @JsonProperty("scores")
    public void setScores(List<FullContactScore> scores) {
        this.scores = scores;
    }

    @JsonProperty("topics")
    public List<FullContactTopic> getTopics() {
        return topics;
    }

    @JsonProperty("topics")
    public void setTopics(List<FullContactTopic> topics) {
        this.topics = topics;
    }

    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

}
