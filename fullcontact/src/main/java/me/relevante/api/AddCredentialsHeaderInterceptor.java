package me.relevante.api;

import feign.RequestInterceptor;
import feign.RequestTemplate;

class AddCredentialsHeaderInterceptor implements RequestInterceptor {

    private String apiKey;

    AddCredentialsHeaderInterceptor(String apiKey) {
        this.apiKey = apiKey;
    }

    @Override
    public void apply(RequestTemplate template) {
        template.header("X-FullContact-APIKey", apiKey);
    }

}
