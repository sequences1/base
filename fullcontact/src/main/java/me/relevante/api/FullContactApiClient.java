package me.relevante.api;

import me.relevante.core.FullContactCompanyData;
import me.relevante.core.FullContactPersonData;
import org.springframework.cloud.netflix.feign.FeignClient;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.List;

@FeignClient(name = "full-contact", url = "${full-contact.url}", configuration = FullContactClientConfig.class)
public interface FullContactApiClient {

    @RequestMapping(method = RequestMethod.GET, value = "/person.json", params = {"email"})
    FullContactPersonData getAllDataFromEmail(@RequestParam("email") String email);

    @RequestMapping(method = RequestMethod.GET, value = "/company/search.json", params = {"companyName"})
    List<FullContactCompanyData> getCompanyDataByName(@RequestParam("companyName") String companyName);

}
