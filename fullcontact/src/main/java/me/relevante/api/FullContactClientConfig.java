package me.relevante.api;

import feign.RequestInterceptor;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class FullContactClientConfig {

    @Value("${full-contact.api-key}")
    private String apiKey;

    @Bean
    public RequestInterceptor requestInterceptors() {
        return new AddCredentialsHeaderInterceptor(apiKey);
    }
}
