package me.relevante.core;

import org.apache.commons.lang3.StringUtils;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import java.util.Date;

/**
 * Created by daniel-ibanez on 19/08/16.
 */
@Document(collection = "WordpressFullProfile")
public class WordpressFullProfile implements NetworkFullProfile<Wordpress, WordpressProfile, WordpressFullProfile> {

    @Id
    private String id;
    private WordpressProfile profile;
    private String hubId;
    private String username;
    private String uniqueProfileId;
    private Date lastUpdatedTimestamp;

    public WordpressFullProfile() {
        super();
    }

    public WordpressFullProfile(WordpressProfile profile) {
        this();
        this.id = profile.getId();
        this.profile = profile;
    }

    @Override
    public String getId() {
        return String.valueOf(id);
    }

    @Override
    public Wordpress getNetwork() {
        return Wordpress.getInstance();
    }

    @Override
    public WordpressProfile getProfile() {
        return profile;
    }

    @Override
    public String getUniqueProfileId() {
        return uniqueProfileId;
    }

    @Override
    public void setUniqueProfileId(final String uniqueProfileId) {
        this.uniqueProfileId = uniqueProfileId;
    }

    public String getHubId() {
        return hubId;
    }

    public void setHubId(String hubId) {
        this.hubId = hubId;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    @Override
    public void completeMissingDataWith(final WordpressFullProfile fullProfile) {
        this.profile.completeMissingDataWith(fullProfile.getProfile());
        if (StringUtils.isBlank(this.id)) this.id = fullProfile.id;
        if (StringUtils.isBlank(this.hubId)) this.hubId = fullProfile.hubId;
        if (StringUtils.isBlank(this.username)) this.username = fullProfile.username;
        markAsUpdated();
    }

    @Override
    public void updateWithExistingDataIn(final WordpressFullProfile fullProfile) {
        this.profile.updateWithExistingDataIn(fullProfile.getProfile());
        if (StringUtils.isNotBlank(fullProfile.id)) this.id = fullProfile.id;
        if (StringUtils.isNotBlank(fullProfile.hubId)) this.hubId = fullProfile.hubId;
        if (StringUtils.isNotBlank(fullProfile.username)) this.username = fullProfile.username;
        markAsUpdated();
    }

    @Override
    public void replaceDataWith(final WordpressFullProfile fullProfile) {
        this.id = fullProfile.id;
        this.hubId = fullProfile.hubId;
        this.username = fullProfile.username;
        this.profile.completeMissingDataWith(fullProfile.getProfile());
        markAsUpdated();
    }

    @Override
    public Date getLastUpdatedTimestamp() {
        return lastUpdatedTimestamp;
    }

    private void markAsUpdated() {
        this.lastUpdatedTimestamp = new Date();
    }
}
