package me.relevante.core;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by daniel-ibanez on 3/08/16.
 */
public class WordpressPost {

    private LocalDateTime date;
    private LocalDateTime dateGmt;
    private String password;
    private String slug;
    private WordpressStatus status;
    private String title;
    private String content;
    private Long authorId;
    private String excerpt;
    private Long featuredMediaId;
    private boolean openedToComments;
    private boolean openedToPing;
    private WordpressFormat format;
    private boolean sticky;
    private List<Long> categoryIds;
    private List<Long> tagIds;

    public WordpressPost() {
        this.categoryIds = new ArrayList<>();
        this.tagIds = new ArrayList<>();
    }

    public LocalDateTime getDate() {
        return date;
    }

    public void setDate(LocalDateTime date) {
        this.date = date;
    }

    public LocalDateTime getDateGmt() {
        return dateGmt;
    }

    public void setDateGmt(LocalDateTime dateGmt) {
        this.dateGmt = dateGmt;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getSlug() {
        return slug;
    }

    public void setSlug(String slug) {
        this.slug = slug;
    }

    public WordpressStatus getStatus() {
        return status;
    }

    public void setStatus(WordpressStatus status) {
        this.status = status;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public Long getAuthorId() {
        return authorId;
    }

    public void setAuthor(Long authorId) {
        this.authorId = authorId;
    }

    public String getExcerpt() {
        return excerpt;
    }

    public void setExcerpt(String excerpt) {
        this.excerpt = excerpt;
    }

    public Long getFeaturedMediaId() {
        return featuredMediaId;
    }

    public void setFeaturedMediaId(Long featuredMediaId) {
        this.featuredMediaId = featuredMediaId;
    }

    public boolean isOpenedToComments() {
        return openedToComments;
    }

    public void setOpenedToComments(boolean openedToComments) {
        this.openedToComments = openedToComments;
    }

    public boolean isOpenedToPing() {
        return openedToPing;
    }

    public void setOpenedToPing(boolean openedToPing) {
        this.openedToPing = openedToPing;
    }

    public WordpressFormat getFormat() {
        return format;
    }

    public void setFormat(WordpressFormat format) {
        this.format = format;
    }

    public boolean isSticky() {
        return sticky;
    }

    public void setSticky(boolean sticky) {
        this.sticky = sticky;
    }

    public List<Long> getCategoryIds() {
        return categoryIds;
    }

    public List<Long> getTagIds() {
        return tagIds;
    }

}
