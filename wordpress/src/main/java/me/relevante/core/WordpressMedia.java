package me.relevante.core;

import java.time.LocalDateTime;

/**
 * @author daniel-ibanez
 */
public class WordpressMedia {

    private Long id;
    private String renderedGuid;
    private String link;
    private LocalDateTime date;
    private LocalDateTime dateGmt;
    private LocalDateTime modified;
    private LocalDateTime modifiedGmt;
    private String slug;
    private String renderedTitle;
    private String content;
    private Long authorId;
    private boolean openedToComments;
    private boolean openedToPing;
    private String altText;
    private String caption;
    private String description;
    private String mediaType;
    private String mimeType;
    private Long postId;
    private String sourceUrl;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getRenderedGuid() {
        return renderedGuid;
    }

    public void setRenderedGuid(String renderedGuid) {
        this.renderedGuid = renderedGuid;
    }

    public String getLink() {
        return link;
    }

    public void setLink(String link) {
        this.link = link;
    }

    public LocalDateTime getDate() {
        return date;
    }

    public void setDate(LocalDateTime date) {
        this.date = date;
    }

    public LocalDateTime getDateGmt() {
        return dateGmt;
    }

    public void setDateGmt(LocalDateTime dateGmt) {
        this.dateGmt = dateGmt;
    }

    public LocalDateTime getModified() {
        return modified;
    }

    public void setModified(LocalDateTime modified) {
        this.modified = modified;
    }

    public LocalDateTime getModifiedGmt() {
        return modifiedGmt;
    }

    public void setModifiedGmt(LocalDateTime modifiedGmt) {
        this.modifiedGmt = modifiedGmt;
    }

    public String getSlug() {
        return slug;
    }

    public void setSlug(String slug) {
        this.slug = slug;
    }

    public String getRenderedTitle() {
        return renderedTitle;
    }

    public void setRenderedTitle(String renderedTitle) {
        this.renderedTitle = renderedTitle;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public Long getAuthorId() {
        return authorId;
    }

    public void setAuthorId(Long authorId) {
        this.authorId = authorId;
    }

    public boolean isOpenedToComments() {
        return openedToComments;
    }

    public void setOpenedToComments(boolean openedToComments) {
        this.openedToComments = openedToComments;
    }

    public boolean isOpenedToPing() {
        return openedToPing;
    }

    public void setOpenedToPing(boolean openedToPing) {
        this.openedToPing = openedToPing;
    }

    public String getAltText() {
        return altText;
    }

    public void setAltText(String altText) {
        this.altText = altText;
    }

    public String getCaption() {
        return caption;
    }

    public void setCaption(String caption) {
        this.caption = caption;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getMediaType() {
        return mediaType;
    }

    public void setMediaType(String mediaType) {
        this.mediaType = mediaType;
    }

    public String getMimeType() {
        return mimeType;
    }

    public void setMimeType(String mimeType) {
        this.mimeType = mimeType;
    }

    public Long getPostId() {
        return postId;
    }

    public void setPostId(Long postId) {
        this.postId = postId;
    }

    public String getSourceUrl() {
        return sourceUrl;
    }

    public void setSourceUrl(String sourceUrl) {
        this.sourceUrl = sourceUrl;
    }
}
