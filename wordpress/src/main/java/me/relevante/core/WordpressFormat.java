package me.relevante.core;

/**
 * Created by daniel-ibanez on 3/08/16.
 */
public enum WordpressFormat {

    STANDARD("standard"),
    ASIDE("aside"),
    CHAT("chat"),
    GALLERY("gallery"),
    LINK("link"),
    IMAGE("image"),
    QUOTE("quote"),
    STATUS("status"),
    VIDEO("video"),
    AUDIO("audio");

    private String name;

    WordpressFormat(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }
}
