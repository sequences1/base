package me.relevante.core;

import org.springframework.stereotype.Component;

@Component
public class Wordpress extends AbstractNetwork implements Network {

    private static final String NAME = "wordpress";
    private static Wordpress instance = new Wordpress();

    public static Wordpress getInstance() {
        return instance;
    }

    private Wordpress() {
        super(NAME);
    }

}
