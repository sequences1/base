package me.relevante.core;

/**
 * Created by daniel-ibanez on 3/08/16.
 */
public enum WordpressStatus {
    PUBLISH("publish"),
    FUTURE("future"),
    DRAFT("draft"),
    PENDING("pending"),
    PRIVATE("private");

    private String name;

    WordpressStatus(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public static WordpressStatus fromName(String name) {
        for (WordpressStatus value : values()) {
            if (value.getName().equals(name)) {
                return value;
            }
        }
        return null;
    }
}
