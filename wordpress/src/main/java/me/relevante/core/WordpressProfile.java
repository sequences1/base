package me.relevante.core;

import org.apache.commons.lang3.StringUtils;

/**
 * Created by daniel-ibanez on 19/08/16.
 */
public class WordpressProfile implements NetworkProfile<Wordpress, WordpressProfile> {

    private String id;
    private Long localId;
    private String name;
    private String url;
    private String description;
    private String link;
    private String slug;

    @Override
    public String getId() {
        return String.valueOf(id);
    }

    @Override
    public Wordpress getNetwork() {
        return Wordpress.getInstance();
    }

    @Override
    public String getNlpAnalyzableContent() {
        return name;
    }

    @Override
    public void completeMissingDataWith(final WordpressProfile profile) {
        if (StringUtils.isBlank(this.id)) this.id = profile.id;
        if (this.localId == null) this.localId = profile.localId;
        if (StringUtils.isBlank(this.name)) this.name = profile.name;
        if (StringUtils.isBlank(this.url)) this.url = profile.url;
        if (StringUtils.isBlank(this.description)) this.description = profile.description;
        if (StringUtils.isBlank(this.link)) this.link = profile.link;
        if (StringUtils.isBlank(this.slug)) this.slug = profile.slug;
    }

    @Override
    public void updateWithExistingDataIn(final WordpressProfile profile) {
        if (StringUtils.isNotBlank(profile.id)) this.id = profile.id;
        if (profile.localId != null) this.localId = profile.localId;
        if (StringUtils.isNotBlank(profile.name)) this.name = profile.name;
        if (StringUtils.isNotBlank(profile.url)) this.url = profile.url;
        if (StringUtils.isNotBlank(profile.description)) this.description = profile.description;
        if (StringUtils.isNotBlank(profile.link)) this.link = profile.link;
        if (StringUtils.isNotBlank(profile.slug)) this.slug = profile.slug;
    }

    @Override
    public void replaceDataWith(final WordpressProfile profile) {
        this.id = profile.id;
        this.localId = profile.localId;
        this.name = profile.name;
        this.url = profile.url;
        this.description = profile.description;
        this.link = profile.link;
        this.slug = profile.slug;
    }

    public void setId(String id) {
        this.id = id;
    }

    public Long getLocalId() {
        return localId;
    }

    public void setLocalId(Long localId) {
        this.localId = localId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getLink() {
        return link;
    }

    public void setLink(String link) {
        this.link = link;
    }

    public String getSlug() {
        return slug;
    }

    public void setSlug(String slug) {
        this.slug = slug;
    }

}
