package me.relevante.core;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

/**
 * Created by daniel-ibanez on 24/08/16.
 */
@Document(collection = "WordpressHub")
public class WordpressHub {

    @Id
    private String id;
    private String url;

    public WordpressHub() {
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }
}
