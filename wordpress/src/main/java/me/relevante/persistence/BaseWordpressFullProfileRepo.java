package me.relevante.persistence;

import me.relevante.core.WordpressFullProfile;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.data.repository.NoRepositoryBean;

/**
 * @author daniel-ibanez
 */
@NoRepositoryBean
public interface BaseWordpressFullProfileRepo extends MongoRepository<WordpressFullProfile, String> {
    WordpressFullProfile findOneByProfileLink(String profileLink);
}
