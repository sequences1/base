package me.relevante.integration;

import me.relevante.core.UniqueProfile;
import me.relevante.core.Wordpress;
import me.relevante.core.WordpressFullProfile;
import me.relevante.persistence.BaseWordpressFullProfileRepo;
import me.relevante.queue.QueueService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Component;

@Component
public class WordpressProfileIntegrator
        extends AbstractNetworkProfileIntegrator<Wordpress, WordpressFullProfile>
        implements NetworkProfileIntegrator<Wordpress, WordpressFullProfile> {

    private BaseWordpressFullProfileRepo fullProfileRepo;

    @Autowired
    public WordpressProfileIntegrator(
            final CrudRepository<UniqueProfile, String> uniqueProfileRepo,
            final BaseWordpressFullProfileRepo fullProfileRepo,
            final QueueService queueService) {
        super(uniqueProfileRepo, fullProfileRepo, queueService);
        this.fullProfileRepo = fullProfileRepo;
    }

    @Override
    protected WordpressFullProfile findAlreadyExistingProfile(final WordpressFullProfile fullProfile) {
        return fullProfileRepo.findOneByProfileLink(fullProfile.getProfile().getLink());
    }

    @Override
    public Wordpress getNetwork() {
        return Wordpress.getInstance();
    }
}
