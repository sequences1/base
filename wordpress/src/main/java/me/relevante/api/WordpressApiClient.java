package me.relevante.api;

import me.relevante.core.WordpressCategory;
import me.relevante.core.WordpressMedia;
import me.relevante.core.WordpressPost;
import me.relevante.core.WordpressProfile;
import me.relevante.core.WordpressTag;
import me.relevante.core.Wordpress;

import java.util.List;

public interface WordpressApiClient extends NetworkApiClient<Wordpress, WordpressProfile> {

    void createPost(WordpressPost post) throws WordpressApiException;
    void createPostByLanguage(WordpressPost post, String language) throws WordpressApiException;
    WordpressMedia uploadImage(String imageUrl) throws WordpressApiException;
    List<WordpressProfile> getProfiles() throws WordpressApiException;
    List<WordpressCategory> getCategories() throws WordpressApiException;
    List<WordpressCategory> getCategoriesByLanguage(String language) throws WordpressApiException;
    List<WordpressTag> getTags() throws WordpressApiException;
}
