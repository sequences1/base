package me.relevante.api;

public class WordpressApiException extends RuntimeException {

    public WordpressApiException() {
        super();
    }

    public WordpressApiException(String message) {
        super(message);
    }

    public WordpressApiException(Throwable cause) {
        super(cause);
    }
}
