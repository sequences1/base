package me.relevante.api;

import me.relevante.auth.BasicAuthPair;
import me.relevante.core.WordpressCategory;
import me.relevante.core.WordpressMedia;
import me.relevante.core.WordpressPost;
import me.relevante.core.WordpressProfile;
import me.relevante.core.WordpressTag;
import me.relevante.core.Wordpress;
import org.apache.commons.io.IOUtils;
import org.apache.commons.lang3.StringEscapeUtils;
import org.apache.http.HttpEntity;
import org.apache.http.HttpHeaders;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.ByteArrayEntity;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.HttpClients;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.InputStream;
import java.io.StringWriter;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Iterator;
import java.util.List;

public class WordpressApiClientImpl implements WordpressApiClient {

    private static final Logger logger = LoggerFactory.getLogger(WordpressApiClientImpl.class);
    private static final String API_URL = "/wp-json/wp/v2";
    private static final String CATEGORIES_URL = API_URL + "/categories?lang=%s";
    private static final String MEDIA_URL = API_URL + "/media";
    private static final String TAGS_URL = API_URL + "/tags";
    private static final String POSTS_URL = API_URL + "/posts";
    private static final String PROFILES_URL = API_URL + "/users";

    private static final List<String> languages = Arrays.asList("en", "es");

    private String hubUrl;
    private String username;
    private BasicAuthPair authentication;

    public WordpressApiClientImpl(String hubUrl,
                                  String username,
                                  String password) {
        this.hubUrl = hubUrl;
        this.username = username;
        authentication = new BasicAuthPair(username, password);
    }

    @Override
    public Wordpress getNetwork() {
        return Wordpress.getInstance();
    }

    @Override
    public WordpressProfile getProfile() {
        List<WordpressProfile> profiles = getProfiles();
        for (WordpressProfile profile : profiles) {
            if (profile.getName().equals(username)) {
                return profile;
            }
        }
        return null;
    }

    @Override
    public void createPost(WordpressPost post) throws WordpressApiException {

        JSONObject jsonObject = convertPostToJsonPost(post);
        try {
            executeWithJSONObject(hubUrl + POSTS_URL, jsonObject);
        } catch (Exception e) {
            throw new WordpressApiException(e);
        }
    }

    @Override
    public void createPostByLanguage(final WordpressPost post,
                                     final String language) throws WordpressApiException {

        JSONObject jsonObject = convertPostToJsonPost(post);
        try {
            executeWithJSONObject(hubUrl + POSTS_URL + "?lang=" + language, jsonObject);
        } catch (Exception e) {
            throw new WordpressApiException(e);
        }
    }

    @Override
    public WordpressMedia uploadImage(String imageUrl) throws WordpressApiException {
        try {
            JSONObject jsonObject = uploadImageToUrl(hubUrl + MEDIA_URL, imageUrl);
            WordpressMedia media = convertJsonMediaToMedia(jsonObject);
            return media;
        } catch (Exception e) {
            throw new WordpressApiException(e);
        }
    }

    @Override
    public List<WordpressProfile> getProfiles() throws WordpressApiException {
        JSONArray jsonArray;
        try {
            jsonArray = getJSONArray(hubUrl + PROFILES_URL);
        } catch (Exception e) {
            throw new WordpressApiException(e);
        }
        List<WordpressProfile> profiles = new ArrayList<>();
        Iterator iterator = jsonArray.iterator();
        while (iterator.hasNext()) {
            JSONObject jsonObject = (JSONObject) iterator.next();
            WordpressProfile profile = convertJsonProfileToProfile(jsonObject);
            profiles.add(profile);
        }
        return profiles;
    }

    @Override
    public List<WordpressCategory> getCategories() throws WordpressApiException {
        final List<WordpressCategory> allLanguagesCategories = new ArrayList<>();
        for (final String language : languages) {
            allLanguagesCategories.addAll(getCategoriesByLanguage(language));
        }
        return allLanguagesCategories;
    }

    @Override
    public List<WordpressCategory> getCategoriesByLanguage(final String language) throws WordpressApiException {
        final JSONArray jsonArray;
        final String categoriesByLanguageUrl = String.format(hubUrl + CATEGORIES_URL, language);
        try {
            jsonArray = getJSONArray(categoriesByLanguageUrl);
        } catch (Exception e) {
            throw new WordpressApiException(e);
        }
        final List<WordpressCategory> categories = new ArrayList<>();
        final Iterator iterator = jsonArray.iterator();
        while (iterator.hasNext()) {
            final JSONObject jsonCategory = (JSONObject) iterator.next();
            final WordpressCategory category = convertJsonCategoryToCategory(jsonCategory);
            category.setLanguage(language);
            categories.add(category);
        }
        return categories;
    }

    @Override
    public List<WordpressTag> getTags() throws WordpressApiException {
        JSONArray jsonArray;
        try {
            jsonArray = getJSONArray(hubUrl + TAGS_URL);
        } catch (Exception e) {
            throw new WordpressApiException(e);
        }
        List<WordpressTag> tags = new ArrayList<>();
        Iterator iterator = jsonArray.iterator();
        while (iterator.hasNext()) {
            JSONObject jsonTag = (JSONObject) iterator.next();
            WordpressTag tag = convertJsonTagToTag(jsonTag);
            tags.add(tag);
        }
        return tags;
    }


    private HttpResponse executeWithJSONObject(String url,
                                               JSONObject jsonObject) throws Exception {

        HttpClient httpClient = HttpClients.createDefault();
        HttpPost httpPost = new HttpPost(url);

        httpPost.addHeader(HttpHeaders.AUTHORIZATION, authentication.getAuthHeaderValue());
        httpPost.addHeader("Content-Type", "application/json; charset=UTF-8");
        httpPost.setEntity(new StringEntity(jsonObject.toString(), "UTF-8"));

        HttpResponse response = httpClient.execute(httpPost);

        return response;
    }

    private JSONObject uploadImageToUrl(String url,
                                        String imageUrl) throws Exception {

        HttpClient httpClient = HttpClients.createDefault();
        HttpPost httpPost = new HttpPost(url);

        HttpResponse resp = httpClient.execute(new HttpGet(imageUrl));
        byte[] bytes = IOUtils.toByteArray(resp.getEntity().getContent());
        ByteArrayEntity entity = new ByteArrayEntity(bytes);

        httpPost.addHeader(HttpHeaders.AUTHORIZATION, authentication.getAuthHeaderValue());
        httpPost.addHeader("Content-Disposition", "attachment; filename=\"" + imageUrl + "\"");
        httpPost.addHeader(HttpHeaders.CONTENT_TYPE, "application/json; charset=UTF-8");
        httpPost.setEntity(entity);

        HttpResponse response = httpClient.execute(httpPost);
        JSONObject jsonObject = getJsonObjectFromResponse(response);
        return jsonObject;
    }

    private JSONObject getJSONObject(String url) throws Exception {

        HttpGet httpGet = createHttpGet(url);
        HttpResponse response = HttpClients.createDefault().execute(httpGet);
        JSONObject jsonObject = getJsonObjectFromResponse(response);
        return jsonObject;
    }

    private JSONArray getJSONArray(String url) throws Exception {

        JSONArray jsonArray;
        int page = 1;
        do {
            jsonArray = getJSONArrayByPage(url, page++);
        } while (jsonArray.isEmpty());
        return jsonArray;
    }

    private JSONArray getJSONArrayByPage(String url, int page) throws Exception {

        String pagedUrl = url + (url.contains("?") ? "&" : "?") + "page=" + page + "&per_page=100";
        HttpGet httpGet = createHttpGet(pagedUrl);
        HttpResponse response = HttpClients.createDefault().execute(httpGet);
        JSONArray jsonArray = getJsonArrayFromResponse(response);
        return jsonArray;
    }

    private HttpGet createHttpGet(String url) {

        HttpGet httpGet = new HttpGet(url);
        httpGet.addHeader(HttpHeaders.AUTHORIZATION, authentication.getAuthHeaderValue());
        httpGet.addHeader(HttpHeaders.CONTENT_TYPE, "application/json; charset=UTF-8");

        return httpGet;
    }

    private JSONArray getJsonArrayFromResponse(HttpResponse response) throws Exception {
        HttpEntity entity = response.getEntity();
        InputStream inputStream = entity.getContent();
        StringWriter writer = new StringWriter();
        IOUtils.copy(inputStream, writer, "UTF-8");
        JSONArray jsonArray = (JSONArray) new JSONParser().parse(writer.toString());
        return jsonArray;
    }

    private JSONObject getJsonObjectFromResponse(HttpResponse response) throws Exception {
        HttpEntity entity = response.getEntity();
        InputStream inputStream = entity.getContent();
        StringWriter writer = new StringWriter();
        IOUtils.copy(inputStream, writer, "UTF-8");
        JSONObject jsonObject = (JSONObject) new JSONParser().parse(writer.toString());
        return jsonObject;
    }

    private JSONObject convertPostToJsonPost(WordpressPost post) {
        JSONObject jsonObject = new JSONObject();
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd'T'HH:mm:ss'Z'");
        jsonObject.put("date", post.getDate().format(formatter));
        jsonObject.put("date_gmt", post.getDateGmt().format(formatter));
        jsonObject.put("status", post.getStatus().getName());
        jsonObject.put("title", post.getTitle());
        jsonObject.put("content", post.getContent());
        jsonObject.put("author", post.getAuthorId());
        jsonObject.put("excerpt", post.getExcerpt());
        jsonObject.put("comment_status", post.isOpenedToComments() ? "open" : "closed");
        jsonObject.put("ping_status", post.isOpenedToPing() ? "open" : "closed");
        jsonObject.put("format", post.getFormat().getName());
        jsonObject.put("sticky", post.isSticky());
        jsonObject.put("featured_media", post.getFeaturedMediaId());
        JSONArray jsonCategories = new JSONArray();
        post.getCategoryIds().forEach(category -> jsonCategories.add(category));
        jsonObject.put("categories", jsonCategories);
        JSONArray jsonTags = new JSONArray();
        post.getTagIds().forEach(tag -> jsonTags.add(tag));
        jsonObject.put("tags", jsonTags);
        return jsonObject;
    }

    private WordpressProfile convertJsonProfileToProfile(JSONObject jsonObject) {
        WordpressProfile profile = new WordpressProfile();
        profile.setName(StringEscapeUtils.unescapeHtml4((String) jsonObject.get("name")));
        profile.setUrl(hubUrl);
        profile.setDescription(StringEscapeUtils.unescapeHtml4((String) jsonObject.get("description")));
        profile.setLink((String) jsonObject.get("link"));
        profile.setId(composeUrlBasedId(profile.getLink()));
        profile.setLocalId((Long) jsonObject.get("id"));
        profile.setSlug((String) jsonObject.get("slug"));
        return profile;
    }

    private WordpressMedia convertJsonMediaToMedia(JSONObject jsonObject) {
        WordpressMedia media = new WordpressMedia();
        media.setId((Long) jsonObject.get("id"));
        JSONObject guid = (JSONObject) jsonObject.get("guid");
        media.setRenderedTitle(guid != null ? (String) guid.get("rendered") : null);
        media.setLink((String) jsonObject.get("link"));
        media.setDate(parseTimestamp((String) jsonObject.get("date")));
        media.setDateGmt(parseTimestamp((String) jsonObject.get("date_gmt")));
        media.setModified(parseTimestamp((String) jsonObject.get("modified")));
        media.setModifiedGmt(parseTimestamp((String) jsonObject.get("modified_gmt")));
        media.setSlug((String) jsonObject.get("slug"));
        JSONObject title = (JSONObject) jsonObject.get("title");
        media.setRenderedTitle(title != null ? (String) title.get("rendered") : null);
        media.setContent((String) jsonObject.get("content"));
        media.setAuthorId((Long) jsonObject.get("author"));
        String commentStatus = (String) jsonObject.get("comment_status");
        media.setOpenedToComments(((commentStatus != null) && (commentStatus.equals("open"))));
        String pingStatus = (String) jsonObject.get("ping_status");
        media.setOpenedToPing(((pingStatus != null) && (pingStatus.equals("open"))));
        media.setAltText((String) jsonObject.get("alt_text"));
        media.setCaption((String) jsonObject.get("caption"));
        media.setDescription((String) jsonObject.get("description"));
        media.setMediaType((String) jsonObject.get("media_type"));
        media.setMimeType((String) jsonObject.get("mime_type"));
        media.setPostId((Long) jsonObject.get("post"));
        media.setSourceUrl((String) jsonObject.get("source_url"));
        return media;
    }

    private WordpressCategory convertJsonCategoryToCategory(JSONObject jsonObject) {
        WordpressCategory category = new WordpressCategory();
        category.setId((Long) jsonObject.get("id"));
        category.setCount((Long) jsonObject.get("count"));
        category.setDescription(StringEscapeUtils.unescapeHtml4((String) jsonObject.get("description")));
        category.setLink((String) jsonObject.get("link"));
        category.setName(StringEscapeUtils.unescapeHtml4((String) jsonObject.get("name")));
        category.setSlug((String) jsonObject.get("slug"));
        category.setTaxonomy((String) jsonObject.get("taxonomy"));
        category.setParent((Long) jsonObject.get("parent"));
        return category;
    }

    private WordpressTag convertJsonTagToTag(JSONObject jsonObject) {
        WordpressTag tag = new WordpressTag();
        tag.setId((Long) jsonObject.get("id"));
        tag.setCount((Long) jsonObject.get("count"));
        tag.setDescription(StringEscapeUtils.unescapeHtml4((String) jsonObject.get("description")));
        tag.setLink((String) jsonObject.get("link"));
        tag.setName(StringEscapeUtils.unescapeHtml4((String) jsonObject.get("name")));
        tag.setSlug((String) jsonObject.get("slug"));
        tag.setTaxonomy((String) jsonObject.get("taxonomy"));
        return tag;
    }

    private String composeUrlBasedId(String url) {

        int protocolEndIndex = url.indexOf(":");
        if (protocolEndIndex < 0) {
            throw new IllegalArgumentException("URL not valid");
        }
        String urlId = url.substring(protocolEndIndex + 3);
        return urlId;
    }

    private LocalDateTime parseTimestamp(String timestamp) {
        if (timestamp == null) {
            return null;
        }
        LocalDateTime localDateTime = LocalDateTime.parse(timestamp, DateTimeFormatter.ISO_DATE_TIME);
        return localDateTime;
    }
}
