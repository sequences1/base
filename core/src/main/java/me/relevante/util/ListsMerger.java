package me.relevante.util;

import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

@Component
public class ListsMerger<T> {

    public List<T> merge(List<? extends T> first,
                         List<? extends T> second) {
        List<T> mergedList = new ArrayList<>();
        mergedList.addAll(first);
        mergedList.addAll(second);
        return mergedList;
    }

    public List<T> mergeAlternatingElements(int maxElements,
                                            Collection<List<? extends T>> lists) {

        return mergeAlternatingElements(0, maxElements, lists);
    }

    public List<T> mergeAlternatingElements(int startingIndex,
                                            int maxElements,
                                            Collection<List<? extends T>> lists) {

        List<T> mergedList = mergeAlternatingElements(lists);
        int fromIndex = startingIndex;
        int toIndex = Math.min(startingIndex + maxElements, mergedList.size());
        List<T> slicedList = mergedList.subList(fromIndex, toIndex);

        return slicedList;
    }

    public List<T> mergeAlternatingElements(Collection<List<? extends T>> lists) {

        List<T> mergedList = new ArrayList<>();
        int longestListSize = 0;
        for (List<? extends T> list : lists) {
            if (list.size() > longestListSize) {
                longestListSize = list.size();
            }
        }
        for (int i = 0; i < longestListSize; i++) {
            for (List<? extends T> list : lists) {
                if (i < list.size()) {
                    mergedList.add(list.get(i));
                }
            }
        }
        return mergedList;
    }


}
