package me.relevante.util.validation;

/**
 * Created by daniel-ibanez on 21/07/16.
 */
public interface Validator<T> {
    boolean isValid(T objectToValidate);
}
