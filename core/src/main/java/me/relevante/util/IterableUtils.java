package me.relevante.util;

import java.util.ArrayList;
import java.util.List;

/**
 * @author Daniel Ibanez
 */
public class IterableUtils {

    public static <T> List<T> toList(Iterable<T> iterable) {
        List<T> collection = new ArrayList<>();
        iterable.forEach(object -> collection.add(object));
        return collection;
    }

}
