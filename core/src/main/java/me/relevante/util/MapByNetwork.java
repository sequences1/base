package me.relevante.util;

import me.relevante.core.NetworkEntity;

import java.util.Collection;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by daniel-ibanez on 18/07/16.
 */
public class MapByNetwork {

    public static <T extends NetworkEntity> Map<String, T> from(final Collection<T> collection) {
        final Map<String, T> map = new HashMap<>();
        collection.forEach(item -> map.put(item.getNetwork().getName(), item));
        return map;
    }
}
