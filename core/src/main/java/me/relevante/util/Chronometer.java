package me.relevante.util;

import org.springframework.stereotype.Component;

import java.util.concurrent.TimeUnit;

@Component
public class Chronometer {

    private long startTimeInNanoSeconds;

    public Chronometer() {
        initialize();
    }

    public void initialize() {
        this.startTimeInNanoSeconds = System.nanoTime();
    }

    public String getElapsedTimeText() {
        return getElapsedTimeText(TimeUnit.MILLISECONDS);
    }

    public String getElapsedTimeText(final TimeUnit timeUnit) {
        final long endTimeInNanoSeconds = System.nanoTime();
        final long elapsedNanoSeconds = endTimeInNanoSeconds - startTimeInNanoSeconds;
        return timeUnit.convert(elapsedNanoSeconds, TimeUnit.NANOSECONDS) + " " + timeUnit.toString().toLowerCase();
    }

}
