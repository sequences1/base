package me.relevante.persistence;

import me.relevante.core.NetworkEngagePostAction;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.data.repository.NoRepositoryBean;

import java.util.List;

/**
 * @author daniel-ibanez
 */
@NoRepositoryBean
public interface NetworkPostActionRepo<A extends NetworkEngagePostAction> extends MongoRepository<A, String> {
    A findOneByTargetPostIdAndAuthorId(String postId, String authorId);
    List<A> findByAuthorId(String authorId);
}
