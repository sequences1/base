package me.relevante.persistence;

import me.relevante.core.NetworkFullPost;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.data.repository.NoRepositoryBean;

import java.util.Collection;
import java.util.List;

/**
 * @author daniel-ibanez
 */
@NoRepositoryBean
public interface NetworkFullPostRepo<P extends NetworkFullPost> extends MongoRepository<P, String> {
    List<P> findByPostAuthorId(String authorId);
    List<P> findByIdIn(Collection<String> ids);
}
