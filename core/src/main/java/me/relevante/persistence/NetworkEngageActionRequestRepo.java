package me.relevante.persistence;

import me.relevante.request.NetworkActionRequest;
import org.springframework.data.repository.NoRepositoryBean;

import java.util.List;

/**
 * @author daniel-ibanez
 */
@NoRepositoryBean
public interface NetworkEngageActionRequestRepo<A extends NetworkActionRequest> extends NetworkActionRequestRepo<A> {
    List<A> findByRequesterRelevanteIdAndTargetProfileId(String requesterRelevanteId, String targetProfileId);
    A findOneByRequesterRelevanteIdAndTargetProfileId(String requesterRelevanteId, String targetProfileId);
}
