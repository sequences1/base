package me.relevante.persistence;

import me.relevante.request.NetworkActionRequest;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.data.repository.NoRepositoryBean;

import java.util.List;

/**
 * @author daniel-ibanez
 */
@NoRepositoryBean
public interface NetworkActionRequestRepo<A extends NetworkActionRequest> extends MongoRepository<A, String> {
    List<A> findByRequesterRelevanteIdAndStatus(String requesterRelevanteId, String status);
}
