package me.relevante.persistence;

import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.data.repository.NoRepositoryBean;

import java.util.Collection;
import java.util.List;

/**
 * @author daniel-ibanez
 */
@NoRepositoryBean
public interface NetworkFindByIdsRepo<T> extends MongoRepository<T, String> {
    List<T> findByIdIn(Collection<String> ids);
}
