package me.relevante.guesser;

import org.apache.commons.lang3.StringUtils;

public class Clues {

    private String name;
    private String surname;
    private String company;
    private String domain;
    private String email;

    public Clues() {
    }

    public boolean hasDomain() {
        return StringUtils.isNotBlank(domain);
    }

    public String getName() {
        return name;
    }

    public String getSurname() {
        return surname;
    }

    public String getCompany() {
        return company;
    }

    public String getDomain() {
        return domain;
    }

    public String getEmail() {
        return email;
    }

    public Clues withName(final String name, final String surname) {
        this.name = name;
        this.surname = surname;
        return this;
    }

    public Clues withCompany(final String company) {
        this.company = company;
        return this;
    }

    public Clues withDomain(final String domain) {
        this.domain = domain;
        return this;
    }

    public Clues withEmail(final String email) {
        this.email = email;
        return this;
    }

    @Override
    public String toString() {
        return "Clues{" +
                "name='" + name + '\'' +
                ", surname='" + surname + '\'' +
                ", company='" + company + '\'' +
                ", domain='" + domain + '\'' +
                ", email='" + email + '\'' +
                '}';
    }
}
