package me.relevante.guesser.profile;

public class GuessedProfile {

    private final String type;
    private final String username;
    private final String url;

    public GuessedProfile(final String type,
                          final String username,
                          final String url) {
        this.type = type;
        this.username = username;
        this.url = url;
    }

    public String getType() {
        return type;
    }

    public String getUsername() {
        return username;
    }

    public String getUrl() {
        return url;
    }


}
