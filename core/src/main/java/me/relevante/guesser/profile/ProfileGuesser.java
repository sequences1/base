package me.relevante.guesser.profile;

import me.relevante.guesser.Clues;

import java.util.List;

public interface ProfileGuesser {
    List<GuessedProfile> guess(Clues clues);
}
