package me.relevante.guesser;

public interface GuesserProvider {
    String getName();
}
