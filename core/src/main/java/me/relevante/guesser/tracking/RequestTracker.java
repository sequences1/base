package me.relevante.guesser.tracking;

import me.relevante.guesser.GuesserProvider;

public interface RequestTracker {

    void track(GuesserProvider provider, String message);

}
