package me.relevante.guesser.domain;

import me.relevante.guesser.Clues;

import java.util.List;

public interface DomainGuesser {
    List<GuessedDomain> guess(Clues clues);
}
