package me.relevante.guesser.domain;

public class GuessedDomain {

    private final String domain;
    private final long score;

    public GuessedDomain(final String domain,
                         final long score) {
        this.domain = domain;
        this.score = score;
    }

    public String getDomain() {
        return domain;
    }

    public long getScore() {
        return score;
    }
}
