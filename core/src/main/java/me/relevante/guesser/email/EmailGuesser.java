package me.relevante.guesser.email;

import me.relevante.guesser.Clues;

import java.util.List;

public interface EmailGuesser {
    List<GuessedEmail> guess(Clues clues);
    List<GuessedEmailWithProfiles> guessWithProfiles(Clues clues);
}
