package me.relevante.guesser.email;

public final class GuessedEmail {

    private final String email;
    private final int score;

    public GuessedEmail(final String email,
                        final int score) {
        this.email = email;
        this.score = score;
    }

    public String getEmail() {
        return email;
    }

    public int getScore() {
        return score;
    }
}
