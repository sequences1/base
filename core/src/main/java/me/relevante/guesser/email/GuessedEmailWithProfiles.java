package me.relevante.guesser.email;

import me.relevante.guesser.profile.GuessedProfile;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

public final class GuessedEmailWithProfiles {

    private final String email;
    private final int score;
    private List<GuessedProfile> guessedProfiles;

    public GuessedEmailWithProfiles(final String email,
                                    final int score,
                                    final Collection<GuessedProfile> guessedProfiles) {
        this.email = email;
        this.score = score;
        this.guessedProfiles = new ArrayList<>(guessedProfiles);
    }

    public String getEmail() {
        return email;
    }

    public int getScore() {
        return score;
    }

    public List<GuessedProfile> getGuessedProfiles() {
        return new ArrayList<>(guessedProfiles);
    }
}
