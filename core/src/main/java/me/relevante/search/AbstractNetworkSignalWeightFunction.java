package me.relevante.search;

import me.relevante.core.Network;
import me.relevante.core.NetworkSignal;

import java.util.Map;
import java.util.function.Function;

public abstract class AbstractNetworkSignalWeightFunction<N extends Network> implements NetworkSignalWeightFunction<N> {

    private final Map<Class<? extends NetworkSignal<N>>, Function<Double, Double>> miscellaneaWeightFunctionsBySignal;
    private final Map<Class<? extends NetworkSignal<N>>, Function<Double, Double>> professionalsWeightFunctionsBySignal;
    private final Map<Class<? extends NetworkSignal<N>>, Function<Double, Double>> prospectsWeightFunctionsBySignal;

    public AbstractNetworkSignalWeightFunction() {
        this.miscellaneaWeightFunctionsBySignal = createMiscellaneaWeightFunctionsBySignal();
        this.professionalsWeightFunctionsBySignal = createProfessionalsWeightFunctionsBySignal();
        this.prospectsWeightFunctionsBySignal = createProspectsWeightFunctionsBySignal();
    }

    @Override
    public double getSignalWeight(final Class<? extends NetworkSignal<N>> clazz,
                                  final double signalsNumber,
                                  final SearchGoal searchGoal) {
        final Map<Class<? extends NetworkSignal<N>>, Function<Double, Double>> weightFunctionsBySignal = getWeightFunctionsBySearchGoal(searchGoal);
        return weightFunctionsBySignal.get(clazz).apply(signalsNumber);
    }

    private Map<Class<? extends NetworkSignal<N>>, Function<Double, Double>> getWeightFunctionsBySearchGoal(final SearchGoal searchGoal) {
        switch (searchGoal) {
            case PROFESSIONALS:
                return professionalsWeightFunctionsBySignal;
            case PROSPECTS:
                return prospectsWeightFunctionsBySignal;
            case MISCELLANEA:
                return miscellaneaWeightFunctionsBySignal;
            default:
                return miscellaneaWeightFunctionsBySignal;
        }
    }

    protected abstract Map<Class<? extends NetworkSignal<N>>, Function<Double, Double>> createMiscellaneaWeightFunctionsBySignal();

    protected abstract Map<Class<? extends NetworkSignal<N>>, Function<Double, Double>> createProfessionalsWeightFunctionsBySignal();

    protected abstract Map<Class<? extends NetworkSignal<N>>, Function<Double, Double>> createProspectsWeightFunctionsBySignal();
}
