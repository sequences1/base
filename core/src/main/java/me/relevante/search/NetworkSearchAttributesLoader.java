package me.relevante.search;

import me.relevante.core.Network;
import me.relevante.core.NetworkEntity;

/**
 * @author daniel-ibanez
 */
public interface NetworkSearchAttributesLoader<N extends Network> extends NetworkEntity<N> {
    SearchAttributes<N> loadSearchAttributes(String networkProfileId);
}
