package me.relevante.search;

import me.relevante.core.Network;
import me.relevante.core.NetworkEntity;
import me.relevante.core.NetworkSignal;

public interface NetworkSignalWeightFunction<N extends Network> extends NetworkEntity<N> {
    double getSignalWeight(Class<? extends NetworkSignal<N>> clazz, double signalsNumber, SearchGoal searchGoal);
}
