package me.relevante.search;

public enum SearchGoal {

    MISCELLANEA("misc"),
    PROFESSIONALS("professionals"),
    PROSPECTS("prospects");

    private String name;

    SearchGoal(String name) {
        this.name = name;
    }

    public static SearchGoal fromName(String name) {
        if (name == null) {
            return null;
        }
        for (SearchGoal searchGoal : SearchGoal.values()) {
            if (searchGoal.name.equals(name)) {
                return searchGoal;
            }
        }
        return null;
    }

    public String getName() {
        return name;
    }

}
