package me.relevante.search;

import me.relevante.core.Network;
import me.relevante.core.NetworkFullPost;
import me.relevante.core.NetworkFullProfile;

import java.util.List;

public abstract class AbstractNetworkSearchTextExtractor<N extends Network, F extends NetworkFullProfile<N, ?, F>, P extends NetworkFullPost<N, ?, P>>
        implements NetworkSearchTextExtractor<N, F, P> {

    @Override
    public String extractSearchText(final F fullProfile, final List<P> fullPosts) {
        final StringBuilder allSearchText = new StringBuilder();
        allSearchText.append(extractSearchText(fullProfile));
        for (final P fullPost : fullPosts) {
            allSearchText.append(extractSearchText(fullPost));
        }
        return allSearchText.toString();
    }

}
