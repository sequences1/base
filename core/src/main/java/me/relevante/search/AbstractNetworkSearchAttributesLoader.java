package me.relevante.search;

import me.relevante.core.Network;
import me.relevante.core.NetworkFullLoader;
import me.relevante.core.NetworkFullPost;
import me.relevante.core.NetworkFullProfile;
import me.relevante.core.NetworkSignal;
import me.relevante.nlp.NetworkSignalExtractor;

import java.util.List;

public abstract class AbstractNetworkSearchAttributesLoader<N extends Network, F extends NetworkFullProfile<N, ?, F>, P extends NetworkFullPost<N, ?, P>>
        implements NetworkSearchAttributesLoader<N> {

    protected final NetworkFullLoader<N, F, P> fullLoader;
    protected final NetworkSignalExtractor<N, F, P> signalExtractor;
    protected final NetworkSearchTextExtractor<N, F, P> searchTextExtractor;

    public AbstractNetworkSearchAttributesLoader(final NetworkFullLoader<N, F, P> fullLoader,
                                                 final NetworkSignalExtractor<N, F, P> signalExtractor,
                                                 final NetworkSearchTextExtractor<N, F, P> searchTextExtractor) {
        this.fullLoader = fullLoader;
        this.signalExtractor = signalExtractor;
        this.searchTextExtractor = searchTextExtractor;
    }

    @Override
    public SearchAttributes<N> loadSearchAttributes(final String networkProfileId) {
        final F fullProfile = fullLoader.loadFullProfile(networkProfileId);
        final List<P> fullPosts = fullLoader.loadFullPosts(networkProfileId);
        final List<NetworkSignal<N>> allSignals = signalExtractor.extractSignals(fullProfile, fullPosts);
        final String fullText = searchTextExtractor.extractSearchText(fullProfile, fullPosts);
        return createSearchAttributes(fullProfile, allSignals, fullText);
    }

    protected abstract SearchAttributes<N> createSearchAttributes(final F fullProfile, final List<NetworkSignal<N>> signals, final String fullText);
}
