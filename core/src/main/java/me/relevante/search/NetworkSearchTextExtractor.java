package me.relevante.search;

import me.relevante.core.Network;
import me.relevante.core.NetworkEntity;
import me.relevante.core.NetworkFullPost;
import me.relevante.core.NetworkFullProfile;

import java.util.List;

/**
 * @author Daniel Ibanez
 */
public interface NetworkSearchTextExtractor<N extends Network, F extends NetworkFullProfile<N, ?, F>, P extends NetworkFullPost<N, ?, P>>
        extends NetworkEntity<N> {
    String extractSearchText(F fullProfile);
    String extractSearchText(P fullPost);
    String extractSearchText(F fullProfile, List<P> fullPosts);
}
