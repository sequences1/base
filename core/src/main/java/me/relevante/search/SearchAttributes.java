package me.relevante.search;

import me.relevante.core.Network;
import me.relevante.core.NetworkSignal;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

/**
 * @author daniel-ibanez
 */
public class SearchAttributes<N extends Network> {

    private String location;
    private String industry;
    private List<String> companies;
    private String fullText;
    private List<NetworkSignal<N>> signals;

    public SearchAttributes(final String location,
                            final String industry,
                            final Collection<String> companies,
                            final String fullText,
                            final Collection<NetworkSignal<N>> signals) {
        this.location = location;
        this.industry = industry;
        this.companies = new ArrayList<>(companies);
        this.fullText = fullText;
        this.signals = new ArrayList<>(signals);
    }

    public String getLocation() {
        return location;
    }

    public String getIndustry() {
        return industry;
    }

    public List<String> getCompanies() {
        return companies;
    }

    public String getFullText() {
        return fullText;
    }

    public List<NetworkSignal<N>> getSignals() {
        return signals;
    }
}
