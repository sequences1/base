package me.relevante.queue;

/**
 * @author daniel-ibanez
 */
public class RemoveProfileFromIndexMessage implements QueueMessage {

    private String uniqueProfileId;

    private RemoveProfileFromIndexMessage() {
        this(null);
    }

    public RemoveProfileFromIndexMessage(final String uniqueProfileId) {
        this.uniqueProfileId = uniqueProfileId;
    }

    public String getUniqueProfileId() {
        return uniqueProfileId;
    }
}
