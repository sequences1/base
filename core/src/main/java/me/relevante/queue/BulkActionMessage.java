package me.relevante.queue;

import me.relevante.request.NetworkActionRequest;

/**
 * @author daniel-ibanez
 */
public class BulkActionMessage implements QueueMessage {

    private String actionClassName;
    private String requestId;

    private BulkActionMessage() {
        this(null);
    }

    public BulkActionMessage(final NetworkActionRequest networkActionRequest) {
        if (networkActionRequest == null) {
            this.actionClassName = null;
            this.requestId = null;
        } else {
            this.actionClassName = networkActionRequest.getClass().getSimpleName();
            this.requestId = networkActionRequest.getId();
        }
    }

    public String getActionClassName() {
        return actionClassName;
    }

    public String getRequestId() {
        return requestId;
    }

    @Override
    public String toString() {
        return "BulkActionMessage{" +
                "actionClassName='" + actionClassName + '\'' +
                ", requestId='" + requestId + '\'' +
                '}';
    }
}
