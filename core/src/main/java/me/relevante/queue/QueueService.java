package me.relevante.queue;

/**
 * @author Daniel Ibanez
 */
public interface QueueService {

    void sendBulkActionMessage(final BulkActionMessage message);

    void sendDeduplicationMessage(final DeduplicationMessage message);

    void sendEnrichmentMessage(final EnrichmentMessage message);

    void sendProfileIndexMessage(final ProfileIndexMessage message);

    void sendRemoveProfileFromIndexMessage(final RemoveProfileFromIndexMessage message);

    void sendSendEmailMessage(final SendEmailMessage message);
}
