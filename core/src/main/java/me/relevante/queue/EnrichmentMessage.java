package me.relevante.queue;

/**
 * @author daniel-ibanez
 */
public class EnrichmentMessage implements QueueMessage {

    private String requestId;
    private String relevanteId;
    private String targetUniqueProfileId;

    private EnrichmentMessage() {
        this(null, null, null);
    }

    public EnrichmentMessage(final String requestId,
                             final String relevanteId,
                             final String targetUniqueProfileId) {
        this.requestId = requestId;
        this.relevanteId = relevanteId;
        this.targetUniqueProfileId = targetUniqueProfileId;
    }

    public String getRequestId() {
        return requestId;
    }

    public String getRelevanteId() {
        return relevanteId;
    }

    public String getTargetUniqueProfileId() {
        return targetUniqueProfileId;
    }
}
