package me.relevante.queue;

public class SendEmailMessage implements QueueMessage {
	
	private String from;
	private String to;
	private String subject;
	private String content;
	
	private SendEmailMessage() {
        this(null, null, null, null);
    }
	
	public SendEmailMessage(final String from,
                            final String to,
                            final String subject,
                            final String content) {
		this.from = from;
		this.to = to;
		this.subject = subject;
		this.content = content;
	}

    public String getFrom() {
        return from;
    }

    public void setFrom(final String from) {
        this.from = from;
    }

    public String getTo() {
        return to;
    }

    public void setTo(final String to) {
        this.to = to;
    }

    public String getSubject() {
        return subject;
    }

    public void setSubject(final String subject) {
        this.subject = subject;
    }

    public String getContent() {
        return content;
    }

    public void setContent(final String content) {
        this.content = content;
    }
}
