package me.relevante.queue;

/**
 * @author daniel-ibanez
 */
public class ProfileIndexMessage implements QueueMessage {

    private String uniqueProfileId;

    private ProfileIndexMessage() {
        this(null);
    }

    public ProfileIndexMessage(final String uniqueProfileId) {
        this.uniqueProfileId = uniqueProfileId;
    }

    public String getUniqueProfileId() {
        return uniqueProfileId;
    }
}
