package me.relevante.queue;

/**
 * @author daniel-ibanez
 */
public class DeduplicationMessage implements QueueMessage {

    private String requestId;
    private String targetUniqueProfileId;

    private DeduplicationMessage() {
        this(null, null);
    }

    public DeduplicationMessage(final String requestId,
                                final String targetUniqueProfileId) {
        this.requestId = requestId;
        this.targetUniqueProfileId = targetUniqueProfileId;
    }

    public String getRequestId() {
        return requestId;
    }

    public String getTargetUniqueProfileId() {
        return targetUniqueProfileId;
    }
}
