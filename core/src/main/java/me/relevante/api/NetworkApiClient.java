package me.relevante.api;

import me.relevante.core.Network;
import me.relevante.core.NetworkEntity;
import me.relevante.core.NetworkProfile;

/**
 * Created by daniel-ibanez on 16/07/16.
 */
public interface NetworkApiClient<N extends Network, P extends NetworkProfile<N, ?>> extends NetworkEntity<N> {
    <E extends Exception> P getProfile() throws E;
}
