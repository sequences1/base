package me.relevante.service;

/**
 * @author Daniel Ibanez
 */
public interface Merger<T> {
    T merge(T targetObject, T sourceObject);
}
