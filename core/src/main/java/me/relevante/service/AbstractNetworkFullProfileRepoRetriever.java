package me.relevante.service;

import me.relevante.core.Network;
import me.relevante.core.NetworkFullProfile;
import org.springframework.data.repository.CrudRepository;

public abstract class AbstractNetworkFullProfileRepoRetriever<N extends Network, F extends NetworkFullProfile<N, ?, F>>
        implements NetworkFullProfileRepoRetriever<N, F> {

    protected final CrudRepository<F, String> fullProfileRepo;

    public AbstractNetworkFullProfileRepoRetriever(final CrudRepository<F, String> fullProfileRepo) {
        this.fullProfileRepo = fullProfileRepo;
    }

    @Override
    public CrudRepository<F, String> getFullProfileRepo() {
        return fullProfileRepo;
    }
}