package me.relevante.service;

import me.relevante.core.Network;
import me.relevante.core.NetworkEntity;
import me.relevante.request.NetworkEngageStatus;

public interface NetworkEngageStatusMerger<N extends Network, E extends NetworkEngageStatus<N, E>> extends Merger<E>, NetworkEntity<N> {
}
