package me.relevante.service;

import me.relevante.core.Network;
import me.relevante.request.ActionEngageStatus;
import me.relevante.request.NetworkEngageStatus;

public abstract class AbstractNetworkEngageStatusMerger<N extends Network, E extends NetworkEngageStatus<N, E>>
        extends AbstractMerger<E>
        implements NetworkEngageStatusMerger<N, E> {

    private final ActionEngageStatusMerger actionEngageStatusMerger;

    public AbstractNetworkEngageStatusMerger(final ActionEngageStatusMerger actionEngageStatusMerger) {
        this.actionEngageStatusMerger = actionEngageStatusMerger;
    }

    protected ActionEngageStatus mergeActionStatus(final ActionEngageStatus actionEngageStatus1,
                                                   final ActionEngageStatus actionEngageStatus2) {
        return actionEngageStatusMerger.merge(actionEngageStatus1, actionEngageStatus2);
    }
}