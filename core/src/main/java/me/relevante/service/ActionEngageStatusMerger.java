package me.relevante.service;

import me.relevante.request.ActionEngageStatus;
import me.relevante.request.ActionRequestResult;
import me.relevante.request.ActionRequestStatus;
import org.springframework.stereotype.Service;

@Service
public class ActionEngageStatusMerger
        extends AbstractMerger<ActionEngageStatus>
        implements Merger<ActionEngageStatus> {

    @Override
    public ActionEngageStatus mergeNotNullObjects(final ActionEngageStatus engageStatus1,
                                                  final ActionEngageStatus engageStatus2) {
        if (engageStatus1.getStatus().equals(ActionRequestStatus.PROCESSED) && engageStatus2.getStatus().equals(ActionRequestStatus.IN_PROGRESS)) {
            return engageStatus1.clone();
        }
        if (engageStatus2.getStatus().equals(ActionRequestStatus.PROCESSED) && engageStatus1.getStatus().equals(ActionRequestStatus.IN_PROGRESS)) {
            return engageStatus2.clone();
        }
        if (engageStatus1.getResult().equals(ActionRequestResult.SUCCESS) || engageStatus1.getResult().equals(ActionRequestResult.ALREADY_SUCCEEDED)) {
            return engageStatus1.clone();
        }
        return engageStatus2.clone();
    }
}