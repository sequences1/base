package me.relevante.service;

import me.relevante.core.Cloneable;

public abstract class AbstractMerger<T extends Cloneable<T>> implements Merger<T> {

    @Override
    public T merge(final T targetObject, final T sourceObject) {
        if (targetObject == null && sourceObject == null) {
            return null;
        }
        if (targetObject == null) {
            return sourceObject.clone();
        }
        if (sourceObject == null) {
            return targetObject.clone();
        }
        return mergeNotNullObjects(targetObject, sourceObject);
    }

    protected abstract T mergeNotNullObjects(final T targetObject, final T sourceObject);

}