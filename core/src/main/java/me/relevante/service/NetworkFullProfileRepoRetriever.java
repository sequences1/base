package me.relevante.service;

import me.relevante.core.Network;
import me.relevante.core.NetworkEntity;
import me.relevante.core.NetworkFullProfile;
import org.springframework.data.repository.CrudRepository;

/**
 * @author Daniel Ibanez
 */
public interface NetworkFullProfileRepoRetriever<N extends Network, F extends NetworkFullProfile<N, ?, F>> extends NetworkEntity<N> {
    CrudRepository<F, String> getFullProfileRepo();
}
