package me.relevante.nlp;

import me.relevante.core.Network;
import me.relevante.core.NetworkFullPost;
import me.relevante.core.NetworkFullProfile;
import me.relevante.core.NetworkSignal;
import me.relevante.nlp.core.Keyword;
import me.relevante.nlp.core.NlpCore;
import me.relevante.search.NetworkSignalWeightFunction;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

public abstract class AbstractNetworkSignalExtractor<N extends Network, F extends NetworkFullProfile<N, ?, F>, P extends NetworkFullPost<N, ?, P>>
        implements NetworkSignalExtractor<N, F, P> {

    protected final NetworkSignalWeightFunction<N> vectorizer;
    protected final NlpCore nlpCore;

    public AbstractNetworkSignalExtractor(final NetworkSignalWeightFunction<N> vectorizer,
                                          final NlpCore nlpCore) {
        this.vectorizer = vectorizer;
        this.nlpCore = nlpCore;
    }

    @Override
    public List<NetworkSignal<N>> extractSignals(final P fullPost) {
        final List<NetworkSignal<N>> allSignals = extract(fullPost);
        allSignals.forEach(this::setSignalContentStems);
        return allSignals;
    }

    @Override
    public List<NetworkSignal<N>> extractSignals(final F fullProfile) {
        final List<NetworkSignal<N>> allSignals = extract(fullProfile);
        allSignals.forEach(this::setSignalContentStems);
        return allSignals;
    }

    @Override
    public List<NetworkSignal<N>> extractSignals(final F fullProfile, final List<P> fullPosts) {
        final List<NetworkSignal<N>> allSignals = new ArrayList<>();
        allSignals.addAll(extractSignals(fullProfile));
        for (final P fullPost : fullPosts) {
            allSignals.addAll(extractSignals(fullPost));
        }
        return allSignals;
    }

    protected abstract List<NetworkSignal<N>> extract(F fullProfile);

    protected abstract List<NetworkSignal<N>> extract(P fullPost);

    private void setSignalContentStems(NetworkSignal<N> signal) {
        final Map<String, Keyword> keywords = nlpCore.extractKeywords(signal.getNlpAnalyzableContent());
        signal.getContentStems().clear();
        signal.getContentStems().addAll(keywords.keySet());
    }

}
