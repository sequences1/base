package me.relevante.nlp;

import me.relevante.core.Network;
import me.relevante.core.NetworkEntity;
import me.relevante.core.NetworkFullPost;
import me.relevante.core.NetworkFullProfile;
import me.relevante.core.NetworkSignal;

import java.util.List;

/**
 * @author daniel-ibanez
 */
public interface NetworkSignalExtractor<N extends Network, F extends NetworkFullProfile<N, ?, F>, P extends NetworkFullPost<N, ?, P>>
        extends NetworkEntity<N> {
    List<NetworkSignal<N>> extractSignals(F fullProfile);
    List<NetworkSignal<N>> extractSignals(P fullPost);
    List<NetworkSignal<N>> extractSignals(F fullProfile, List<P> fullPosts);
}
