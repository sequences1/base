package me.relevante.auth;

import me.relevante.core.AbstractNetwork;
import me.relevante.core.Network;
import me.relevante.core.NetworkEntity;

public abstract class NetworkCredentials<N extends Network> implements NetworkEntity {

    private String network;
    private String profileId;

    public NetworkCredentials() {
    }

    public NetworkCredentials(N network,
                              String profileId) {
        this();
        this.network = network.getName();
        this.profileId = profileId;
    }

    @Override
    public Network getNetwork() {
        final String networkName = this.network;
        Network network = new AbstractNetwork(networkName) {};
        return network;
    }

    public String getProfileId() {
        return profileId;
    }

}
