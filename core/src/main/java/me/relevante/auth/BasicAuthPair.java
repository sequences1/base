package me.relevante.auth;

import java.util.Base64;

public class BasicAuthPair {

    private final String authItem;

    public BasicAuthPair(final String username,
                         final String password) {
        final String encoding = username + ":" + password;
        this.authItem = Base64.getEncoder().encodeToString(encoding.getBytes());
    }

    public String getAuthItem() {
        return authItem;
    }

    public String getAuthHeaderValue() {
        return "Basic " + authItem;
    }

}
