package me.relevante.auth;

import me.relevante.core.Network;

public class OAuthTokenPair<N extends Network> {

    protected String token;
    protected String secret;

    public OAuthTokenPair(String token, String secret) {
        this.token = token;
        this.secret = secret;
    }

    public String getToken() {
        return token;
    }

    public String getSecret() {
        return secret;
    }
}
