package me.relevante.integration;

import me.relevante.core.Network;
import me.relevante.core.NetworkFullProfile;
import me.relevante.core.NetworkProfileId;
import me.relevante.core.UniqueProfile;
import me.relevante.core.UpdateStrategy;
import me.relevante.queue.ProfileIndexMessage;
import me.relevante.queue.QueueService;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.repository.CrudRepository;

import java.util.Collections;

public abstract class AbstractNetworkProfileIntegrator<N extends Network, F extends NetworkFullProfile<N, ?, F>>
        implements NetworkProfileIntegrator<N, F> {

    private static final Logger LOGGER = LoggerFactory.getLogger(AbstractNetworkProfileIntegrator.class);

    private final CrudRepository<UniqueProfile, String> uniqueProfileRepo;
    private final CrudRepository<F, String> fullProfileRepo;
    private final QueueService queueService;

    public AbstractNetworkProfileIntegrator(final CrudRepository<UniqueProfile, String> uniqueProfileRepo,
                                            final CrudRepository<F, String> fullProfileRepo,
                                            final QueueService queueService) {
        this.uniqueProfileRepo = uniqueProfileRepo;
        this.fullProfileRepo = fullProfileRepo;
        this.queueService = queueService;
    }

    @Override
    public UniqueProfile integrate(final F newNetworkFullProfile,
                                   final UpdateStrategy updateStrategy) {
        final F persistedNetworkFullProfile = processNetworkFullProfile(newNetworkFullProfile, updateStrategy);
        final UniqueProfile associatedUniqueProfile = associateWithUniqueProfile(persistedNetworkFullProfile);
        queueService.sendProfileIndexMessage(new ProfileIndexMessage(associatedUniqueProfile.getId()));
        return associatedUniqueProfile;
    }

    protected abstract F findAlreadyExistingProfile(F networkFullProfile);

    private F processNetworkFullProfile(final F newNetworkFullProfile, final UpdateStrategy updateStrategy) {
        final F existingNetworkFullProfile = findAlreadyExistingProfile(newNetworkFullProfile);
        if (existingNetworkFullProfile != null) {
            updateNetworkFullProfile(existingNetworkFullProfile, newNetworkFullProfile, updateStrategy);
            return fullProfileRepo.save(existingNetworkFullProfile);
        } else {
            return fullProfileRepo.save(newNetworkFullProfile);
        }
    }

    private UniqueProfile associateWithUniqueProfile(final F networkFullProfile) {
        final UniqueProfile uniqueProfile;
        if (StringUtils.isBlank(networkFullProfile.getUniqueProfileId()) ||
                !uniqueProfileRepo.exists(networkFullProfile.getUniqueProfileId())) {
            uniqueProfile = createUniqueProfile(networkFullProfile);
            networkFullProfile.setUniqueProfileId(uniqueProfile.getId());
            fullProfileRepo.save(networkFullProfile);
            uniqueProfileRepo.save(uniqueProfile);
        } else {
            uniqueProfile = uniqueProfileRepo.findOne(networkFullProfile.getUniqueProfileId());
        }
        return uniqueProfile;
    }

    private UniqueProfile createUniqueProfile(final F networkFullProfile) {
        final UniqueProfile uniqueProfile = new UniqueProfile();
        final NetworkProfileId networkProfileId = new NetworkProfileId(getNetwork().getName(), networkFullProfile.getId());
        uniqueProfile.setNetworkProfileIds(Collections.singletonList(networkProfileId));
        return uniqueProfile;
    }

    private void updateNetworkFullProfile(final NetworkFullProfile existingNetworkFullProfile,
                                          final NetworkFullProfile newNetworkFullProfile,
                                          final UpdateStrategy updateStrategy) {
        switch (updateStrategy) {
            case UPDATE_WITH_EXISTING_PROVIDED:
                existingNetworkFullProfile.updateWithExistingDataIn(newNetworkFullProfile);
                break;
            case COMPLETE_MISSING:
                existingNetworkFullProfile.completeMissingDataWith(newNetworkFullProfile);
                break;
            case REPLACE:
                existingNetworkFullProfile.replaceDataWith(newNetworkFullProfile);
                break;
            default:
                LOGGER.error("Unexpected UpdateStrategy value: {}", updateStrategy);
                throw new IllegalArgumentException("Unexpected UpdateStrategy value: " + updateStrategy);
        }
    }
}
