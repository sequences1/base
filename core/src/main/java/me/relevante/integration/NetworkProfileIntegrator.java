package me.relevante.integration;

import me.relevante.core.Network;
import me.relevante.core.NetworkEntity;
import me.relevante.core.NetworkFullProfile;
import me.relevante.core.UniqueProfile;
import me.relevante.core.UpdateStrategy;

/**
 * @author Daniel Ibanez
 */
public interface NetworkProfileIntegrator<N extends Network, F extends NetworkFullProfile<N, ?, F>> extends NetworkEntity<N> {
    UniqueProfile integrate(F newNetworkFullProfile, UpdateStrategy updateStrategy);
}
