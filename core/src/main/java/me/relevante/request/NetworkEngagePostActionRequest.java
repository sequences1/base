package me.relevante.request;

import me.relevante.core.Network;

public interface NetworkEngagePostActionRequest<N extends Network> extends NetworkEngageActionRequest<N> {
    String getTargetPostId();
}