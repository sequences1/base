package me.relevante.request;

import java.util.Date;

public interface ActionRequest {

    String getId();
    String getRequesterRelevanteId();
    boolean isCreated();
    boolean isNotCreated();
    boolean isInProgress();
    boolean isNotInProgress();
    boolean isProcessed();
    boolean isNotProcessed();
    boolean isIntegrated();
    boolean isNotIntegrated();
    boolean isSuccessful();
    boolean isNotSuccessful();
    boolean isAlreadySuceeded();
    boolean isNotAlreadySuceeded();
    boolean isForbidden();
    boolean isNotForbidden();
    boolean isUserManaged();
    boolean isNotUserManaged();
    ActionRequestResult getResult();
    ActionRequestStatus getStatus();
    Date getCreationTimestamp();
    Date getProcessedTimestamp();

    void setSuccess();
    void setError();
    void setAlreadySucceeded();
    void setForbidden();
    void setUserManaged();
    void setIntegrated();
}