package me.relevante.request;

import me.relevante.core.Cloneable;

public class ActionEngageStatus implements Cloneable<ActionEngageStatus> {

    private final ActionRequestStatus status;
    private final ActionRequestResult result;

    public ActionEngageStatus(final ActionRequestStatus status,
                              final ActionRequestResult result) {
        this.status = status;
        this.result = result;
    }

    public ActionRequestStatus getStatus() {
        return status;
    }

    public ActionRequestResult getResult() {
        return result;
    }

    @Override
    public ActionEngageStatus clone() {
        return new ActionEngageStatus(this.status, this.result);
    }
}
