package me.relevante.request;

public enum ActionRequestResult {
    ALREADY_SUCCEEDED,
    ERROR,
    FORBIDDEN,
    SUCCESS,
    USER_MANAGED
}
