package me.relevante.request;

public enum ActionRequestStatus {
    IN_PROGRESS,
    PROCESSED
}
