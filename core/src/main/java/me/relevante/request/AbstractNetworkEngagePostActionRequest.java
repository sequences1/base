package me.relevante.request;

import me.relevante.core.Network;

public abstract class AbstractNetworkEngagePostActionRequest<N extends Network>
        extends AbstractNetworkEngageActionRequest<N>
        implements NetworkEngagePostActionRequest<N> {

    protected String targetPostId;

    public AbstractNetworkEngagePostActionRequest(final String requesterRelevanteId,
                                                  final String targetProfileId,
                                                  final String targetPostId) {
        super(requesterRelevanteId, targetProfileId);
        this.targetPostId = targetPostId;
    }

    @Override
    public String getTargetPostId() {
        return targetPostId;
    }
}