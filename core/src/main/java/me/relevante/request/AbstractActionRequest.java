package me.relevante.request;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.index.CompoundIndex;
import org.springframework.data.mongodb.core.index.CompoundIndexes;

import java.util.Date;
import java.util.UUID;

@CompoundIndexes({@CompoundIndex(name = "requesterRelevanteId_1_status_1", def = "{ 'requesterRelevanteId': 1, 'status': 1 }", unique = false)})
public abstract class AbstractActionRequest implements ActionRequest {

    @Id
    protected String id;
    protected String requesterRelevanteId;
    protected ActionRequestStatus status;
    protected ActionRequestResult result;
    protected boolean integrated;
    protected Date creationTimestamp;
    protected Date inProgressTimestamp;
    protected Date processedTimestamp;
    protected Date integrationTimestamp;

    public AbstractActionRequest(final String requesterRelevanteId) {
        this.id = UUID.randomUUID().toString();
        this.requesterRelevanteId = requesterRelevanteId;
        setCreated();
        setInProgress();
    }

    @Override
    public String getId() {
        return id;
    }

    @Override
    public String getRequesterRelevanteId() {
        return requesterRelevanteId;
    }

    @Override
    public Date getCreationTimestamp() {
        return creationTimestamp;
    }

    public Date getInProgressTimestamp() {
        return inProgressTimestamp;
    }

    @Override
    public Date getProcessedTimestamp() {
        return processedTimestamp;
    }

    @Override
    public boolean isCreated() {
        return status == null;
    }

    @Override
    public boolean isNotCreated() {
        return !isCreated();
    }

    @Override
    public boolean isInProgress() {
        return status.equals(ActionRequestStatus.IN_PROGRESS);
    }

    @Override
    public boolean isNotInProgress() {
        return !isInProgress();
    }

    @Override
    public boolean isProcessed() {
        return (status.equals(ActionRequestStatus.PROCESSED));
    }

    @Override
    public boolean isNotProcessed() {
        return !isProcessed();
    }

    @Override
    public boolean isIntegrated() {
        return integrated;
    }

    @Override
    public boolean isNotIntegrated() {
        return !isIntegrated();
    }

    @Override
    public boolean isSuccessful() {
        return (isProcessed() && result != null && result.equals(ActionRequestResult.SUCCESS));
    }

    @Override
    public boolean isNotSuccessful() {
        return !isSuccessful();
    }

    @Override
    public boolean isAlreadySuceeded() {
        return (isProcessed() && result != null && result.equals(ActionRequestResult.ALREADY_SUCCEEDED));
    }

    @Override
    public boolean isNotAlreadySuceeded() {
        return !isAlreadySuceeded();
    }

    @Override
    public boolean isForbidden() {
        return (isProcessed() && result != null && result.equals(ActionRequestResult.FORBIDDEN));
    }

    @Override
    public boolean isNotForbidden() {
        return !isForbidden();
    }

    @Override
    public boolean isUserManaged() {
        return (isProcessed() && result != null && result.equals(ActionRequestResult.USER_MANAGED));
    }

    @Override
    public boolean isNotUserManaged() {
        return !isUserManaged();
    }

    @Override
    public ActionRequestStatus getStatus() {
        return status;
    }

    @Override
    public ActionRequestResult getResult() {
        return result;
    }

    @Override
    public void setSuccess() {
        this.result = ActionRequestResult.SUCCESS;
        this.status = ActionRequestStatus.PROCESSED;
        this.processedTimestamp = new Date();
    }

    @Override
    public void setError() {
        this.result = ActionRequestResult.ERROR;
        this.status = ActionRequestStatus.PROCESSED;
        this.processedTimestamp = new Date();
    }

    @Override
    public void setAlreadySucceeded() {
        this.result = ActionRequestResult.ALREADY_SUCCEEDED;
        this.status = ActionRequestStatus.PROCESSED;
        this.processedTimestamp = new Date();
    }

    @Override
    public void setForbidden() {
        this.result = ActionRequestResult.FORBIDDEN;
        this.status = ActionRequestStatus.PROCESSED;
        this.processedTimestamp = new Date();
    }

    @Override
    public void setUserManaged() {
        this.result = ActionRequestResult.USER_MANAGED;
        this.status = ActionRequestStatus.PROCESSED;
        this.processedTimestamp = new Date();
    }

    @Override
    public void setIntegrated() {
        this.integrated = true;
        this.integrationTimestamp = new Date();
    }

    private void setCreated() {
        this.creationTimestamp = new Date();
        this.status = null;
        this.result = null;
        this.integrated = false;
    }

    private void setInProgress() {
        this.inProgressTimestamp = new Date();
        this.status = ActionRequestStatus.IN_PROGRESS;
    }

    @Override
    public String toString() {
        return "AbstractActionRequest{" +
                "id='" + id + '\'' +
                ", requesterRelevanteId='" + requesterRelevanteId + '\'' +
                ", status=" + status +
                ", result=" + result +
                ", integrated=" + integrated +
                ", creationTimestamp=" + creationTimestamp +
                ", inProgressTimestamp=" + inProgressTimestamp +
                ", processedTimestamp=" + processedTimestamp +
                ", integrationTimestamp=" + integrationTimestamp +
                '}';
    }
}