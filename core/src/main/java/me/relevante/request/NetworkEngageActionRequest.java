package me.relevante.request;

import me.relevante.core.Network;

public interface NetworkEngageActionRequest<N extends Network> extends NetworkActionRequest<N> {
    String getTargetProfileId();
}
