package me.relevante.request;

import me.relevante.core.Cloneable;
import me.relevante.core.Network;
import me.relevante.core.NetworkEntity;

public interface NetworkEngageStatus<N extends Network, E extends NetworkEngageStatus<N, E>> extends NetworkEntity<N>, Cloneable<E> {
    ActionEngageStatus getLevel1Status();
    void setLevel1Status(ActionEngageStatus level1Status);
    ActionEngageStatus getLevel2Status();
    void setLevel2Status(ActionEngageStatus level2Status);
    ActionEngageStatus getLevel3Status();
    void setLevel3Status(ActionEngageStatus level3Status);
    ActionEngageStatus getLevel4Status();
    void setLevel4Status(ActionEngageStatus level4Status);
}
