package me.relevante.request;

import me.relevante.core.Network;

public abstract class AbstractNetworkEngageActionRequest<N extends Network>
        extends AbstractActionRequest
        implements NetworkEngageActionRequest<N> {

    protected String targetProfileId;

    public AbstractNetworkEngageActionRequest(final String requesterRelevanteId,
                                              final String targetProfileId) {
        super(requesterRelevanteId);
        this.targetProfileId = targetProfileId;
    }

    @Override
    public String getTargetProfileId() {
        return targetProfileId;
    }

}