package me.relevante.core;

public interface NetworkEntity<N extends Network> {
    N getNetwork();
}
