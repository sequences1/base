package me.relevante.core;

public interface NetworkFullPost<N extends Network, P extends NetworkPost<N, P, ?>, F extends NetworkFullPost<N, P, F>> extends NetworkEntity<N>, Updatable<F> {
    String getId();
    P getPost();
}
