package me.relevante.core;

import java.util.List;

/**
 * @author daniel-ibanez
 */
public interface NetworkFullLoader<N extends Network, F extends NetworkFullProfile<N, ?, F>, P extends NetworkFullPost<N, ?, P>>
        extends NetworkEntity<N> {
    F loadFullProfile(String networkProfileId);
    List<P> loadFullPosts(String networkProfileId);
}
