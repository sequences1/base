package me.relevante.core;

public enum UpdateStrategy {
    COMPLETE_MISSING,
    UPDATE_WITH_EXISTING_PROVIDED,
    REPLACE
}
