package me.relevante.core;

import java.util.Objects;

public class ScoredEmail {

    private final String email;
    private final Double score;

    public ScoredEmail(final String email, final double score) {
        this.email = email;
        this.score = new Double(score);
    }

    public String getEmail() {
        return email;
    }

    public Double getScore() {
        return score;
    }

    @Override
    public boolean equals(final Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof ScoredEmail)) {
            return false;
        }
        final ScoredEmail that = (ScoredEmail) o;
        return Objects.equals(email, that.email);
    }

    @Override
    public int hashCode() {
        return Objects.hash(email);
    }

    @Override
    public String toString() {
        return "ScoredEmail{" +
                "email='" + email + '\'' +
                ", score=" + score +
                '}';
    }
}