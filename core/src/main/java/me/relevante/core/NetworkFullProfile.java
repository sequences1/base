package me.relevante.core;

import java.util.Date;

public interface NetworkFullProfile<N extends Network, P extends NetworkProfile<N, P>, F extends NetworkFullProfile<N, P, F>>
        extends NetworkEntity<N>, Updatable<F> {
    String getId();
    P getProfile();
    String getUniqueProfileId();
    void setUniqueProfileId(String uniqueProfileId);
    Date getLastUpdatedTimestamp();
}
