package me.relevante.core;

public enum SignalCategory {
    SELF_DESCRIPTION,
    CONTENT_ACTIVITY,
    OWNERSHIP,
    NETWORK
}
