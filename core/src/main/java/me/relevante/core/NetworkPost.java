package me.relevante.core;

import me.relevante.nlp.core.NlpEntity;

import java.util.Date;

public interface NetworkPost<N extends Network, P extends NetworkPost<N, ?, ?>, U extends NetworkProfile<N, ?>> extends NetworkEntity<N>, NlpEntity, Updatable<P> {
    String getId();
    String getAuthorId();
    Date getCreationTimestamp();
    void setCreationTimestamp(Date timestamp);
    U getAuthor();
    void setAuthor(U author);
}