package me.relevante.core;

import java.util.Date;

public abstract class AbstractNetworkEngageAction<N extends Network> extends AbstractNetworkAction<N> implements NetworkEngageAction<N> {

    protected String targetProfileId;

    public AbstractNetworkEngageAction(final String authorId,
                                       final String targetProfileId) {
        this(authorId, targetProfileId, new Date());
    }

    public AbstractNetworkEngageAction(final String authorId,
                                       final String targetProfileId,
                                       final Date creationTimestamp) {
        super(authorId, creationTimestamp);
        this.targetProfileId = targetProfileId;
    }

    @Override
    public String getTargetProfileId() {
        return targetProfileId;
    }
}