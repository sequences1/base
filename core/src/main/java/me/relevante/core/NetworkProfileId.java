package me.relevante.core;

import java.util.Objects;

/**
 * Created by daniel-ibanez on 7/09/16.
 */
public class NetworkProfileId {

    private String network;
    private String profileId;

    public NetworkProfileId(String network, String profileId) {
        this.network = network;
        this.profileId = profileId;
    }

    public String getNetwork() {
        return network;
    }

    public String getProfileId() {
        return profileId;
    }

    @Override
    public boolean equals(final Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof NetworkProfileId)) {
            return false;
        }
        final NetworkProfileId that = (NetworkProfileId) o;
        return Objects.equals(network, that.network) &&
                Objects.equals(profileId, that.profileId);
    }

    @Override
    public int hashCode() {
        return Objects.hash(network, profileId);
    }
}
