package me.relevante.core;

import me.relevante.persistence.NetworkFullPostRepo;
import me.relevante.persistence.NetworkPostActionRepo;
import org.springframework.data.repository.CrudRepository;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

public abstract class AbstractNetworkFullLoader<N extends Network, F extends NetworkFullProfile<N, ?, F>, P extends NetworkFullPost<N, ?, P>,
        L1 extends NetworkEngagePostAction<N>, L2 extends NetworkEngagePostAction<N>> implements NetworkFullLoader<N, F, P> {

    protected final CrudRepository<F, String> fullProfileRepo;
    protected final NetworkFullPostRepo<P> fullPostRepo;
    protected final NetworkPostActionRepo<L1> level1ActionRepo;
    protected final NetworkPostActionRepo<L2> level2ActionRepo;

    public AbstractNetworkFullLoader(final CrudRepository<F, String> fullProfileRepo,
                                     final NetworkFullPostRepo<P> fullPostRepo,
                                     final NetworkPostActionRepo<L1> level1ActionRepo,
                                     final NetworkPostActionRepo<L2> level2ActionRepo) {
        this.fullProfileRepo = fullProfileRepo;
        this.fullPostRepo = fullPostRepo;
        this.level1ActionRepo = level1ActionRepo;
        this.level2ActionRepo = level2ActionRepo;
    }

    @Override
    public F loadFullProfile(final String networkProfileId) {
        return fullProfileRepo.findOne(networkProfileId);
    }

    @Override
    public List<P> loadFullPosts(final String networkProfileId) {
        List<P> postsWithSignals = new ArrayList<>();
        List<P> postsAuthoredByProfile = fullPostRepo.findByPostAuthorId(networkProfileId);
        postsWithSignals.addAll(postsAuthoredByProfile);
        List<P> postsWithActionsAuthoredByProfile = getFullPostsWithActions(networkProfileId);
        postsWithSignals.addAll(postsWithActionsAuthoredByProfile);
        return postsWithSignals;
    }

    protected abstract void addLevel1ActionToFullPost(L1 level1Action, P fullPost);

    protected abstract void addLevel2ActionToFullPost(L2 level2Action, P fullPost);

    private List<P> getFullPostsWithActions(String networkProfileId) {

        // Find these profile actions
        List<L1> level1Actions = level1ActionRepo.findByAuthorId(networkProfileId);
        List<L2> level2Actions = level2ActionRepo.findByAuthorId(networkProfileId);

        // Find these actions's posts
        Set<String> postIds = new HashSet<>();
        level1Actions.forEach(level1Action -> postIds.add(level1Action.getTargetPostId()));
        level2Actions.forEach(level2Action -> postIds.add(level2Action.getTargetPostId()));
        List<P> fullPostsWithActions = fullPostRepo.findByIdIn(postIds);

        // Assign actions to posts
        Map<String, P> fullPostsById = new HashMap<>();
        fullPostsWithActions.forEach(fullPost -> fullPostsById.put(fullPost.getId(), fullPost));
        for (L1 level1Action : level1Actions) {
            P fullPost = fullPostsById.get(level1Action.getTargetPostId());
            addLevel1ActionToFullPost(level1Action, fullPost);
        }
        for (L2 level2Action : level2Actions) {
            P fullPost = fullPostsById.get(level2Action.getTargetPostId());
            addLevel2ActionToFullPost(level2Action, fullPost);
        }
        return fullPostsWithActions;
    }


}
