package me.relevante.core;

public interface Updatable<T> {

    void completeMissingDataWith(T sourceObject);

    void updateWithExistingDataIn(T sourceObject);

    void replaceDataWith(T sourceObject);

}
