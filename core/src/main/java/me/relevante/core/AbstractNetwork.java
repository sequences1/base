package me.relevante.core;

/**
 * Created by daniel-ibanez on 10/05/16.
 */
public abstract class AbstractNetwork implements Network {

    protected String name;

    public AbstractNetwork(String name) {
        this.name = name;
    }

    @Override
    public String getName() {
        return name;
    }

    /**
     * DON'T REMOVE THIS METHOD if you are not sure whether you should.
     */
    @Override
    public boolean equals(Object o) {
        AbstractNetwork that = (AbstractNetwork) o;
        return !(name != null ? !name.equals(that.getName()) : that.getName() != null);

    }

    /**
     * DON'T REMOVE THIS METHOD if you are not sure whether you should.
     */
    @Override
    public int hashCode() {
        return name != null ? name.hashCode() : 0;
    }
}
