package me.relevante.core;

import me.relevante.nlp.core.NlpEntity;

import java.util.Date;
import java.util.List;

public interface NetworkSignal<N extends Network> extends NlpEntity, NetworkEntity<N> {
    String getShortDescription();
    Date getSignalTimestamp();
    String getRelatedProfileId();
    List<String> getContentStems();
    SignalCategory getCategory();
}
