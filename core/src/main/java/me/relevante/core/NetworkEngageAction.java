package me.relevante.core;

public interface NetworkEngageAction<N extends Network> extends NetworkAction<N> {
    String getTargetProfileId();
}
