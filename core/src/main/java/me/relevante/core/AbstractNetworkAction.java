package me.relevante.core;

import java.util.Date;

public abstract class AbstractNetworkAction<N extends Network> implements NetworkAction<N> {

    protected String authorId;
    protected Date creationTimestamp;

    public AbstractNetworkAction(final String authorId) {
        this(authorId, new Date());
    }

    public AbstractNetworkAction(final String authorId,
                                 final Date creationTimestamp) {
        this.authorId = authorId;
        this.creationTimestamp = creationTimestamp;
    }

    @Override
    public String getAuthorId() {
        return authorId;
    }

    @Override
    public Date getCreationTimestamp() {
        return creationTimestamp;
    }

}