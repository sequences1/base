package me.relevante.core;

/**
 * @author Daniel Ibanez
 */
public interface Cloneable<T> {
    T clone();
}
