package me.relevante.core;

import java.util.Date;

public abstract class AbstractNetworkEngagePostAction<N extends Network> extends AbstractNetworkEngageAction<N> implements NetworkEngagePostAction<N> {

    protected String targetPostId;

    private AbstractNetworkEngagePostAction() {
        this(null, null, null, null);
    }

    public AbstractNetworkEngagePostAction(final String authorId,
                                           final String targetProfileId,
                                           final String targetPostId) {
        this(authorId, targetProfileId, targetPostId, new Date());
    }

    public AbstractNetworkEngagePostAction(final String authorId,
                                           final String targetProfileId,
                                           final String targetPostId,
                                           final Date creationTimestamp) {
        super(authorId, targetProfileId, creationTimestamp);
        this.targetPostId = targetPostId;
    }

    @Override
    public String getTargetPostId() {
        return targetPostId;
    }
}