package me.relevante.core;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.UUID;

/**
 * Created by daniel-ibanez on 7/09/16.
 */
@Document(collection = "UniqueProfile")
public class UniqueProfile implements Cloneable<UniqueProfile> {
    @Id
    private String id;
    private List<ScoredEmail> emails;
    private List<NetworkProfileId> networkProfileIds;
    private List<String> keywords;

    public UniqueProfile() {
        this.id = UUID.randomUUID().toString();
        this.emails = new ArrayList<>();
        this.networkProfileIds = new ArrayList<>();
        this.keywords = new ArrayList<>();
    }

    public String getId() {
        return id;
    }

    public List<ScoredEmail> getScoredEmails() {
        return new ArrayList<>(emails);
    }

    public void setEmails(final Collection<ScoredEmail> emails) {
        this.emails = new ArrayList<>(emails);
    }

    public List<NetworkProfileId> getNetworkProfileIds() {
        return new ArrayList<>(networkProfileIds);
    }

    public void setNetworkProfileIds(final Collection<NetworkProfileId> networkProfileIds) {
        this.networkProfileIds = new ArrayList<>(networkProfileIds);
    }

    public List<String> getKeywords() {
        return new ArrayList<>(keywords);
    }

    public void setKeywords(final Collection<String> keywords) {
        this.keywords = new ArrayList<>(keywords);
    }

    public boolean existsInNetwork(final Network network) {
        return (getProfileIdByNetwork(network) != null);
    }

    public String getProfileIdByNetwork(final Network network) {
        return getProfileIdByNetwork(network.getName());
    }

    public String getProfileIdByNetwork(final String networkName) {
        for (final NetworkProfileId networkProfileId : networkProfileIds) {
            if (networkProfileId.getNetwork().equals(networkName)) {
                return networkProfileId.getProfileId();
            }
        }
        return null;
    }

    @Override
    public UniqueProfile clone() {
        final UniqueProfile cloned = new UniqueProfile();
        cloned.id = this.id;
        cloned.emails = new ArrayList<>(this.emails);
        cloned.networkProfileIds = new ArrayList<>(this.networkProfileIds);
        cloned.keywords = new ArrayList<>(this.keywords);
        return cloned;
    }

    @Override
    public String toString() {
        return "UniqueProfile{" +
                "id='" + id + '\'' +
                ", emails=" + emails +
                ", networkProfileIds=" + networkProfileIds +
                ", keywords=" + keywords +
                '}';
    }
}
