package me.relevante.core;

public interface NetworkEngagePostAction<N extends Network> extends NetworkEngageAction<N> {
    String getTargetPostId();
}