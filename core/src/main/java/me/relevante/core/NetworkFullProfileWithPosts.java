package me.relevante.core;

import java.util.Date;

public interface NetworkFullProfileWithPosts<N extends Network, R extends NetworkProfile<N, R>, P extends NetworkFullPost<N, ?, P>, F extends NetworkFullProfile<N, R, F>>
        extends NetworkFullProfile<N, R, F> {
    P getLastPost();
    void setLastPost(P lastPost);
    Date getLastActivityUpdatedTimestamp();
}
