package me.relevante.core;

import java.util.Date;

public interface NetworkAction<N extends Network> extends NetworkEntity<N> {
    String getAuthorId();
    Date getCreationTimestamp();
}