package me.relevante.core;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import org.springframework.data.annotation.Transient;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@JsonIgnoreProperties({"category", "relatedProfileId", "contentStems", "network"})
public abstract class AbstractNetworkSignal<N extends Network> implements NetworkSignal<N> {

    protected String shortDescription;
    protected String nlpAnalyzableContent;
    protected Date signalTimestamp;

    @Transient
    protected List<String> contentStems;
    @Transient
    protected String relatedProfileId;

    public AbstractNetworkSignal(final Date signalTimestamp,
                                 final String relatedProfileId,
                                 final String nlpAnalyzableContent) {
        this.signalTimestamp = (signalTimestamp != null) ? signalTimestamp : new Date();
        this.relatedProfileId = relatedProfileId;
        this.nlpAnalyzableContent = nlpAnalyzableContent;
        this.contentStems = new ArrayList<>();
    }

    @Override
    public String getShortDescription() {
        return shortDescription != null ? shortDescription : nlpAnalyzableContent;
    }

    @Override
    public String getRelatedProfileId() {
        return relatedProfileId;
    }

    @Override
    public Date getSignalTimestamp() {
        return signalTimestamp;
    }

    @Override
    public List<String> getContentStems() {
        return contentStems;
    }

    @Override
    public String getNlpAnalyzableContent() {
        return nlpAnalyzableContent;
    }

}
