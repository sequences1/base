package me.relevante.core;

import me.relevante.nlp.core.NlpEntity;

public interface NetworkProfile<N extends Network, P extends NetworkProfile<N, ?>> extends NetworkEntity<N>, NlpEntity, Updatable<P> {
    String getId();
}
