package me.relevante.integration;

import me.relevante.core.Twitter;
import me.relevante.core.TwitterFullProfile;
import me.relevante.core.UniqueProfile;
import me.relevante.persistence.BaseTwitterFullProfileRepo;
import me.relevante.queue.QueueService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Component;

@Component
public class TwitterProfileIntegrator
        extends AbstractNetworkProfileIntegrator<Twitter, TwitterFullProfile>
        implements NetworkProfileIntegrator<Twitter, TwitterFullProfile> {

    private final BaseTwitterFullProfileRepo fullProfileRepo;

    @Autowired
    public TwitterProfileIntegrator(
            final CrudRepository<UniqueProfile, String> uniqueProfileRepo,
            final BaseTwitterFullProfileRepo fullProfileRepo,
            final QueueService queueService) {
        super(uniqueProfileRepo, fullProfileRepo, queueService);
        this.fullProfileRepo = fullProfileRepo;
    }

    @Override
    protected TwitterFullProfile findAlreadyExistingProfile(final TwitterFullProfile fullProfile) {
        return fullProfileRepo.findOneByProfileId(fullProfile.getProfile().getTwitterId());
    }

    @Override
    public Twitter getNetwork() {
        return Twitter.getInstance();
    }
}
