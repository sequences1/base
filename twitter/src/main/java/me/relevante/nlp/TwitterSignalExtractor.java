package me.relevante.nlp;

import me.relevante.core.NetworkSignal;
import me.relevante.core.Twitter;
import me.relevante.core.TwitterFav;
import me.relevante.core.TwitterFullPost;
import me.relevante.core.TwitterFullProfile;
import me.relevante.core.TwitterProfile;
import me.relevante.core.TwitterReply;
import me.relevante.core.TwitterRetweet;
import me.relevante.core.TwitterSignalBioDescription;
import me.relevante.core.TwitterSignalDescription;
import me.relevante.core.TwitterSignalPostAuthorship;
import me.relevante.core.TwitterSignalPostFav;
import me.relevante.core.TwitterSignalPostReply;
import me.relevante.core.TwitterSignalPostRetweet;
import me.relevante.nlp.core.NlpCore;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;

@Component
public class TwitterSignalExtractor
        extends AbstractNetworkSignalExtractor<Twitter, TwitterFullProfile, TwitterFullPost>
        implements NetworkSignalExtractor<Twitter, TwitterFullProfile, TwitterFullPost> {

    @Autowired
    public TwitterSignalExtractor(TwitterSignalWeightFunction signalWeightFunction,
                                  NlpCore nlpCore) {
        super(signalWeightFunction, nlpCore);
    }

    @Override
    public Twitter getNetwork() {
        return Twitter.getInstance();
    }

    @Override
    protected List<NetworkSignal<Twitter>> extract(final TwitterFullProfile fullProfile) {

        final List<NetworkSignal<Twitter>> signals = new ArrayList<>();

        final TwitterProfile profile = fullProfile.getProfile();
        if (profile.getDescription() != null) {
            final TwitterSignalDescription signal = new TwitterSignalDescription(profile);
            signals.add(signal);
        }
        if (profile.getBioDescription() != null) {
            final TwitterSignalBioDescription signal = new TwitterSignalBioDescription(profile);
            signals.add(signal);
        }

        return signals;
    }

    @Override
    protected List<NetworkSignal<Twitter>> extract(TwitterFullPost fullPost) {

        final List<NetworkSignal<Twitter>> signals = new ArrayList<>();

        if (fullPost.getPost().getAuthorId() != null) {
            final TwitterSignalPostAuthorship signal = new TwitterSignalPostAuthorship(fullPost.getPost());
            signals.add(signal);
        }
        for (final TwitterReply reply : fullPost.getReplies()) {
            final TwitterSignalPostReply signal = new TwitterSignalPostReply(fullPost.getPost(), reply.getAuthorId(), reply.getCreationTimestamp());
            signals.add(signal);
        }
        for (final TwitterRetweet retweet : fullPost.getRetweets()) {
            final TwitterSignalPostRetweet signal = new TwitterSignalPostRetweet(fullPost.getPost(), retweet.getAuthorId(), retweet.getCreationTimestamp());
            signals.add(signal);
        }
        for (final TwitterFav fav : fullPost.getFavs()) {
            final TwitterSignalPostFav signal = new TwitterSignalPostFav(fullPost.getPost(), fav.getAuthorId(), fav.getCreationTimestamp());
            signals.add(signal);
        }

        return new ArrayList<>(signals);
    }

}
