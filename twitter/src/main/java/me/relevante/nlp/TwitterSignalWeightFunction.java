package me.relevante.nlp;

import me.relevante.core.NetworkSignal;
import me.relevante.core.TwitterSignalBioDescription;
import me.relevante.core.TwitterSignalDescription;
import me.relevante.core.TwitterSignalPostAuthorship;
import me.relevante.core.TwitterSignalPostFav;
import me.relevante.core.TwitterSignalPostReply;
import me.relevante.core.TwitterSignalPostRetweet;
import me.relevante.core.Twitter;
import me.relevante.search.AbstractNetworkSignalWeightFunction;
import me.relevante.search.NetworkSignalWeightFunction;
import org.springframework.stereotype.Component;

import java.util.HashMap;
import java.util.Map;
import java.util.function.Function;

@Component
public class TwitterSignalWeightFunction extends AbstractNetworkSignalWeightFunction<Twitter> implements NetworkSignalWeightFunction<Twitter> {

    public TwitterSignalWeightFunction() {
        super();
    }

    @Override
    public Twitter getNetwork() {
        return Twitter.getInstance();
    }

    @Override
    protected Map<Class<? extends NetworkSignal<Twitter>>, Function<Double, Double>> createMiscellaneaWeightFunctionsBySignal() {

        Map<Class<? extends NetworkSignal<Twitter>>, Function<Double, Double>> weightFunctionsBySignal = new HashMap<>();
        weightFunctionsBySignal.put(TwitterSignalDescription.class, n -> 12.5);
        weightFunctionsBySignal.put(TwitterSignalBioDescription.class, n -> 12.5);

        weightFunctionsBySignal.put(TwitterSignalPostAuthorship.class, n -> 8.333 / (1 + Math.pow(5.0, -n)));
        weightFunctionsBySignal.put(TwitterSignalPostRetweet.class, n -> 4.167 / (1 + Math.pow(5.0, -n)));
        weightFunctionsBySignal.put(TwitterSignalPostFav.class, n -> 4.167 / (1 + Math.pow(5.0, -n)));
        weightFunctionsBySignal.put(TwitterSignalPostReply.class, n -> 8.333 / (1 + Math.pow(5.0, -n)));

        return weightFunctionsBySignal;
    }

    @Override
    protected Map<Class<? extends NetworkSignal<Twitter>>, Function<Double, Double>> createProfessionalsWeightFunctionsBySignal() {

        Map<Class<? extends NetworkSignal<Twitter>>, Function<Double, Double>> weightFunctionsBySignal = new HashMap<>();
        weightFunctionsBySignal.put(TwitterSignalDescription.class, n -> 12.5);
        weightFunctionsBySignal.put(TwitterSignalBioDescription.class, n -> 12.5);

        weightFunctionsBySignal.put(TwitterSignalPostAuthorship.class, n -> 8.333 / (1 + Math.pow(5.0, -n)));
        weightFunctionsBySignal.put(TwitterSignalPostRetweet.class, n -> 8.333 / (1 + Math.pow(5.0, -n)));
        weightFunctionsBySignal.put(TwitterSignalPostFav.class, n -> 0.0);
        weightFunctionsBySignal.put(TwitterSignalPostReply.class, n -> 0.0);
        return weightFunctionsBySignal;
    }

    @Override
    protected Map<Class<? extends NetworkSignal<Twitter>>, Function<Double, Double>> createProspectsWeightFunctionsBySignal() {

        Map<Class<? extends NetworkSignal<Twitter>>, Function<Double, Double>> weightFunctionsBySignal = new HashMap<>();
        weightFunctionsBySignal.put(TwitterSignalDescription.class, n -> -12.5);
        weightFunctionsBySignal.put(TwitterSignalBioDescription.class, n -> -12.5);

        weightFunctionsBySignal.put(TwitterSignalPostAuthorship.class, n -> 4.167 / (1 + Math.pow(5.0, -n)));
        weightFunctionsBySignal.put(TwitterSignalPostRetweet.class, n -> 4.167 / (1 + Math.pow(5.0, -n)));
        weightFunctionsBySignal.put(TwitterSignalPostFav.class, n -> 8.333 / (1 + Math.pow(5.0, -n)));
        weightFunctionsBySignal.put(TwitterSignalPostReply.class, n -> 8.333 / (1 + Math.pow(5.0, -n)));

        return weightFunctionsBySignal;
    }
}
