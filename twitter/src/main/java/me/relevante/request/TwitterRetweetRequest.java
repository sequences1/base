package me.relevante.request;

import me.relevante.core.Twitter;
import org.springframework.data.mongodb.core.mapping.Document;

@Document(collection = "TwitterRetweetRequest")
public class TwitterRetweetRequest extends AbstractNetworkEngagePostActionRequest<Twitter> implements NetworkEngagePostActionRequest<Twitter> {

    private TwitterRetweetRequest() {
        this(null, null, null);
    }

    public TwitterRetweetRequest(final String requesterRelevanteId,
                                 final String targetProfileId,
                                 final String targetPostId) {
        super(requesterRelevanteId, targetProfileId, targetPostId);
    }

}
