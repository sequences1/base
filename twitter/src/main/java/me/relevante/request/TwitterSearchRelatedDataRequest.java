package me.relevante.request;

import me.relevante.core.Twitter;
import me.relevante.ingest.TwitterIngestPost;
import me.relevante.ingest.TwitterIngestProfile;
import org.springframework.data.mongodb.core.mapping.Document;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.List;

/**
 * Created by daniel-ibanez on 22/09/16.
 */
@Document(collection = "TwitterSearchRelatedDataRequest")
public class TwitterSearchRelatedDataRequest extends AbstractActionRequest implements NetworkActionRequest<Twitter> {

    private List<String> searchTerms;
    private List<TwitterIngestProfile> extractedProfiles;
    private List<TwitterIngestPost> extractedPosts;

    private TwitterSearchRelatedDataRequest() {
        this(null, Collections.emptyList());
    }

    public TwitterSearchRelatedDataRequest(final String requesterRelevanteId,
                                           final List<String> searchTerms) {
        super(requesterRelevanteId);
        this.searchTerms = new ArrayList<>(searchTerms);
        this.extractedProfiles = new ArrayList<>();
        this.extractedPosts = new ArrayList<>();
    }

    public List<String> getSearchTerms() {
        return new ArrayList<>(searchTerms);
    }

    public List<TwitterIngestProfile> getExtractedProfiles() {
        return new ArrayList<>(extractedProfiles);
    }

    public void setExtractedProfiles(final Collection<TwitterIngestProfile> extractedProfiles) {
        this.extractedProfiles = new ArrayList<>(extractedProfiles);
    }

    public List<TwitterIngestPost> getExtractedPosts() {
        return new ArrayList<>(extractedPosts);
    }

    public void setExtractedPosts(final Collection<TwitterIngestPost> extractedPosts) {
        this.extractedPosts = new ArrayList<>(extractedPosts);
    }

}
