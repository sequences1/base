package me.relevante.request;

import me.relevante.core.Twitter;
import org.springframework.data.mongodb.core.mapping.Document;

@Document(collection = "TwitterFollowRequest")
public class TwitterFollowRequest extends AbstractNetworkEngageActionRequest<Twitter> implements NetworkEngageActionRequest<Twitter> {

    private TwitterFollowRequest() {
        this(null, null);
    }

    public TwitterFollowRequest(String requesterRelevanteId,
                                String targetProfileId) {
        super(requesterRelevanteId, targetProfileId);
    }

}
