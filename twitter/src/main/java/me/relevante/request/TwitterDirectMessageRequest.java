package me.relevante.request;

import me.relevante.core.Twitter;
import org.springframework.data.mongodb.core.mapping.Document;

import java.util.Date;

@Document(collection = "TwitterDirectMessageRequest")
public class TwitterDirectMessageRequest extends AbstractNetworkEngageActionRequest<Twitter> implements NetworkEngageActionRequest<Twitter> {

    private String message;

    private TwitterDirectMessageRequest() {
        this(null, null, null);
    }

    public TwitterDirectMessageRequest(final String requesterRelevanteId,
                                       final String targetProfileId,
                                       final String message) {
        this(requesterRelevanteId, targetProfileId, message, null);
    }

    public TwitterDirectMessageRequest(final String requesterRelevanteId,
                                       final String targetProfileId,
                                       final String message,
                                       final Date createdAt) {
        super(requesterRelevanteId, targetProfileId);
        this.message = message;
        this.creationTimestamp = createdAt;
    }

    public String getMessage() {
        return message;
    }

}
