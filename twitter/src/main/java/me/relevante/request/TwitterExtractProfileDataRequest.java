package me.relevante.request;

import me.relevante.core.Twitter;
import me.relevante.ingest.TwitterIngestProfile;
import org.springframework.data.mongodb.core.mapping.Document;

/**
 * Created by daniel-ibanez on 22/09/16.
 */
@Document(collection = "TwitterExtractProfileDataRequest")
public class TwitterExtractProfileDataRequest extends AbstractNetworkEngageActionRequest<Twitter> implements NetworkActionRequest<Twitter> {

    private TwitterIngestProfile extractedProfile;

    private TwitterExtractProfileDataRequest() {
        this(null, null);
    }

    public TwitterExtractProfileDataRequest(final String requesterRelevanteId,
                                            final String targetProfileId) {
        super(requesterRelevanteId, targetProfileId);
    }

    public TwitterIngestProfile getExtractedProfile() {
        return extractedProfile;
    }

    public void setExtractedProfile(final TwitterIngestProfile extractedProfile) {
        this.extractedProfile = extractedProfile;
    }

}
