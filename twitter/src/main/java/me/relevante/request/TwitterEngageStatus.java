package me.relevante.request;

import me.relevante.core.Twitter;

/**
 * @author Daniel Ibanez
 */
public class TwitterEngageStatus implements NetworkEngageStatus<Twitter, TwitterEngageStatus> {

    private ActionEngageStatus favStatus;
    private ActionEngageStatus replyStatus;
    private ActionEngageStatus retweetStatus;
    private ActionEngageStatus followStatus;
    private ActionEngageStatus directMessageStatus;

    @Override
    public Twitter getNetwork() {
        return Twitter.getInstance();
    }

    public ActionEngageStatus getFavStatus() {
        return favStatus;
    }

    public void setFavStatus(final ActionEngageStatus favStatus) {
        this.favStatus = favStatus;
    }

    public ActionEngageStatus getReplyStatus() {
        return replyStatus;
    }

    public void setReplyStatus(final ActionEngageStatus replyStatus) {
        this.replyStatus = replyStatus;
    }

    public ActionEngageStatus getRetweetStatus() {
        return retweetStatus;
    }

    public void setRetweetStatus(final ActionEngageStatus retweetStatus) {
        this.retweetStatus = retweetStatus;
    }

    public ActionEngageStatus getFollowStatus() {
        return followStatus;
    }

    public void setFollowStatus(final ActionEngageStatus followStatus) {
        this.followStatus = followStatus;
    }

    public ActionEngageStatus getDirectMessageStatus() {
        return directMessageStatus;
    }

    public void setDirectMessageStatus(final ActionEngageStatus directMessageStatus) {
        this.directMessageStatus = directMessageStatus;
    }

    @Override
    public ActionEngageStatus getLevel1Status() {
        return getFavStatus();
    }

    @Override
    public void setLevel1Status(final ActionEngageStatus level1Status) {
        setFavStatus(level1Status);
    }

    @Override
    public ActionEngageStatus getLevel2Status() {
        return getReplyStatus();
    }

    @Override
    public void setLevel2Status(final ActionEngageStatus level2Status) {
        setReplyStatus(level2Status);
    }

    @Override
    public ActionEngageStatus getLevel3Status() {
        return getFollowStatus();
    }

    @Override
    public void setLevel3Status(final ActionEngageStatus level3Status) {
        setFollowStatus(level3Status);
    }

    @Override
    public ActionEngageStatus getLevel4Status() {
        return getDirectMessageStatus();
    }

    @Override
    public void setLevel4Status(final ActionEngageStatus level4Status) {
        setDirectMessageStatus(level4Status);
    }

    @Override
    public TwitterEngageStatus clone() {
        final TwitterEngageStatus cloned = new TwitterEngageStatus();
        cloned.favStatus = this.favStatus == null ? null : this.favStatus.clone();
        cloned.replyStatus = this.replyStatus == null ? null : this.replyStatus.clone();
        cloned.retweetStatus = this.retweetStatus == null ? null : this.retweetStatus.clone();
        cloned.followStatus = this.followStatus == null ? null : this.followStatus.clone();
        cloned.directMessageStatus = this.directMessageStatus == null ? null : this.directMessageStatus.clone();
        return cloned;
    }
}
