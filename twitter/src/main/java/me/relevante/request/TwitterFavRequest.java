package me.relevante.request;

import me.relevante.core.Twitter;
import org.springframework.data.mongodb.core.mapping.Document;

@Document(collection = "TwitterFavRequest")
public class TwitterFavRequest extends AbstractNetworkEngagePostActionRequest<Twitter> implements NetworkEngagePostActionRequest<Twitter> {

    private TwitterFavRequest() {
        this(null, null, null);
    }

    public TwitterFavRequest(final String requesterRelevanteId,
                             final String targetProfileId,
                             final String targetPostId) {
        super(requesterRelevanteId, targetProfileId, targetPostId);
    }

}
