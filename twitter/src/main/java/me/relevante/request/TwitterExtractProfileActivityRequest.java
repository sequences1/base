package me.relevante.request;

import me.relevante.core.Twitter;
import me.relevante.ingest.TwitterIngestPost;
import org.springframework.data.mongodb.core.mapping.Document;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by daniel-ibanez on 22/09/16.
 */
@Document(collection = "TwitterExtractProfileActivityRequest")
public class TwitterExtractProfileActivityRequest extends AbstractNetworkEngageActionRequest<Twitter> implements NetworkActionRequest<Twitter> {

    private List<TwitterIngestPost> extractedPosts;

    private TwitterExtractProfileActivityRequest() {
        this(null, null);
    }

    public TwitterExtractProfileActivityRequest(final String requesterRelevanteId,
                                                final String targetProfileId) {
        super(requesterRelevanteId, targetProfileId);
        this.extractedPosts = new ArrayList<>();
    }

    public List<TwitterIngestPost> getExtractedPosts() {
        return extractedPosts;
    }

    public void setExtractedPosts(final List<TwitterIngestPost> extractedPosts) {
        this.extractedPosts = extractedPosts;
    }

}
