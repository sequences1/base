package me.relevante.request;

import me.relevante.core.Twitter;
import org.springframework.data.mongodb.core.mapping.Document;

@Document(collection = "TwitterReplyRequest")
public class TwitterReplyRequest extends AbstractNetworkEngagePostActionRequest<Twitter> implements NetworkEngagePostActionRequest<Twitter> {

    private String replyText;

    private TwitterReplyRequest() {
        this(null, null, null, null);
    }

    public TwitterReplyRequest(final String requesterRelevanteId,
                               final String targetProfileId,
                               final String tweetId,
                               final String replyText) {
        super(requesterRelevanteId, targetProfileId, tweetId);
        this.replyText = replyText;
    }

    public String getReplyText() {
        return replyText;
    }

}
