package me.relevante.service;

import me.relevante.core.Twitter;
import me.relevante.core.TwitterFullProfile;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Component;

/**
 * @author Daniel Ibanez
 */
@Component
public class TwitterFullProfileRepoRetriever
        extends AbstractNetworkFullProfileRepoRetriever<Twitter, TwitterFullProfile>
        implements NetworkFullProfileRepoRetriever<Twitter, TwitterFullProfile> {

    public TwitterFullProfileRepoRetriever(final CrudRepository<TwitterFullProfile, String> fullProfileRepo) {
        super(fullProfileRepo);
    }

    @Override
    public Twitter getNetwork() {
        return Twitter.getInstance();
    }
}
