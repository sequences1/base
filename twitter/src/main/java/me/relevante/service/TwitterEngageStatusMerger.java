package me.relevante.service;

import me.relevante.core.Twitter;
import me.relevante.request.TwitterEngageStatus;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * @author Daniel Ibanez
 */
@Component
public class TwitterEngageStatusMerger extends AbstractNetworkEngageStatusMerger<Twitter, TwitterEngageStatus>
        implements NetworkEngageStatusMerger<Twitter, TwitterEngageStatus> {

    @Autowired
    public TwitterEngageStatusMerger(final ActionEngageStatusMerger actionEngageStatusMerger) {
        super(actionEngageStatusMerger);
    }

    @Override
    protected TwitterEngageStatus mergeNotNullObjects(final TwitterEngageStatus status1,
                                                      final TwitterEngageStatus status2) {
        final TwitterEngageStatus mergedEngageStatus = new TwitterEngageStatus();
        mergedEngageStatus.setFavStatus(mergeActionStatus(status1.getFavStatus(), status2.getFavStatus()));
        mergedEngageStatus.setReplyStatus(mergeActionStatus(status1.getReplyStatus(), status2.getReplyStatus()));
        mergedEngageStatus.setRetweetStatus(mergeActionStatus(status1.getRetweetStatus(), status2.getRetweetStatus()));
        mergedEngageStatus.setFollowStatus(mergeActionStatus(status1.getFollowStatus(), status2.getFollowStatus()));
        mergedEngageStatus.setDirectMessageStatus(mergeActionStatus(status1.getDirectMessageStatus(), status2.getDirectMessageStatus()));
        return mergedEngageStatus;
    }

    @Override
    public Twitter getNetwork() {
        return Twitter.getInstance();
    }
}
