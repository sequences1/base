package me.relevante.persistence;

import me.relevante.core.TwitterFullProfile;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.data.repository.NoRepositoryBean;

/**
 * @author daniel-ibanez
 */
@NoRepositoryBean
public interface BaseTwitterFullProfileRepo extends MongoRepository<TwitterFullProfile, String> {
    TwitterFullProfile findOneByProfileId(Long profileId);
}
