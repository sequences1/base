package me.relevante.core;

import java.util.Date;

public abstract class AbstractTwitterSignal extends AbstractNetworkSignal<Twitter> implements NetworkSignal<Twitter> {

    public AbstractTwitterSignal(final Date timestamp,
                                 final String relatedProfileId,
                                 final String nlpAnalyzableContent) {
        super(timestamp, relatedProfileId, nlpAnalyzableContent);
    }

    @Override
    public Twitter getNetwork() {
        return Twitter.getInstance();
    }
}
