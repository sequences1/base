package me.relevante.core;

import java.util.Date;

public class TwitterSignalPostReply extends AbstractTwitterSignal implements NetworkSignal<Twitter> {

    protected TwitterSignalPostReply() {
        super(null, null, null);
    }

    public TwitterSignalPostReply(final TwitterPost post,
                                  final String commentAuthorId,
                                  final Date commentTimestamp) {
        super(commentTimestamp,
                commentAuthorId,
                post.getText());
    }

    @Override
    public SignalCategory getCategory() {
        return SignalCategory.CONTENT_ACTIVITY;
    }
}
