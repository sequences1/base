package me.relevante.core;

import java.util.Date;

public class TwitterSignalPostFav extends AbstractTwitterSignal implements NetworkSignal<Twitter> {

    protected TwitterSignalPostFav() {
        super(null, null, null);
    }

    public TwitterSignalPostFav(final TwitterPost post,
                                final String commentAuthorId,
                                final Date commentTimestamp) {
        super(commentTimestamp,
                commentAuthorId,
                post.getText());
    }

    @Override
    public SignalCategory getCategory() {
        return SignalCategory.CONTENT_ACTIVITY;
    }
}
