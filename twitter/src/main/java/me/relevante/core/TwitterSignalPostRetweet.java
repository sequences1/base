package me.relevante.core;

import java.util.Date;

public class TwitterSignalPostRetweet extends AbstractTwitterSignal implements NetworkSignal<Twitter> {

    protected TwitterSignalPostRetweet() {
        super(null, null, null);
    }

    public TwitterSignalPostRetweet(final TwitterPost post,
                                    final String commentAuthorId,
                                    final Date commentTimestamp) {
        super(commentTimestamp,
                commentAuthorId,
                post.getText());
    }

    @Override
    public SignalCategory getCategory() {
        return SignalCategory.CONTENT_ACTIVITY;
    }
}
