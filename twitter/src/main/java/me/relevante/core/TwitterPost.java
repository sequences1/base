package me.relevante.core;

import me.relevante.nlp.core.Keyword;
import org.apache.commons.lang3.StringUtils;
import org.springframework.data.annotation.Transient;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public final class TwitterPost implements NetworkPost<Twitter, TwitterPost, TwitterProfile> {

    private static final String POST_URL = "https://twitter.com/%s/status/%s";

    private Long id;
    private String authorId;
    private int latitude;
    private int longitude;
    private String language;
    private String text;
    private String tweetUrl;
    private Date creationTimestamp;

    @Transient
    private TwitterProfile author;
    @Transient
    private List<TwitterPostFav> favs;
    @Transient
    private List<TwitterPostRetweet> retweets;
    @Transient
    private List<TwitterPostReply> replies;
    @Transient
    private List<Keyword> keywords;

    public TwitterPost() {
        favs = new ArrayList<>();
        retweets = new ArrayList<>();
        replies = new ArrayList<>();
        keywords = new ArrayList<>();
    }

    @Override
    public Twitter getNetwork() {
        return Twitter.getInstance();
    }

    @Override
    public String getId() {
        return String.valueOf(id);
    }

    @Override
    public String getAuthorId() {
        return authorId;
    }

    @Override
    public Date getCreationTimestamp() {
        return creationTimestamp;
    }

    @Override
    public void setCreationTimestamp(Date timestamp) {
        this.creationTimestamp = timestamp;
    }

    @Override
    public TwitterProfile getAuthor() {
        return this.author;
    }

    @Override
    public void setAuthor(TwitterProfile author) {
        this.author = author;
        this.authorId = (author != null) ? String.valueOf(author.getTwitterId()) : null;
    }

    public List<Keyword> getKeywords() {
        return new ArrayList<>(keywords);
    }

    @Override
    public String getNlpAnalyzableContent() {
        String analyzableContent = StringUtils.defaultString(this.text) + ". ";
        return analyzableContent;
    }

    @Override
    public void completeMissingDataWith(final TwitterPost post) {
        if (this.id == null) this.id = post.id;
        if (StringUtils.isBlank(this.authorId)) this.authorId = post.authorId;
        if (this.latitude == 0) this.latitude = post.latitude;
        if (this.longitude == 0) this.longitude = post.longitude;
        if (StringUtils.isBlank(this.language)) this.language = post.language;
        if (StringUtils.isBlank(this.text)) this.text = post.text;
        if (StringUtils.isBlank(this.tweetUrl)) this.tweetUrl = post.tweetUrl;
        if (this.creationTimestamp == null) this.creationTimestamp = post.creationTimestamp;
    }

    @Override
    public void updateWithExistingDataIn(final TwitterPost post) {
        if (post.id != null) this.id = post.id;
        if (StringUtils.isNotBlank(post.authorId)) this.authorId = post.authorId;
        if (post.latitude != 0) this.latitude = post.latitude;
        if (post.longitude != 0) this.longitude = post.longitude;
        if (StringUtils.isNotBlank(post.language)) this.language = post.language;
        if (StringUtils.isNotBlank(post.text)) this.text = post.text;
        if (StringUtils.isNotBlank(post.tweetUrl)) this.tweetUrl = post.tweetUrl;
        if (post.creationTimestamp != null) this.creationTimestamp = post.creationTimestamp;
    }

    @Override
    public void replaceDataWith(final TwitterPost post) {
        this.id = post.id;
        this.authorId = post.authorId;
        this.latitude = post.latitude;
        this.longitude = post.longitude;
        this.language = post.language;
        this.text = post.text;
        this.tweetUrl = post.tweetUrl;
        this.creationTimestamp = post.creationTimestamp;
    }

    public Long getTwitterId() {
        return id;
    }

    public void setTwitterId(Long twitterId) {
        this.id = twitterId;
    }

    public String getAuthorTwitterId() {
        return authorId;
    }

    public void setAuthorTwitterId(String authorTwitterId) {
        this.authorId = authorTwitterId;
    }

    public int getLatitude() {
        return latitude;
    }

    public void setLatitude(int latitude) {
        this.latitude = latitude;
    }

    public int getLongitude() {
        return longitude;
    }

    public void setLongitude(int longitude) {
        this.longitude = longitude;
    }

    public String getLanguage() {
        return language;
    }

    public void setLanguage(String language) {
        this.language = language;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public String getTweetUrl() {
        if (author != null && author.getScreenName() != null) {
            return String.format(POST_URL, author.getScreenName(), id);
        }
        return tweetUrl;
    }

    public void setTweetUrl(String tweetUrl) {
        this.tweetUrl = tweetUrl;
    }

    public List<TwitterPostFav> getFavs() {
        return favs;
    }

    public void setFavs(List<TwitterPostFav> favs) {
        this.favs = favs;
    }

    public List<TwitterPostRetweet> getRetweets() {
        return retweets;
    }

    public void setRetweets(List<TwitterPostRetweet> retweets) {
        this.retweets = retweets;
    }

    public List<TwitterPostReply> getReplies() {
        return replies;
    }

    public void setReplies(List<TwitterPostReply> replies) {
        this.replies = replies;
    }

    public TwitterPostReply findReplyByAuthorId(Long authorId) {
        if (authorId == null) {
            return null;
        }
        for (TwitterPostReply reply : this.replies) {
            if (authorId.equals(reply.getAuthorId())) {
                return reply;
            }
        }
        return null;
    }

    public TwitterPostRetweet findRetweetByAuthorId(Long authorId) {
        if (authorId == null) {
            return null;
        }
        for (TwitterPostRetweet retweet : this.retweets) {
            if (authorId.equals(retweet.getAuthorId())) {
                return retweet;
            }
        }
        return null;
    }

    public TwitterPostFav findFavByAuthorId(Long authorId) {
        if (authorId == null) {
            return null;
        }
        for (TwitterPostFav fav : this.favs) {
            if (authorId.equals(fav.getAuthorId())) {
                return fav;
            }
        }
        return null;
    }

    @Override
    public String toString() {
        return "TwitterPost{" +
                "id=" + id +
                ", authorId=" + authorId +
                ", latitude=" + latitude +
                ", longitude=" + longitude +
                ", language='" + language + '\'' +
                ", text='" + text + '\'' +
                ", tweetUrl='" + tweetUrl + '\'' +
                ", creationTimestamp=" + creationTimestamp +
                ", favs=" + favs +
                ", retweets=" + retweets +
                ", replies=" + replies +
                ", author=" + author +
                '}';
    }
}
