package me.relevante.core;

import org.springframework.data.mongodb.core.mapping.Document;

import java.util.Objects;

@Document(collection = "TwitterRetweet")
public class TwitterRetweet extends AbstractNetworkEngagePostAction<Twitter> implements NetworkEngagePostAction<Twitter> {

    private TwitterRetweet() {
        this(null, null, null);
    }

    public TwitterRetweet(final String authorId,
                          final String tweetProfileId,
                          final String tweetId) {
        super(authorId, tweetProfileId, tweetId);
    }

    @Override
    public Twitter getNetwork() {
        return Twitter.getInstance();
    }

    @Override
    public boolean equals(final Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        final TwitterRetweet that = (TwitterRetweet) o;
        return Objects.equals(this.authorId, that.authorId) &&
                Objects.equals(this.targetProfileId, that.targetProfileId) &&
                Objects.equals(this.targetPostId, that.targetPostId);
    }

    @Override
    public int hashCode() {
        return Objects.hash(authorId, targetProfileId, targetPostId);
    }
}
