package me.relevante.core;

import org.springframework.stereotype.Component;

@Component
public class Twitter extends AbstractNetwork implements Network {

    private static final String NAME = "twitter";
    private static Twitter instance = new Twitter();

    public static Twitter getInstance() {
        return instance;
    }

    private Twitter() {
        super(NAME);
    }

}
