package me.relevante.core;

import org.apache.commons.lang3.StringUtils;
import org.springframework.data.annotation.Id;
import org.springframework.data.annotation.Transient;

import java.util.ArrayList;
import java.util.List;

public final class TwitterProfile implements NetworkProfile<Twitter, TwitterProfile> {

    private static final String PROFILE_URL = "https://twitter.com/%s";

    @Id
    private Long id;
    private String name;
    private String screenName;
    private String location;
    private String description;
    private String profileUrl;
    private String imageUrl;
    private String bioDescription;
    private String bioUrl;
    private String followers;
    private String following;

    @Transient
    private List<TwitterFollow> follows;
    @Transient
    private List<TwitterDirectMessage> directMessages;

    public TwitterProfile() {
        this.follows = new ArrayList<>();
        this.directMessages = new ArrayList<>();
    }

    @Override
    public Twitter getNetwork() {
        return Twitter.getInstance();
    }

    @Override
    public String getId() {
        return String.valueOf(id);
    }

    @Override
    public String getNlpAnalyzableContent() {
        String analyzableContent = StringUtils.defaultString(description) + ". " +
                StringUtils.defaultString(bioDescription) + ". ";
        return analyzableContent;
    }

    public Long getTwitterId() {
        return id;
    }

    public void setTwitterId(Long twitterId) {
        this.id = twitterId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getScreenName() {
        return screenName;
    }

    public void setScreenName(String screenName) {
        this.screenName = screenName;
    }

    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getProfileUrl() {
        if (screenName != null) {
            return String.format(PROFILE_URL, screenName);
        }
        return profileUrl;
    }

    public void setProfileUrl(String profileUrl) {
        this.profileUrl = profileUrl;
    }

    public String getImageUrl() {
        return imageUrl;
    }

    public void setImageUrl(String imageUrl) {
        this.imageUrl = imageUrl;
    }

    public String getBioDescription() {
        return bioDescription;
    }

    public void setBioDescription(String bioDescription) {
        this.bioDescription = bioDescription;
    }

    public String getBioUrl() {
        return bioUrl;
    }

    public void setBioUrl(String bioUrl) {
        this.bioUrl = bioUrl;
    }

    public String getFollowers() {
        return followers;
    }

    public void setFollowers(String followers) {
        this.followers = followers;
    }

    public String getFollowing() {
        return following;
    }

    public void setFollowing(String following) {
        this.following = following;
    }

    public List<TwitterFollow> getFollows() {
        return follows;
    }

    public List<TwitterDirectMessage> getDirectMessages() {
        return directMessages;
    }

    @Override
    public void completeMissingDataWith(final TwitterProfile profile) {
        if (this.id == null) this.id = profile.id;
        if (StringUtils.isBlank(this.name)) this.name = profile.name;
        if (StringUtils.isBlank(this.screenName)) this.screenName = profile.screenName;
        if (StringUtils.isBlank(this.location)) this.location = profile.location;
        if (StringUtils.isBlank(this.description)) this.description = profile.description;
        if (StringUtils.isBlank(this.profileUrl)) this.profileUrl = profile.profileUrl;
        if (StringUtils.isBlank(this.imageUrl)) this.imageUrl = profile.imageUrl;
        if (StringUtils.isBlank(this.bioDescription)) this.bioDescription = profile.bioDescription;
        if (StringUtils.isBlank(this.bioUrl)) this.bioUrl = profile.bioUrl;
        if (StringUtils.isBlank(this.followers)) this.followers = profile.followers;
        if (StringUtils.isBlank(this.following)) this.following = profile.following;
    }

    @Override
    public void updateWithExistingDataIn(final TwitterProfile profile) {
        if (profile.id != null) this.id = profile.id;
        if (StringUtils.isNotBlank(profile.name)) this.name = profile.name;
        if (StringUtils.isNotBlank(profile.screenName)) this.screenName = profile.screenName;
        if (StringUtils.isNotBlank(profile.location)) this.location = profile.location;
        if (StringUtils.isNotBlank(profile.description)) this.description = profile.description;
        if (StringUtils.isNotBlank(profile.profileUrl)) this.profileUrl = profile.profileUrl;
        if (StringUtils.isNotBlank(profile.imageUrl)) this.imageUrl = profile.imageUrl;
        if (StringUtils.isNotBlank(profile.bioDescription)) this.bioDescription = profile.bioDescription;
        if (StringUtils.isNotBlank(profile.bioUrl)) this.bioUrl = profile.bioUrl;
        if (StringUtils.isNotBlank(profile.followers)) this.followers = profile.followers;
        if (StringUtils.isNotBlank(profile.following)) this.following = profile.following;
    }

    @Override
    public void replaceDataWith(final TwitterProfile profile) {
        this.id = profile.id;
        this.name = profile.name;
        this.screenName = profile.screenName;
        this.location = profile.location;
        this.description = profile.description;
        this.profileUrl = profile.profileUrl;
        this.imageUrl = profile.imageUrl;
        this.bioDescription = profile.bioDescription;
        this.bioUrl = profile.bioUrl;
        this.followers = profile.followers;
        this.following = profile.following;
    }

    public TwitterFollow findFollowByAuthorId(Long authorId) {
        if (authorId == null) {
            return null;
        }
        for (TwitterFollow follow : this.follows) {
            if (authorId.equals(follow.getAuthorId())) {
                return follow;
            }
        }
        return null;
    }

    public TwitterDirectMessage findDirectMessageByAuthorId(Long authorId) {
        if (authorId == null) {
            return null;
        }
        for (TwitterDirectMessage directMessage : this.directMessages) {
            if (authorId.equals(directMessage.getAuthorId())) {
                return directMessage;
            }
        }
        return null;
    }

    @Override
    public String toString() {
        return "TwitterProfile{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", screenName='" + screenName + '\'' +
                ", location='" + location + '\'' +
                ", description='" + description + '\'' +
                ", profileUrl='" + profileUrl + '\'' +
                ", imageUrl='" + imageUrl + '\'' +
                ", bioDescription='" + bioDescription + '\'' +
                ", bioUrl='" + bioUrl + '\'' +
                ", followers='" + followers + '\'' +
                ", following='" + following + '\'' +
                ", follows=" + follows +
                ", directMessages=" + directMessages +
                '}';
    }
}