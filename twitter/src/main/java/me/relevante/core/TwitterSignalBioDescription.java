package me.relevante.core;

import java.util.Date;

public class TwitterSignalBioDescription extends AbstractTwitterSignal implements NetworkSignal<Twitter> {

    protected TwitterSignalBioDescription() {
        super(null, null, null);
    }

    public TwitterSignalBioDescription(final TwitterProfile profile) {
        super(new Date(),
                profile.getId(),
                profile.getBioDescription());
    }

    @Override
    public SignalCategory getCategory() {
        return SignalCategory.SELF_DESCRIPTION;
    }
}
