package me.relevante.core;

import org.springframework.data.annotation.Id;
import org.springframework.data.annotation.Transient;
import org.springframework.data.mongodb.core.mapping.Document;

import java.util.ArrayList;
import java.util.List;

@Document(collection = "TwitterFullPost")
public class TwitterFullPost implements NetworkFullPost<Twitter, TwitterPost, TwitterFullPost> {

    @Id
    private String id;
    private TwitterPost post;

    @Transient
    private List<TwitterFav> favs;
    @Transient
    private List<TwitterRetweet> retweets;
    @Transient
    private List<TwitterReply> replies;

    private TwitterFullPost() {
        this.favs = new ArrayList<>();
        this.retweets = new ArrayList<>();
        this.replies = new ArrayList<>();
    }

    public TwitterFullPost(TwitterPost post) {
        this();
        this.id = post.getId();
        this.post = post;
    }

    @Override
    public Twitter getNetwork() {
        return Twitter.getInstance();
    }

    @Override
    public String getId() {
        return id;
    }

    @Override
    public TwitterPost getPost() {
        return post;
    }

    @Override
    public int hashCode() {
        return this.id.hashCode();
    }

    @Override
    public boolean equals(Object obj) {
        TwitterFullPost that = (TwitterFullPost) obj;
        return this.id.equals(that.id);
    }

    @Override
    public void completeMissingDataWith(final TwitterFullPost fullPost) {
        this.post.completeMissingDataWith(fullPost.post);
        if (this.favs.isEmpty()) this.favs = new ArrayList<>(fullPost.favs);
        if (this.replies.isEmpty()) this.replies = new ArrayList<>(fullPost.replies);
        if (this.retweets.isEmpty()) this.retweets = new ArrayList<>(fullPost.retweets);
    }

    @Override
    public void updateWithExistingDataIn(final TwitterFullPost fullPost) {
        this.post.updateWithExistingDataIn(fullPost.post);
        if (!fullPost.favs.isEmpty()) this.favs = new ArrayList<>(fullPost.favs);
        if (!fullPost.replies.isEmpty()) this.replies = new ArrayList<>(fullPost.replies);
        if (!fullPost.retweets.isEmpty()) this.retweets = new ArrayList<>(fullPost.retweets);
    }

    @Override
    public void replaceDataWith(final TwitterFullPost fullPost) {
        this.post.replaceDataWith(fullPost.post);
        this.favs = new ArrayList<>(fullPost.favs);
        this.replies = new ArrayList<>(fullPost.replies);
        this.retweets = new ArrayList<>(fullPost.retweets);
    }

    public List<TwitterFav> getFavs() {
        return favs;
    }

    public void setFavs(List<TwitterFav> favs) {
        this.favs = favs;
    }

    public List<TwitterRetweet> getRetweets() {
        return retweets;
    }

    public void setRetweets(List<TwitterRetweet> retweets) {
        this.retweets = retweets;
    }

    public List<TwitterReply> getReplies() {
        return replies;
    }

    public void setReplies(List<TwitterReply> replies) {
        this.replies = replies;
    }

    public List<TwitterReply> findRepliesByAuthorId(String authorId) {
        List<TwitterReply> replies = new ArrayList<>();
        if (authorId == null) {
            return replies;
        }
        for (TwitterReply reply : this.replies) {
            if (authorId.equals(reply.getAuthorId())) {
                replies.add(reply);
            }
        }
        return replies;
    }

    public List<TwitterRetweet> findRetweetsByAuthorId(String authorId) {
        List<TwitterRetweet> retweets = new ArrayList<>();
        if (authorId == null) {
            return retweets;
        }
        for (TwitterRetweet retweet : this.retweets) {
            if (authorId.equals(retweet.getAuthorId())) {
                retweets.add(retweet);
            }
        }
        return retweets;
    }

    public List<TwitterFav> findFavsByAuthorId(String authorId) {
        List<TwitterFav> favs = new ArrayList<>();
        if (authorId == null) {
            return favs;
        }
        for (TwitterFav fav : this.favs) {
            if (authorId.equals(fav.getAuthorId())) {
                favs.add(fav);
            }
        }
        return favs;
    }
}
