package me.relevante.core;

public class TwitterSignalPostAuthorship extends AbstractTwitterSignal implements NetworkSignal<Twitter> {

    protected TwitterSignalPostAuthorship() {
        super(null, null, null);
    }

    public TwitterSignalPostAuthorship(final TwitterPost post) {
        super(post.getCreationTimestamp(),
                post.getAuthorId(),
                post.getNlpAnalyzableContent());
        this.shortDescription = post.getText();
    }

    @Override
    public SignalCategory getCategory() {
        return SignalCategory.CONTENT_ACTIVITY;
    }
}
