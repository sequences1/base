package me.relevante.core;

import org.springframework.data.mongodb.core.mapping.Document;

import java.util.Objects;

@Document(collection = "TwitterFollow")
public class TwitterFollow extends AbstractNetworkEngageAction<Twitter> implements NetworkEngageAction<Twitter> {

    private TwitterFollow() {
        this(null, null);
    }

    public TwitterFollow(final String authorId,
                         final String targetProfileId) {
        super(authorId, targetProfileId);
    }

    @Override
    public Twitter getNetwork() {
        return Twitter.getInstance();
    }

    @Override
    public boolean equals(final Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        final TwitterFollow that = (TwitterFollow) o;
        return Objects.equals(this.authorId, that.authorId) &&
                Objects.equals(this.targetProfileId, that.targetProfileId);
    }

    @Override
    public int hashCode() {
        return Objects.hash(authorId, targetProfileId);
    }

}
