package me.relevante.core;

import org.springframework.data.mongodb.core.mapping.Document;

import java.util.Date;
import java.util.Objects;

@Document(collection = "TwitterDirectMessage")
public class TwitterDirectMessage extends AbstractNetworkEngageAction<Twitter> implements NetworkEngageAction<Twitter> {

    private String message;

    private TwitterDirectMessage() {
        this(null, null, null);
    }

    public TwitterDirectMessage(final String authorId,
                                final String targetProfileId,
                                final String message) {
        this(authorId, targetProfileId, message, new Date());
    }

    public TwitterDirectMessage(final String authorId,
                                final String targetProfileId,
                                final String message,
                                final Date createdAt) {
        super(authorId, targetProfileId);
        this.message = message;
        this.creationTimestamp = createdAt;
    }

    @Override
    public Twitter getNetwork() {
        return Twitter.getInstance();
    }

    public String getMessage() {
        return message;
    }

    @Override
    public boolean equals(final Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        final TwitterDirectMessage that = (TwitterDirectMessage) o;
        return Objects.equals(this.authorId, that.authorId) &&
                Objects.equals(this.targetProfileId, that.targetProfileId) &&
                Objects.equals(this.creationTimestamp, that.creationTimestamp);
    }

    @Override
    public int hashCode() {
        return Objects.hash(authorId, targetProfileId, creationTimestamp);
    }

}
