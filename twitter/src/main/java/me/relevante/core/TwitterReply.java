package me.relevante.core;

import org.springframework.data.mongodb.core.mapping.Document;

import java.util.Objects;

@Document(collection = "TwitterReply")
public class TwitterReply extends AbstractNetworkEngagePostAction<Twitter> implements NetworkEngagePostAction<Twitter> {

    private String replyText;

    private TwitterReply() {
        this(null, null, null, null);
    }

    public TwitterReply(final String authorId,
                        final String tweetProfileId,
                        final String tweetId,
                        final String replyText) {
        super(authorId, tweetProfileId, tweetId);
        this.replyText = replyText;
    }

    @Override
    public Twitter getNetwork() {
        return Twitter.getInstance();
    }

    public String getReplyText() {
        return replyText;
    }

    @Override
    public boolean equals(final Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        final TwitterReply that = (TwitterReply) o;
        return Objects.equals(this.authorId, that.authorId) &&
                Objects.equals(this.targetProfileId, that.targetProfileId) &&
                Objects.equals(this.targetPostId, that.targetPostId) &&
                Objects.equals(this.creationTimestamp, that.creationTimestamp);
    }

    @Override
    public int hashCode() {
        return Objects.hash(authorId, targetProfileId, targetPostId, creationTimestamp);
    }
}
