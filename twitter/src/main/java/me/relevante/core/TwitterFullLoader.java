package me.relevante.core;

import me.relevante.persistence.NetworkFullPostRepo;
import me.relevante.persistence.NetworkPostActionRepo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Component;

/**
 * @author Daniel Ibanez
 */
@Component
public class TwitterFullLoader
        extends AbstractNetworkFullLoader<Twitter, TwitterFullProfile, TwitterFullPost, TwitterFav, TwitterReply>
        implements NetworkFullLoader<Twitter, TwitterFullProfile, TwitterFullPost> {

    @Autowired
    public TwitterFullLoader(final CrudRepository<TwitterFullProfile, String> fullProfileRepo,
                             final NetworkFullPostRepo<TwitterFullPost> fullPostRepo,
                             final NetworkPostActionRepo<TwitterFav> level1ActionRepo,
                             final NetworkPostActionRepo<TwitterReply> level2ActionRepo) {
        super(fullProfileRepo, fullPostRepo, level1ActionRepo, level2ActionRepo);
    }

    @Override
    protected void addLevel1ActionToFullPost(final TwitterFav like, final TwitterFullPost fullPost) {
        fullPost.getFavs().add(like);
    }

    @Override
    protected void addLevel2ActionToFullPost(final TwitterReply comment, final TwitterFullPost fullPost) {
        fullPost.getReplies().add(comment);
    }

    @Override
    public Twitter getNetwork() {
        return Twitter.getInstance();
    }
}
