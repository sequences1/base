package me.relevante.core;

import java.util.Date;

public class TwitterSignalDescription extends AbstractTwitterSignal implements NetworkSignal<Twitter> {

    protected TwitterSignalDescription() {
        super(null, null, null);
    }

    public TwitterSignalDescription(final TwitterProfile profile) {
        super(new Date(),
                profile.getId(),
                profile.getDescription());
    }

    @Override
    public SignalCategory getCategory() {
        return SignalCategory.SELF_DESCRIPTION;
    }
}
