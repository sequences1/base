package me.relevante.core;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.index.CompoundIndex;
import org.springframework.data.mongodb.core.index.CompoundIndexes;
import org.springframework.data.mongodb.core.mapping.Document;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Document(collection = "TwitterFullProfile")
@CompoundIndexes({
        @CompoundIndex(name = "profile._id_1", def = "{ 'profile._id': 1 }", unique = true)
})
public class TwitterFullProfile implements NetworkFullProfileWithPosts<Twitter, TwitterProfile, TwitterFullPost, TwitterFullProfile> {

    @Id
    private String id;
    private TwitterProfile profile;
    private TwitterFullPost lastPost;
    private List<TwitterFollow> follows;
    private List<TwitterDirectMessage> directMessages;
    private List<String> postIds;
    private String uniqueProfileId;
    private Date lastUpdatedTimestamp;
    private Date lastActivityUpdatedTimestamp;

    public TwitterFullProfile() {
        super();
        this.follows = new ArrayList<>();
        this.directMessages = new ArrayList<>();
        this.postIds = new ArrayList<>();
    }

    public TwitterFullProfile(TwitterProfile profile) {
        this();
        this.profile = profile;
        this.id = profile.getId();
    }

    @Override
    public Twitter getNetwork() {
        return Twitter.getInstance();
    }

    @Override
    public String getId() {
        return id;
    }

    @Override
    public TwitterProfile getProfile() {
        return profile;
    }

    @Override
    public TwitterFullPost getLastPost() {
        return lastPost;
    }

    @Override
    public void setLastPost(final TwitterFullPost lastPost) {
        this.lastPost = lastPost;
    }

    @Override
    public String getUniqueProfileId() {
        return uniqueProfileId;
    }

    @Override
    public void setUniqueProfileId(final String uniqueProfileId) {
        this.uniqueProfileId = uniqueProfileId;
    }

    @Override
    public void completeMissingDataWith(final TwitterFullProfile fullProfile) {
        this.profile.completeMissingDataWith(fullProfile.profile);
        if (this.lastPost == null) this.lastPost = fullProfile.lastPost;
        if (this.follows.isEmpty()) this.follows = new ArrayList<>(fullProfile.follows);
        if (this.directMessages.isEmpty()) this.directMessages = new ArrayList<>(fullProfile.directMessages);
        if (this.postIds.isEmpty()) this.postIds = new ArrayList<>(fullProfile.postIds);
        markAsUpdated();
    }

    @Override
    public void updateWithExistingDataIn(final TwitterFullProfile fullProfile) {
        this.profile.updateWithExistingDataIn(fullProfile.profile);
        if (fullProfile.lastPost != null) this.lastPost = fullProfile.lastPost;
        if (!fullProfile.follows.isEmpty()) this.follows = new ArrayList<>(fullProfile.follows);
        if (!fullProfile.directMessages.isEmpty()) this.directMessages = new ArrayList<>(fullProfile.directMessages);
        if (!fullProfile.postIds.isEmpty()) this.postIds = new ArrayList<>(fullProfile.postIds);
        markAsUpdated();
    }

    @Override
    public void replaceDataWith(final TwitterFullProfile fullProfile) {
        this.profile.replaceDataWith(fullProfile.profile);
        this.lastPost = fullProfile.lastPost;
        this.follows = new ArrayList<>(fullProfile.follows);
        this.directMessages = new ArrayList<>(fullProfile.directMessages);
        this.postIds = new ArrayList<>(fullProfile.postIds);
        markAsUpdated();
    }

    @Override
    public Date getLastUpdatedTimestamp() {
        return lastUpdatedTimestamp;
    }

    @Override
    public Date getLastActivityUpdatedTimestamp() {
        return lastActivityUpdatedTimestamp;
    }

    public void setActivityUpdated() {
        this.lastActivityUpdatedTimestamp = new Date();
    }

    public List<TwitterFollow> getFollows() {
        return follows;
    }

    public List<TwitterDirectMessage> getDirectMessages() {
        return directMessages;
    }

    public List<String> getPostIds() {
        return postIds;
    }

    public List<TwitterFollow> findFollowsByAuthorId(String authorId) {
        List<TwitterFollow> follows = new ArrayList<>();
        if (authorId == null) {
            return follows;
        }
        for (TwitterFollow follow : this.follows) {
            if (authorId.equals(follow.getAuthorId())) {
                follows.add(follow);
            }
        }
        return follows;
    }

    public List<TwitterDirectMessage> findDirectMessagesByAuthorId(String authorId) {
        List<TwitterDirectMessage> directMessages = new ArrayList<>();
        if (authorId == null) {
            return directMessages;
        }
        for (TwitterDirectMessage directMessage : this.directMessages) {
            if (authorId.equals(directMessage.getAuthorId())) {
                directMessages.add(directMessage);
            }
        }
        return directMessages;
    }

    private void markAsUpdated() {
        this.lastUpdatedTimestamp = new Date();
    }

}
