package me.relevante.core;

import org.springframework.data.mongodb.core.mapping.Document;

import java.util.Date;

@Document
public class TwitterPostReply implements NetworkEntity {

    private long authorId;
    private long tweetId;
    private String replyText;
    private Date creationTimestamp;

    private TwitterPostReply() {
    }

    public TwitterPostReply(long authorId,
                            long tweetId,
                            String replyText,
                            Date creationTimestamp) {
        this.authorId = authorId;
        this.tweetId = tweetId;
        this.replyText = replyText;
        this.creationTimestamp = creationTimestamp;
    }

    @Override
    public Network getNetwork() {
        return Twitter.getInstance();
    }

    public String getAuthorId() {
        return String.valueOf(authorId);
    }

    public String getPostId() {
        return String.valueOf(tweetId);
    }

    public Date getCreationTimestamp() {
        return creationTimestamp;
    }

    public void setCreationTimestamp(Date timestamp) {
        this.creationTimestamp = timestamp;
    }

    public String getReplyText() {
        return replyText;
    }

    public Long getAuthorTwitterId() {
        return authorId;
    }

    public Long getPostTwitterId() {
        return tweetId;
    }

}
