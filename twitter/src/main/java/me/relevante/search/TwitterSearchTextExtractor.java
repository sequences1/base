package me.relevante.search;

import me.relevante.core.Twitter;
import me.relevante.core.TwitterFullPost;
import me.relevante.core.TwitterFullProfile;
import me.relevante.core.TwitterPost;
import me.relevante.core.TwitterProfile;
import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Component;

@Component
public class TwitterSearchTextExtractor
        extends AbstractNetworkSearchTextExtractor<Twitter, TwitterFullProfile, TwitterFullPost>
        implements NetworkSearchTextExtractor<Twitter, TwitterFullProfile, TwitterFullPost> {

    @Override
    public String extractSearchText(final TwitterFullProfile fullProfile) {
        return fullProfile.getNetwork().getName() + " " +
                extractSearchText(fullProfile.getProfile());
    }

    @Override
    public String extractSearchText(final TwitterFullPost fullPost) {
        return extractSearchText(fullPost.getPost());
    }

    @Override
    public Twitter getNetwork() {
        return Twitter.getInstance();
    }

    private String extractSearchText(final TwitterProfile profile) {
        return StringUtils.defaultString(profile.getBioDescription()) + " " +
                StringUtils.defaultString(profile.getDescription()) + " " +
                StringUtils.defaultString(profile.getLocation()) + " " +
                StringUtils.defaultString(profile.getName()) + " " +
                StringUtils.defaultString(profile.getScreenName());
    }

    private String extractSearchText(final TwitterPost post) {
        return StringUtils.defaultString(post.getText()) + " ";
    }

}
