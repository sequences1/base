package me.relevante.search;

import me.relevante.core.NetworkSignal;
import me.relevante.core.Twitter;
import me.relevante.core.TwitterFullLoader;
import me.relevante.core.TwitterFullPost;
import me.relevante.core.TwitterFullProfile;
import me.relevante.core.TwitterProfile;
import me.relevante.nlp.TwitterSignalExtractor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.Collections;
import java.util.List;

/**
 * @author daniel-ibanez
 */
@Component
public class TwitterSearchAttributesLoader
        extends AbstractNetworkSearchAttributesLoader<Twitter, TwitterFullProfile, TwitterFullPost>
        implements NetworkSearchAttributesLoader<Twitter> {

    @Autowired
    public TwitterSearchAttributesLoader(final TwitterFullLoader fullLoader,
                                         final TwitterSignalExtractor signalExtractor,
                                         final TwitterSearchTextExtractor textSearchExtractor) {
        super(fullLoader, signalExtractor, textSearchExtractor);
    }

    @Override
    public Twitter getNetwork() {
        return Twitter.getInstance();
    }

    @Override
    protected SearchAttributes<Twitter> createSearchAttributes(final TwitterFullProfile fullProfile,
                                                               final List<NetworkSignal<Twitter>> networkSignals,
                                                               final String fullText) {
        final TwitterProfile profile = fullProfile.getProfile();
        return new SearchAttributes(profile.getLocation(), null, Collections.emptyList(), fullText, networkSignals);
    }
}
