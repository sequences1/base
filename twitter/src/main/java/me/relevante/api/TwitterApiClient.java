package me.relevante.api;

import me.relevante.core.TwitterPost;
import me.relevante.core.TwitterProfile;
import me.relevante.core.Twitter;

import java.util.List;

/**
 * Created by daniel-ibanez on 16/07/16.
 */
public interface TwitterApiClient extends NetworkApiClient<Twitter, TwitterProfile> {

    TwitterProfile getProfile(String profileId) throws TwitterApiException;
    TwitterProfile getProfileByScreenName(String screenName) throws TwitterApiException;
    List<TwitterProfile> getFriends(String profileId) throws TwitterApiException;
    List<TwitterPost> getTimeline(String profileId) throws TwitterApiException;
    TwitterPost getTweet(String id) throws TwitterApiException;
    List<TwitterPost> getSearchResults(List<String> searchTerms) throws TwitterApiException;
    TwitterApiResponse follow(String profileId) throws TwitterApiException;
    TwitterApiResponse sendDirectMessage(String profileId, String message) throws TwitterApiException;
    TwitterApiResponse replyPost(String postId, String screenName, String text) throws TwitterApiException;
    TwitterApiResponse retweet(String tweetId) throws TwitterApiException;
    TwitterApiResponse favTweet(String tweetId) throws TwitterApiException;

}
