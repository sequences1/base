package me.relevante.api;

import me.relevante.core.TwitterDirectMessage;
import me.relevante.core.TwitterPost;
import me.relevante.core.TwitterProfile;
import me.relevante.core.Twitter;
import me.relevante.auth.OAuthKeyPair;
import me.relevante.auth.OAuthTokenPair;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import twitter4j.DirectMessage;
import twitter4j.PagableResponseList;
import twitter4j.Query;
import twitter4j.QueryResult;
import twitter4j.ResponseList;
import twitter4j.Status;
import twitter4j.StatusUpdate;
import twitter4j.TwitterException;
import twitter4j.TwitterFactory;
import twitter4j.User;
import twitter4j.conf.ConfigurationBuilder;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Optional;
import java.util.UUID;

public class TwitterApiClientImpl implements TwitterApiClient {

    private static final Logger logger = LoggerFactory.getLogger(TwitterApiClientImpl.class);
	private static final String TWITTER_URL = "http://twitter.com/";
    private static final int ERROR_CODE_ALREADY_FAV = 139;
    private static final int ERROR_CODE_ALREADY_RETWEETED = 327;
    private static final int ERROR_CODE_STATUS_IS_A_DUPLICATE = 187;

    private String id;
    private twitter4j.Twitter twitter4j;

    public TwitterApiClientImpl(OAuthKeyPair<Twitter> oAuthConsumerKeyPair,
                                OAuthTokenPair<Twitter> oAuthAccessToken) {
        this.id = UUID.randomUUID().toString();
        this.twitter4j = createTwitter4j(oAuthConsumerKeyPair, oAuthAccessToken);
    }

    @Override
    public Twitter getNetwork() {
        return Twitter.getInstance();
    }

    @Override
    public TwitterProfile getProfile() throws TwitterApiException {
        try {
            final User user = twitter4j.verifyCredentials();
            final TwitterProfile profile = createTwitterProfileFromTwitter4jUser(user);
            return profile;
        }
        catch (TwitterException e) {
            logger.error("Error getting Profile Data in Twitter API", e);
            throw new TwitterApiException(e);
        }
    }

    @Override
    public TwitterProfile getProfile(String profileId) throws TwitterApiException {
        try {
            final User user = twitter4j.showUser(Long.parseLong(profileId));
            final TwitterProfile profile = createTwitterProfileFromTwitter4jUser(user);
            return profile;
        }
        catch (TwitterException e) {
            logger.error("Error getting Profile Data in Twitter API", e);
            throw new TwitterApiException(e);
        }
    }

    @Override
    public TwitterProfile getProfileByScreenName(String screenName) throws TwitterApiException {

        try {
            User user = twitter4j.showUser(screenName);
            TwitterProfile profile = createTwitterProfileFromTwitter4jUser(user);
            return profile;
        }
        catch (TwitterException e) {
            throw new TwitterApiException(e);
        }
    }

    @Override
    public List<TwitterProfile> getFriends(String profileId) throws TwitterApiException {

        try {
            List<TwitterProfile> friends = new ArrayList<>();
            long cursor = -1;
            PagableResponseList<User> friendsList;
            do {
                friendsList = twitter4j.getFriendsList(Long.parseLong(profileId), cursor, 200);
                Iterator<User> iterator = friendsList.iterator();
                while (iterator.hasNext()) {
                    User user = iterator.next();
                    TwitterProfile friend = createTwitterProfileFromTwitter4jUser(user);
                    friends.add(friend);
                }
                cursor = friendsList.getNextCursor();
            }
            while (friendsList.hasNext());

            return friends;
        }
        catch (TwitterException e) {
            throw new TwitterApiException(e);
        }
    }

    @Override
    public List<TwitterPost> getTimeline(String profileId) throws TwitterApiException {
        try {
            List<TwitterPost> posts = new ArrayList<>();
            ResponseList<Status> homeTimeline = twitter4j.getUserTimeline(Long.parseLong(profileId));
            for (Status status : homeTimeline) {
                TwitterPost post = processTweet(status);
                posts.add(post);
            }
            return posts;
        }
        catch (TwitterException e) {
            throw new TwitterApiException(e);
        }
    }

    @Override
    public TwitterPost getTweet(String tweetId) throws TwitterApiException {
        try {
            Status status = twitter4j.showStatus(Long.parseLong(tweetId));
            TwitterPost twitterPost = processTweet(status);
            return twitterPost;
        }
        catch (TwitterException e) {
            throw new TwitterApiException(e);
        }
    }

    @Override
    public List<TwitterPost> getSearchResults(List<String> searchTerms) throws TwitterApiException {

        try {
            Query query = createQueryFromSearchArray(searchTerms);
            QueryResult queryResult = twitter4j.search().search(query);
            List<TwitterPost> resultPosts = new ArrayList<>();
            for (Status status : queryResult.getTweets()) {
                TwitterPost twitterPost = processTweet(status);
                resultPosts.add(twitterPost);
            }
            return resultPosts;
        }
        catch (TwitterException e) {
            throw new TwitterApiException(e);
        }
    }

    @Override
    public TwitterApiResponse follow(String profileId) throws TwitterApiException {
        try {
            User twitter4jUser = twitter4j.createFriendship(Long.parseLong(profileId));
            TwitterProfile profile = createTwitterProfileFromTwitter4jUser(twitter4jUser);
            return new TwitterApiResponse(TwitterApiResult.SUCCESS, profile);
        }
        catch (TwitterException e) {
            throw new TwitterApiException(e);
        }
    }

    @Override
    public TwitterApiResponse sendDirectMessage(String profileId,
                                                String message) throws TwitterApiException {
        try {
            DirectMessage directMessage = twitter4j.sendDirectMessage(Long.parseLong(profileId), message);
            TwitterDirectMessage twitterDirectMessage = createDirectMessageFromTwitter4jDirectMessage(directMessage);
            return new TwitterApiResponse(TwitterApiResult.SUCCESS, twitterDirectMessage);
        }
        catch (TwitterException e) {
            throw new TwitterApiException(e);
        }
    }

    @Override
    public TwitterApiResponse replyPost(String postId,
                                        String screenName,
                                        String text) throws TwitterApiException {
        try {
            StatusUpdate statusUpdate = new StatusUpdate("@" + screenName + " " + text);
            statusUpdate.inReplyToStatusId(Long.parseLong(postId));
            Status status = twitter4j.updateStatus(statusUpdate);
            TwitterPost twitterPost = processTweet(status);
            return new TwitterApiResponse(TwitterApiResult.SUCCESS, twitterPost);
        }
        catch (TwitterException e) {
            throw new TwitterApiException(e);
        }
    }

    @Override
    public TwitterApiResponse retweet(String tweetId) throws TwitterApiException {
        try {
            Status status = twitter4j.retweetStatus(Long.parseLong(tweetId));
            TwitterPost twitterPost = processTweet(status);
            return new TwitterApiResponse(TwitterApiResult.SUCCESS, twitterPost);
        }
        catch (TwitterException e) {
            if (e.getErrorCode() == ERROR_CODE_ALREADY_RETWEETED || e.getErrorCode() == ERROR_CODE_STATUS_IS_A_DUPLICATE) {
                return new TwitterApiResponse(TwitterApiResult.ALREADY_PROCESSED);
            }
            throw new TwitterApiException(e);
        }
    }

    @Override
    public TwitterApiResponse favTweet(String tweetId) throws TwitterApiException {
        try {
            Status status = twitter4j.createFavorite(Long.parseLong(tweetId));
            TwitterPost twitterPost = processTweet(status);
            return new TwitterApiResponse(TwitterApiResult.SUCCESS, twitterPost);
        } catch (TwitterException e) {
            if (e.getErrorCode() == ERROR_CODE_ALREADY_FAV || e.getErrorCode() == ERROR_CODE_STATUS_IS_A_DUPLICATE) {
                return new TwitterApiResponse(TwitterApiResult.ALREADY_PROCESSED);
            }
            throw new TwitterApiException(e);
        }
    }

    public String getId() {
        return id;
    }

    private TwitterPost processTweet(Status apiStatus) {

        TwitterPost post = createTwitterPostFromTwitter4jShare(apiStatus);
        User apiUser = apiStatus.getUser();
        TwitterProfile profile = createTwitterProfileFromTwitter4jUser(apiUser);
        post.setAuthor(profile);
        return post;
    }

    private Query createQueryFromSearchArray(List<String> searchTerms) {

        String query = "";
        for (int i = 0; i < searchTerms.size(); i++ ) {
            String keyword = searchTerms.get(i);
            if (searchTerms.size() == 1) {
                query = keyword;
            }
            else if ((i + 1) < searchTerms.size()) {
                String keywordNext = searchTerms.get(i+1);
                if (!query.equals("") ) {
                    query = query + " OR ";
                }
                query = query + " ( " + keyword + " AND " + keywordNext + " ) ";
            }
        }

        Query twitterQuery = new Query(query);
        twitterQuery.setCount(100);

        return twitterQuery;
    }

    private twitter4j.Twitter createTwitter4j(OAuthKeyPair<Twitter> oAuthConsumerKeyPair,
                                              OAuthTokenPair<Twitter> oAuthAccessTokenPair) {
        //TwitterUser Conf.
        ConfigurationBuilder cb = new ConfigurationBuilder()
                .setDebugEnabled(true)
                .setOAuthConsumerKey(oAuthConsumerKeyPair.getKey())
                .setOAuthConsumerSecret(oAuthConsumerKeyPair.getSecret())
                .setOAuthAccessToken(oAuthAccessTokenPair.getToken())
                .setOAuthAccessTokenSecret(oAuthAccessTokenPair.getSecret());

        TwitterFactory tf = new TwitterFactory(cb.build());
        twitter4j.Twitter twitter = tf.getInstance();

        return twitter;
    }

    private TwitterProfile createTwitterProfileFromTwitter4jUser(User twitter4JUser) {
        TwitterProfile profile = new TwitterProfile();
        profile.setTwitterId(twitter4JUser.getId());
        profile.setImageUrl(twitter4JUser.getProfileImageURL());
        profile.setScreenName(twitter4JUser.getScreenName());
        profile.setName(twitter4JUser.getName());
        profile.setDescription(twitter4JUser.getDescription());
        profile.setLocation(twitter4JUser.getLocation());
        profile.setProfileUrl(TWITTER_URL + twitter4JUser.getScreenName());
        profile.setFollowers(String.valueOf(twitter4JUser.getFollowersCount()));
        return profile;
    }

    private TwitterPost createTwitterPostFromTwitter4jShare(Status status) {
        TwitterPost post = new TwitterPost();
        post.setLanguage(status.getLang());
        post.setTwitterId(status.getId());
        post.setAuthorTwitterId(String.valueOf(status.getUser().getId()));
        post.setText(status.getText().replace("'", ""));
        post.setCreationTimestamp(status.getCreatedAt()); // Calendar.getInstance().getTime();
        post.setTweetUrl(TWITTER_URL + status.getUser().getScreenName() + "/status/" + status.getId());
        Optional.ofNullable(status.getGeoLocation()).ifPresent(location -> {
            post.setLatitude(new Double(location.getLatitude()).intValue());
            post.setLongitude(new Double(location.getLongitude()).intValue());
        });
        return post;
    }

    private TwitterDirectMessage createDirectMessageFromTwitter4jDirectMessage(DirectMessage directMessage) {
        TwitterDirectMessage twitterDirectMessage = new TwitterDirectMessage(Long.toString(directMessage.getSenderId()), Long.toString(directMessage.getRecipientId()), directMessage.getText(), directMessage.getCreatedAt());
        return twitterDirectMessage;
    }


    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        TwitterApiClientImpl that = (TwitterApiClientImpl) o;

        return !(id != null ? !id.equals(that.id) : that.id != null);

    }

    @Override
    public int hashCode() {
        return id != null ? id.hashCode() : 0;
    }
}
