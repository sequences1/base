package me.relevante.api;

public class TwitterApiResponse {

    private TwitterApiResult result;
    private Object relatedObject;

    public TwitterApiResponse(TwitterApiResult result,
                              Object expectedObject) {
        this.result = result;
        this.relatedObject = expectedObject;
    }

    public TwitterApiResponse(TwitterApiResult result) {
        this(result, null);
    }

    public TwitterApiResult getResult() {
        return result;
    }

    public Object getRelatedObject() {
        return relatedObject;
    }
}
