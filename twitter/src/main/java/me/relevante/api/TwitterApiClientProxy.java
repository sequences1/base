package me.relevante.api;

import me.relevante.core.TwitterPost;
import me.relevante.core.TwitterProfile;
import me.relevante.core.Twitter;
import me.relevante.auth.OAuthKeyPair;
import me.relevante.auth.OAuthTokenPair;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.LinkedList;
import java.util.List;

public class TwitterApiClientProxy implements TwitterApiClient {

    private static final Logger logger = LoggerFactory.getLogger(TwitterApiClientProxy.class);

    private LinkedList<TwitterApiClientImpl> readPool;
    private LinkedList<TwitterApiClientImpl> writePool;

    public TwitterApiClientProxy(List<OAuthKeyPair<Twitter>> oAuthConsumerKeyPairs,
                                 OAuthTokenPair<Twitter> oAuthAccessToken) {
        this.readPool = new LinkedList<>();
        this.writePool = new LinkedList<>();
        for (OAuthKeyPair<Twitter> oAuthConsumerKeyPair : oAuthConsumerKeyPairs) {
            this.readPool.add(new TwitterApiClientImpl(oAuthConsumerKeyPair, oAuthAccessToken));
            this.writePool.add(new TwitterApiClientImpl(oAuthConsumerKeyPair, oAuthAccessToken));
        }
    }

    @Override
    public Twitter getNetwork() {
        return Twitter.getInstance();
    }

    @Override
    public TwitterProfile getProfile() throws TwitterApiException {
        TwitterApiClientImpl apiAdapter = readPool.get(0);
        try {
            return apiAdapter.getProfile();
        } catch (TwitterApiException e) {
            processReadTwitterApiException(e);
            return getProfile();
        }
    }

    @Override
    public TwitterProfile getProfile(String profileId) throws TwitterApiException {
        TwitterApiClientImpl apiAdapter = readPool.get(0);
        try {
            return apiAdapter.getProfile(profileId);
        } catch (TwitterApiException e) {
            processReadTwitterApiException(e);
            return getProfile();
        }
    }

    @Override
    public TwitterProfile getProfileByScreenName(String screenName) throws TwitterApiException {
        TwitterApiClientImpl apiAdapter = readPool.get(0);
        try {
            TwitterProfile profile = apiAdapter.getProfileByScreenName(screenName);
            return profile;
        } catch (TwitterApiException e) {
            processReadTwitterApiException(e);
            return getProfile();
        }
    }

    @Override
    public List<TwitterProfile> getFriends(String profileId) throws TwitterApiException {
        TwitterApiClientImpl apiAdapter = readPool.get(0);
        try {
            List<TwitterProfile> profiles = apiAdapter.getFriends(profileId);
            return profiles;
        } catch (TwitterApiException e) {
            processReadTwitterApiException(e);
            return getFriends(profileId);
        }
    }

    @Override
    public List<TwitterPost> getTimeline(String profileId) throws TwitterApiException {
        TwitterApiClientImpl apiAdapter = readPool.get(0);
        try {
            List<TwitterPost> tweets = apiAdapter.getTimeline(profileId);
            return tweets;
        } catch (TwitterApiException e) {
            processReadTwitterApiException(e);
            return getTimeline(profileId);
        }
    }

    @Override
    public TwitterPost getTweet(String tweetId) throws TwitterApiException {
        TwitterApiClientImpl apiAdapter = readPool.get(0);
        try {
            TwitterPost tweet = apiAdapter.getTweet(tweetId);
            return tweet;
        } catch (TwitterApiException e) {
            processReadTwitterApiException(e);
            return getTweet(tweetId);
        }
    }

    @Override
    public List<TwitterPost> getSearchResults(List<String> searchTerms) throws TwitterApiException {
        TwitterApiClientImpl apiAdapter = readPool.get(0);
        try {
            List<TwitterPost> tweets = apiAdapter.getSearchResults(searchTerms);
            return tweets;
        } catch (TwitterApiException e) {
            processReadTwitterApiException(e);
            return getSearchResults(searchTerms);
        }
    }

    @Override
    public TwitterApiResponse follow(String profileId) throws TwitterApiException {
        TwitterApiClientImpl apiAdapter = writePool.get(0);
        try {
            TwitterApiResponse response = apiAdapter.follow(profileId);
            return response;
        } catch (TwitterApiException e) {
            processWriteTwitterApiException(e);
            return follow(profileId);
        }
    }

    @Override
    public TwitterApiResponse sendDirectMessage(String profileId, String message) throws TwitterApiException {
        TwitterApiClientImpl apiAdapter = writePool.get(0);
        try {
            TwitterApiResponse response = apiAdapter.sendDirectMessage(profileId, message);
            return response;
        } catch (TwitterApiException e) {
            processWriteTwitterApiException(e);
            return sendDirectMessage(profileId, message);
        }
    }

    @Override
    public TwitterApiResponse replyPost(String postId, String screenName, String text) throws TwitterApiException {
        TwitterApiClientImpl apiAdapter = writePool.get(0);
        try {
            TwitterApiResponse response = apiAdapter.replyPost(postId, screenName, text);
            return response;
        } catch (TwitterApiException e) {
            processWriteTwitterApiException(e);
            return replyPost(postId, screenName, text);
        }
    }

    @Override
    public TwitterApiResponse retweet(String tweetId) throws TwitterApiException {
        TwitterApiClientImpl apiAdapter = writePool.get(0);
        try {
            TwitterApiResponse response = apiAdapter.retweet(tweetId);
            return response;
        } catch (TwitterApiException e) {
            processWriteTwitterApiException(e);
            return retweet(tweetId);
        }
    }

    @Override
    public TwitterApiResponse favTweet(String tweetId) throws TwitterApiException {
        TwitterApiClientImpl apiAdapter = writePool.get(0);
        try {
            TwitterApiResponse response = apiAdapter.favTweet(tweetId);
            return response;
        } catch (TwitterApiException e) {
            processWriteTwitterApiException(e);
            return favTweet(tweetId);
        }
    }

    private void processReadTwitterApiException(TwitterApiException e) throws TwitterApiException {
        if (e.getRetryAfter() >= 0) {
            waitUntilApiIsAvailable(e.getRetryAfter());
            return;
        }
        TwitterApiClientImpl currentTwitterApiAdapter = readPool.get(0);
        this.readPool.remove(currentTwitterApiAdapter);
        this.writePool.remove(currentTwitterApiAdapter);
        if (readPool.isEmpty()) {
            throw new TwitterApiException(e.getMessage());
        }
    }

    private void processWriteTwitterApiException(TwitterApiException e) throws TwitterApiException {
        if (e.getRetryAfter() >= 0) {
            waitUntilApiIsAvailable(e.getRetryAfter());
            return;
        }
        this.writePool.remove(0);
        if (writePool.isEmpty()) {
            throw new TwitterApiException(e.getMessage());
        }
    }

    private void waitUntilApiIsAvailable(int retryAfter) {
        long millisToSleep = retryAfter * 1000;
        try {
            Thread.sleep(millisToSleep);
        } catch (InterruptedException e) {
            logger.error("Error", e);
        }
    }
}


