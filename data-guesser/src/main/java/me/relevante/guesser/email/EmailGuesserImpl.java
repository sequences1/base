package me.relevante.guesser.email;

import me.relevante.guesser.Clues;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Primary;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.List;

@Component
@Primary
public class EmailGuesserImpl implements EmailGuesser {

    private final List<? extends EmailGuesser> providerEmailGuessers;

    @Autowired
    public EmailGuesserImpl(final Collection<? extends EmailGuesser> providerEmailGuessers) {
        this.providerEmailGuessers = new ArrayList<>(providerEmailGuessers);
    }

    @Override
    public List<GuessedEmail> guess(final Clues clues) {
        for (final EmailGuesser providerEmailGuesser : providerEmailGuessers) {
            final List<GuessedEmail> guessedEmails = providerEmailGuesser.guess(clues);
            if (!guessedEmails.isEmpty()) {
                return guessedEmails;
            }
        }
        return Collections.emptyList();
    }

    @Override
    public List<GuessedEmailWithProfiles> guessWithProfiles(final Clues clues) {
        for (final EmailGuesser providerEmailGuesser : providerEmailGuessers) {
            final List<GuessedEmailWithProfiles> guessedEmailsWithProfiles = providerEmailGuesser.guessWithProfiles(clues);
            if (!guessedEmailsWithProfiles.isEmpty()) {
                return guessedEmailsWithProfiles;
            }
        }
        return Collections.emptyList();
    }
}
