package me.relevante.guesser.domain;

import me.relevante.guesser.Clues;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Primary;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.List;

@Component
@Primary
public class DomainGuesserImpl implements DomainGuesser {

    private final List<? extends DomainGuesser> profileDomainGuessers;

    @Autowired
    public DomainGuesserImpl(final Collection<? extends DomainGuesser> profileDomainGuessers) {
        this.profileDomainGuessers = new ArrayList<>(profileDomainGuessers);
    }

    @Override
    public List<GuessedDomain> guess(final Clues clues) {
        for (final DomainGuesser providerDomainGuesser : profileDomainGuessers) {
            final List<GuessedDomain> guessedDomains = providerDomainGuesser.guess(clues);
            if (!guessedDomains.isEmpty()) {
                return guessedDomains;
            }
        }
        return Collections.emptyList();
    }

}
