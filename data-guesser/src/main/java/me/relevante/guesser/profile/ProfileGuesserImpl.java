package me.relevante.guesser.profile;

import me.relevante.guesser.Clues;
import org.springframework.context.annotation.Primary;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.List;

@Component
@Primary
public class ProfileGuesserImpl implements ProfileGuesser {

    private final List<? extends ProfileGuesser> providerProfileGuessers;

    public ProfileGuesserImpl(final Collection<? extends ProfileGuesser> providerProfileGuessers) {
        this.providerProfileGuessers = new ArrayList<>(providerProfileGuessers);
    }

    @Override
    public List<GuessedProfile> guess(final Clues clues) {
        for (final ProfileGuesser providerProfileGuesser : providerProfileGuessers) {
            final List<GuessedProfile> guessedProfiles = providerProfileGuesser.guess(clues);
            if (!guessedProfiles.isEmpty()) {
                return guessedProfiles;
            }
        }

        return Collections.emptyList();
    }
}
