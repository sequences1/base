package me.relevante.guesser.tracking;

import me.relevante.guesser.GuesserProvider;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

@Component
public class LogRequestsTracker implements RequestTracker {

    private static final Logger logger = LoggerFactory.getLogger(LogRequestsTracker.class);

    @Override
    public void track(final GuesserProvider provider,
                      final String message) {
        logger.info(provider.getName() + " | " + message);
    }
}
