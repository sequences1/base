package me.relevante.core;

import org.springframework.stereotype.Component;

@Component
public class Stripe extends AbstractNetwork implements Network {

    private static final String NAME = "stripe";
    private static Stripe instance = new Stripe();

    public static Stripe getInstance() {
        return instance;
    }

    private Stripe() {
        super(NAME);
    }

}
