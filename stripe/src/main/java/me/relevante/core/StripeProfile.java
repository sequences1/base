package me.relevante.core;

import org.apache.commons.lang3.StringUtils;

public final class StripeProfile implements NetworkProfile<Stripe, StripeProfile> {

    private String email;
    private String token;

    public StripeProfile(final String email,
                         final String token) {
        this.email = email;
        this.token = token;
    }

    public String getEmail() {
        return email;
    }

    public String getToken() {
        return token;
    }

    @Override
    public String getId() {
        return email;
    }

    @Override
    public Stripe getNetwork() {
        return Stripe.getInstance();
    }

    @Override
    public String getNlpAnalyzableContent() {
        return "";
    }

    @Override
    public void completeMissingDataWith(final StripeProfile profile) {
        if (StringUtils.isBlank(this.email)) this.email = profile.email;
        if (StringUtils.isBlank(this.token)) this.token = profile.token;
    }

    @Override
    public void updateWithExistingDataIn(final StripeProfile profile) {
        if (StringUtils.isNotBlank(profile.email)) this.email = profile.email;
        if (StringUtils.isNotBlank(profile.token)) this.token = profile.token;
    }

    @Override
    public void replaceDataWith(final StripeProfile profile) {
        this.email = profile.email;
        this.token = profile.token;
    }
}
