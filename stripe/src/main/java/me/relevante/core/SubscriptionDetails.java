package me.relevante.core;

import java.time.LocalDate;

final public class SubscriptionDetails {

    private String customerId;
    private String pricingPlan;
    private String coupon;
    private LocalDate trialPeriodFinish;

    public SubscriptionDetails(final String customerId,
                               final String pricingPlan) {
        this(customerId, pricingPlan, null);
    }

    public SubscriptionDetails(final String customerId,
                               final String pricingPlan,
                               final String coupon) {
        this.customerId = customerId;
        this.pricingPlan = pricingPlan;
        this.coupon = coupon;
    }

    public String getCustomerId() {
        return customerId;
    }

    public String getPricingPlan() {
        return pricingPlan;
    }

    public String getCoupon() {
        return coupon;
    }

    public boolean hasCoupon() {
        return coupon != null;
    }

    public LocalDate getTrialPeriod() {
        return trialPeriodFinish;
    }

    public boolean hasTrialPeriod() {
        return this.trialPeriodFinish != null;
    }
}
