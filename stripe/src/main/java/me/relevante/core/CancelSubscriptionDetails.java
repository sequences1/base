package me.relevante.core;

final public class CancelSubscriptionDetails {

    private final String customer;
    private final String subscription;

    public CancelSubscriptionDetails(String customer, String subscription) {
        this.customer = customer;
        this.subscription = subscription;
    }

    public String getCustomer() {
        return customer;
    }

    public String getSubscription() {
        return subscription;
    }
}
