package me.relevante.core;

public class CouponDetails {

    private final Long percentOff;

    public CouponDetails(Long percentOff) {
        this.percentOff = percentOff;
    }

    public Long getPercentOff() {
        return percentOff;
    }

}
