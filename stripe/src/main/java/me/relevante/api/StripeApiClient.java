package me.relevante.api;

import com.stripe.exception.*;
import com.stripe.model.Charge;
import com.stripe.model.Coupon;
import com.stripe.model.Customer;
import com.stripe.model.Subscription;
import me.relevante.core.CouponDetails;
import me.relevante.core.Stripe;
import me.relevante.core.StripeProfile;
import me.relevante.core.SubscriptionDetails;

public interface StripeApiClient extends NetworkApiClient<Stripe, StripeProfile> {

    Customer createCustomer(StripeProfile customerDetails, String retryKey) throws StripeApiException;

    Subscription subscribeCustomer(SubscriptionDetails subscriptionDetails, String retryKey) throws StripeApiException;

    void updateSubscription(String subscriptionId, String pricingPlan) throws StripeApiException;

    Subscription getSubscription(String subscriptionId) throws StripeApiException;

    void deleteCustomer(String customerId) throws StripeApiException;

    void unsubscribeCustomer(String customerId, String subscriptionId) throws StripeApiException;

    Coupon createCoupon(CouponDetails couponDetails) throws StripeApiException;

    Charge createUniquePayment(String customerId, Integer amountWithNoDecimals) throws StripeApiException;
}
