package me.relevante.api;

public class StripeApiException extends RuntimeException {

    public StripeApiException() {
        super();
    }

    public StripeApiException(String message) {
        super(message);
    }

    public StripeApiException(Throwable cause) {
        super(cause);
    }
}
