package me.relevante.api;

import com.stripe.Stripe;
import com.stripe.exception.APIConnectionException;
import com.stripe.exception.APIException;
import com.stripe.exception.AuthenticationException;
import com.stripe.exception.CardException;
import com.stripe.exception.InvalidRequestException;
import com.stripe.model.*;
import com.stripe.net.RequestOptions;
import me.relevante.core.CouponDetails;
import me.relevante.core.StripeProfile;
import me.relevante.core.SubscriptionDetails;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

@Component
public class StripeApiClientImpl implements StripeApiClient {

    private String stripeApiKey;
    private Double tax;

    public StripeApiClientImpl(@Value("${stripe.api-key}") final String stripeApiKey,
                               @Value("${stripe.tax}") final String tax) {
        this.stripeApiKey = stripeApiKey;
        this.tax = Double.parseDouble(tax);
    }

    @Override
    public Coupon createCoupon(final CouponDetails couponDetails) throws StripeApiException {
        try {
            Map<String, Object> params = new HashMap<>();
            params.put("id", UUID.randomUUID().toString());
            params.put("duration", "once");
            params.put("percent_off", couponDetails.getPercentOff());
            params.put("max_redemptions", 1);
            return Coupon.create(params);
        } catch (final Exception e) {
            throw new StripeApiException(e);
        }
    }

    @Override
    public Charge createUniquePayment(String customerId, Integer amountWithNoDecimals) throws StripeApiException {
        try {
            return Charge.create(buildChargeParams(amountWithNoDecimals, customerId));
        } catch (final Exception e) {
            throw new StripeApiException(e);
        }
    }

    @Override
    public Customer createCustomer(final StripeProfile customerDetails,
                                   final String retryKey) throws StripeApiException {
        try {
            return Customer.create(buildCustomerParams(customerDetails), idempotencyKey(retryKey));
        } catch (final Exception e) {
            throw new StripeApiException(e);
        }
    }

    @Override
    public void deleteCustomer(final String customerId) throws StripeApiException {
        try {
            Customer.retrieve(customerId).delete();
        } catch (final Exception e) {
            throw new StripeApiException(e);
        }
    }

    @Override
    public me.relevante.core.Stripe getNetwork() {
        return me.relevante.core.Stripe.getInstance();
    }

    @Override
    public <E extends Exception> StripeProfile getProfile() throws E {
        return null;
    }

    @Override
    public Subscription getSubscription(String subscriptionId) throws StripeApiException {
        try {
            return Subscription.retrieve(subscriptionId);
        } catch (final Exception e) {
            throw new StripeApiException(e);
        }
    }

    @Override
    public Subscription subscribeCustomer(final SubscriptionDetails subscriptionDetails,
                                          final String retryKey) throws StripeApiException {
        try {
            return Subscription.create(buildSubscriptionParams(subscriptionDetails), idempotencyKey(retryKey));
        } catch (final Exception e) {
            throw new StripeApiException(e);
        }
    }

    @Override
    public void unsubscribeCustomer(final String customerId,
                                    final String subscriptionId) throws StripeApiException {
        try {
            Customer.retrieve(customerId).getSubscriptions().retrieve(subscriptionId).cancel(Collections.singletonMap("at_period_end", true));
        } catch (final Exception e) {
            throw new StripeApiException(e);
        }
    }

    @Override
    public void updateSubscription(final String subscriptionId,
                                   final String pricingPlan) throws StripeApiException {
        try {
            final Subscription subscription = Subscription.retrieve(subscriptionId);
            subscription.update(buildUpdateSubscriptionParams(pricingPlan));
        } catch (final Exception e) {
            throw new StripeApiException(e);
        }
    }

    private Map<String, Object> buildCustomerParams(StripeProfile customerDetails) {
        Map<String, Object> customerParams = new HashMap<>();
        customerParams.put("email", customerDetails.getEmail());
        customerParams.put("source", customerDetails.getToken());
        return customerParams;
    }

    private Map<String, Object> buildSubscriptionParams(final SubscriptionDetails subscriptionDetails)
            throws CardException, APIException, AuthenticationException, InvalidRequestException, APIConnectionException {
        Map<String, Object> subscriptionParams = new HashMap<>();

        Plan plan = Plan.retrieve(subscriptionDetails.getPricingPlan());
        subscriptionParams.put("metadata", plan.getMetadata());
        subscriptionParams.put("plan", subscriptionDetails.getPricingPlan());
        subscriptionParams.put("customer", subscriptionDetails.getCustomerId());
        subscriptionParams.put("tax_percent", tax);
        if (subscriptionDetails.hasTrialPeriod()) {
            subscriptionParams.put("trial_end", subscriptionDetails.getTrialPeriod().toEpochDay());
        }
        if (subscriptionDetails.hasCoupon()) {
            subscriptionParams.put("coupon", subscriptionDetails.getCoupon());
        }
        return subscriptionParams;
    }

    private Map<String, Object> buildUpdateSubscriptionParams(final String pricingPlan) throws StripeApiException {
        try {
            Map<String, Object> params = new HashMap<>();
            params.put("plan", pricingPlan);
            Plan plan = Plan.retrieve(pricingPlan);
            params.put("metadata", plan.getMetadata());
            return params;
        } catch (final Exception e) {
            throw new StripeApiException(e);
        }
    }

    private Map<String, Object> buildChargeParams(final Integer amount, final String customerId) {
        Map<String, Object> chargeParams = new HashMap<>();
        chargeParams.put("amount", amount);
        chargeParams.put("currency", "EUR");
        chargeParams.put("customer", customerId);
        return chargeParams;
    }

    private RequestOptions idempotencyKey(String retryKey) {
        return RequestOptions
                .builder()
                .setIdempotencyKey(retryKey)
                .build();
    }

    @PostConstruct
    private void setApiKey() {
        Stripe.apiKey = stripeApiKey;
    }

}
