package me.relevante.integration;

import me.relevante.core.Pocket;
import me.relevante.core.PocketFullProfile;
import me.relevante.core.UniqueProfile;
import me.relevante.persistence.BasePocketFullProfileRepo;
import me.relevante.queue.QueueService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Component;

@Component
public class PocketProfileIntegrator
        extends AbstractNetworkProfileIntegrator<Pocket, PocketFullProfile>
        implements NetworkProfileIntegrator<Pocket, PocketFullProfile> {

    private final BasePocketFullProfileRepo fullProfileRepo;

    @Autowired
    public PocketProfileIntegrator(
            final CrudRepository<UniqueProfile, String> uniqueProfileRepo,
            final BasePocketFullProfileRepo fullProfileRepo,
            final QueueService queueService) {
        super(uniqueProfileRepo, fullProfileRepo, queueService);
        this.fullProfileRepo = fullProfileRepo;
    }

    @Override
    protected PocketFullProfile findAlreadyExistingProfile(final PocketFullProfile fullProfile) {
        return fullProfileRepo.findOneByProfileUsername(fullProfile.getProfile().getUsername());
    }

    @Override
    public Pocket getNetwork() {
        return Pocket.getInstance();
    }
}
