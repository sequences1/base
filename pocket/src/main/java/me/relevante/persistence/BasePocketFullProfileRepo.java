package me.relevante.persistence;

import me.relevante.core.PocketFullProfile;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.data.repository.NoRepositoryBean;

/**
 * @author daniel-ibanez
 */
@NoRepositoryBean
public interface BasePocketFullProfileRepo extends MongoRepository<PocketFullProfile, String> {
    PocketFullProfile findOneByProfileUsername(String username);
}
