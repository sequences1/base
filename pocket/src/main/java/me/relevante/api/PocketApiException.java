package me.relevante.api;

public class PocketApiException extends RuntimeException {

    public PocketApiException() {
        super();
    }

    public PocketApiException(String message) {
        super(message);
    }
}
