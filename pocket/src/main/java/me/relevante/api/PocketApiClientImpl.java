package me.relevante.api;

import me.relevante.core.PocketAuthor;
import me.relevante.core.PocketDetailType;
import me.relevante.core.PocketImage;
import me.relevante.core.PocketItem;
import me.relevante.core.PocketProfile;
import me.relevante.core.PocketSearchTerms;
import me.relevante.core.PocketSort;
import me.relevante.core.PocketVideo;
import me.relevante.core.Pocket;
import me.relevante.auth.OAuthKeyPair;
import me.relevante.auth.OAuthTokenPair;
import org.apache.commons.io.IOUtils;
import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.message.BasicNameValuePair;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.InputStream;
import java.io.StringWriter;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

public class PocketApiClientImpl implements PocketApiClient {

    private static final Logger logger = LoggerFactory.getLogger(PocketApiClientImpl.class);
    private static final String GET_URL = "https://getpocket.com/v3/get";

    private OAuthKeyPair<Pocket> oAuthConsumerKeyPair;
    private OAuthTokenPair<Pocket> oAuthAccessTokenPair;

    public PocketApiClientImpl(OAuthKeyPair oAuthConsumerKeyPair,
                               OAuthTokenPair oAuthAccessTokenPair) {
        this.oAuthConsumerKeyPair = oAuthConsumerKeyPair;
        this.oAuthAccessTokenPair = oAuthAccessTokenPair;
    }

    @Override
    public Pocket getNetwork() {
        return Pocket.getInstance();
    }

    @Override
    public PocketProfile getProfile() throws UnsupportedOperationException {
        throw new UnsupportedOperationException();
    }

    @Override
    public List<PocketItem> getItems() throws PocketApiException {
        return getItemsByTag(null);
    }

    @Override
    public List<PocketItem> getItems(PocketSearchTerms searchTerms) throws PocketApiException {
        HttpClient httpClient = HttpClients.createDefault();
        HttpPost httpPost = new HttpPost(GET_URL);

        httpPost.addHeader("Content-Type", "application/x-www-form-urlencoded; charset=UTF-8");
        httpPost.addHeader("X-Accept", "application/json");

        List<NameValuePair> params = new ArrayList<>();
        params.add(new BasicNameValuePair("consumer_key", oAuthConsumerKeyPair.getKey()));
        params.add(new BasicNameValuePair("access_token", oAuthAccessTokenPair.getToken()));
        if (searchTerms.getDetailType() != null) {
            params.add(new BasicNameValuePair("detailType", searchTerms.getDetailType().getName()));
        }
        if (searchTerms.getContentType() != null) {
            params.add(new BasicNameValuePair("contentType", searchTerms.getContentType().getName()));
        }
        if (searchTerms.getSince() != null) {
            params.add(new BasicNameValuePair("since", String.valueOf(searchTerms.getSince().getTime())));
        }
        if (searchTerms.getSort() != null) {
            params.add(new BasicNameValuePair("sort", searchTerms.getSort().getName()));
        }
        if (searchTerms.getTag() != null) {
            params.add(new BasicNameValuePair("tag", searchTerms.getTag()));
        }
        if (searchTerms.getDomain() != null) {
            params.add(new BasicNameValuePair("domain", searchTerms.getDomain()));
        }
        if (searchTerms.getCount() != null) {
            params.add(new BasicNameValuePair("count", String.valueOf(searchTerms.getCount())));
        }
        if (searchTerms.getOffset() != null) {
            params.add(new BasicNameValuePair("offset", String.valueOf(searchTerms.getOffset())));
        }
        if (searchTerms.getFavorite() != null) {
            params.add(new BasicNameValuePair("favorite", searchTerms.getFavorite().getName()));
        }
        if (searchTerms.getSearch() != null) {
            params.add(new BasicNameValuePair("search", searchTerms.getSearch()));
        }

        try {
            httpPost.setEntity(new UrlEncodedFormEntity(params, "UTF-8"));
            HttpResponse response = httpClient.execute(httpPost);
            List<PocketItem> items = createItemsFromResponse(response);
            return items;
        } catch (Exception e) {
            logger.error("Error", e);
        }
        return null;
    }

    @Override
    public List<PocketItem> getItemsByTag(String tag) throws PocketApiException {
        PocketSearchTerms searchTerms = new PocketSearchTerms();
        searchTerms.setSince(new Date(2000, 1, 1));
        searchTerms.setSort(PocketSort.NEWEST);
        searchTerms.setDetailType(PocketDetailType.COMPLETE);
        searchTerms.setTag(tag);
        return getItems(searchTerms);
    }

    @Override
    public List<String> getTags(PocketSearchTerms searchTerms) throws PocketApiException {
        List<PocketItem> items =  getItems(searchTerms);
        Set<String> tags = new HashSet<>();
        for (PocketItem item : items) {
            item.getTags().forEach(tag -> tags.add(tag));
        }
        return new ArrayList<>(tags);
    }

    private List<PocketItem> createItemsFromResponse(HttpResponse response) throws Exception {

        HttpEntity entity = response.getEntity();
        if (entity == null) {
            throw new IllegalArgumentException();
        }
        InputStream inputStream = entity.getContent();
        StringWriter writer = new StringWriter();
        IOUtils.copy(inputStream, writer, "UTF-8");
        JSONObject jsonObject = (JSONObject) new JSONParser().parse(writer.toString());
        List<PocketItem> items = new ArrayList<>();
        if (jsonObject == null) {
            return items;
        }
        if (jsonObject.get("list").getClass().equals(JSONArray.class)) {
            return items;
        }
        JSONObject jsonList = (JSONObject) jsonObject.get("list");
        Iterator itemsIterator = jsonList.entrySet().iterator();
        while (itemsIterator.hasNext()) {
            Map.Entry<String, JSONObject> next = (Map.Entry) itemsIterator.next();
            String itemId = next.getKey();
            JSONObject jsonItem = next.getValue();
            PocketItem pocketPost = createItemFromJsonItem(itemId, jsonItem);
            if (pocketPost.isArchived() || pocketPost.shouldBeDeleted()) {
                continue;
            }
            items.add(pocketPost);
        }

        return items;
    }

    private PocketItem createItemFromJsonItem(String id,
                                              JSONObject jsonObject) {

        PocketItem pocketItem = new PocketItem(id);
        pocketItem.setAuthorId(oAuthAccessTokenPair.getSecret());
        pocketItem.setResolvedId((String) jsonObject.get("resolved_id"));
        pocketItem.setGivenUrl((String) jsonObject.get("given_url"));
        pocketItem.setResolvedUrl((String) jsonObject.get("resolved_url"));
        pocketItem.setGivenTitle((String) jsonObject.get("given_title"));
        pocketItem.setResolvedTitle((String) jsonObject.get("resolved_title"));
        pocketItem.setFavorite((boolean) valueEquals(jsonObject.get("favorite"), false, "1"));
        pocketItem.setArchived((boolean) valueEquals(jsonObject.get("status"), false, "1"));
        pocketItem.setShouldBeDeleted((boolean) valueEquals(jsonObject.get("status"), false, "2"));
        pocketItem.setExcerpt((String) jsonObject.get("excerpt"));
        pocketItem.setIsArticle((boolean) valueEquals(jsonObject.get("is_article"), false, "1"));
        pocketItem.setHasImage((boolean) valueEquals(jsonObject.get("has_image"), false, "1"));
        pocketItem.setIsImage((boolean) valueEquals(jsonObject.get("has_image"), false, "2"));
        pocketItem.setHasVideo((boolean) valueEquals(jsonObject.get("has_video"), false, "1"));
        pocketItem.setIsVideo((boolean) valueEquals(jsonObject.get("has_video"), false, "2"));
        pocketItem.setWordCount((jsonObject.get("word_count") == null ? 0 : Integer.parseInt((String) jsonObject.get("word_count"))));

        JSONObject jsonAuthors = (JSONObject) jsonObject.get("authors");
        if (jsonAuthors != null) {
            Iterator itemsIterator = jsonAuthors.entrySet().iterator();
            while (itemsIterator.hasNext()) {
                Map.Entry<String, JSONObject> next = (Map.Entry) itemsIterator.next();
                PocketAuthor pocketAuthor = createAuthorFromApiAuthor(next.getKey(), next.getValue());
                pocketItem.getAuthors().add(pocketAuthor);
            }
        }
        JSONObject jsonImages = (JSONObject) jsonObject.get("images");
        if (jsonImages != null) {
            Iterator itemsIterator = jsonImages.entrySet().iterator();
            while (itemsIterator.hasNext()) {
                Map.Entry<String, JSONObject> next = (Map.Entry) itemsIterator.next();
                PocketImage pocketImage = createImageFromApiImage(next.getKey(), next.getValue());
                pocketItem.getImages().add(pocketImage);
            }
        }
        JSONObject jsonVideos = (JSONObject) jsonObject.get("videos");
        if (jsonVideos != null) {
            Iterator itemsIterator = jsonVideos.entrySet().iterator();
            while (itemsIterator.hasNext()) {
                Map.Entry<String, JSONObject> next = (Map.Entry) itemsIterator.next();
                PocketVideo pocketVideo = createVideoFromApiVideo(next.getKey(), next.getValue());
                pocketItem.getVideos().add(pocketVideo);
            }
        }
        JSONObject jsonTags = (JSONObject) jsonObject.get("tags");
        if (jsonTags != null) {
            Iterator itemsIterator = jsonTags.entrySet().iterator();
            while (itemsIterator.hasNext()) {
                Map.Entry<String, JSONObject> next = (Map.Entry) itemsIterator.next();
                String pocketTag = next.getKey();
                pocketItem.getTags().add(pocketTag);
            }
        }

        return pocketItem;
    }

    private PocketAuthor createAuthorFromApiAuthor(String id,
                                                   JSONObject jsonObject) {

        PocketAuthor pocketAuthor = new PocketAuthor(id);
        pocketAuthor.setName((String) jsonObject.get("name"));
        pocketAuthor.setAuthorId((String) jsonObject.get("author_id"));
        pocketAuthor.setUrl((String) jsonObject.get("url"));

        return pocketAuthor;
    }

    private PocketImage createImageFromApiImage(String id,
                                                JSONObject jsonObject) {

        PocketImage pocketImage = new PocketImage(id);
        pocketImage.setSrc((String) jsonObject.get("src"));
        pocketImage.setCaption((String) jsonObject.get("caption"));
        pocketImage.setCredit((String) jsonObject.get("credit"));
        pocketImage.setWidth(jsonObject.get("width") == null ? 0 : Integer.parseInt((String) jsonObject.get("width")));
        pocketImage.setHeight(jsonObject.get("height") == null ? 0 : Integer.parseInt((String) jsonObject.get("height")));
        pocketImage.setImageId((String) jsonObject.get("image_id"));

        return pocketImage;
    }

    private PocketVideo createVideoFromApiVideo(String id,
                                                JSONObject jsonObject) {

        PocketVideo pocketVideo = new PocketVideo(id);
        pocketVideo.setSrc((String) jsonObject.get("src"));
        pocketVideo.setVid((String) jsonObject.get("vid"));
        pocketVideo.setType((String) jsonObject.get("type"));
        pocketVideo.setWidth(jsonObject.get("width") == null ? 0 : Integer.parseInt((String) jsonObject.get("width")));
        pocketVideo.setHeight(jsonObject.get("height") == null ? 0 : Integer.parseInt((String) jsonObject.get("height")));
        pocketVideo.setVideoId((String) jsonObject.get("video_id"));

        return pocketVideo;
    }

    private Object valueEquals(Object value,
                               Object valueIfNull,
                               Object valueToMatch) {
        if (value == null) {
            return valueIfNull;
        }
        return value.equals(valueToMatch);
    }

}
