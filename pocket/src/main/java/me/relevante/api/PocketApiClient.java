package me.relevante.api;

import me.relevante.core.PocketItem;
import me.relevante.core.PocketProfile;
import me.relevante.core.PocketSearchTerms;
import me.relevante.core.Pocket;

import java.util.List;

public interface PocketApiClient extends NetworkApiClient<Pocket, PocketProfile> {

    List<PocketItem> getItems() throws PocketApiException;
    List<PocketItem> getItems(PocketSearchTerms searchTerms) throws PocketApiException;
    List<String> getTags(PocketSearchTerms searchTerms) throws PocketApiException;
    List<PocketItem> getItemsByTag(String tag) throws PocketApiException;

}
