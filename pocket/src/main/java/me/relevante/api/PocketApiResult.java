package me.relevante.api;

public enum PocketApiResult {
    EXCEPTION,
    FORBIDDEN,
    SUCCESS,
    UNDEFINED_ERROR

}
