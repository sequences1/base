package me.relevante.api;

public class PocketApiResponse {

    private PocketApiResult result;
    private Object relatedObject;

    public PocketApiResponse(PocketApiResult result,
                             Object expectedObject) {
        this.result = result;
        this.relatedObject = expectedObject;
    }

    public PocketApiResponse(PocketApiResult result) {
        this(result, null);
    }

    public PocketApiResult getResult() {
        return result;
    }

    public Object getRelatedObject() {
        return relatedObject;
    }
}
