package me.relevante.core;

/**
 * Created by daniel-ibanez on 3/08/16.
 */
public enum PocketFavorite {

    FAVORITE("1"),
    UN_FAVORITE("0");

    private String name;

    PocketFavorite(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }
}
