package me.relevante.core;

/**
 * Created by daniel-ibanez on 3/08/16.
 */
public enum PocketDetailType {

    SIMPLE("simple"),
    COMPLETE("complete");

    private String name;

    PocketDetailType(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }
}
