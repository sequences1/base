package me.relevante.core;

import org.springframework.stereotype.Component;

@Component
public class Pocket extends AbstractNetwork implements Network {

    private static final String NAME = "pocket";
    private static Pocket instance = new Pocket();

    public static Pocket getInstance() {
        return instance;
    }

    private Pocket() {
        super(NAME);
    }

}
