package me.relevante.core;

/**
 * Created by daniel-ibanez on 2/08/16.
 */
public class PocketAuthor {

    private String id;
    private String name;
    private String authorId;
    private String url;

    public PocketAuthor(String id) {
        this.id = id;
    }

    public String getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public String getAuthorId() {
        return authorId;
    }

    public String getUrl() {
        return url;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setAuthorId(String authorId) {
        this.authorId = authorId;
    }

    public void setUrl(String url) {
        this.url = url;
    }
}
