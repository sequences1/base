package me.relevante.core;

import org.apache.commons.lang3.StringUtils;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * Created by daniel-ibanez on 2/08/16.
 */
@Document(collection = "PocketItem")
public class PocketItem implements NetworkPost<Pocket, PocketItem, PocketProfile> {

    @Id
    private String id;
    private String authorId;
    private Date creationTimestamp;
    private String resolvedId;
    private String givenUrl;
    private String resolvedUrl;
    private String givenTitle;
    private String resolvedTitle;
    private boolean favorite;
    private boolean archived;
    private boolean shouldBeDeleted;
    private String excerpt;
    private boolean isArticle;
    private boolean hasImage;
    private boolean isImage;
    private boolean hasVideo;
    private boolean isVideo;
    private int wordCount;
    private List<PocketAuthor> authors;
    private List<PocketImage> images;
    private List<PocketVideo> videos;
    private List<String> tags;

    public PocketItem() {
        this.authors = new ArrayList<>();
        this.images = new ArrayList<>();
        this.videos = new ArrayList<>();
        this.tags = new ArrayList<>();
    }

    public PocketItem(String id) {
        this();
        this.id = id;
    }

    @Override
    public Pocket getNetwork() {
        return Pocket.getInstance();
    }

    @Override
    public String getId() {
        return id;
    }

    @Override
    public String getAuthorId() {
        return authorId;
    }

    @Override
    public Date getCreationTimestamp() {
        return creationTimestamp;
    }

    @Override
    public void setCreationTimestamp(Date creationTimestamp) {
        this.creationTimestamp = creationTimestamp;
    }

    @Override
    public String getNlpAnalyzableContent() {
        return "";
    }

    @Override
    public void completeMissingDataWith(final PocketItem pocketItem) {
        if (StringUtils.isBlank(this.id)) this.id = pocketItem.id;
        if (StringUtils.isBlank(this.authorId)) this.authorId = pocketItem.authorId;
        if (this.creationTimestamp == null) this.creationTimestamp = pocketItem.creationTimestamp;
        if (StringUtils.isBlank(this.resolvedId)) this.resolvedId = pocketItem.resolvedId;
        if (StringUtils.isBlank(this.givenUrl)) this.givenUrl = pocketItem.givenUrl;
        if (StringUtils.isBlank(this.resolvedUrl)) this.resolvedUrl = pocketItem.resolvedUrl;
        if (StringUtils.isBlank(this.givenTitle)) this.givenTitle = pocketItem.givenTitle;
        if (StringUtils.isBlank(this.resolvedTitle)) this.resolvedTitle = pocketItem.resolvedTitle;
        if (StringUtils.isBlank(this.excerpt)) this.excerpt = pocketItem.excerpt;
        if (this.authors.isEmpty()) this.authors = new ArrayList<>(pocketItem.authors);
        if (this.images.isEmpty()) this.images = new ArrayList<>(pocketItem.images);
        if (this.videos.isEmpty()) this.videos = new ArrayList<>(pocketItem.videos);
        if (this.tags.isEmpty()) this.tags = new ArrayList<>(pocketItem.tags);
    }

    @Override
    public void updateWithExistingDataIn(final PocketItem pocketItem) {
        if (StringUtils.isNotBlank(pocketItem.id)) this.id = pocketItem.id;
        if (StringUtils.isNotBlank(pocketItem.authorId)) this.authorId = pocketItem.authorId;
        if (pocketItem.creationTimestamp != null) this.creationTimestamp = pocketItem.creationTimestamp;
        if (StringUtils.isNotBlank(pocketItem.resolvedId)) this.resolvedId = pocketItem.resolvedId;
        if (StringUtils.isNotBlank(pocketItem.givenUrl)) this.givenUrl = pocketItem.givenUrl;
        if (StringUtils.isNotBlank(pocketItem.resolvedUrl)) this.resolvedUrl = pocketItem.resolvedUrl;
        if (StringUtils.isNotBlank(pocketItem.givenTitle)) this.givenTitle = pocketItem.givenTitle;
        if (StringUtils.isNotBlank(pocketItem.resolvedTitle)) this.resolvedTitle = pocketItem.resolvedTitle;
        if (StringUtils.isNotBlank(pocketItem.excerpt)) this.excerpt = pocketItem.excerpt;
        this.favorite = pocketItem.favorite;
        this.archived = pocketItem.archived;
        this.shouldBeDeleted = pocketItem.shouldBeDeleted;
        this.excerpt = pocketItem.excerpt;
        this.isArticle = pocketItem.isArticle;
        this.hasImage = pocketItem.hasImage;
        this.isImage = pocketItem.isImage;
        this.hasVideo = pocketItem.hasVideo;
        this.isVideo = pocketItem.isVideo;
        this.wordCount = pocketItem.wordCount;
        if (!pocketItem.authors.isEmpty()) this.authors = new ArrayList<>(pocketItem.authors);
        if (!pocketItem.images.isEmpty()) this.images = new ArrayList<>(pocketItem.images);
        if (!pocketItem.videos.isEmpty()) this.videos = new ArrayList<>(pocketItem.videos);
        if (!pocketItem.tags.isEmpty()) this.tags = new ArrayList<>(pocketItem.tags);
    }

    @Override
    public void replaceDataWith(final PocketItem pocketItem) {
        this.id = pocketItem.id;
        this.authorId = pocketItem.authorId;
        this.creationTimestamp = pocketItem.creationTimestamp;
        this.resolvedId = pocketItem.resolvedId;
        this.givenUrl = pocketItem.givenUrl;
        this.resolvedUrl = pocketItem.resolvedUrl;
        this.givenTitle = pocketItem.givenTitle;
        this.resolvedTitle = pocketItem.resolvedTitle;
        this.favorite = pocketItem.favorite;
        this.archived = pocketItem.archived;
        this.shouldBeDeleted = pocketItem.shouldBeDeleted;
        this.excerpt = pocketItem.excerpt;
        this.isArticle = pocketItem.isArticle;
        this.hasImage = pocketItem.hasImage;
        this.isImage = pocketItem.isImage;
        this.hasVideo = pocketItem.hasVideo;
        this.isVideo = pocketItem.isVideo;
        this.wordCount = pocketItem.wordCount;
        this.authors = new ArrayList<>(pocketItem.authors);
        this.images = new ArrayList<>(pocketItem.images);
        this.videos = new ArrayList<>(pocketItem.videos);
        this.tags = new ArrayList<>(pocketItem.tags);
    }

    @Override
    public PocketProfile getAuthor() {
        throw new UnsupportedOperationException();
    }

    @Override
    public void setAuthor(PocketProfile pocketProfile) {
        throw new UnsupportedOperationException();
    }

    public String getResolvedId() {
        return resolvedId;
    }

    public String getGivenUrl() {
        return givenUrl;
    }

    public String getResolvedUrl() {
        return resolvedUrl;
    }

    public String getGivenTitle() {
        return givenTitle;
    }

    public String getResolvedTitle() {
        return resolvedTitle;
    }

    public boolean isFavorite() {
        return favorite;
    }

    public boolean shouldBeDeleted() {
        return shouldBeDeleted;
    }

    public boolean isArchived() {
        return archived;
    }

    public String getExcerpt() {
        return excerpt;
    }

    public boolean isArticle() {
        return isArticle;
    }

    public boolean hasImage() {
        return hasImage;
    }

    public boolean isImage() {
        return isImage;
    }

    public boolean hasVideo() {
        return hasVideo;
    }

    public boolean isVideo() {
        return isVideo;
    }

    public int getWordCount() {
        return wordCount;
    }

    public List<PocketAuthor> getAuthors() {
        return authors;
    }

    public List<PocketImage> getImages() {
        return images;
    }

    public List<PocketVideo> getVideos() {
        return videos;
    }

    public List<String> getTags() {
        return tags;
    }

    public void setAuthorId(String authorId) {
        this.authorId = authorId;
    }

    public void setResolvedId(String resolvedId) {
        this.resolvedId = resolvedId;
    }

    public void setGivenUrl(String givenUrl) {
        this.givenUrl = givenUrl;
    }

    public void setResolvedUrl(String resolvedUrl) {
        this.resolvedUrl = resolvedUrl;
    }

    public void setGivenTitle(String givenTitle) {
        this.givenTitle = givenTitle;
    }

    public void setResolvedTitle(String resolvedTitle) {
        this.resolvedTitle = resolvedTitle;
    }

    public void setFavorite(boolean favorite) {
        this.favorite = favorite;
    }

    public void setArchived(boolean archived) {
        this.archived = archived;
    }

    public void setShouldBeDeleted(boolean shouldBeDeleted) {
        this.shouldBeDeleted = shouldBeDeleted;
    }

    public void setExcerpt(String excerpt) {
        this.excerpt = excerpt;
    }

    public void setIsArticle(boolean isArticle) {
        this.isArticle = isArticle;
    }

    public void setHasImage(boolean hasImage) {
        this.hasImage = hasImage;
    }

    public void setIsImage(boolean isImage) {
        this.isImage = isImage;
    }

    public void setHasVideo(boolean hasVideo) {
        this.hasVideo = hasVideo;
    }

    public void setIsVideo(boolean isVideo) {
        this.isVideo = isVideo;
    }

    public void setWordCount(int wordCount) {
        this.wordCount = wordCount;
    }
}
