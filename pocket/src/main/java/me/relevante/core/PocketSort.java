package me.relevante.core;

/**
 * Created by daniel-ibanez on 3/08/16.
 */
public enum PocketSort {

    NEWEST("newest"),
    OLDEST("oldest"),
    TITLE("title"),
    SITE("site");
    private String name;

    PocketSort(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }
}
