package me.relevante.core;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * Created by daniel-ibanez on 2/08/16.
 */
@Document(collection = "PocketFullProfile")
public class PocketFullProfile implements NetworkFullProfile<Pocket, PocketProfile, PocketFullProfile> {

    @Id
    private String id;
    private PocketProfile profile;
    private List<String> postIds;
    private String uniqueProfileId;
    private Date lastUpdatedTimestamp;

    public PocketFullProfile() {
        super();
        this.postIds = new ArrayList<>();
    }

    public PocketFullProfile(final PocketProfile profile) {
        this();
        this.id = profile.getId();
        this.profile = profile;
    }

    @Override
    public Pocket getNetwork() {
        return Pocket.getInstance();
    }

    @Override
    public String getId() {
        return id;
    }

    @Override
    public PocketProfile getProfile() {
        return profile;
    }


    @Override
    public String getUniqueProfileId() {
        return uniqueProfileId;
    }

    @Override
    public void setUniqueProfileId(final String uniqueProfileId) {
        this.uniqueProfileId = uniqueProfileId;
    }

    @Override
    public void completeMissingDataWith(final PocketFullProfile fullProfile) {
        this.profile.completeMissingDataWith(fullProfile.getProfile());
        if (this.postIds.isEmpty()) this.postIds = new ArrayList<>(fullProfile.postIds);
        markAsUpdated();
    }

    @Override
    public void updateWithExistingDataIn(final PocketFullProfile fullProfile) {
        this.profile.updateWithExistingDataIn(fullProfile.getProfile());
        if (!fullProfile.postIds.isEmpty()) this.postIds = new ArrayList<>(fullProfile.postIds);
        markAsUpdated();
    }

    @Override
    public void replaceDataWith(final PocketFullProfile fullProfile) {
        this.profile.replaceDataWith(fullProfile.getProfile());
        this.postIds = new ArrayList<>(fullProfile.postIds);
        markAsUpdated();
    }

    @Override
    public Date getLastUpdatedTimestamp() {
        return lastUpdatedTimestamp;
    }

    private void markAsUpdated() {
        this.lastUpdatedTimestamp = new Date();
    }

    public List<String> getPostIds() {
        return postIds;
    }
}
