package me.relevante.core;

/**
 * Created by daniel-ibanez on 3/08/16.
 */
public enum PocketState {

    UNREAD("unread"),
    ARCHIVE("archive"),
    ALL("all");

    private String name;

    PocketState(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }
}
