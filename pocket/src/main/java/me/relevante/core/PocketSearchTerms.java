package me.relevante.core;

import java.util.Date;

/**
 * Created by daniel-ibanez on 2/08/16.
 */
public class PocketSearchTerms {

    private PocketState state;
    private PocketFavorite favorite;
    private String tag;
    private PocketContentType contentType;
    private PocketSort sort;
    private PocketDetailType detailType;
    private String search;
    private String domain;
    private Date since;
    private Integer count;
    private Integer offset;

    public PocketState getState() {
        return state;
    }

    public void setState(PocketState state) {
        this.state = state;
    }

    public PocketFavorite getFavorite() {
        return favorite;
    }

    public void setFavorite(PocketFavorite favorite) {
        this.favorite = favorite;
    }

    public String getTag() {
        return tag;
    }

    public void setTag(String tag) {
        this.tag = tag;
    }

    public PocketContentType getContentType() {
        return contentType;
    }

    public void setContentType(PocketContentType contentType) {
        this.contentType = contentType;
    }

    public PocketSort getSort() {
        return sort;
    }

    public void setSort(PocketSort sort) {
        this.sort = sort;
    }

    public PocketDetailType getDetailType() {
        return detailType;
    }

    public void setDetailType(PocketDetailType detailType) {
        this.detailType = detailType;
    }

    public String getSearch() {
        return search;
    }

    public void setSearch(String search) {
        this.search = search;
    }

    public String getDomain() {
        return domain;
    }

    public void setDomain(String domain) {
        this.domain = domain;
    }

    public Date getSince() {
        return since;
    }

    public void setSince(Date since) {
        this.since = since;
    }

    public Integer getCount() {
        return count;
    }

    public void setCount(Integer count) {
        this.count = count;
    }

    public Integer getOffset() {
        return offset;
    }

    public void setOffset(Integer offset) {
        this.offset = offset;
    }
}

