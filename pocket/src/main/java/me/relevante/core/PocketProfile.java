package me.relevante.core;

import org.apache.commons.lang3.StringUtils;

/**
 * Created by daniel-ibanez on 2/08/16.
 */
public class PocketProfile implements NetworkProfile<Pocket, PocketProfile> {

    private String id;
    private String username;

    public PocketProfile(String id) {
        this.id = id;
        this.username = id;
    }

    @Override
    public Pocket getNetwork() {
        return Pocket.getInstance();
    }

    @Override
    public String getNlpAnalyzableContent() {
        return "";
    }

    @Override
    public String getId() {
        return id;
    }

    @Override
    public void completeMissingDataWith(final PocketProfile profile) {
        if (StringUtils.isBlank(this.id)) this.id = profile.id;
        if (StringUtils.isBlank(this.username)) this.username = profile.username;
    }

    @Override
    public void updateWithExistingDataIn(final PocketProfile profile) {
        if (StringUtils.isNotBlank(profile.id)) this.id = profile.id;
        if (StringUtils.isNotBlank(profile.username)) this.username = profile.username;
    }

    @Override
    public void replaceDataWith(final PocketProfile profile) {
        this.id = profile.id;
        this.username = profile.username;
    }

    public String getUsername() {
        return username;
    }
}
